﻿using IBatisNet.DataMapper;

namespace SCMPLUS
{
    public class BaseHelper
    {
        private ISqlMapper mapper = null;

        public ISqlMapper Mapper()
        {
            if (null == mapper)
            {
                IBatisNet.DataMapper.Mapper.InitMapper();
                mapper = IBatisNet.DataMapper.Mapper.Instance();
            }

            return mapper;  
        }

    }
}
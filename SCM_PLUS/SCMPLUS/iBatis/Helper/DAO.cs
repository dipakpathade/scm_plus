﻿using IBatisNet.DataMapper;

namespace SCMPLUS
{
    public class Dao
    {
        public static CategoryHelper Category()
        {
            return new CategoryHelper();
        }

        public static NodeHelper Node()
        {
            return new NodeHelper();
        }

        public static SKUHelper SKU()
        {
            return new SKUHelper();
        }

        public static AttributeHelper Attributes()
        {
            return new AttributeHelper();
        }

        public static SKUAttributeHelper SKUAttributeMapping()
        {
            return new SKUAttributeHelper();
        }

        public static NodeSKUHelper NodeSKUMapping()
        {
            return new NodeSKUHelper();
        }

        public static BufferNormHelper BufferNorm()
        {
            return new BufferNormHelper();
        }

        public static TransactionHelper Transaction()
        {
            return new TransactionHelper();
        }

        public static SKUCategoryHelper SKUCategory()
        {
            return new SKUCategoryHelper();
        }

        public static SKUChartHelper SKUChart()
        {
            return new SKUChartHelper();
        }

        public static ImportExportLogHelper ImportExportLog()
        {
            return new ImportExportLogHelper();
        }

        public static DynamicBufferManagementHelper DynamicBufferManagement()
        {
            return new DynamicBufferManagementHelper();
        }

        public static SKUGridHelper SKUGrid()
        {
            return new SKUGridHelper();
        }

        public static ReplenishmentHelper Replenishment()
        {
            return new ReplenishmentHelper();
        }

        public static UserHelper User()
        {
            return new UserHelper();
        }

        public static UserModuleHelper UserModule()
        {
            return new UserModuleHelper();
        }

        public static ExcessInventoryHelper ExcessInventory()
        {
            return new ExcessInventoryHelper();
        }

        public static StockoutAvailabilityChartHelper StockoutAvailability()
        {
            return new StockoutAvailabilityChartHelper();
        }

        public static InventoryTurnsHelper InventoryTurns()
        {
            return new InventoryTurnsHelper();
        }

        public static CntQtyFillRateChartHelper CntQtyFillRateChart()
        {
            return new CntQtyFillRateChartHelper();
        }

        public static UserDataPermissionHelper UserDataPermission()
        {
            return new UserDataPermissionHelper();
        }

        public static RoleHelper Role()
        {
            return new RoleHelper();
        }

        public static BOMHelper BOM()
        {
            return new BOMHelper();
        }

        public static Matrix1Helper Matrix1()
        {
            return new Matrix1Helper();
        }

    }
}
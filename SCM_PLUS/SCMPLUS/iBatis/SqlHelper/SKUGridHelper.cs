﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class SKUGridHelper : BaseHelper
    {
        public SKUGrid Select(Transaction item)
        {
            return Mapper().QueryForObject<SKUGrid>("SelectSKUGrid", item);
        }

        public List<SKUGrid> SelectSKUGridList(TempSKUGRID item)
        {
            return Mapper().QueryForList<SKUGrid>("SelectSKUGridBulk", item).Cast<SKUGrid>().ToList();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class SKUAttributeHelper : BaseHelper
    {
        public SKUAttributeMapping Select(long id)
        {
            return Mapper().QueryForObject<SKUAttributeMapping>("SelectSKUAttributeMapping", id);
        }

        public SKUAttributeMapping SelectBySKUAttribute(SKUAttributeMapping item)
        {
            return Mapper().QueryForObject<SKUAttributeMapping>("SelectSKUAttributeMappingBySKUAttribute", item);
        }

        public List<SKUAttributeMapping> SelectBySKU(long sku_id)
        {
            return Mapper().QueryForList<SKUAttributeMapping>("SelectSKUAttributeMappingBySKUAttribute", sku_id).Cast<SKUAttributeMapping>().ToList();
        }

        public List<SKUAttributeMapping> SelectAll()
        {
            return Mapper().QueryForList("SelectSKUAttributeMapping", null).Cast<SKUAttributeMapping>().ToList();
        }

        public long Insert(SKUAttributeMapping item)
        {
            return (long)Mapper().Insert("InsertSKUAttributeMapping", item);
        }

        public int Update(SKUAttributeMapping item)
        {
            return Mapper().Update("UpdateSKUAttributeMapping", item);
        }

        //public int Delete(SKUAttributeMapping item)
        //{
        //    return Mapper().Delete("DeleteSKUAttributeMapping", item);
        //}

        public void InsertBatch(List<SKUAttributeMapping> item)
        {
            Mapper().Insert("InserSKUAttributeBatch", item);
        }

        public int DeleteBatch(List<SKUAttributeMapping> item)
        {
            return Mapper().Delete("DeleteMultiSKUAttributeMapping", item);
        }
    }
}
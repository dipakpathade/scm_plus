﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class AttributeHelper : BaseHelper
    {
        public Attributes Select(long id)
        {
            return Mapper().QueryForObject<Attributes>("SelectAttribute", id);
        }

        public Attributes SelectByCode(string code)
        {
            return Mapper().QueryForObject<Attributes>("SelectAttributeByCode", code);
        }

        public List<Attributes> SelectAll()
        {
            return Mapper().QueryForList("SelectAttribute", null).Cast<Attributes>().ToList();
        }

        public long Insert(Attributes item)
        {
            return (long)Mapper().Insert("InsertAttribute", item);
        }

        public int Update(Attributes item)
        {
            return Mapper().Update("UpdateAttribute", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteAttribute", id);
        }
    }
}
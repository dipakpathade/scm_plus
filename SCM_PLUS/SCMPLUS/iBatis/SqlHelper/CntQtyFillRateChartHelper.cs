﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class CntQtyFillRateChartHelper : BaseHelper
    {
        public List<CntQtyFillRateChart> Select(string date)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChart", date).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectByNode(string date)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartNode", date).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectByCategory(string date)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartCategory", date).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectBySKU(string date)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartSKU", date).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectByNodeSKU(MapperBatch item)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartNodeSKU", item).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectByNodeCategory(MapperBatch item)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartNodeCategory", item).Cast<CntQtyFillRateChart>().ToList();
        }

        public List<CntQtyFillRateChart> SelectCntQtyFillRateChartReport(MapperBatch item)
        {
            return Mapper().QueryForList<CntQtyFillRateChart>("SelectCntQtyFillRateChartReport", item).Cast<CntQtyFillRateChart>().ToList();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class ImportExportLogHelper : BaseHelper
    {
        public ImportExportLog Select(long id)
        {
            return Mapper().QueryForObject<ImportExportLog>("SelectImportExportLog", id);
        }

        public ImportExportLog SelectByType(string code)
        {
            return Mapper().QueryForObject<ImportExportLog>("SelectImportExportLogByType", code);
        }

        public List<ImportExportLog> SelectAll()
        {
            return Mapper().QueryForList("SelectImportExportLog", null).Cast<ImportExportLog>().ToList();
        }

        public long Insert(ImportExportLog item)
        {
            return (long)Mapper().Insert("InsertImportExportLog", item);
        }

        public int Update(ImportExportLog item)
        {
            return Mapper().Update("UpdateImportExportLog", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteImportExportLog", id);
        }
    }
}
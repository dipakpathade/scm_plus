﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class UserModuleHelper : BaseHelper
    {
        public UserModule Select(long id)
        {
            return Mapper().QueryForObject<UserModule>("SelectUserModule", id);
        }

        public List<UserModule> SelectAll()
        {
            return Mapper().QueryForList("SelectUserModule", null).Cast<UserModule>().ToList();
        }

        public long Insert(UserModule item)
        {
            return (long)Mapper().Insert("InsertUserModule", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteUserModule", id);
        }

        public int DeleteModuleByRole(long id)
        {
            return Mapper().Delete("DeleteModuleByRole", id);
        }

        public void InsertBatch(List<UserModule> item)
        {
            Mapper().Insert("InsertUserModuleBatch", item);
        }


        public List<UserModule> SelectByRole(long id)
        {
            return Mapper().QueryForList("SelectUserModuleByRole", id).Cast<UserModule>().ToList();
        }
    }
}
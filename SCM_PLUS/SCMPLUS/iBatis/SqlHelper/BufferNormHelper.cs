﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class BufferNormHelper : BaseHelper
    {
        public BufferNorm Select(long id)
        {
            return Mapper().QueryForObject<BufferNorm>("SelectBufferNorm", id);
        }

        public BufferNorm SelectBySKUNode(BufferNorm item)
        {
            return Mapper().QueryForObject<BufferNorm>("SelectBufferNormBySKUNode", item);
        }

        public List<BufferNorm> SelectAll()
        {
            return Mapper().QueryForList("SelectBufferNorm", null).Cast<BufferNorm>().ToList();
        }

        public BufferNorm SelectList(long id)
        {
            return Mapper().QueryForObject<BufferNorm>("SelectBufferNormList", id);
        }

        public List<BufferNorm> SelectList()
        {
            return Mapper().QueryForList<BufferNorm>("SelectBufferNormList", null).Cast<BufferNorm>().ToList();
        }

        public long Insert(BufferNorm item)
        {
            return (long)Mapper().Insert("InsertBufferNorm", item);
        }

        public int Update(BufferNorm item)
        {
            return Mapper().Update("UpdateBufferNorm", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteBufferNorm", id);
        }

        public void InsertBatch(List<BufferNorm> item)
        {
            Mapper().Insert("InsertBufferNormBatch", item);
        }

        public List<BufferNorm> SelectBatchInsertedBufferNorm(string identifier)
        {
            return Mapper().QueryForList("SelectBatchInsertedBufferNorm", identifier).Cast<BufferNorm>().ToList();
        }

        public void CreateTempTable(string tmpTable)
        {
            Mapper().Update("CreateTempBufferNormTable", tmpTable);
        }

        public void InsertTempBatch(TempBufferNorm item)
        {
            Mapper().Insert("InsertTmpBufferNormBatch", item);
        }

        public List<BufferNorm> SelectTmpNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpBufferNormNotDuplicate", tmpTable).Cast<BufferNorm>().ToList();
        }

        public List<BufferNorm> SelectTmpDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpBufferNormDuplicate", tmpTable).Cast<BufferNorm>().ToList();
        }

        public List<BufferNorm> SelectDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectBufferNormDuplicate", tmpTable).Cast<BufferNorm>().ToList();
        }

        public void DropTempTable(string tmpTable)
        {
            Mapper().Delete("DropTmpBufferNormTable", tmpTable);
        }

        public BufferNorm SelectBufferNormBySKUNodeMaxEffectiveFrom(BufferNorm item)
        {
            return Mapper().QueryForObject<BufferNorm>("SelectBufferNormBySKUNodeMaxEffectiveFrom", item);
        }
    }
}
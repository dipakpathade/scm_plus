﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class StockoutAvailabilityChartHelper : BaseHelper
    {
        public List<StockoutAvailabilityChart> SelectByNodeSKU(MapperBatch item)
        {
            return Mapper().QueryForList<StockoutAvailabilityChart>("SelectStockoutAvailabilityNodeSKU", item).Cast<StockoutAvailabilityChart>().ToList();
        }

        public List<StockoutAvailabilityChart> SelectByNodeCategory(MapperBatch item)
        {
            return Mapper().QueryForList<StockoutAvailabilityChart>("SelectStockoutAvailabilityNodeCategory", item).Cast<StockoutAvailabilityChart>().ToList();
        }

        public List<StockoutAvailabilityChart> SelectStockoutAvailabilityReport(MapperBatch item)
        {
            return Mapper().QueryForList<StockoutAvailabilityChart>("SelectStockoutAvailabilityReport", item).Cast<StockoutAvailabilityChart>().ToList();
        }
    }
}
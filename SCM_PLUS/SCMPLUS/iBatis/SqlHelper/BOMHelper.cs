﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class BOMHelper : BaseHelper
    {
        public void CreateTempBOMTable(string tmpTable)
        {
            Mapper().Update("CreateTempBOMTable", tmpTable);
        }

        public void InsertTempBatch(TempBOM item)
        {
            Mapper().Insert("InsertTmpBOMBatch", item);
        }

        public List<BOM> SelectTmpBOMNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpBOMNotDuplicate", tmpTable).Cast<BOM>().ToList();
        }

        public List<BOM> SelectTmpBOMDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpBOMDuplicate", tmpTable).Cast<BOM>().ToList();
        }

        public List<BOM> SelectBOMDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectBOMDuplicate", tmpTable).Cast<BOM>().ToList();
        }

        public void InsertBatch(List<BOM> item)
        {
            Mapper().Insert("InsertBOMBatch", item);
        }

        public void DropTempBOMTable(string tmpTable)
        {
            Mapper().Delete("DropTmpBOMTable", tmpTable);
        }

        public List<BOM> SelectList()
        {
            return Mapper().QueryForList<BOM>("SelectBOMList", null).Cast<BOM>().ToList();
        }

        public Dictionary<string, long> SelectAllDictionary()
        {
            return Mapper().QueryForMap("SelectBOMDictionary", null, "sku_code", "sku_id").Cast<System.Collections.DictionaryEntry>().ToDictionary(kvp => (string)kvp.Key, kvp => (long)kvp.Value); ;
        }
    }
}
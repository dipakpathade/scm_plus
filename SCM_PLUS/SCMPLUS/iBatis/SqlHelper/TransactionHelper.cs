﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class TransactionHelper : BaseHelper
    {
        public Transaction Select(long id)
        {
            return Mapper().QueryForObject<Transaction>("SelectTransaction", id);
        }

        public Transaction SelectBySKUNode(Transaction item)
        {
            return Mapper().QueryForObject<Transaction>("SelectTransactionBySKUNode", item);
        }

        public Transaction SelectMaxTransactionDate()
        {
            return Mapper().QueryForObject<Transaction>("SelectMaxTransactionDate", null);
        }

        public List<Transaction> SelectSKUNodeGroup()
        {
            return Mapper().QueryForList<Transaction>("SelectSKUNodeGroup", null).Cast<Transaction>().ToList();
        }

        public List<Transaction> SelectByDate(string date)
        {
            return Mapper().QueryForList<Transaction>("SelectTransactionByDate", date).Cast<Transaction>().ToList();
        }

        public List<Transaction> SelectAll()
        {
            return Mapper().QueryForList("SelectTransaction", null).Cast<Transaction>().ToList();
        }

        public List<Replenishment> SelectList(MapperBatch mapperBatch)
        {
            return Mapper().QueryForList<Replenishment>("SelectReplenishmentList", mapperBatch).Cast<Replenishment>().ToList();
        }

        public List<Replenishment> SelectList()
        {
            return Mapper().QueryForList<Replenishment>("SelectReplenishmentList", null).Cast<Replenishment>().ToList();
        }

        public long Insert(Transaction item)
        {
            return (long)Mapper().Insert("InsertTransaction", item);
        }

        public int UpdateTransactionDBSync(List<long> item)
        {
            return Mapper().Update("UpdateTransactionDBSync", item);
        }

        public int UpdateTransactionMongoSync(List<long> item)
        {
            return Mapper().Update("UpdateTransactionMongoSync", item);
        }

        public int Update(Transaction item)
        {
            return Mapper().Update("UpdateTransaction", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteTransaction", id);
        }

        public int DeleteAll()
        {
            return Mapper().Delete("DeleteTransaction", null);
        }

        public int DeleteFailedImportTransaction(Transaction item)
        {
            return Mapper().Delete("DeleteFailedImportTransaction", item);
        }

        public int GetTransactionCountForImport(string transaction_date)
        {
            return (int)Mapper().QueryForObject("SelectTransactionCountForImport", transaction_date);
        }

        public List<Transaction> GetTransactionIdsForImport(string transaction_date)
        {
            return Mapper().QueryForList<Transaction>("SelectTransactionIdsForImport", transaction_date).Cast<Transaction>().ToList();
        }

        public int GetCountAll()
        {
            return (int)Mapper().QueryForObject("SelectTransactionCountForImport", null);
        }

        public int GetTransactionCountByDate(string transaction_date)
        {
            return (int)Mapper().QueryForObject("SelectTransactionCountByDate", transaction_date);
        }

        public long InsertComment(TransactionComment item)
        {
            return (long)Mapper().Insert("InsertTransactionComment", item);
        }

        public List<TransactionComment> SelectCommentList(DataFilter item)
        {
            return Mapper().QueryForList<TransactionComment>("SelectTransactionCommentList", item).Cast<TransactionComment>().ToList();
        }

        public List<Transaction> SelectTransactionDates(int dayLimit)
        {
            return Mapper().QueryForList("SelectTransactionDates", dayLimit).Cast<Transaction>().ToList();
        }

        public List<Transaction> SelectBNCalculationTransactionDates(string date)
        {
            return Mapper().QueryForList("SelectBNCalculationTransactionDates", date).Cast<Transaction>().ToList();
        }

        public List<Transaction> GetTransactionIdsForBNCalculation(Transaction item)
        {
            return Mapper().QueryForList<Transaction>("SelectTransactionIdsForBNCalculation", item).Cast<Transaction>().ToList();
        }

        public Transaction SelectTransactionForDuplicate(Transaction item)
        {
            return Mapper().QueryForObject<Transaction>("SelectTransactionForDuplicate", item);
        }

        public void InsertBatch(List<Transaction> item)
        {
            Mapper().Insert("InsertTransactionBatch", item);
        }

        public List<Transaction> SelectBatchInsertedTransactionIds(string identifier)
        {
            return Mapper().QueryForList("SelectBatchInsertedTransactionID", identifier).Cast<Transaction>().ToList();
        }

        public void InsertTempBatch(TempTransaction item)
        {
            Mapper().Insert("InsertTmpTransactionBatch", item);
        }

        public void CreateTempTransactionTable(string tmpTable)
        {
            Mapper().Update("CreateTempTransactionTable", tmpTable);
        }

        public void DropTempTransactionTable(string tmpTable)
        {
            Mapper().Delete("DropTmpTransactionTable", tmpTable);
        }

        public List<Transaction> SelectTmpTransactionNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpTransactionNotDuplicate", tmpTable).Cast<Transaction>().ToList();
        }

        public List<Transaction> SelectTmpTransactionDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpTransactionDuplicate", tmpTable).Cast<Transaction>().ToList();
        }

        public void InsertUpdateTransactionBatch(List<Transaction> item)
        {
            Mapper().Insert("InsertUpdatesTransaction", item);
        }

        public void DeleteUpdateTransactionBatch(string identifier)
        {
            Mapper().Delete("DeleteUpdatesTransaction", identifier);
        }

        public List<Transaction> GetUpdateTransactionIdsForImport(string identifier)
        {
            return Mapper().QueryForList<Transaction>("SelectUpdateTransactionImport", identifier).Cast<Transaction>().ToList();
        }

        public List<Transaction> SelectTransactionDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTransactionDuplicate", tmpTable).Cast<Transaction>().ToList();
        }
    }
}
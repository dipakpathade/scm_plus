﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class ReplenishmentHelper : BaseHelper
    {
        public List<Replenishment> SelectByDate(string date)
        {
            return Mapper().QueryForList<Replenishment>("SelectReplenishmentData", date).Cast<Replenishment>().ToList();
        }

        public int UpdateReplenishmentDBSync(List<long> item)
        {
            return Mapper().Update("UpdateReplenishmentDBSync", item);
        }

        public int UpdateReplenishmentMongoSync(List<long> item)
        {
            return Mapper().Update("UpdateReplenishmentMongoSync", item);
        }

        public int DeleteFailedImportReplenishment(Replenishment item)
        {
            return Mapper().Delete("DeleteFailedImportReplenishment", item);
        }

        public string InsertReplenishmentList(MapperBatch item)
        {
            return (string)Mapper().Insert("InsertReplenishmentList", item);
        }

        public string InsertReplenishmentListFullSync(MapperBatch item)
        {
            return (string)Mapper().Insert("InsertReplenishmentListFullSync", item);
        }

        public int GetCountByTransactionDate(string transaction_date)
        {
            return (int)Mapper().QueryForObject("SelectReplenishmentCount", transaction_date);
        }

        public int UpdateBufferNormCalculation(List<long> item)
        {
            return Mapper().Update("UpdateReplenishmentBufferNormCalculation", item);
        }

        public List<Replenishment> SelectByDateSKUNode(Transaction item)
        {
            return Mapper().QueryForList<Replenishment>("SelectReplenishmentBySKUNode", item).Cast<Replenishment>().ToList();
        }

        public List<Replenishment> SelectReplenishmentDates(MapperBatch item)
        {
            return Mapper().QueryForList<Replenishment>("SelectReplenishmentDates", item).Cast<Replenishment>().ToList();
        }

        public Replenishment SelectMaxReplenishmentDate()
        {
            return Mapper().QueryForObject<Replenishment>("SelectMaxReplenishmentDate", null);
        }
    }
}
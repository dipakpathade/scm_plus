﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class ExcessInventoryHelper : BaseHelper
    {
        public List<ExcessInventoryChart> SelectByNodeSKU(MapperBatch item)
        {
            return Mapper().QueryForList<ExcessInventoryChart>("SelectExcessInventoryNodeSKU", item).Cast<ExcessInventoryChart>().ToList();
        }

        public List<ExcessInventoryChart> SelectByNodeCategory(MapperBatch item)
        {
            return Mapper().QueryForList<ExcessInventoryChart>("SelectExcessInventoryNodeCategory", item).Cast<ExcessInventoryChart>().ToList();
        }

        public List<ExcessInventoryChart> SelectReportData(MapperBatch item)
        {
            return Mapper().QueryForList<ExcessInventoryChart>("SelectExcessInventoryReport", item).Cast<ExcessInventoryChart>().ToList();
        }
    }
}
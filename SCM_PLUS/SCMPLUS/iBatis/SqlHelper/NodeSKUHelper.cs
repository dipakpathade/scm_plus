﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class NodeSKUHelper : BaseHelper
    {
        public NodeSKUMapping Select(long id)
        {
            return Mapper().QueryForObject<NodeSKUMapping>("SelectNodeSKUMapping", id);
        }

        public NodeSKUMapping SelectByNodeSKU(NodeSKUMapping item)
        {
            return Mapper().QueryForObject<NodeSKUMapping>("SelectNodeSKUMappingByNodeSKU", item);
        }

        public List<NodeSKUMapping> SelectAll()
        {
            return Mapper().QueryForList("SelectNodeSKUMapping", null).Cast<NodeSKUMapping>().ToList();
        }

        public NodeSKUMapping SelectList(long id)
        {
            return Mapper().QueryForObject<NodeSKUMapping>("SelectNodeSKUMappingList", id);
        }

        public List<NodeSKUMapping> SelectList()
        {
            return Mapper().QueryForList<NodeSKUMapping>("SelectNodeSKUMappingList", null).Cast<NodeSKUMapping>().ToList();
        }

        public long Insert(NodeSKUMapping item)
        {
            return (long)Mapper().Insert("InsertNodeSKUMapping", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteNodeSKUMapping", id);
        }

        public void InsertBatch(List<NodeSKUMapping> item)
        {
            Mapper().Insert("InsertNodeSKUMappingBatch", item);
        }

        public void CreateTempTable(string tmpTable)
        {
            Mapper().Update("CreateTempNodeSKUMappingTable", tmpTable);
        }

        public void InsertTempBatch(TempNodeSKUMapping item)
        {
            Mapper().Insert("InsertTmpNodeSKUMappingBatch", item);
        }

        public List<NodeSKUMapping> SelectTmpNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpNodeSKUMappingNotDuplicate", tmpTable).Cast<NodeSKUMapping>().ToList();
        }

        public List<NodeSKUMapping> SelectTmpDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpNodeSKUMappingDuplicate", tmpTable).Cast<NodeSKUMapping>().ToList();
        }

        public List<NodeSKUMapping> SelectDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectNodeSKUMappingDuplicate", tmpTable).Cast<NodeSKUMapping>().ToList();
        }

        public void DropTempTable(string tmpTable)
        {
            Mapper().Delete("DropTmpNodeSKUMappingTable", tmpTable);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class SKUChartHelper : BaseHelper
    {
        public SKUChart Select(string date)
        {
            return Mapper().QueryForObject<SKUChart>("SelectSKUChart", date);
        }

        public List<SKUChart> SelectByNode(string date)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartNode", date).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectByCategory(string date)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartCategory", date).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectBySKU(string date)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartSKU", date).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectByNodeSKU(MapperBatch item)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartNodeSKU", item).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectByNodeCategory(MapperBatch item)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartNodeCategory", item).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectSkuChartReport(MapperBatch item)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartReport", item).Cast<SKUChart>().ToList();
        }

        public List<SKUChart> SelectSkuChartReportSKU(MapperBatch item)
        {
            return Mapper().QueryForList<SKUChart>("SelectSKUChartReportSKU", item).Cast<SKUChart>().ToList();
        }
    }
}
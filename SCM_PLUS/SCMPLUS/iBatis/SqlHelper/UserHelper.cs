﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class UserHelper : BaseHelper
    {
        public User Select(long id)
        {
            return Mapper().QueryForObject<User>("SelectUser", id);
        }

        public List<User> SelectAll()
        {
            return Mapper().QueryForList("SelectUser", null).Cast<User>().ToList();
        }

        public long Insert(User item)
        {
            return (long)Mapper().Insert("InsertUser", item);
        }

        public int Update(User item)
        {
            return Mapper().Update("UpdateUser", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteUser", id);
        }

        public List<User> SelectUserNameDuplicate(User item)
        {
            return Mapper().QueryForList("SelectUserNameDuplicate", item).Cast<User>().ToList();
        }
        public List<User> SelectUserMobileDuplicate(User item)
        {
            return Mapper().QueryForList("SelectUserMobileDuplicate", item).Cast<User>().ToList();
        }

        public User SelectUserCheckAuthenticate(User item)
        {
            return Mapper().QueryForObject<User>("SelectUserCheckAuthenticate", item);
        }

        public User SelectUserCheckLocked(User item)
        {
            return Mapper().QueryForObject<User>("SelectUserCheckLocked", item);
        }
    }
}
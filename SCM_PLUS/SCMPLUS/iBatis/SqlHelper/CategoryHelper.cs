﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class CategoryHelper : BaseHelper
    {
        public Category Select(long id)
        {
            return Mapper().QueryForObject<Category>("SelectCategory", id);
        }

        public Category SelectBy(string column, string value)
        {
            IDictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("column", column);
            dict.Add("value", value);
            return Mapper().QueryForObject<Category>("SelectCategoryBy", dict);
        }

        public Category SelectByCode(string code)
        {
            return Mapper().QueryForObject<Category>("SelectCategoryByCode", code);
        }

        public Category SelectCategoryForDuplicate(Category item)
        {
            return Mapper().QueryForObject<Category>("SelectCategoryForDuplicate", item);
        }

        public List<Category> SelectAll()
        {
            return Mapper().QueryForList("SelectCategory", null).Cast<Category>().ToList();
        }

        public Category SelectList(long id)
        {
            return Mapper().QueryForObject<Category>("SelectCategoryList", id);
        }

        public List<Category> SelectList()
        {
            return Mapper().QueryForList<Category>("SelectCategoryList", null).Cast<Category>().ToList();
        }

        public long Insert(Category item)
        {
            return (long)Mapper().Insert("InsertCategory", item);
        }

        public int Update(Category item)
        {
            return Mapper().Update("UpdateCategory", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteCategory", id);
        }

        public void InsertBatch(List<Category> item)
        {
            Mapper().Insert("InsertCategoryBatch", item);
        }

        public Dictionary<string, long> SelectAllDictionary()
        {
            return Mapper().QueryForMap("SelectCategoryDictionary", null, "category_code", "category_id").Cast<System.Collections.DictionaryEntry>().ToDictionary(kvp => (string)kvp.Key, kvp => (long)kvp.Value); ;
        }

        public void CreateTempTable(string tmpTable)
        {
            Mapper().Update("CreateTempCategoryTable", tmpTable);
        }

        public void InsertTempBatch(TempCategory item)
        {
            Mapper().Insert("InsertTmpCategoryBatch", item);
        }

        public List<Category> SelectTmpNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpCategoryNotDuplicate", tmpTable).Cast<Category>().ToList();
        }

        public List<Category> SelectTmpDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpCategoryDuplicate", tmpTable).Cast<Category>().ToList();
        }

        public List<Category> SelectDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectCategoryDuplicate", tmpTable).Cast<Category>().ToList();
        }

        public void DropTempTable(string tmpTable)
        {
            Mapper().Delete("DropTmpCategoryTable", tmpTable);
        }
    }
}
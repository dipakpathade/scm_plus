﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class RoleHelper : BaseHelper
    {
        public long Insert(Role item)
        {
            return (long)Mapper().Insert("InsertRole", item);
        }

        public int Update(Role item)
        {
            return Mapper().Update("UpdateRole", item);
        }

        public List<Role> SelectRoleNameDuplicate(Role item)
        {
            return Mapper().QueryForList("SelectRoleNameDuplicate", item).Cast<Role>().ToList();
        }
    }
}
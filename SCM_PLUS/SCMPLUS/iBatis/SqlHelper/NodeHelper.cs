﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class NodeHelper : BaseHelper
    {
        public Node Select(long id)
        {
            return Mapper().QueryForObject<Node>("SelectNode", id);
        }

        public Node SelectByCode(string code)
        {
            return Mapper().QueryForObject<Node>("SelectNodeByCode", code);
        }

        public Node SelectNodeForDuplicate(Node item)
        {
            return Mapper().QueryForObject<Node>("SelectNodeForDuplicate", item);
        }

        public List<Node> SelectAll()
        {
            return Mapper().QueryForList("SelectNode", null).Cast<Node>().ToList();
        }

        public Node SelectList(long id)
        {
            return Mapper().QueryForObject<Node>("SelectNodeList", id);
        }

        public List<Node> SelectList()
        {
            return Mapper().QueryForList<Node>("SelectNodeList", null).Cast<Node>().ToList();
        }

        public long Insert(Node item)
        {
            return (long)Mapper().Insert("InsertNode", item);
        }

        public int Update(Node item)
        {
            return Mapper().Update("UpdateNode", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteNode", id);
        }

        public void InsertBatch(List<Node> item)
        {
            Mapper().Insert("InsertNodeBatch", item);
        }

        public Dictionary<string, long> SelectAllDictionary()
        {
            return Mapper().QueryForMap("SelectNodeDictionary", null, "node_code", "node_id").Cast<System.Collections.DictionaryEntry>().ToDictionary(kvp => (string)kvp.Key, kvp => (long)kvp.Value); ;
        }

        public void CreateTempTable(string tmpTable)
        {
            Mapper().Update("CreateTempNodeTable", tmpTable);
        }

        public void InsertTempBatch(TempNode item)
        {
            Mapper().Insert("InsertTmpNodeBatch", item);
        }

        public List<Node> SelectTmpNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpNodeNotDuplicate", tmpTable).Cast<Node>().ToList();
        }

        public List<Node> SelectTmpDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpNodeDuplicate", tmpTable).Cast<Node>().ToList();
        }

        public List<Node> SelectDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectNodeDuplicate", tmpTable).Cast<Node>().ToList();
        }

        public void DropTempTable(string tmpTable)
        {
            Mapper().Delete("DropTmpNodeTable", tmpTable);
        }
    }
}
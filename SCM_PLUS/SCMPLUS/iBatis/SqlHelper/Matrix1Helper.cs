﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class Matrix1Helper : BaseHelper
    {
        public List<dynamic> SelectReports(MapperBatch item)
        {
            return Mapper().QueryForList<dynamic>("SelectMatrix1Report", item).Cast<dynamic>().ToList();
        }

        public List<Matrix1> SelectMatrix1Column(MapperBatch item)
        {
            return Mapper().QueryForList<Matrix1>("SelectMatrix1Column", item).Cast<Matrix1>().ToList();
        }

        public List<Node> SelectMatrix1Node(MapperBatch item)
        {
            return Mapper().QueryForList<Node>("SelectMatrix1Node", item).Cast<Node>().ToList();
        }


        public List<Matrix1> SelectMatrix2Column(MapperBatch item)
        {
            return Mapper().QueryForList<Matrix1>("SelectMatrix2Column", item).Cast<Matrix1>().ToList();
        }

        public Matrix1 SelectMatrix2RecordCount(MapperBatch item)
        {
            return Mapper().QueryForObject<Matrix1>("SelectMatrix2RecordCountReplenishment", item);
        }

        public List<dynamic> SelectMatrix2Report(MapperBatch item)
        {
            return Mapper().QueryForList<dynamic>("SelectMatrix2ReportReplenishment", item).Cast<dynamic>().ToList();
        }

        public List<dynamic> SelectMatrix2Export(MapperBatch item)
        {
            return Mapper().QueryForList<dynamic>("SelectMatrix2ExportByReplenishment", item).Cast<dynamic>().ToList();
        }
    }
}
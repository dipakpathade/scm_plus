﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class DynamicBufferManagementHelper : BaseHelper
    {
        public DynamicBufferManagement Select(long id)
        {
            return Mapper().QueryForObject<DynamicBufferManagement>("SelectDynamicBufferManagement", id);
        }

        public List<DynamicBufferManagement> SelectAll()
        {
            return Mapper().QueryForList("SelectDynamicBufferManagement", null).Cast<DynamicBufferManagement>().ToList();
        }

        public long Insert(DynamicBufferManagement item)
        {
            return (long)Mapper().Insert("InsertDynamicBufferManagement", item);
        }

        public int Update(DynamicBufferManagement item)
        {
            return Mapper().Update("UpdateDynamicBufferManagement", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteDynamicBufferManagement", id);
        }

        public int DeleteAll()
        {
            return Mapper().Delete("DeleteDynamicBufferManagement", null);
        }

        public DynamicBufferManagement SelectLatestDynamicBufferManagement()
        {
            return Mapper().QueryForObject<DynamicBufferManagement>("SelectLatestDynamicBufferManagement", null);
        }

        public string InsertPenetrationListForTransactionDate(string date)
        {
            return (string)Mapper().Insert("InsertPenetrationListForTransactionDate", date);
        }

        public List<Penetration> SelectPenetrationListByTransactionDate(string date)
        {
            return Mapper().QueryForList("SelectPenetrationListByTransactionDate", date).Cast<Penetration>().ToList();
        }

        public List<Penetration> SelectPenetrationListByTransactionDateForBNChange(string date)
        {
            return Mapper().QueryForList("SelectPenetrationListByTransactionDateForBNChange", date).Cast<Penetration>().ToList();
        }

        public int UpdatePenetrationBufferNorm(Penetration item)
        {
            return Mapper().Update("UpdatePenetrationBufferNorm", item);
        }

        public List<Penetration> SelectPenetrationReport(MapperBatch item)
        {
            return Mapper().QueryForList("SelectPenetrationReport", item).Cast<Penetration>().ToList();
        }

        public Penetration SelectPenetrationReportCount(MapperBatch item)
        {
            return Mapper().QueryForObject<Penetration>("SelectPenetrationReportCount", item);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class UserDataPermissionHelper : BaseHelper
    {
        public long Insert(UserDataPermissions item)
        {
            return (long)Mapper().Insert("InsertUserDataPermission", item);
        }

        public int DeleteDataPermissionByRole(long id)
        {
            return Mapper().Delete("DeleteDataPermissionByRole", id);
        }

        public void InsertBatch(List<UserDataPermissions> item)
        {
            Mapper().Insert("InsertDataPermissionBatch", item);
        }

        public List<UserDataPermissions> SelectDataPermissionByRole(UserDataPermissions item)
        {
            return Mapper().QueryForList("SelectDataPermissionByRole", item).Cast<UserDataPermissions>().ToList();
        }
    }
}
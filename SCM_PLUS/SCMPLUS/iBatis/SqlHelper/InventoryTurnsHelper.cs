﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class InventoryTurnsHelper : BaseHelper
    {
        public List<InventoryTurns> Select(string date)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurns", date).Cast<InventoryTurns>().ToList();
        }

        public List<InventoryTurns> SelectByNode(MapperBatch item)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurnsNode", item).Cast<InventoryTurns>().ToList();
        }

        public List<InventoryTurns> SelectBySKU(string date)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurnsSKU", date).Cast<InventoryTurns>().ToList();
        }

        public List<InventoryTurns> SelectByNodeSKU(MapperBatch item)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurnsNodeSKU", item).Cast<InventoryTurns>().ToList();
        }

        public List<InventoryTurns> SelectByNodeCategory(MapperBatch item)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurnsNodeCategory", item).Cast<InventoryTurns>().ToList();
        }

        public List<InventoryTurns> SelectInventoryTurnsReport(MapperBatch item)
        {
            return Mapper().QueryForList<InventoryTurns>("SelectInventoryTurnsReport", item).Cast<InventoryTurns>().ToList();
        }
    }
}
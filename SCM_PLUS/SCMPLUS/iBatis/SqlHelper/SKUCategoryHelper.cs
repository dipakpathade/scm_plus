﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class SKUCategoryHelper : BaseHelper
    {
        public SKUCategory Select(long id)
        {
            return Mapper().QueryForObject<SKUCategory>("SelectSKUCategory", id);
        }

        public List<SKUCategory> SelectAll()
        {
            return Mapper().QueryForList("SelectSKUCategory", null).Cast<SKUCategory>().ToList();
        }

        public SKUCategory SelectBySKUCategory(SKUCategory item)
        {
            return Mapper().QueryForObject<SKUCategory>("SelectSKUCategoryBySKUCategory", item);
        }

        public SKUCategory SelectList(long id)
        {
            return Mapper().QueryForObject<SKUCategory>("SelectSKUCategoryList", id);
        }

        public List<SKUCategory> SelectList()
        {
            return Mapper().QueryForList<SKUCategory>("SelectSKUCategoryList", null).Cast<SKUCategory>().ToList();
        }

        public long Insert(SKUCategory item)
        {
            return (long)Mapper().Insert("InsertSKUCategory", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteSKUCategory", id);
        }

        public int DeleteAll()
        {
            return Mapper().Delete("DeleteSKUCategory", null);
        }

        public void InsertBatch(List<SKUCategory> item)
        {
            Mapper().Insert("InsertSKUCategoryBatch", item);
        }

        public void CreateTempTable(string tmpTable)
        {
            Mapper().Update("CreateTempSKUCategoryTable", tmpTable);
        }

        public void InsertTempBatch(TempSKUCategory item)
        {
            Mapper().Insert("InsertTmpSKUCategoryBatch", item);
        }

        public List<SKUCategory> SelectTmpNotDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpSKUCategoryNotDuplicate", tmpTable).Cast<SKUCategory>().ToList();
        }

        public List<SKUCategory> SelectTmpDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectTmpSKUCategoryDuplicate", tmpTable).Cast<SKUCategory>().ToList();
        }

        public List<SKUCategory> SelectDuplicate(string tmpTable)
        {
            return Mapper().QueryForList("SelectSKUCategoryDuplicate", tmpTable).Cast<SKUCategory>().ToList();
        }

        public void DropTempTable(string tmpTable)
        {
            Mapper().Delete("DropTmpSKUCategoryTable", tmpTable);
        }

    }
}
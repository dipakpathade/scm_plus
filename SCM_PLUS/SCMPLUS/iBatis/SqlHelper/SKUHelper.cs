﻿using System.Collections.Generic;
using System.Linq;

namespace SCMPLUS
{
    public class SKUHelper : BaseHelper
    {
        public SKU Select(long id)
        {
            return Mapper().QueryForObject<SKU>("SelectSKU", id);
        }

        public SKU SelectByCode(string code)
        {
            return Mapper().QueryForObject<SKU>("SelectSKUByCode", code);
        }

        public SKU SelectSKUForDuplicate(SKU item)
        {
            return Mapper().QueryForObject<SKU>("SelectSKUForDuplicate", item);
        }

        public List<SKU> SelectAll()
        {
            return Mapper().QueryForList("SelectSKU", null).Cast<SKU>().ToList();
        }

        public long Insert(SKU item)
        {
            return (long)Mapper().Insert("InsertSKU", item);
        }

        public int Update(SKU item)
        {
            return Mapper().Update("UpdateSKU", item);
        }

        public int Delete(long id)
        {
            return Mapper().Delete("DeleteSKU", id);
        }

        public void InsertBatch(List<SKU> item)
        {
            Mapper().Insert("InsertSKUBatch", item);
        }

        public List<SKU> SelectBatchInsertedSKU(int limit)
        {
            return Mapper().QueryForList("SelectBatchInsertedSKU", limit).Cast<SKU>().ToList();
        }

        public Dictionary<string, long> SelectAllDictionary()
        {
            return Mapper().QueryForMap("SelectSKUDictionary", null, "sku_code", "sku_id").Cast<System.Collections.DictionaryEntry>().ToDictionary(kvp => (string)kvp.Key, kvp => (long)kvp.Value); ;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using DataTablePager.Core;
using DataTablePager.Utils;
using MongoDB.Driver.Builders;
using MongoDB.Web.Providers;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.Reflection;
using MongoDB.Driver;
using log4net;
using MongoDB.Bson;

namespace SCMPLUS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AuthenticatingHeader]

    public class Download : IDownload
    {
        private JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));
        public static HSSFWorkbook workbook = new HSSFWorkbook();

        public Stream DownloadFile(string fileName)
        {
            string downloadFilePath = Path.Combine(AppSettings.ExportFolder, fileName);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream";
            return File.OpenRead(downloadFilePath);
        }

        public static void PrepareWorkbook(string subject, string sheetName, out ISheet sheet1, out ICellStyle headerStyle, out ICellStyle style)
        {
            workbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = AppSettings.Company;
            workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = subject;
            workbook.SummaryInformation = si;

            sheet1 = workbook.CreateSheet(sheetName);

            headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            HSSFFont font = (HSSFFont)workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(font);

            style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
        }

        public static ISheet[] PrepareWorkbook(string subject, string sheetName, ISheet[] sheets, out ICellStyle headerStyle, out ICellStyle style)
        {
            workbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = AppSettings.Company;
            workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = subject;
            workbook.SummaryInformation = si;

            if (sheets.Length > 0)
            {
                for (int i = 0; i < sheets.Length; i++)
                {
                    sheets[i] = workbook.CreateSheet(sheetName.Trim() + "_Sheet" + (i + 1).ToString());
                }
            }

            headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            HSSFFont font = (HSSFFont)workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(font);

            style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;

            return sheets;
        }

        public static void CreateCell(IRow row, ICellStyle style, int column, string value, bool isBold)
        {
            ICell cell = row.CreateCell(column);
            cell.CellStyle = style;
            cell.SetCellValue(value);
        }

        #region Category
        public string ExportCategoryList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string categoryXlsName = AppSettings.CategoryXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Category> _lst = new List<Category>();
                string category_collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? categoryXlsName : aoDataList.GetNVValue("referenceTitle"))
                    : categoryXlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + categoryXlsName).Replace("\\", "/");
                log.reference_type = "Category";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);

                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                Category filterdata = (Category)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Category));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "category_code":
                            if (filterdata.category_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("category_code", filterdata.category_code);
                            break;
                        case "category_name":
                            if (filterdata.category_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("category_name", filterdata.category_code);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Category> dataTablePager1 = new DataTablePager<Category>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Category>.Paginate(category_collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                foreach (Category cat in _lst)
                {
                    Category _cat = MongoHelper<Category>.Single(category_collection, Query.And(Query.EQ("status", 1), Query.EQ("category_id", cat.parent_id)));
                    cat.parent_category = _cat != null ? _cat.category_name : "NA";
                }

                DataTablePager<Category> dataTablePager = new DataTablePager<Category>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Category> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;


                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Category List", "Category", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Category> _lstBatch = new List<Category>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Category item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "category_code":
                                        CreateCell(row, style, column_counter, item.category_code, false);
                                        ++column_counter;
                                        break;
                                    case "category_name":
                                        CreateCell(row, style, column_counter, item.category_name, false);
                                        ++column_counter;
                                        break;
                                    case "parent_category":
                                        CreateCell(row, style, column_counter, item.parent_category, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }

                string zipFileName = WriteExcelFile(paramWorkbookPath, categoryXlsName);

                if (File.Exists(paramWorkbookPath + categoryXlsName))
                {
                    File.Delete(paramWorkbookPath + categoryXlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtCategoryList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static string WriteExcelFile(string paramWorkbookPath, string categoryXlsName)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(paramWorkbookPath + categoryXlsName, FileMode.Create);
            workbook.Write(file);
            file.Close();

            string zipFileName = categoryXlsName + ".zip";

            if (!Common.Instance.CreateZip(categoryXlsName, paramWorkbookPath, zipFileName, paramWorkbookPath, AppSettings.ExportFilePassword))
                throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);
            return zipFileName;
        }
        #endregion

        #region Node
        public string ExportNodeList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.NodeXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Node> _lst = new List<Node>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Node";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                Node filterdata = (Node)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Node));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "node_code":
                            if (filterdata.node_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("region_code", filterdata.node_code);
                            break;
                        case "region_name":
                            if (filterdata.node_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("node_name", filterdata.node_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Node> dataTablePager1 = new DataTablePager<Node>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Node>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                foreach (Node item in _lst)
                {
                    Node _obj = MongoHelper<Node>.Single(collection, Query.And(Query.EQ("status", 1), Query.EQ("node_id", item.parent_id)));
                    item.parent_node = _obj != null ? _obj.node_name : "NA";
                }

                DataTablePager<Node> dataTablePager = new DataTablePager<Node>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Node> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Node List", "Node", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Node> _lstBatch = new List<Node>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Node item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "node_code":
                                        CreateCell(row, style, column_counter, item.node_code, false);
                                        ++column_counter;
                                        break;
                                    case "node_name":
                                        CreateCell(row, style, column_counter, item.node_name, false);
                                        ++column_counter;
                                        break;
                                    case "parent_node":
                                        CreateCell(row, style, column_counter, item.parent_node, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtNodeList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region SKU
        public string ExportSKUList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.SKUXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<SKU> _lst = new List<SKU>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "SKU";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                SKU filterdata = (SKU)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(SKU));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "sku_code":
                            if (filterdata.sku_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_code", filterdata.sku_code);
                            break;
                        case "receiver_node":
                            if (filterdata.sku_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_name", filterdata.sku_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<SKU> dataTablePager1 = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<SKU>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<SKU> dataTablePager = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKU> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("SKU List", "SKU", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<SKU> _lstBatch = new List<SKU>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (SKU item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "sku_code":
                                        CreateCell(row, style, column_counter, item.sku_code, false);
                                        ++column_counter;
                                        break;
                                    case "sku_name":
                                        CreateCell(row, style, column_counter, item.sku_name, false);
                                        ++column_counter;
                                        break;
                                    case "sku_description":
                                        CreateCell(row, style, column_counter, item.sku_description, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtSKUList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region Region
        public string ExportRegionList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.RegionXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Region> _lst = new List<Region>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.REGION);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Region";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                Region filterdata = (Region)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Region));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "region_code":
                            if (filterdata.region_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("region_code", filterdata.region_code);
                            break;
                        case "region_name":
                            if (filterdata.region_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("region_name", filterdata.region_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Region> dataTablePager1 = new DataTablePager<Region>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Region>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                foreach (Region item in _lst)
                {
                    Country _obj = MongoHelper<Country>.Single(Common.Instance.GetEnumValue(Common.CollectionName.COUNTRY), Query.And(Query.EQ("status", 1), Query.EQ("country_id", item.country_id)));
                    item.country = _obj;
                }

                DataTablePager<Region> dataTablePager = new DataTablePager<Region>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Region> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Region List", "Region", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Region> _lstBatch = new List<Region>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Region item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "region_code":
                                        CreateCell(row, style, column_counter, item.region_code, false);
                                        ++column_counter;
                                        break;
                                    case "region_name":
                                        CreateCell(row, style, column_counter, item.region_name, false);
                                        ++column_counter;
                                        break;
                                    case "country.country_name":
                                        CreateCell(row, style, column_counter, item.country.country_name, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtRegionList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region Buffer Norm
        public string ExportBufferNormList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.BufferNormXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<BufferNorm> _lst = new List<BufferNorm>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Buffer Norm";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                BufferNorm filterdata = (BufferNorm)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(BufferNorm));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "sku":
                            if (filterdata.sku != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_name", filterdata.sku.sku_name);
                            break;
                        case "node":
                            if (filterdata.node != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("node.node_name", filterdata.node.node_name);
                            break;
                    }
                }

                DataTablePager<BufferNorm> dataTablePager1 = new DataTablePager<BufferNorm>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<BufferNorm>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<BufferNorm> dataTablePager = new DataTablePager<BufferNorm>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<BufferNorm> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Buffer Norm List", "BufferNorm", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<BufferNorm> _lstBatch = new List<BufferNorm>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (BufferNorm item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "effective_from":
                                        CreateCell(row, style, column_counter, item.effective_from.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "sku.sku_name":
                                        CreateCell(row, style, column_counter, item.sku.sku_name, false);
                                        ++column_counter;
                                        break;
                                    case "node.node_name":
                                        CreateCell(row, style, column_counter, item.node.node_name, false);
                                        ++column_counter;
                                        break;
                                    case "buffer_norm":
                                        CreateCell(row, style, column_counter, item.buffer_norm.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "is_default_zone":
                                        CreateCell(row, style, column_counter, item.is_default_zone.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "red_zone":
                                        CreateCell(row, style, column_counter, item.red_zone.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "yellow_zone":
                                        CreateCell(row, style, column_counter, item.yellow_zone.ToString(), false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtBufferNormList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region Vendor
        public string ExportVendorList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.VendorXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Vendor> _lst = new List<Vendor>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.VENDOR);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Supplier";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                Vendor filterdata = (Vendor)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Vendor));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "vendor_code":
                            if (filterdata.vendor_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("vendor_code", filterdata.vendor_code);
                            break;
                        case "vendor_name":
                            if (filterdata.vendor_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("vendor_name", filterdata.vendor_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Vendor> dataTablePager1 = new DataTablePager<Vendor>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<Vendor>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Vendor item in _lst)
                {
                    Region _obj = MongoHelper<Region>.Single(Common.Instance.GetEnumValue(Common.CollectionName.REGION), Query.And(Query.EQ("status", 1), Query.EQ("region_id", item.region_id)));
                    item.region = _obj;
                }

                DataTablePager<Vendor> dataTablePager = new DataTablePager<Vendor>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Vendor> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Vendor List", "Vendor", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Vendor> _lstBatch = new List<Vendor>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Vendor item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "vendor_code":
                                        CreateCell(row, style, column_counter, item.vendor_code, false);
                                        ++column_counter;
                                        break;
                                    case "vendor_name":
                                        CreateCell(row, style, column_counter, item.vendor_name, false);
                                        ++column_counter;
                                        break;
                                    case "region.region_name":
                                        CreateCell(row, style, column_counter, item.region.region_name, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtVendorList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region User
        public string ExportUserList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.UserXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<User> _lst = new List<User>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.USER);
                string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "User";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                User filterdata = (User)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(User));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "first_name":
                            if (filterdata.first_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("first_name", filterdata.first_name);
                            break;
                        case "last_name":
                            if (filterdata.last_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("last_name", filterdata.last_name);
                            break;
                        case "user_name":
                            if (filterdata.user_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("user_name", filterdata.user_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<User> dataTablePager1 = new DataTablePager<User>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<User>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<User> dataTablePager = new DataTablePager<User>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<User> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("User List", "User", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<User> _lstBatch = new List<User>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (User item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "first_name":
                                        CreateCell(row, style, column_counter, item.first_name, false);
                                        ++column_counter;
                                        break;
                                    case "last_name":
                                        CreateCell(row, style, column_counter, item.last_name, false);
                                        ++column_counter;
                                        break;
                                    case "user_name":
                                        CreateCell(row, style, column_counter, item.user_name, false);
                                        ++column_counter;
                                        break;
                                    case "mobile":
                                        CreateCell(row, style, column_counter, Convert.ToString(item.mobile), false);
                                        ++column_counter;
                                        break;
                                    case "description":
                                        CreateCell(row, style, column_counter, item.description, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtUserList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region Node SKU Mapping
        public string ExportNodeSKUList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.SKUNodeXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<NodeSKUMapping> _lst = new List<NodeSKUMapping>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE_SKU_MAPPING);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Node SKU Mapping";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                NodeSKUMapping filterdata = (NodeSKUMapping)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(NodeSKUMapping));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "supplier_node":
                            if (filterdata.supplier_node != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("supplier_node.node_code", filterdata.supplier_node.node_code);
                            break;
                        case "receiver_node":
                            if (filterdata.receiver_node != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("receiver_node.node_code", filterdata.receiver_node.node_code);
                            break;
                        case "sku":
                            if (filterdata.sku != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_code", filterdata.sku.sku_code);
                            break;
                    }
                }

                DataTablePager<NodeSKUMapping> dataTablePager1 = new DataTablePager<NodeSKUMapping>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<NodeSKUMapping>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<NodeSKUMapping> dataTablePager = new DataTablePager<NodeSKUMapping>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<NodeSKUMapping> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Node SKU Mapping List", "Node SKU Mapping", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<NodeSKUMapping> _lstBatch = new List<NodeSKUMapping>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (NodeSKUMapping item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "effective_date":
                                        CreateCell(row, style, column_counter, item.effective_date.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "receiver_node.node_code":
                                        CreateCell(row, style, column_counter, item.receiver_node.node_code, false);
                                        ++column_counter;
                                        break;
                                    case "sku.sku_code":
                                        CreateCell(row, style, column_counter, item.sku.sku_code, false);
                                        ++column_counter;
                                        break;
                                    case "supplier_node.node_code":
                                        CreateCell(row, style, column_counter, item.supplier_node.node_code, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtNodeSKUList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region User
        public string ExportRoleList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.RoleXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Role> _lst = new List<Role>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.ROLE);
                string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Role";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                Role filterdata = (Role)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Role));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "role_name":
                            if (filterdata.role_name != null)
                                qc[i++] = MongoHelper<Role>.MatchesStatement("role_name", filterdata.role_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Role> dataTablePager1 = new DataTablePager<Role>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<Role>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<Role> dataTablePager = new DataTablePager<Role>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Role> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Role List", "Role", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Role> _lstBatch = new List<Role>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Role item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "role_name":
                                        CreateCell(row, style, column_counter, item.role_name, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExoprtRoleList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region SKU Category Mapping
        public string ExportSKUCategoryList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.SKUCategoryXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<SKUCategory> _lst = new List<SKUCategory>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                    : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "SKU Category";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                SKUCategory filterdata = (SKUCategory)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(SKUCategory));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "category":
                            if (filterdata.category != null)
                                qc[i++] = MongoHelper<Category>.MatchesStatement("category.category_code", filterdata.category.category_code);
                            break;
                        case "sku":
                            if (filterdata.sku != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_code", filterdata.sku.sku_code);
                            break;
                    }
                }

                DataTablePager<SKUCategory> dataTablePager1 = new DataTablePager<SKUCategory>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<SKUCategory>.Paginate(collection, query, sorted, out iTotalRecords);

                /****************************************************************/

                DataTablePager<SKUCategory> dataTablePager = new DataTablePager<SKUCategory>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKUCategory> formattedList = dataTablePager.Filter(isExport: true);
                _lst = formattedList.aaData;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("SKU Category List", "SKU Category", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<SKUCategory> _lstBatch = new List<SKUCategory>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (SKUCategory item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "category.category_code":
                                        CreateCell(row, style, column_counter, item.category.category_code, false);
                                        ++column_counter;
                                        break;
                                    case "sku.sku_code":
                                        CreateCell(row, style, column_counter, item.sku.sku_code, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExportSKUCategoryList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion


        public string ExportMatrix1List(List<jsonAOData> jsonData)
        {
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.Matrix1XlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                   ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                   : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Matrix Formate1";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);


                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<dynamic> _lst = new List<dynamic>();

                string Date = "";
                if (filterdata.date != null)
                {
                    string[] date = filterdata.date.Split('/');
                    Date = date[2] + "-" + date[1] + "-" + date[0];
                }

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                _log.Info("ExportMatrix1List query execution start: " + DateTime.Now.ToString("o"));

                MapperBatch param = new MapperBatch();
                param.date = Date;

                if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                {
                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.ToList<long>();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission)
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission)
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                {

                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission && _matchNodePermission)
                    {
                        if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                        else
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);

                }
                else if (filterdata.categorySelection != null)
                {
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchNodePermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchCategoryPermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.supplynodeSelection != null)
                {

                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.bomSelection != null)
                {

                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.bomSelection.Length > 0)
                        param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }

                _log.Info("ExportMatrix1List query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = _lst.Count;

                ICellStyle style;
                ICellStyle headerStyle;

                int row_counter = 0;
                int column_counter = 0;

                DataTablePager<dynamic> dataTablePager = new DataTablePager<dynamic>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook("Matrix - Formate1", "Matrix - Formate1", sheet, out headerStyle, out style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<dynamic> _lstBatch = new List<dynamic>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (var arr in _lstBatch.ToArray())
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        int clnloop = 0;
                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.mData != "srno")
                            {
                                if (ao.bVisible)
                                {
                                    CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                    CreateCell(row, style, column_counter, arr[clnloop].ToString(), false);
                                    ++column_counter;
                                }
                                clnloop = clnloop + 1;
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;

            }
            catch (Exception e)
            {
                _log.Error("ExportMatrix1List::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }

        }

        public string ExportMatrix2List(List<jsonAOData> jsonData)
        {
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string xlsName = AppSettings.Matrix2XlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                   ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? xlsName : aoDataList.GetNVValue("referenceTitle"))
                   : xlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + xlsName).Replace("\\", "/");
                log.reference_type = "Matrix Formate2";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<dynamic> _lst = new List<dynamic>();

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                _log.Info("ExportMatrix2List query execution start: " + DateTime.Now.ToString("o"));

                int recCount = 0;
                int limit = 500;

                MapperBatch param = new MapperBatch();
                param.fromdate = sqlFromDate;
                param.todate = sqlToDate;
                param.replenishment = Dao.Replenishment().SelectReplenishmentDates(param);

                if (param.replenishment != null)
                {
                    if (param.replenishment.Count > 0)
                    {
                        if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                        {
                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            }

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {

                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission && _matchNodePermission)
                            {
                                if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                                else
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.bomSelection != null)
                        {

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;
                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                int cnt = recCount;
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = limit * offset;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Export(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                    }
                }

                _log.Info("ExportMatrix2List query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = _lst.Count;

                ICellStyle style;
                ICellStyle headerStyle;
                ICellStyle blue_style;
                ICellStyle green_style;
                ICellStyle white_style;
                ICellStyle yellow_style;
                ICellStyle red_style;
                ICellStyle black_style;

                int row_counter = 0;
                int column_counter = 0;

                DataTablePager<dynamic> dataTablePager = new DataTablePager<dynamic>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopExCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopExCount];
                sheet = PrepareWorkbook("Matrix - Formate2", "Matrix - Formate2", sheet, out headerStyle, out style, out blue_style, out green_style, out white_style, out yellow_style, out red_style, out black_style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopExCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<dynamic> _lstBatch = new List<dynamic>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (var arr in _lstBatch.ToArray())
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        int clnloop = 0;
                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.mData != "srno")
                            {
                                if (ao.bVisible)
                                {
                                    if (ao.mData != "node_code" && ao.mData != "node_name" && ao.mData != "sku_code" && ao.mData != "sku_name")
                                    {
                                        CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                        string value = arr[clnloop] != null ? arr[clnloop].ToString() : "";
                                        string[] str = value.Split('_');
                                        string _zone = "White";
                                        string _clQty = "0";

                                        if (str.Length >= 2)
                                        {
                                            _zone = str[0].ToString().Trim();
                                            _clQty = str[1].ToString().Trim();
                                        }
                                        else if (str.Length == 1)
                                        {
                                            _clQty = str[0].ToString().Trim();
                                        }
                                        CreateCell(row, style, column_counter, _clQty, false);
                                        ++column_counter;

                                        CreateCell(headerRow, headerStyle, column_counter, ao.sTitle + " Zone", true);
                                        switch (_zone)
                                        {
                                            case "White":
                                                CreateCell(row, white_style, column_counter, "", false);
                                                break;
                                            case "Black":
                                                CreateCell(row, black_style, column_counter, "", false);
                                                break;
                                            case "Red":
                                                CreateCell(row, red_style, column_counter, "", false);
                                                break;
                                            case "Green":
                                                CreateCell(row, green_style, column_counter, "", false);
                                                break;
                                            case "Yellow":
                                                CreateCell(row, yellow_style, column_counter, "", false);
                                                break;
                                            case "Blue":
                                                CreateCell(row, blue_style, column_counter, "", false);
                                                break;
                                            default:
                                                CreateCell(row, style, column_counter, "", false);
                                                break;
                                        }
                                        ++column_counter;
                                    }
                                    else
                                    {
                                        CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                        CreateCell(row, style, column_counter, arr[clnloop].ToString(), false);
                                        ++column_counter;
                                    }
                                }
                                clnloop = clnloop + 1;
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + xlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = xlsName + ".zip";

                if (!Common.Instance.CreateZip(xlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + xlsName))
                {
                    File.Delete(paramWorkbookPath + xlsName);
                }

                log.status = 1;
                log.comment = "{rows_affected:" + iTotalRecords + "}";
                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status_text = "" + iTotalRecords + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;

            }
            catch (Exception e)
            {
                _log.Error("ExportMatrix2List::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }

        }

        private ISheet[] PrepareWorkbook(string subject, string sheetName, ISheet[] sheets, out ICellStyle headerStyle, out ICellStyle style, out ICellStyle blue_style, out ICellStyle green_style, out ICellStyle white_style, out ICellStyle yellow_style, out ICellStyle red_style, out ICellStyle black_style)
        {
            workbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = AppSettings.Company;
            workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = subject;
            workbook.SummaryInformation = si;

            if (sheets.Length > 0)
            {
                for (int i = 0; i < sheets.Length; i++)
                {
                    sheets[i] = workbook.CreateSheet(sheetName + "_" + (i + 1).ToString());
                }
            }

            headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            HSSFFont font = (HSSFFont)workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(font);

            style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;

            ICellStyle zone_style = workbook.CreateCellStyle();
            zone_style.BorderBottom = BorderStyle.THIN;
            zone_style.BorderLeft = BorderStyle.THIN;
            zone_style.BorderRight = BorderStyle.THIN;
            zone_style.BorderTop = BorderStyle.THIN;

            blue_style = workbook.CreateCellStyle();
            blue_style.BorderBottom = BorderStyle.THIN;
            blue_style.BorderLeft = BorderStyle.THIN;
            blue_style.BorderRight = BorderStyle.THIN;
            blue_style.BorderTop = BorderStyle.THIN;
            blue_style.FillForegroundColor = HSSFColor.BLUE.index;
            blue_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            green_style = workbook.CreateCellStyle();
            green_style.BorderBottom = BorderStyle.THIN;
            green_style.BorderLeft = BorderStyle.THIN;
            green_style.BorderRight = BorderStyle.THIN;
            green_style.BorderTop = BorderStyle.THIN;
            green_style.FillForegroundColor = HSSFColor.GREEN.index;
            green_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            white_style = workbook.CreateCellStyle();
            white_style.BorderBottom = BorderStyle.THIN;
            white_style.BorderLeft = BorderStyle.THIN;
            white_style.BorderRight = BorderStyle.THIN;
            white_style.BorderTop = BorderStyle.THIN;
            white_style.FillForegroundColor = HSSFColor.WHITE.index;
            white_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            yellow_style = workbook.CreateCellStyle();
            yellow_style.BorderBottom = BorderStyle.THIN;
            yellow_style.BorderLeft = BorderStyle.THIN;
            yellow_style.BorderRight = BorderStyle.THIN;
            yellow_style.BorderTop = BorderStyle.THIN;
            yellow_style.FillForegroundColor = HSSFColor.YELLOW.index;
            yellow_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            red_style = workbook.CreateCellStyle();
            red_style.BorderBottom = BorderStyle.THIN;
            red_style.BorderLeft = BorderStyle.THIN;
            red_style.BorderRight = BorderStyle.THIN;
            red_style.BorderTop = BorderStyle.THIN;
            red_style.FillForegroundColor = HSSFColor.RED.index;
            red_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            black_style = workbook.CreateCellStyle();
            black_style.BorderBottom = BorderStyle.THIN;
            black_style.BorderLeft = BorderStyle.THIN;
            black_style.BorderRight = BorderStyle.THIN;
            black_style.BorderTop = BorderStyle.THIN;
            black_style.FillForegroundColor = HSSFColor.BLACK.index;
            black_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            return sheets;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using DataTablePager.Core;
using DataTablePager.Utils;
using IBatisNet.DataMapper;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Web.Providers;
using System.Reflection;
using MongoDB.Driver;
using System.Text.RegularExpressions;
using log4net;

namespace SCMPLUS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AuthenticatingHeader]

    public class Master : IMaster
    {
        private JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));

        #region Node
        public List<Node> GetNodes()
        {
            try
            {
                List<Node> _lst = new List<Node>();
                string node = Common.Instance.GetEnumValue(Common.CollectionName.NODE);
                _lst = MongoHelper<Node>.Find(node, Query.EQ("status", 1));
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetNodes::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<Node> GetNodeList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Node> _lst = new List<Node>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Node filterdata = (Node)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Node));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;

                // For Getting Permission ID START
                IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                // For Getting Permission ID END

                int cntDict = dict.Count + 1;
                IMongoQuery[] qc = new IMongoQuery[cntDict];
                qc[i++] = _query_per;

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "node_code":
                            if (filterdata.node_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("node_code", filterdata.node_code);
                            break;
                        case "node_name":
                            if (filterdata.node_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("node_name", filterdata.node_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Node> dataTablePager1 = new DataTablePager<Node>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = cntDict > 0 ? cntDict == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Node>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Node item in _lst)
                {
                    Node _obj = MongoHelper<Node>.Single(collection, Query.And(Query.EQ("status", 1), Query.EQ("node_id", item.parent_id)));
                    item.parent_node = _obj != null ? _obj.node_name : "NA";
                }

                DataTablePager<Node> dataTablePager = new DataTablePager<Node>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Node> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetNodeList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<Node> GetActiveNodes()
        {
            List<Node> _lst = new List<Node>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

            _lst = MongoHelper<Node>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("node_code"));

            foreach (Node item in _lst)
            {
                Node _obj = MongoHelper<Node>.Single(collection, Query.And(Query.EQ("status", 1), Query.EQ("node_id", item.parent_id)));
                item.parent_node = _obj != null ? _obj.node_name : "NA";
            }
            return _lst;
        }

        public List<Node> GetActiveNodesHierarchy(DataFilter param = null)
        {
            try
            {
                // For Getting Permission ID START
                IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                // For Getting Permission ID END

                List<Node> _lst = new List<Node>();
                List<Node> _filterlst = new List<Node>();
                string node = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

                if (param != null)
                {
                    if (param.node_code != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_code", param.node_code), _query_per));
                    else if (param.node_name != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_name", param.node_name), _query_per));
                }

                if (param == null)
                {
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0), _query_per));
                }
                else
                {
                    long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0), Query.In("node_id", new BsonArray(parent_ids)), _query_per));
                }

                if (_lst != null)
                    if (_lst.Count > 0)
                    {
                        _lst = RecursiveNodesHierarchy(_lst, param, _filterlst);
                    }
                    else
                    {
                        long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                        foreach (var id in parent_ids)
                        {
                            if (id > 0)
                            {
                                List<Node> _clst = new List<Node>();
                                Node _cnode = MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", id), _query_per));
                                _lst.Add(MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", _cnode.parent_id), _query_per)));

                                _clst.Add(_cnode);
                                _clst = RecursiveNodesHierarchy(_clst, param, _filterlst);
                                _lst[_lst.Count - 1].node_list = _clst;
                            }
                        }
                    }

                return _lst.Count > 0 ? _lst : _filterlst;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveNodesHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public List<Node> RecursiveNodesHierarchy(List<Node> _lst, DataFilter param, List<Node> _filterlst)
        {
            // For Getting Permission ID START
            IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
            // For Getting Permission ID END

            long[] node_ids = _filterlst.Select(o => o.node_id).Distinct().ToArray();
            foreach (Node _nodelst in _lst)
            {
                List<Node> _nodelstchk = new List<Node>();

                if (param == null)
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id), _query_per));
                else
                {
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id), Query.In("node_id", new BsonArray(node_ids)), _query_per));
                }

                if (_nodelstchk != null)
                    if (_nodelstchk.Count > 0)
                        _nodelst.node_list = RecursiveNodesHierarchy(_nodelstchk, param, _filterlst);
            }
            return _lst;
        }


        public List<Node> GetActiveSupplyNodesHierarchy(DataFilter param = null)
        {
            try
            {
                // For Getting Permission ID START
                IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                // For Getting Permission ID END

                List<Node> _lst = new List<Node>();
                List<Node> _filterlst = new List<Node>();
                string node = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

                if (param != null)
                {
                    if (param.node_code != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_code", param.node_code), _query_per));
                    else if (param.node_name != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_name", param.node_name), _query_per));
                }

                if (param == null)
                {
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0), _query_per));
                }
                else
                {
                    long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0), Query.In("node_id", new BsonArray(parent_ids)), _query_per));
                }

                if (_lst != null)
                    if (_lst.Count > 0)
                    {
                        _lst = RecursiveSupplyNodesHierarchy(_lst, param, _filterlst);
                    }
                    else
                    {
                        long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                        foreach (var id in parent_ids)
                        {
                            if (id > 0)
                            {
                                List<Node> _clst = new List<Node>();
                                Node _cnode = MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", id), _query_per));
                                _lst.Add(MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", _cnode.parent_id), _query_per)));

                                _clst.Add(_cnode);
                                _clst = RecursiveSupplyNodesHierarchy(_clst, param, _filterlst);
                                _lst[_lst.Count - 1].node_list = _clst;
                            }
                        }
                    }

                return _lst.Count > 0 ? _lst : _filterlst;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveSupplyNodesHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public List<Node> RecursiveSupplyNodesHierarchy(List<Node> _lst, DataFilter param, List<Node> _filterlst)
        {
            // For Getting Permission ID START
            IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
            // For Getting Permission ID END

            long[] node_ids = _filterlst.Select(o => o.node_id).Distinct().ToArray();
            foreach (Node _nodelst in _lst)
            {
                List<Node> _nodelstchk = new List<Node>();

                if (param == null)
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id), _query_per));
                else
                {
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id), Query.In("node_id", new BsonArray(node_ids)), _query_per));
                }

                if (_nodelstchk != null)
                    if (_nodelstchk.Count > 0)
                        _nodelst.node_list = RecursiveSupplyNodesHierarchy(_nodelstchk, param, _filterlst);
            }
            return _lst;
        }

        public List<Node> GetActiveNodesPermissionHierarchy(DataFilter param = null)
        {
            try
            {
                List<Node> _lst = new List<Node>();
                List<Node> _filterlst = new List<Node>();
                string node = Common.Instance.GetEnumValue(Common.CollectionName.NODE);

                if (param != null)
                {
                    if (param.node_code != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_code", param.node_code)));
                    else if (param.node_name != null)
                        _filterlst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), MongoHelper<Node>.MatchesStatement("node_name", param.node_name)));
                }

                if (param == null)
                {
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0)));
                }
                else
                {
                    long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                    _lst = MongoHelper<Node>.Find(node, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0), Query.In("node_id", new BsonArray(parent_ids))));
                }

                if (_lst != null)
                    if (_lst.Count > 0)
                    {
                        _lst = RecursiveNodesPermissionHierarchy(_lst, param, _filterlst);
                    }
                    else
                    {
                        long[] parent_ids = _filterlst.Select(o => o.parent_id).Distinct().ToArray();
                        foreach (var id in parent_ids)
                        {
                            if (id > 0)
                            {
                                List<Node> _clst = new List<Node>();
                                Node _cnode = MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", id)));
                                _lst.Add(MongoHelper<Node>.Single(node, Query.And(Query.EQ("status", 1), Query.EQ("node_id", _cnode.parent_id))));

                                _clst.Add(_cnode);
                                _clst = RecursiveNodesPermissionHierarchy(_clst, param, _filterlst);
                                _lst[_lst.Count - 1].node_list = _clst;
                            }
                        }
                    }

                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveNodesHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public List<Node> RecursiveNodesPermissionHierarchy(List<Node> _lst, DataFilter param, List<Node> _filterlst)
        {
            long[] node_ids = _filterlst.Select(o => o.node_id).Distinct().ToArray();
            foreach (Node _nodelst in _lst)
            {
                List<Node> _nodelstchk = new List<Node>();

                if (param == null)
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id)));
                else
                {
                    _nodelstchk = MongoHelper<Node>.Find(Common.Instance.GetEnumValue(Common.CollectionName.NODE), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _nodelst.node_id), Query.In("node_id", new BsonArray(node_ids))));
                }

                if (_nodelstchk != null)
                    if (_nodelstchk.Count > 0)
                        _nodelst.node_list = RecursiveNodesPermissionHierarchy(_nodelstchk, param, _filterlst);
            }
            return _lst;
        }

        #endregion

        #region Category
        public List<Category> GetCategories()
        {
            try
            {
                List<Category> _lst = new List<Category>();
                string category = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);
                _lst = MongoHelper<Category>.Find(category, Query.EQ("status", 1));
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetCaegories::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<Category> GetCategoryList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Category> _lst = new List<Category>();
                string category_collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Category filterdata = (Category)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Category));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count + 1];

                // For Getting Permission ID START
                IMongoQuery _query_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                // For Getting Permission ID END

                qc[i++] = _query_per;

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "category_code":
                            if (filterdata.category_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("category_code", filterdata.category_code);
                            break;
                        case "category_name":
                            if (filterdata.category_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("category_name", filterdata.category_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Category> dataTablePager1 = new DataTablePager<Category>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Category>.Paginate(category_collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Category cat in _lst)
                {
                    Category _cat = MongoHelper<Category>.Single(category_collection, Query.And(Query.EQ("status", 1), Query.EQ("category_id", cat.parent_id)));
                    cat.parent_category = _cat != null ? _cat.category_name : "NA";
                }

                DataTablePager<Category> dataTablePager = new DataTablePager<Category>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Category> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetCategoryList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<Category> GetActiveCategories()
        {
            List<Category> _lst = new List<Category>();
            string category_collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);

            _lst = MongoHelper<Category>.Find(category_collection, Query.EQ("status", 1), SortBy.Ascending("category_code"));

            foreach (Category cat in _lst)
            {
                Category _cat = MongoHelper<Category>.Single(category_collection, Query.And(Query.EQ("status", 1), Query.EQ("category_id", cat.parent_id)));
                cat.parent_category = _cat != null ? _cat.category_name : "NA";
            }
            return _lst;
        }

        public List<Category> GetActiveCategoryHierarchy(DataFilter param = null)
        {
            try
            {
                string type = "";
                string Collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY_HIERARCHY);
                List<CategoryHierarchy> _lst = MongoHelper<CategoryHierarchy>.All(Collection);

                if (_lst == null)
                    return null;
                else
                {
                    if (_lst.Count <= 0)
                        return null;
                    else
                    {
                        if (_lst[0].categoryList == null)
                            return null;
                    }
                }

                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");

                if (param != null)
                {
                    if (param.category_code != null)
                        type = "category_code";
                    else if (param.category_name != null)
                        type = "category_name";
                    else if (param.sku_code != null)
                        type = "sku_code";
                    else if (param.sku_name != null)
                        type = "sku_name";
                }
                foreach (var item in _lst[0].categoryList)
                {
                    bool found = false;
                    switch (type)
                    {
                        case "category_code":
                            found = FindCategory(item, type, param.category_code, _matchArrayCategoryPermission);
                            break;
                        case "category_name":
                            found = FindCategory(item, type, param.category_name, _matchArrayCategoryPermission);
                            break;
                        case "sku_code":
                            found = FindCategory(item, type, param.sku_code, _matchArrayCategoryPermission);
                            break;
                        case "sku_name":
                            found = FindCategory(item, type, param.sku_name, _matchArrayCategoryPermission);
                            break;
                        default:
                            found = FindCategory(item, "", "", _matchArrayCategoryPermission);
                            break;
                    }

                    item.status = found ? 1 : 0;
                }


                return _lst[0].categoryList;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveCategoryHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        private bool FindCategory(Category cat, string type, string name, long[] _matchArrayCategoryPermission)
        {
            bool found = false;
            bool isvalid = false;


            int pos = Array.IndexOf(_matchArrayCategoryPermission, cat.category_id);
            cat.is_valid = 0;
            if (pos > -1)
            {
                cat.is_valid = 1;

                if (type.Trim().Length > 0 && name.Trim().Length > 0)
                {
                    if (type == "category_code")
                        found = cat.category_code.ToLower().StartsWith(name.ToLower());
                    else if (type == "category_name")
                        found = cat.category_name.ToLower().StartsWith(name.ToLower());
                }

                if (cat.category_list != null)
                    foreach (var child in cat.category_list)
                    {
                        pos = Array.IndexOf(_matchArrayCategoryPermission, child.category_id);
                        child.is_valid = 0;
                        if (pos > -1)
                        {
                            child.is_valid = 1;

                            bool result = FindCategory(child, type, name, _matchArrayCategoryPermission);
                            child.status = result ? 1 : 0;

                            if (result)
                                found = true;
                        }
                    }

                //////if (cat.sku_list != null)
                //////    foreach (var child in cat.sku_list)
                //////    {
                //////        bool result = false;
                //////        if (type == "sku_code")
                //////            result = child.sku_code.ToLower().StartsWith(name.ToLower());
                //////        else if (type == "sku_name")
                //////            result = child.sku_name.ToLower().StartsWith(name.ToLower());

                //////        child.status = result ? 1 : 0;

                //////        if (result)
                //////            found = true;
                //////    }
            }
            return found;
        }

        public List<Category> GetActiveCategoryPermissionHierarchy(DataFilter param = null)
        {
            try
            {
                string type = "";
                string Collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY_HIERARCHY);
                List<CategoryHierarchy> _lst = _lst = MongoHelper<CategoryHierarchy>.All(Collection);

                if (_lst == null)
                    return null;
                else
                {
                    if (_lst.Count <= 0)
                        return null;
                    else
                    {
                        if (_lst[0].categoryList == null)
                            return null;
                    }
                }

                ////if (param != null)
                ////{
                ////    if (param.category_code != null)
                ////        type = "category_code";
                ////    else if (param.category_name != null)
                ////        type = "category_name";
                ////    else if (param.sku_code != null)
                ////        type = "sku_code";
                ////    else if (param.sku_name != null)
                ////        type = "sku_name";

                ////    foreach (var item in _lst[0].categoryList)
                ////    {
                ////        bool found = false;
                ////        switch (type)
                ////        {
                ////            case "category_code":
                ////                found = FindCategoryPermission(item, type, param.category_code);
                ////                break;
                ////            case "category_name":
                ////                found = FindCategoryPermission(item, type, param.category_name);
                ////                break;
                ////            case "sku_code":
                ////                found = FindCategoryPermission(item, type, param.sku_code);
                ////                break;
                ////            case "sku_name":
                ////                found = FindCategoryPermission(item, type, param.sku_name);
                ////                break;
                ////        }

                ////        item.status = found ? 1 : 0;
                ////    }
                ////}

                return _lst[0].categoryList;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveCategoryHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        private bool FindCategoryPermission(Category cat, string type, string name)
        {
            bool found = false;

            if (type == "category_code")
                found = cat.category_code.ToLower().StartsWith(name.ToLower());
            else if (type == "category_name")
                found = cat.category_name.ToLower().StartsWith(name.ToLower());

            if (cat.category_list != null)
                foreach (var child in cat.category_list)
                {
                    bool result = FindCategoryPermission(child, type, name);
                    child.status = result ? 1 : 0;

                    if (result)
                        found = true;
                }

            //////if (cat.sku_list != null)
            //////    foreach (var child in cat.sku_list)
            //////    {
            //////        bool result = false;
            //////        if (type == "sku_code")
            //////            result = child.sku_code.ToLower().StartsWith(name.ToLower());
            //////        else if (type == "sku_name")
            //////            result = child.sku_name.ToLower().StartsWith(name.ToLower());

            //////        child.status = result ? 1 : 0;

            //////        if (result)
            //////            found = true;
            //////    }

            return found;
        }

        public List<Category> RecursiveCategoryHierarchy(List<Category> _lst)
        {
            //string[] repskuIds = MongoHelper<BsonDocument>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT), "sku_id");
            //repskuIds = repskuIds.Where(x => x != "").Distinct().ToArray();

            foreach (Category _category in _lst)
            {
                List<Category> _catlist = MongoHelper<Category>.Find(Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY), Query.And(Query.EQ("status", 1), Query.EQ("parent_id", _category.category_id)));
                if (_catlist != null)
                    if (_catlist.Count > 0)
                        _category.category_list = RecursiveCategoryHierarchy(_catlist);

                //string[] skuIds = MongoHelper<BsonDocument>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY), "sku_id", Query.EQ("category_id", _category.category_id));
                //skuIds = skuIds.Where(x => x != "").Distinct().ToArray();
                //
                //if (skuIds.Length > 0 && repskuIds.Length > 0)
                //    _category.sku_list = MongoHelper<SKU>.Find(Common.Instance.GetEnumValue(Common.CollectionName.SKU), Query.And(Query.In("sku_id", new BsonArray(skuIds)), Query.In("sku_id", new BsonArray(repskuIds)), Query.EQ("status", 1))); ;
            }
            return _lst;
        }
        #endregion

        #region SKU
        public List<SKU> GetSKUs()
        {
            try
            {
                List<SKU> _lst = new List<SKU>();
                string sku = Common.Instance.GetEnumValue(Common.CollectionName.SKU);
                string[] includeColumns = { "sku_id", "sku_name", "sku_code", "sku_description" };
                _lst = MongoHelper<SKU>.Find(sku, Query.EQ("status", 1), includeColumns);
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUs::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<SKU> GetSKUList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<SKU> _lst = new List<SKU>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";
                bool isCatSelectionPresent = false;

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                SKU filterdata = (SKU)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(SKU));

                if (filterdata.categorySelection == null)
                {
                    filterdata.categorySelection = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                    isCatSelectionPresent = true;
                }

                PropertyInfo[] props = filterdata.GetType().GetProperties();
                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                if (isCatSelectionPresent)
                {
                    dict.Add("categorySelection", Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id"));
                }

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count + 1];
                IMongoQuery _queryPermi = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                qc[i++] = _queryPermi;
                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "sku_code":
                            if (filterdata.sku_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_code", filterdata.sku_code);
                            break;
                        case "sku_name":
                            if (filterdata.sku_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_name", filterdata.sku_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                IMongoQuery _query = Query.In("category_id", new BsonArray(filterdata.categorySelection));
                long[] skuIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY), "sku_id", _query).Distinct().ToArray();
                if (skuIDs != null)
                {
                    if (skuIDs.Length > 0)
                        qc[i++] = Query.In("sku_id", new BsonArray(skuIDs));
                    else
                        qc[i++] = Query.EQ("sku_id", 0);
                }
                else
                    qc[i++] = Query.EQ("sku_id", 0);


                DataTablePager<SKU> dataTablePager1 = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<SKU>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (var item in _lst)
                {
                    item.attributes = MongoHelper<SKUAttributeMapping>.Find(Common.Instance.GetEnumValue(Common.CollectionName.SKU_ATTRIBUTE_MAPPING), Query.EQ("sku_id", item.sku_id));
                }

                DataTablePager<SKU> dataTablePager = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKU> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<SKU> GetSKUPermissionList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<SKU> _lst = new List<SKU>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                SKU filterdata = (SKU)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(SKU));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "sku_code":
                            if (filterdata.sku_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_code", filterdata.sku_code);
                            break;
                        case "sku_name":
                            if (filterdata.sku_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku_name", filterdata.sku_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                if (filterdata.categorySelection != null)
                {
                    IMongoQuery _query = Query.In("category_id", new BsonArray(filterdata.categorySelection));
                    long[] skuIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY), "sku_id", _query).Distinct().ToArray();
                    if (skuIDs != null)
                    {
                        if (skuIDs.Length > 0)
                            qc[i++] = Query.In("sku_id", new BsonArray(skuIDs));
                        else
                            qc[i++] = Query.EQ("sku_id", 0);
                    }
                    else
                        qc[i++] = Query.EQ("sku_id", 0);
                }
                else
                    qc[i++] = Query.EQ("sku_id", 0);

                DataTablePager<SKU> dataTablePager1 = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<SKU>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (var item in _lst)
                {
                    item.attributes = MongoHelper<SKUAttributeMapping>.Find(Common.Instance.GetEnumValue(Common.CollectionName.SKU_ATTRIBUTE_MAPPING), Query.EQ("sku_id", item.sku_id));
                }

                DataTablePager<SKU> dataTablePager = new DataTablePager<SKU>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKU> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<SKU> GetActiveSKUs()
        {
            List<SKU> _lst = new List<SKU>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU);

            _lst = MongoHelper<SKU>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("sku_code"));

            return _lst;
        }

        public List<Attributes> GetAttributes()
        {
            try
            {
                List<Attributes> _lst = new List<Attributes>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.ATTRIBUTE);
                _lst = MongoHelper<Attributes>.All(collection);
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetAttributes::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion

        #region Region
        public FormattedList<Region> GetRegionList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Region> _lst = new List<Region>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.REGION);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Region filterdata = (Region)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Region));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "region_code":
                            if (filterdata.region_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("region_code", filterdata.region_code);
                            break;
                        case "region_name":
                            if (filterdata.region_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("region_name", filterdata.region_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Region> dataTablePager1 = new DataTablePager<Region>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.EQ("status", 1);
                _lst = MongoHelper<Region>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Region item in _lst)
                {
                    Country _obj = MongoHelper<Country>.Single(Common.Instance.GetEnumValue(Common.CollectionName.COUNTRY), Query.And(Query.EQ("status", 1), Query.EQ("country_id", item.country_id)));
                    item.country = _obj;
                }

                DataTablePager<Region> dataTablePager = new DataTablePager<Region>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Region> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetRegionList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<Region> GetActiveRegions()
        {
            List<Region> _lst = new List<Region>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.REGION);

            _lst = MongoHelper<Region>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("region_code"));

            foreach (Region item in _lst)
            {
                Country _obj = MongoHelper<Country>.Single(Common.Instance.GetEnumValue(Common.CollectionName.COUNTRY), Query.And(Query.EQ("status", 1), Query.EQ("country_id", item.country_id)));
                item.country = _obj;
            }

            return _lst;
        }
        #endregion

        #region Buffer Norm
        public FormattedList<BufferNorm> GetBufferNormList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<BufferNorm> _lst = new List<BufferNorm>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                BufferNorm filterdata = (BufferNorm)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(BufferNorm));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                int distCnt = dict.Count + 2;
                IMongoQuery[] qc = new IMongoQuery[distCnt];

                // For Getting Permission ID START
                IMongoQuery _queryNode_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                IMongoQuery _querySku_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                // For Getting Permission ID END

                qc[i++] = _queryNode_per;
                qc[i++] = _querySku_per;

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "sku":
                            if (filterdata.sku != null)
                            {
                                if (filterdata.sku.sku_name != null)
                                    qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_name", filterdata.sku.sku_name);
                                else
                                    qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_code", filterdata.sku.sku_code);
                            }
                            break;
                        case "node":
                            if (filterdata.node != null)
                            {
                                if (filterdata.node.node_name != null)
                                    qc[i++] = MongoHelper<Node>.MatchesStatement("node.node_name", filterdata.node.node_name);
                                else
                                    qc[i++] = MongoHelper<Node>.MatchesStatement("node.node_code", filterdata.node.node_code);
                            }
                            break;
                    }
                }

                DataTablePager<BufferNorm> dataTablePager1 = new DataTablePager<BufferNorm>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = distCnt > 0 ? distCnt == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<BufferNorm>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                DataTablePager<BufferNorm> dataTablePager = new DataTablePager<BufferNorm>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<BufferNorm> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetBuffferNormList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<BufferNorm> GetActiveBufferNorms()
        {
            List<BufferNorm> _lst = new List<BufferNorm>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM);

            _lst = MongoHelper<BufferNorm>.Find(collection, Query.NE("_id", ""), SortBy.Ascending("sku_name"));

            return _lst;
        }
        #endregion

        #region Vendor
        public FormattedList<Vendor> GetVendorList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Vendor> _lst = new List<Vendor>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.VENDOR);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Vendor filterdata = (Vendor)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Vendor));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "vendor_code":
                            if (filterdata.vendor_code != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("vendor_code", filterdata.vendor_code);
                            break;
                        case "vendor_name":
                            if (filterdata.vendor_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("vendor_name", filterdata.vendor_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Vendor> dataTablePager1 = new DataTablePager<Vendor>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<Vendor>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Vendor item in _lst)
                {
                    Region _obj = MongoHelper<Region>.Single(Common.Instance.GetEnumValue(Common.CollectionName.REGION), Query.And(Query.EQ("status", 1), Query.EQ("region_id", item.region_id)));
                    item.region = _obj;
                }

                DataTablePager<Vendor> dataTablePager = new DataTablePager<Vendor>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Vendor> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetVendorList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<Vendor> GetActiveVendors()
        {
            List<Vendor> _lst = new List<Vendor>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.VENDOR);

            _lst = MongoHelper<Vendor>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("vendor_code"));

            foreach (Vendor item in _lst)
            {
                Region _obj = MongoHelper<Region>.Single(Common.Instance.GetEnumValue(Common.CollectionName.REGION), Query.And(Query.EQ("status", 1), Query.EQ("region_id", item.region_id)));
                item.region = _obj;
            }
            return _lst;
        }
        #endregion

        #region User
        public FormattedList<User> GetUserList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<User> _lst = new List<User>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.USER);
                string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);
                string user_data_permission_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                User filterdata = (User)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(User));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "first_name":
                            if (filterdata.first_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("first_name", filterdata.first_name);
                            break;
                        case "last_name":
                            if (filterdata.last_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("last_name", filterdata.last_name);
                            break;
                        case "user_name":
                            if (filterdata.user_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("user_name", filterdata.user_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<User> dataTablePager1 = new DataTablePager<User>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<User>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                DataTablePager<User> dataTablePager = new DataTablePager<User>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<User> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetUserList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<User> GetActiveUsers()
        {
            List<User> _lst = new List<User>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.USER);
            string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);

            _lst = MongoHelper<User>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("first_name"));

            return _lst;
        }

        public void SaveUser(User _user)
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.USER);
            string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);
            string user_data_permission_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS);

            if (_user.user_id > 0)
            {
                List<User> chkDuplicate = Dao.User().SelectUserNameDuplicate(_user);
                if (chkDuplicate.Count > 0)
                    throw new WebFaultException<string>("DuplicateUser", HttpStatusCode.Found);
                else
                {
                    chkDuplicate = Dao.User().SelectUserMobileDuplicate(_user);
                    if (chkDuplicate.Count > 0)
                        throw new WebFaultException<string>("DuplicateMobile", HttpStatusCode.Found);
                }

            }
            else
            {
                List<User> chkDuplicate = Dao.User().SelectUserNameDuplicate(_user);
                if (chkDuplicate.Count > 0)
                    throw new WebFaultException<string>("DuplicateUser", HttpStatusCode.Found);
                else
                {
                    chkDuplicate = Dao.User().SelectUserMobileDuplicate(_user);
                    if (chkDuplicate.Count > 0)
                        throw new WebFaultException<string>("DuplicateMobile", HttpStatusCode.Found);
                }
            }

            try
            {
                if (_user.user_id <= 0)
                {
                    _user.status = 1;
                    _user.approved = 1;
                    _user.created_by = Common.Instance.CurrentUser;
                    _user.created_on = DateTime.Now.ToString();
                    _user.user_id = Dao.User().Insert(_user);
                }
                else
                {
                    _user.modified_by = Common.Instance.CurrentUser;
                    _user.modified_on = DateTime.Now.ToString();
                    Dao.User().Update(_user);
                }

                MongoHelper<User>.RemoveAll(collection);
                List<User> lst = Dao.User().SelectAll();
                if (lst != null)
                    if (lst.Count > 0)
                        MongoHelper<User>.Add(collection, lst);

                ////MongoHelper<User>.Save(collection, _user);
            }
            catch (Exception e)
            {
                _log.Error("SaveUser::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public string GetUserPermissions()
        {
            Dictionary<string, string> obj = new Dictionary<string, string>();
            try
            {
                List<UserModule> userModules = MongoHelper<UserModule>.Find(Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE), Query.EQ("role_id", Common.Instance.CurrentRole));
                List<Dictionary<string, object>> permission = new List<Dictionary<string, object>>();

                if (userModules != null)
                    foreach (var item in userModules)
                    {
                        Module module = MongoHelper<Module>.Single(Common.Instance.GetEnumValue(Common.CollectionName.MODULE), Query.EQ("module_id", item.module_id));

                        Dictionary<string, object> perm = new Dictionary<string, object>();
                        perm.Add("module_id", item.module_id);
                        perm.Add("module_name", module.module_name);
                        perm.Add("read", item.read == 1 ? item.read : item.write);
                        perm.Add("write", item.write);
                        permission.Add(perm);
                    }

                obj.Add("permission", (new JavaScriptSerializer()).Serialize(permission));
            }
            catch (Exception e)
            {
                _log.Error("GetUserPermissions::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }

            return (new JavaScriptSerializer()).Serialize(obj);
        }

        #endregion

        #region Role

        public List<Role> GetActiveRoles()
        {
            List<Role> _lst = new List<Role>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.ROLE);

            _lst = MongoHelper<Role>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("role_name"));

            return _lst;
        }

        public FormattedList<Role> GetRoleList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<Role> _lst = new List<Role>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.ROLE);
                string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);
                string user_data_permission_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Role filterdata = (Role)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Role));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                int i = 0;
                IMongoQuery[] qc = new IMongoQuery[dict.Count];

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "role_name":
                            if (filterdata.role_name != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("role_name", filterdata.role_name);
                            break;
                        case "status":
                            qc[i++] = Query.EQ("status", filterdata.status);
                            break;
                    }
                }

                DataTablePager<Role> dataTablePager1 = new DataTablePager<Role>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<Role>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                foreach (Role _role in _lst)
                {
                    _role.user_module_list = MongoHelper<UserModule>.Find(user_module_collection, Query.EQ("role_id", _role.role_id));

                    _role.user_node_list = MongoHelper<UserDataPermissions>.Find(user_data_permission_collection, Query.And(Query.EQ("role_id", _role.role_id), Query.EQ("object_type", "NODE")));
                    _role.user_supply_node_list = MongoHelper<UserDataPermissions>.Find(user_data_permission_collection, Query.And(Query.EQ("role_id", _role.role_id), Query.EQ("object_type", "SUPPLYNODE")));
                    _role.user_category_list = MongoHelper<UserDataPermissions>.Find(user_data_permission_collection, Query.And(Query.EQ("role_id", _role.role_id), Query.EQ("object_type", "CATEGORY")));
                    _role.user_sku_list = MongoHelper<UserDataPermissions>.Find(user_data_permission_collection, Query.And(Query.EQ("role_id", _role.role_id), Query.EQ("object_type", "SKU")));
                }

                DataTablePager<Role> dataTablePager = new DataTablePager<Role>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Role> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetRoleList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public void SaveRole(Role _role)
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.ROLE);
            string user_module_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE);
            string user_data_permission_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS);

            if (_role.role_id > 0)
            {
                List<Role> chkDuplicate = Dao.Role().SelectRoleNameDuplicate(_role);
                if (chkDuplicate.Count > 0)
                    throw new WebFaultException<string>("DuplicateRole", HttpStatusCode.Found);
            }
            else
            {
                List<Role> chkDuplicate = Dao.Role().SelectRoleNameDuplicate(_role);
                if (chkDuplicate.Count > 0)
                    throw new WebFaultException<string>("DuplicateRole", HttpStatusCode.Found);
            }

            try
            {

                List<UserModule> _modelList = new List<UserModule>();
                List<UserDataPermissions> _nodelList = new List<UserDataPermissions>();
                List<UserDataPermissions> _supplynodelList = new List<UserDataPermissions>();
                List<UserDataPermissions> _categorylList = new List<UserDataPermissions>();
                List<UserDataPermissions> _skulList = new List<UserDataPermissions>();

                if (_role.user_module_list != null) _modelList = _role.user_module_list;
                if (_role.user_node_list != null) _nodelList = _role.user_node_list;
                if (_role.user_supply_node_list != null) _supplynodelList = _role.user_supply_node_list;
                if (_role.user_category_list != null) _categorylList = _role.user_category_list;
                if (_role.user_sku_list != null) _skulList = _role.user_sku_list;

                if (_role.role_id <= 0)
                {
                    _role.status = 1;
                    _role.created_by = Common.Instance.CurrentUser;
                    _role.created_on = DateTime.Now.ToString();

                    _role.user_module_list = null;
                    _role.user_node_list = null;
                    _role.user_supply_node_list = null;
                    _role.user_category_list = null;
                    _role.user_sku_list = null;

                    _role.role_id = Dao.Role().Insert(_role);
                }
                else
                {
                    _role.user_module_list = null;
                    _role.user_node_list = null;
                    _role.user_supply_node_list = null;
                    _role.user_category_list = null;
                    _role.user_sku_list = null;

                    _role.modified_by = Common.Instance.CurrentUser;
                    _role.modified_on = DateTime.Now.ToString();
                    Dao.Role().Update(_role);
                }

                if ((MongoHelper<Role>.Save(collection, _role)).Ok)
                {
                    if (MongoHelper<UserModule>.Delete(user_module_collection, Query.EQ("role_id", _role.role_id)).Ok)
                    {
                        Dao.UserModule().DeleteModuleByRole(_role.role_id);
                        if (_modelList != null)
                        {
                            if (_modelList.Count > 0)
                            {
                                _modelList.Select(x =>
                                 {
                                     x._id = ObjectId.Empty;
                                     x.role_id = _role.role_id;
                                     x.module_id = x.module_id;
                                     x.created_by = Common.Instance.CurrentUser;
                                     x.created_on = DateTime.Now.ToString();

                                     return x;
                                 }).ToList();

                                Dao.UserModule().InsertBatch(_modelList);

                                List<UserModule> _modelRoleList = Dao.UserModule().SelectByRole(_role.role_id);

                                if (_modelRoleList != null)
                                    if (_modelRoleList.Count > 0)
                                    {
                                        List<UserModule> _modelRoleList1 = new List<UserModule>();
                                        foreach (UserModule _obj in _modelList)
                                        {
                                            UserModule _userMod = _modelRoleList.Where(x => x.module_id == _obj.module_id).SingleOrDefault();
                                            if (_userMod != null)
                                            {
                                                _userMod.read = _obj.read;
                                                _userMod.write = _obj.write;

                                                _modelRoleList1.Add(_userMod);
                                            }

                                            //_modelRoleList1 = _modelRoleList.Where(x => x.module_id == _obj.module_id).Select(x =>
                                            //   {
                                            //       x.read = _obj.read;
                                            //       x.write = _obj.write;

                                            //       return x;
                                            //   }).ToList();
                                        }

                                        if (_modelRoleList1 != null)
                                            if (_modelRoleList1.Count > 0)
                                                MongoHelper<UserModule>.Add(user_module_collection, _modelRoleList1);
                                    }
                            }


                            //////foreach (UserModule _obj in _modelList)
                            //////{
                            //////    _obj._id = ObjectId.Empty;
                            //////    _obj.role_id = _role.role_id;
                            //////    _obj.module_id = _obj.module_id;
                            //////    _obj.created_by = Common.Instance.CurrentUser;
                            //////    _obj.created_on = DateTime.Now.ToString();

                            //////    _obj.user_module_id = Dao.UserModule().Insert(_obj);
                            //////    MongoHelper<UserModule>.Add(user_module_collection, _obj);
                            //////}
                        }
                    }

                    if (MongoHelper<UserDataPermissions>.Delete(user_data_permission_collection, Query.EQ("role_id", _role.role_id)).Ok)
                    {
                        Dao.UserDataPermission().DeleteDataPermissionByRole(_role.role_id);

                        if (_nodelList != null)
                            InsertDataPermissions(_nodelList, _role, "NODE");

                        if (_supplynodelList != null)
                            InsertDataPermissions(_supplynodelList, _role, "SUPPLYNODE");

                        if (_categorylList != null)
                        {
                            InsertDataPermissions(_categorylList, _role, "CATEGORY");

                            if (_categorylList.Count > 0)
                                if (_skulList != null)
                                    InsertDataPermissions(_skulList, _role, "SKU");
                        }
                    }

                }
            }
            catch (Exception e)
            {
                _log.Error("SaveRole::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        private void InsertDataPermissions(List<UserDataPermissions> list, Role _role, string type)
        {
            string user_data_permission_collection = Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS);
            int bulkInsLimit = 100;

            UserDataPermissions _dataPermission = new UserDataPermissions();
            _dataPermission.role_id = _role.role_id;
            _dataPermission.object_type = type;

            if (list != null)
                if (list.Count > 0)
                {
                    if (type.Trim().ToUpper() == "SKU")
                    {
                        long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");

                        if (_matchArrayCategoryPermission != null)
                            if (_matchArrayCategoryPermission.Length > 0)
                            {
                                IMongoQuery _query = Query.In("category_id", new BsonArray(_matchArrayCategoryPermission));
                                long[] skuIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY), "sku_id", _query).Distinct().ToArray();

                                foreach (UserDataPermissions _obj in list)
                                {
                                    int pos = Array.IndexOf(skuIDs, _obj.object_id);
                                    if (pos > -1)
                                    {
                                        _obj._id = ObjectId.Empty;
                                        _obj.role_id = _role.role_id;
                                        _obj.object_type = _obj.object_type;
                                        _obj.object_id = _obj.object_id;
                                        _obj.chk_state = _obj.chk_state;
                                        _obj.created_by = Common.Instance.CurrentUser;
                                        _obj.created_on = DateTime.Now.ToString();

                                        _obj.user_data_id = Dao.UserDataPermission().Insert(_obj);
                                        MongoHelper<UserDataPermissions>.Add(user_data_permission_collection, _obj);
                                    }
                                }
                            }
                    }
                    else
                    {
                        List<UserDataPermissions> list1 = list.Select(x =>
                           {
                               x._id = ObjectId.Empty;
                               x.role_id = _role.role_id;
                               x.created_by = Common.Instance.CurrentUser;
                               x.created_on = DateTime.Now.ToString();

                               return x;
                           }).ToList();


                        if (list1 != null)
                        {
                            int cnt1 = list1.Count();

                            if (cnt1 > 0)
                            {
                                int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));

                                for (int offset = 0; offset < loopCount1; offset++)
                                {
                                    int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                                    List<UserDataPermissions> _lstInsert = new List<UserDataPermissions>();
                                    _lstInsert.AddRange(list1.GetRange(offset * bulkInsLimit, fetchCnt));

                                    Dao.UserDataPermission().InsertBatch(_lstInsert);
                                }

                                List<UserDataPermissions> _lstDataPermi = Dao.UserDataPermission().SelectDataPermissionByRole(_dataPermission);

                                if (_lstDataPermi != null)
                                    if (_lstDataPermi.Count > 0)
                                        MongoHelper<UserDataPermissions>.Add(user_data_permission_collection, _lstDataPermi);

                            }
                        }

                        ////foreach (UserDataPermissions _obj in list)
                        ////{
                        ////    _obj._id = ObjectId.Empty;
                        ////    _obj.role_id = _role.role_id;
                        ////    _obj.object_type = _obj.object_type;
                        ////    _obj.object_id = _obj.object_id;
                        ////    _obj.chk_state = _obj.chk_state;
                        ////    _obj.created_by = Common.Instance.CurrentUser;
                        ////    _obj.created_on = DateTime.Now.ToString();

                        ////    _obj.user_data_id = Dao.UserDataPermission().Insert(_obj);
                        ////    MongoHelper<UserDataPermissions>.Add(user_data_permission_collection, _obj);
                        ////}
                    }
                }
        }

        #endregion

        #region Node SKU Mapping
        public FormattedList<NodeSKUMapping> GetNodeSKUList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<NodeSKUMapping> _lst = new List<NodeSKUMapping>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE_SKU_MAPPING);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                NodeSKUMapping filterdata = (NodeSKUMapping)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(NodeSKUMapping));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));


                // For Getting Permission ID START
                IMongoQuery _queryRecever_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "receiver_node_id");
                IMongoQuery _querySupplier_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "supplier_node_id");
                IMongoQuery _querySKU_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                // For Getting Permission ID END

                int i = 0;
                int distCnt = dict.Count + 3;
                IMongoQuery[] qc = new IMongoQuery[distCnt];

                qc[i++] = _queryRecever_per;
                qc[i++] = _querySupplier_per;
                qc[i++] = _querySKU_per;

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "supplier_node":
                            if (filterdata.supplier_node != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("supplier_node.node_code", filterdata.supplier_node.node_code);
                            break;
                        case "receiver_node":
                            if (filterdata.receiver_node != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("receiver_node.node_code", filterdata.receiver_node.node_code);
                            break;
                        case "sku":
                            if (filterdata.sku != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_code", filterdata.sku.sku_code);
                            break;
                    }
                }

                DataTablePager<NodeSKUMapping> dataTablePager1 = new DataTablePager<NodeSKUMapping>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = distCnt > 0 ? distCnt == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<NodeSKUMapping>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                DataTablePager<NodeSKUMapping> dataTablePager = new DataTablePager<NodeSKUMapping>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<NodeSKUMapping> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetNodeSKUList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public static List<NodeSKUMapping> GetActiveNodeSKUs()
        {
            List<NodeSKUMapping> _lst = new List<NodeSKUMapping>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE_SKU_MAPPING);

            //_lst = MongoHelper<NodeSKUMapping>.Find(collection, Query.NE("_id", ""), SortBy.Ascending("sku_id"));
            _lst = MongoHelper<NodeSKUMapping>.All(collection);

            return _lst;
        }
        #endregion

        #region Module
        public void SaveModule(Module _module)
        {
            try
            {
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.MODULE);

                if (_module.module_id > 0)
                {
                    //_module.module_id = Guid.NewGuid().ToString();
                    _module.status = 1;
                    _module.created_by = Common.Instance.CurrentUser;
                    _module.created_on = DateTime.Now.ToString();
                }
                else
                {
                    _module.modified_by = Common.Instance.CurrentUser;
                    _module.modified_on = DateTime.Now.ToString();
                }

                MongoHelper<Module>.Save(collection, _module);
            }
            catch (Exception e)
            {
                _log.Error("SaveModule::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public List<Module> GetActiveModules()
        {
            List<Module> _lst = new List<Module>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.MODULE);
            _lst = MongoHelper<Module>.Find(collection, Query.EQ("status", 1), SortBy.Ascending("module_name"));
            return _lst;
        }
        #endregion

        #region Dynamic Buffer Management
        public void SaveDynamicBufferManagement(DynamicBufferManagement item)
        {
            try
            {
                Dao.DynamicBufferManagement().DeleteAll();

                item.created_by = Common.Instance.CurrentUser;
                item.dynamic_buffer_management_id = Dao.DynamicBufferManagement().Insert(item);

                (new Upload()).RefreshMongoDb("dbm");
            }
            catch (Exception e)
            {
                _log.Error("SaveDynamicBufferManagement::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public void TriggerPenetrationCalculation()
        {
            try
            {
                QuartzHelper.startManualDBMFlow();
            }
            catch (Exception e)
            {
                _log.Error("TriggerPenetrationCalculation::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public List<DynamicBufferManagement> GetDynamicBufferManagement()
        {
            List<DynamicBufferManagement> _lst = new List<DynamicBufferManagement>();
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.DYNAMIC_BUFFER_MANAGEMENT);
            _lst = MongoHelper<DynamicBufferManagement>.Find(collection, Query.Not(Query.EQ("_id", "")));
            return _lst;
        }
        #endregion


        public List<BOM> RecursiveBOMHierarchy(List<BOM> _lst)
        {
            foreach (BOM _Bom in _lst)
            {
                List<BOM> _bomlist = MongoHelper<BOM>.Find(Common.Instance.GetEnumValue(Common.CollectionName.BOM), Query.EQ("parent_id", _Bom.sku_id));
                if (_bomlist != null)
                    if (_bomlist.Count > 0)
                        _Bom.bom_list = RecursiveBOMHierarchy(_bomlist);
            }
            return _lst;
        }

        public List<BOM> GetActiveBOMHierarchy(DataFilter param = null)
        {
            try
            {
                string type = "";
                string Collection = Common.Instance.GetEnumValue(Common.CollectionName.BOM_HIERARCHY);
                List<BOMHierarchy> _lst = MongoHelper<BOMHierarchy>.All(Collection);

                if (_lst == null)
                    return null;
                else
                {
                    if (_lst.Count <= 0)
                        return null;
                    else
                    {
                        if (_lst[0].bomList == null)
                            return null;
                    }
                }

                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");

                IMongoQuery _query = Query.NotIn("sku_id", new BsonArray(_matchArraySKUPermission));
                long[] skuIDs = MongoHelper<SKU>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU), "sku_id", _query).Distinct().ToArray();

                if (param.categorySelection == null)
                    param.categorySelection = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "object_id");

                ////if (param.nodeSelection == null)
                ////    param.nodeSelection = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "object_id");

                IMongoQuery _query1 = Query.In("category_id", new BsonArray(param.categorySelection));
                long[] skuCatIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY), "sku_id", _query1).Distinct().ToArray();

                ////IMongoQuery _query2 = Query.In("receiver_node_id", new BsonArray(param.nodeSelection));
                ////long[] skuNodeIDs = MongoHelper<NodeSKUMapping>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.NODE_SKU_MAPPING), "sku_id", _query2).Distinct().ToArray();

                long[] _skuCatCommon = skuIDs.Intersect(skuCatIDs).ToArray();
                ////long[] _skuNodeCommon = skuIDs.Intersect(skuNodeIDs).ToArray();

                ////if (param.categorySelection != null && param.nodeSelection != null)
                ////    _matchArraySKUPermission = _skuCatCommon.Intersect(_skuNodeCommon).ToArray();
                ////else if (param.categorySelection != null && param.nodeSelection == null)
                ////    _matchArraySKUPermission = _skuCatCommon;
                ////else if (param.categorySelection == null && param.nodeSelection != null)
                ////    _matchArraySKUPermission = _skuNodeCommon;
                ////else
                ////    _matchArraySKUPermission = skuIDs;

                if (param.categorySelection != null)
                    _matchArraySKUPermission = _skuCatCommon;
                else
                    _matchArraySKUPermission = skuIDs;

                if (param != null)
                {
                    if (param.sku_code != null)
                        type = "sku_code";
                    else if (param.sku_name != null)
                        type = "sku_name";
                }
                foreach (var item in _lst[0].bomList)
                {
                    bool found = false;
                    switch (type)
                    {
                        case "sku_code":
                            found = FindBOM(item, type, param.sku_code, _matchArraySKUPermission);
                            break;
                        case "sku_name":
                            found = FindBOM(item, type, param.sku_name, _matchArraySKUPermission);
                            break;
                        default:
                            found = FindBOM(item, "", "", _matchArraySKUPermission);
                            break;
                    }

                    item.status = found ? 1 : 0;
                }


                return _lst[0].bomList;
            }
            catch (Exception e)
            {
                _log.Error("GetActiveBOMHierarchy::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        private bool FindBOM(BOM _bom, string type, string name, long[] _matchArraySKUPermission)
        {
            bool found = false;
            bool isvalid = false;


            int pos = Array.IndexOf(_matchArraySKUPermission, _bom.sku_id);
            _bom.is_valid = 0;
            if (pos > -1)
            {
                _bom.is_valid = 1;

                if (type.Trim().Length > 0 && name.Trim().Length > 0)
                {
                    if (type == "sku_code")
                        found = _bom.sku_code.ToLower().StartsWith(name.ToLower());
                    else if (type == "sku_name")
                        found = _bom.sku_name.ToLower().StartsWith(name.ToLower());
                }

                if (_bom.bom_list != null)
                    foreach (var child in _bom.bom_list)
                    {
                        pos = Array.IndexOf(_matchArraySKUPermission, child.sku_id);
                        child.is_valid = 0;
                        if (pos > -1)
                        {
                            child.is_valid = 1;

                            bool result = FindBOM(child, type, name, _matchArraySKUPermission);
                            child.status = result ? 1 : 0;

                            if (result)
                                found = true;
                        }
                    }

            }
            return found;
        }

        #region SKU Category Mapping
        public FormattedList<SKUCategory> GetSKUCategoryList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                List<SKUCategory> _lst = new List<SKUCategory>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY);

                /****************************************************************/

                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                SKUCategory filterdata = (SKUCategory)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(SKUCategory));

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));


                // For Getting Permission ID START
                IMongoQuery _queryCategory_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                IMongoQuery _querySKU_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                // For Getting Permission ID END

                int i = 0;
                int distCnt = dict.Count + 2;
                IMongoQuery[] qc = new IMongoQuery[distCnt];

                qc[i++] = _queryCategory_per;
                qc[i++] = _querySKU_per;

                foreach (PropertyInfo prop in props)
                {
                    switch (prop.Name)
                    {
                        case "category":
                            if (filterdata.category != null)
                                qc[i++] = MongoHelper<Category>.MatchesStatement("category.category_code", filterdata.category.category_code);
                            break;
                        case "sku":
                            if (filterdata.sku != null)
                                qc[i++] = MongoHelper<Node>.MatchesStatement("sku.sku_code", filterdata.sku.sku_code);
                            break;
                    }
                }

                DataTablePager<SKUCategory> dataTablePager1 = new DataTablePager<SKUCategory>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                i = 0;
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = distCnt > 0 ? distCnt == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
                _lst = MongoHelper<SKUCategory>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                /****************************************************************/

                DataTablePager<SKUCategory> dataTablePager = new DataTablePager<SKUCategory>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKUCategory> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUCategoryList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
        #endregion
    }
}
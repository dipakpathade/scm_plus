﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using log4net;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Web.Providers;
using NPOI.SS.UserModel;
using System.Collections;

namespace SCMPLUS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AuthenticatingHeader]

    public class Upload : IUpload
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));

        public string UploadFile(Stream file)
        {
            string filename = "";
            MultipartParser parser = new MultipartParser(file);

            if (parser != null && parser.Success)
            {
                string path = AppSettings.FileUploadPath;
                filename = parser.Filename;

                filename = filename.Substring(0, filename.LastIndexOf(".")) + "_"
                    + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_')
                    + filename.Substring(filename.LastIndexOf("."));

                File.WriteAllBytes(path + filename, parser.FileContents);
            }

            return filename;
        }

        public DocumentResult ImportTemplate(Stream file)
        {
            _log.Info("ImportTemplate start");
            _log.Info("file : " + file);
            DocumentResult obj = new DocumentResult();
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                string filetype = ctx.IncomingRequest.Headers["filetype"];
                if (filetype != null)
                {
                    _log.Info("File upload start");
                    string fileName = UploadFile(file);
                    _log.Info("fileName: " + fileName);
                    _log.Info("File upload success");

                    if (fileName.Trim().Length > 0)
                    {
                        obj.file_name = fileName;

                        _log.Info("filetype: " + filetype);

                        switch (filetype)
                        {
                            case "buffernorm":
                                new Worker().UploadBufferNorm(fileName);
                                break;
                            case "category":
                                new Worker().UploadCategory(fileName);
                                break;
                            case "node":
                                new Worker().UploadNode(fileName);
                                break;
                            case "sku_node":
                                new Worker().UploadNodeSkuMapping(fileName);
                                break;
                            case "sku":
                                new Worker().UploadSKU(fileName);
                                break;
                            case "sku_category":
                                new Worker().UploadSkuCategory(fileName);
                                break;
                            case "transaction":
                                new Worker().UploadTransaction(fileName);
                                break;
                            case "bom":
                                new Worker().UploadBOM(fileName);
                                break;
                            default:
                                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotAcceptable;
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error("ImportTemplate::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
            return obj;
        }

        public void RefreshMongoDb(string collection, string date = null, ImportExportLog item = null, string identifier = null)
        {
            try
            {
                switch (collection)
                {
                    case "category":
                        RefreshMongoCategory();
                        break;
                    case "sku":
                        RefreshMongoSKU();
                        break;
                    case "node":
                        RefreshMongoNode();
                        break;
                    case "buffer_norm":
                        RefreshMongoBufferNorm();
                        break;
                    case "node_sku_mapping":
                        RefreshMongoNodeSkuMapping();
                        break;
                    case "transaction":
                        RefreshMongoTransaction(date);
                        break;
                    case "replenishment":
                        RefreshMongoReplenishment(date, false, item, identifier);
                        break;
                    case "sku_category":
                        RefreshMongoSKUCategory();
                        break;
                    case "sku_chart":
                        RefreshMongoSKUChart(date);
                        break;
                    case "sku_grid":
                        RefreshMongoSKUGrid();
                        break;
                    case "dbm":
                        RefreshMongoDBM();
                        break;
                    case "inventory_turns":
                        RefreshInventoryTurns(date);
                        break;
                    case "category_hierarchy":
                        RefreshMongoCategoryHierarchy();
                        break;
                    case "cntqty_fillrate":
                        RefreshMongoCntQtyFillRate(date);
                        break;
                    case "import_export_log":
                        RefreshMongoImportExportLog();
                        break;
                    case "bom":
                        RefreshMongoBOM();
                        break;
                    case "bom_hierarchy":
                        RefreshMongoBOMHierarchy();
                        break;
                }
            }
            catch (Exception e)
            {
                _log.Error("RefreshMongoDB::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void RefreshMongoBOM()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.BOM);
            MongoHelper<BOM>.RemoveAll(collection);
            List<BOM> lst = Dao.BOM().SelectList();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<BOM>.Add(collection, lst);
        }

        private void RefreshMongoBOMHierarchy()
        {
            string Collection = Common.Instance.GetEnumValue(Common.CollectionName.BOM_HIERARCHY);
            List<BOM> _lst = new List<BOM>();
            string bom = Common.Instance.GetEnumValue(Common.CollectionName.BOM);
            _lst = MongoHelper<BOM>.Find(bom, Query.EQ("parent_id", 0));
            if (_lst != null)
                if (_lst.Count > 0)
                    _lst = (new Master()).RecursiveBOMHierarchy(_lst);

            BOMHierarchy item = new BOMHierarchy();
            item.bomList = _lst;

            MongoHelper<BOMHierarchy>.RemoveAll(Collection);
            MongoHelper<BOMHierarchy>.Add(Collection, item);
        }

        public void MongoReplenishmentFullSync()
        {
            try
            {
                object param = new object();
                new Worker().StartFullSync(param);
            }
            catch (Exception e)
            {
                _log.Error("MongoReplenishmentFullSync::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        public void StartFullSync(object param)
        {
            try
            {
                string date = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                RefreshMongoReplenishment(date, true);
            }
            catch (Exception e)
            {
                _log.Error("StartFullSync::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        public void UploadCategory(object param)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\Category\\Category_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "Category_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "Category\\";
            string zipFileName = "";

            string tmpTable = "tmp_category" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = param.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "Category";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                int bulkInsLimit = 100;

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Category\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("Category_Code") || !dt.Columns.Contains("Parent_Category_Code") || !dt.Columns.Contains("Category_Name"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Category\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                bool wkLimit = true;

                List<Category> _lstExcelStatus = new List<Category>();

                List<Category> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                                            new Category
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                category_code = drow["Category_Code"].ToString().Trim(),
                                                category_name = drow["Category_Name"].ToString().Trim(),
                                                parent_id = 0,
                                                parent_category = drow["Parent_Category_Code"].ToString().Trim(),
                                                created_by = user,
                                                modified_by = user,
                                                status = 1,
                                                identifier = identifier,
                                                row_status = "Row added successfully!"
                                            }
                                        ).ToList();

                List<Category> _lstTemp = _lstAllTemp.Where(rec => (rec.category_code.ToString().Length > 0)).Distinct().ToList<Category>();

                if (dt != null) dt.Dispose();

                item.status_text = "Temp Category Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.Category().CreateTempTable(tmpTable);

                if (_lstTemp != null)
                {
                    int cnt1 = _lstTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<Category> _lstInsert = new List<Category>();
                            _lstInsert.AddRange(_lstTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempCategory _tempTBL = new TempCategory();
                            if (_tempTBL.category == null) _tempTBL.category = new List<Category>();

                            _tempTBL.category.AddRange(_lstInsert);
                            _tempTBL.table_name = tmpTable;

                            Dao.Category().InsertTempBatch(_tempTBL);
                        }
                    }
                }

                item.status_text = "Temp Category Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                List<Category> _lstBulkInsert = Dao.Category().SelectTmpNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<Category> _lstDuplicate = Dao.Category().SelectTmpDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (Category _dup in _lstDuplicate)
                            {
                                List<Category> _chkDulpicate = _lstBulkInsert.Where(rec => rec.category_code == _dup.category_code).ToList<Category>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dup);

                                _dup.row_status = "Duplicate in Excel! Record already exist for Category Code: " + _dup.category_code + "";
                                _lstExcelStatus.Add(_dup);
                            }
                        }
                    }
                }


                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstAllTemp.Where(rec => (rec.category_code.ToString().Length <= 0)).Distinct().ToList<Category>());

                foreach (Category drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Update Records START";
                Common.UpdateLog(item, logfilePath, true);

                List<Category> _lstDuplicate1 = Dao.Category().SelectDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        rows_updated = _lstDuplicate1.Count;

                        Dictionary<string, long> _allCategoryDic = Dao.Category().SelectAllDictionary();

                        foreach (Category _update in _lstDuplicate1)
                        {
                            _update.parent_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allCategoryDic, _update.parent_category)) ? Convert.ToInt64(Common.Instance.getDicValue(_allCategoryDic, _update.parent_category)) : 0);
                            Dao.Category().Update(_update);
                        }
                    }
                }

                item.status_text = "Update Records END";
                Common.UpdateLog(item, logfilePath, true);

                Dao.Category().DropTempTable(tmpTable);

                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int lstcnt = _lstBulkInsert.Count();

                    rows_added = lstcnt;

                    if (lstcnt > 0)
                    {
                        List<Category> _lstLevel0 = _lstBulkInsert.Where(rec => rec.parent_category.ToString().Length <= 0 || rec.parent_category.ToLower().Trim() == "0").ToList<Category>();

                        if (_lstLevel0 != null)
                        {
                            int cnt = _lstLevel0.Count();
                            if (_lstLevel0.Count > 0)
                            {
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));

                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    List<Category> _lstInsert = new List<Category>();
                                    _lstInsert.AddRange(_lstLevel0.GetRange(offset * bulkInsLimit, bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit));

                                    if (_lstInsert.Count > 0)
                                        Dao.Category().InsertBatch(_lstInsert);
                                }

                                InsertCategoryRecursive(_lstBulkInsert, _lstLevel0, bulkInsLimit);
                            }
                        }
                    }
                }
                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);

                RefreshMongoDb("category");
                RefreshMongoDb("category_hierarchy");

                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);

                rows_duplicated = dt.Rows.Count - (rows_added + rows_updated);

                item.log_file = ("Logs\\Category\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added , " + rows_updated + " updated and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added , " + rows_updated + " updated and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                Dao.Category().DropTempTable(tmpTable);

                _log.Error("UploadCategory::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void InsertCategoryRecursive(List<Category> _lstBulkInsert, List<Category> _lstLevel0, int limit)
        {
            foreach (Category _cat in _lstLevel0)
            {
                List<Category> _lstLevel1 = _lstBulkInsert.Where(rec => rec.parent_category.ToLower() == _cat.category_code.ToLower()).ToList<Category>();
                if (_lstLevel1 != null)
                {
                    int cnt1 = _lstLevel1.Count();
                    if (_lstLevel1.Count > 0)
                    {

                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(limit)));
                        for (int offset1 = 0; offset1 < loopCount1; offset1++)
                        {
                            List<Category> _lstInsert1 = new List<Category>();
                            _lstInsert1.AddRange(_lstLevel1.GetRange(offset1 * limit, limit > (cnt1 - (offset1 * limit)) ? (cnt1 - (offset1 * limit)) : limit));

                            if (_lstInsert1.Count > 0)
                                Dao.Category().InsertBatch(_lstInsert1);
                        }

                        InsertCategoryRecursive(_lstBulkInsert, _lstLevel1, limit);

                    }

                }
            }
        }



        private void RefreshMongoCategory()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);
            MongoHelper<Category>.RemoveAll(collection);
            List<Category> lst = Dao.Category().SelectList();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<Category>.Add(collection, lst);
        }

        private void RefreshMongoCategoryHierarchy()
        {
            string Collection = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY_HIERARCHY);
            List<Category> _lst = new List<Category>();
            string category = Common.Instance.GetEnumValue(Common.CollectionName.CATEGORY);
            _lst = MongoHelper<Category>.Find(category, Query.And(Query.EQ("status", 1), Query.EQ("parent_id", 0)));
            if (_lst != null)
                if (_lst.Count > 0)
                    _lst = (new Master()).RecursiveCategoryHierarchy(_lst);

            CategoryHierarchy item = new CategoryHierarchy();
            item.categoryList = _lst;

            MongoHelper<CategoryHierarchy>.RemoveAll(Collection);
            MongoHelper<CategoryHierarchy>.Add(Collection, item);
        }

        public void UploadSKU(object parameter)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\SKU\\SKU_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "SKU_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "SKU\\";
            string zipFileName = "";

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = parameter.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "SKU";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\SKU\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("SKU_code") || !dt.Columns.Contains("SKU_Name") || !dt.Columns.Contains("SKU_Description"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\SKU\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                item.status_text = "Attributes Validation START";
                Common.UpdateLog(item, logfilePath, true);
                foreach (DataColumn dcom in dt.Columns)
                {
                    string columnName = dcom.ToString().Trim();

                    if (columnName.ToUpper() != "SKU_CODE" && columnName.ToUpper() != "SKU_NAME" && columnName.ToUpper() != "SKU_DESCRIPTION")
                    {
                        Attributes _attr = Dao.Attributes().SelectByCode(columnName);
                        if (_attr == null)
                        {
                            Attributes ObjAttribute = new Attributes();
                            ObjAttribute.attribute_code = columnName;
                            ObjAttribute.attribute_name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(columnName.Replace("_", " "));
                            ObjAttribute.data_type = "Varchar";
                            ObjAttribute.optional = 0;
                            ObjAttribute.status = 1;
                            ObjAttribute.created_by = user;

                            ObjAttribute.attribute_id = Dao.Attributes().Insert(ObjAttribute);
                            _attr = Dao.Attributes().SelectByCode(columnName);
                        }

                        dcom.ColumnName = _attr.attribute_id.ToString();
                    }
                }
                item.status_text = "Attributes Validation END";
                Common.UpdateLog(item, logfilePath, true);

                bool wkLimit = true;
                int bulkInsLimit = 100;
                int delLimit = 100;

                List<SKU> _lstBulkInsert = new List<SKU>();
                List<SKU> _lstBulkUpdate = new List<SKU>();

                ////// For Invalid Check START
                Dictionary<string, long> _allSKUDic = null;

                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU Dictionary START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU Dictionary END.";
                    Common.UpdateLog(item, logfilePath, true);
                }
                ////// For Invalid Check END

                List<SKU> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                            new SKU
                            {
                                row_index = dt.Rows.IndexOf(drow) + 1,
                                sku_code = drow["SKU_Code"].ToString().Trim(),
                                sku_name = drow["SKU_Name"].ToString().Trim(),
                                sku_description = drow["SKU_Description"].ToString().Trim(),
                                status = 1,
                                created_by = user,
                                attributes = getAtrributeOfSku(dt, drow, user),
                                isDuplicate = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) : 0) > 0 ? true : false,
                                modified_by = user,
                                identifier = identifier,
                                row_status = drow["SKU_Code"].ToString().Trim().Length <= 0 ? "Invalid Data Found in SKU_Code!" : "Row added successfully!"
                            }
                        ).ToList();

                List<SKU> _lstExcelStatus = new List<SKU>();

                List<SKU> _lstTemp = _lstAllTemp.Where(rec => (rec.sku_code.ToString().Length > 0)).Distinct().GroupBy(x => new { x.sku_code }).Select(gr => gr.First()).ToList<SKU>(); //// Get Not Duplicate Records FROM Excel

                List<SKU> _lstDuplicateTemp = _lstAllTemp.GroupBy(x => new { x.sku_code }).Where(x => x.Skip(1).Any()).SelectMany(gr => gr).ToList(); //// Get Duplicate Records FROM Excel
                foreach (SKU _dup in _lstDuplicateTemp)
                {
                    List<SKU> _chkDulpicate = _lstTemp.Where(rec => rec.row_index == _dup.row_index).ToList<SKU>();
                    if (_chkDulpicate.Count <= 0)
                    {
                        _dup.row_status = "Duplicate in Excel! Record already exist for SKU_Code: " + _dup.sku_code + "";
                        _lstExcelStatus.Add(_dup);
                    }
                }

                rows_duplicated = (_lstAllTemp.Count() - _lstTemp.Count());

                _lstBulkInsert = _lstTemp.Where(rec => rec.isDuplicate == false).ToList<SKU>(); //// Get Not Duplicate Records FROM SKU Master Table
                _lstBulkUpdate = _lstTemp.Where(rec => rec.isDuplicate == true).ToList<SKU>();//// Get Duplicate Records FROM SKU Master Table For Update

                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);

                List<SKUAttributeMapping> _lstSkuAttriInsert = new List<SKUAttributeMapping>();
                if (_lstBulkInsert != null)
                {
                    int cnt = _lstBulkInsert.Count();

                    if (cnt > 0)
                    {
                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset = 0; offset < loopCount; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<SKU> _lstInsert = new List<SKU>();
                            _lstInsert.AddRange(_lstBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            if (_lstInsert.Count > 0)
                            {
                                Dao.SKU().InsertBatch(_lstInsert);

                                foreach (SKU _result in _lstInsert)
                                    _lstSkuAttriInsert.AddRange(_result.attributes);
                            }
                        }
                    }
                    rows_added = cnt;
                }

                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Fetch all SKU Dictionary START.";
                Common.UpdateLog(item, logfilePath, true);
                _allSKUDic = Dao.SKU().SelectAllDictionary(); //// Get All SKU Dictionary
                item.status_text = "Fetch all SKU Dictionary END.";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Bulk SKU Attribute Insert START";
                Common.UpdateLog(item, logfilePath, true);

                List<SKUAttributeMapping> _lstAttriBulkInsert = _lstSkuAttriInsert.Select(drow =>
                                            new SKUAttributeMapping
                                            {
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow.sku_code)) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow.sku_code)) : 0),
                                                attribute_id = drow.attribute_id,
                                                value = drow.value,
                                                created_by = user
                                            }
                                        ).ToList();


                if (_lstAttriBulkInsert != null)
                {
                    int cnt = _lstAttriBulkInsert.Count();

                    if (cnt > 0)
                    {

                        int loopCountDel = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(delLimit)));
                        for (int offset = 0; offset < loopCountDel; offset++)
                        {
                            int fetchCnt = (delLimit > (cnt - (offset * delLimit)) ? (cnt - (offset * delLimit)) : delLimit);

                            List<SKUAttributeMapping> _lstDelete = new List<SKUAttributeMapping>();
                            _lstDelete.AddRange(_lstAttriBulkInsert.GetRange(offset * delLimit, fetchCnt));

                            if (_lstDelete.Count > 0)
                                Dao.SKUAttributeMapping().DeleteBatch(_lstDelete);
                        }

                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<SKUAttributeMapping> _lstInsert = new List<SKUAttributeMapping>();
                            _lstInsert.AddRange(_lstAttriBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            if (_lstInsert.Count > 0)
                                Dao.SKUAttributeMapping().InsertBatch(_lstInsert);
                        }
                    }
                }


                item.status_text = "Bulk SKU Attribute Insert END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Update SKU START";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkUpdate != null)
                {
                    int cnt = _lstBulkUpdate.Count();

                    if (cnt > 0)
                    {
                        List<SKUAttributeMapping> _lstSkuAttriUpdate = new List<SKUAttributeMapping>();

                        foreach (SKU _result in _lstBulkUpdate)
                        {
                            _result.sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, _result.sku_code)) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, _result.sku_code)) : 0);
                            _result.modified_by = user;
                            _result.status = 1;

                            Dao.SKU().Update(_result);

                            _lstSkuAttriUpdate.AddRange(_result.attributes);
                        }

                        List<SKUAttributeMapping> _lstAttriBulkUpdate = _lstSkuAttriUpdate.Select(drow =>
                                                                            new SKUAttributeMapping
                                                                            {
                                                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow.sku_code)) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow.sku_code)) : 0),
                                                                                attribute_id = drow.attribute_id,
                                                                                value = drow.value,
                                                                                created_by = user
                                                                            }
                                                                            ).ToList();

                        item.status_text = "Bulk SKU Attribute Update START";
                        Common.UpdateLog(item, logfilePath, true);

                        if (_lstAttriBulkUpdate != null)
                        {
                            int cntUp = _lstAttriBulkUpdate.Count();

                            if (cntUp > 0)
                            {

                                int loopCountDel = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cntUp) / Convert.ToDouble(delLimit)));
                                for (int offset = 0; offset < loopCountDel; offset++)
                                {
                                    int fetchCnt = (delLimit > (cntUp - (offset * delLimit)) ? (cntUp - (offset * delLimit)) : delLimit);

                                    List<SKUAttributeMapping> _lstDelete = new List<SKUAttributeMapping>();
                                    _lstDelete.AddRange(_lstAttriBulkUpdate.GetRange(offset * delLimit, fetchCnt));

                                    if (_lstDelete.Count > 0)
                                        Dao.SKUAttributeMapping().DeleteBatch(_lstDelete);
                                }

                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cntUp) / Convert.ToDouble(bulkInsLimit)));
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    int fetchCnt = (bulkInsLimit > (cntUp - (offset * bulkInsLimit)) ? (cntUp - (offset * bulkInsLimit)) : bulkInsLimit);

                                    List<SKUAttributeMapping> _lstInsert = new List<SKUAttributeMapping>();
                                    _lstInsert.AddRange(_lstAttriBulkUpdate.GetRange(offset * bulkInsLimit, fetchCnt));

                                    if (_lstInsert.Count > 0)
                                        Dao.SKUAttributeMapping().InsertBatch(_lstInsert);
                                }
                            }
                        }

                        item.status_text = "Bulk SKU Attribute Update END";
                        Common.UpdateLog(item, logfilePath, true);

                    }
                    rows_updated = cnt;
                }

                item.status_text = "Update SKU END";
                Common.UpdateLog(item, logfilePath, true);

                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                foreach (SKU drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);

                RefreshMongoDb("sku");

                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);

                item.log_file = ("Logs\\SKU\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added , " + rows_updated + " updated and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added , " + rows_updated + " updated and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                _log.Error("UploadSKU::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private List<SKUAttributeMapping> getAtrributeOfSku(DataTable dt, DataRow drow, long user)
        {
            List<SKUAttributeMapping> _attr = new List<SKUAttributeMapping>();
            foreach (DataColumn dcom in dt.Columns)
            {
                string columnName = dcom.ToString().Trim();
                string columnValue = drow[columnName].ToString().Trim();

                if (columnName.ToUpper() != "SKU_CODE" && columnName.ToUpper() != "SKU_NAME" && columnName.ToUpper() != "SKU_DESCRIPTION")
                {
                    SKUAttributeMapping param = new SKUAttributeMapping();
                    param.sku_id = 0;
                    param.sku_code = drow["SKU_CODE"].ToString().Trim();
                    param.attribute_id = Convert.ToInt64(columnName);
                    param.value = columnValue;
                    param.created_by = user;

                    _attr.Add(param);
                }
            }
            return _attr;
        }

        private void RefreshMongoSKU()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU);
            string attr_collection = Common.Instance.GetEnumValue(Common.CollectionName.ATTRIBUTE);
            string sku_attr_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_ATTRIBUTE_MAPPING);

            MongoHelper<SKU>.RemoveAll(collection);
            List<SKU> lst = Dao.SKU().SelectAll();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<SKU>.Add(collection, lst);

            MongoHelper<Attributes>.RemoveAll(attr_collection);
            List<Attributes> attr_lst = Dao.Attributes().SelectAll();
            if (attr_lst != null)
                if (attr_lst.Count > 0)
                    MongoHelper<Attributes>.Add(attr_collection, attr_lst);

            MongoHelper<SKUAttributeMapping>.RemoveAll(sku_attr_collection);
            List<SKUAttributeMapping> sku_attr_lst = Dao.SKUAttributeMapping().SelectAll();
            if (sku_attr_lst != null)
                if (sku_attr_lst.Count > 0)
                    MongoHelper<SKUAttributeMapping>.Add(sku_attr_collection, sku_attr_lst);
        }

        public void UploadNode(object param)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\Node\\Node_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "Node_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "Node\\";
            string zipFileName = "";

            string tmpTable = "tmp_node" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = param.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "Node";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                int bulkInsLimit = 100;

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Node\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("Node_Code") || !dt.Columns.Contains("Parent_Node_Code") || !dt.Columns.Contains("Node_Name")
                    || !dt.Columns.Contains("Address1") || !dt.Columns.Contains("Address2"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Node\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                bool wkLimit = true;

                List<Node> _lstExcelStatus = new List<Node>();

                List<Node> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                                            new Node
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,

                                                node_code = drow["Node_Code"].ToString().Trim(),
                                                node_name = drow["Node_Name"].ToString().Trim(),
                                                parent_id = 0,
                                                parent_node = drow["Parent_Node_Code"].ToString().Trim(),
                                                address_line1 = drow["Address1"].ToString().Trim(),
                                                address_line2 = drow["Address2"].ToString().Trim(),
                                                latitude = dt.Columns.Contains("Latitude") ? drow["Latitude"].ToString().Trim() : "",
                                                longitude = dt.Columns.Contains("Longitude") ? drow["Longitude"].ToString().Trim() : "",
                                                location_id = 0,
                                                status = 1,
                                                created_by = user,
                                                modified_by = user,
                                                identifier = identifier,
                                                row_status = "Row added successfully!"
                                            }
                                        ).ToList();

                List<Node> _lstTemp = _lstAllTemp.Where(rec => (rec.node_code.ToString().Length > 0)).Distinct().ToList<Node>();

                if (dt != null) dt.Dispose();

                item.status_text = "Temp Node Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.Node().CreateTempTable(tmpTable);

                if (_lstTemp != null)
                {
                    int cnt1 = _lstTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<Node> _lstInsert = new List<Node>();
                            _lstInsert.AddRange(_lstTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempNode _tempTBL = new TempNode();
                            if (_tempTBL.node == null) _tempTBL.node = new List<Node>();

                            _tempTBL.node.AddRange(_lstInsert);
                            _tempTBL.table_name = tmpTable;

                            Dao.Node().InsertTempBatch(_tempTBL);
                        }
                    }
                }

                item.status_text = "Temp Node Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                List<Node> _lstBulkInsert = Dao.Node().SelectTmpNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<Node> _lstDuplicate = Dao.Node().SelectTmpDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (Node _dup in _lstDuplicate)
                            {
                                List<Node> _chkDulpicate = _lstBulkInsert.Where(rec => rec.node_code == _dup.node_code).ToList<Node>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dup);

                                _dup.row_status = "Duplicate in Excel! Record already exist for Node Code: " + _dup.node_code + "";
                                _lstExcelStatus.Add(_dup);
                            }
                        }
                    }
                }

                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstAllTemp.Where(rec => (rec.node_code.ToString().Length <= 0)).Distinct().ToList<Node>());

                foreach (Node drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Update Records START";
                Common.UpdateLog(item, logfilePath, true);

                List<Node> _lstDuplicate1 = Dao.Node().SelectDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        rows_updated = _lstDuplicate1.Count;

                        Dictionary<string, long> _allNodeDic = Dao.Node().SelectAllDictionary();

                        foreach (Node _update in _lstDuplicate1)
                        {
                            _update.parent_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, _update.parent_node)) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, _update.parent_node)) : 0);
                            Dao.Node().Update(_update);
                        }
                    }
                }

                item.status_text = "Update Records END";
                Common.UpdateLog(item, logfilePath, true);

                Dao.Node().DropTempTable(tmpTable);

                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int lstcnt = _lstBulkInsert.Count();

                    rows_added = lstcnt;

                    if (lstcnt > 0)
                    {
                        List<Node> _lstLevel0 = _lstBulkInsert.Where(rec => rec.parent_node.Trim().Length <= 0 || rec.parent_node.Trim().ToLower() == "0").ToList<Node>();
                        if (_lstLevel0 != null)
                        {
                            int cnt = _lstLevel0.Count();
                            if (_lstLevel0.Count > 0)
                            {
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    List<Node> _lstInsert = new List<Node>();
                                    _lstInsert.AddRange(_lstLevel0.GetRange(offset * bulkInsLimit, bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit));

                                    if (_lstInsert.Count > 0)
                                        Dao.Node().InsertBatch(_lstInsert);
                                }

                                InsertNodeRecursive(_lstBulkInsert, _lstLevel0, bulkInsLimit);
                            }
                        }
                    }
                }
                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);

                RefreshMongoDb("node");

                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);

                rows_duplicated = dt.Rows.Count - (rows_added + rows_updated);

                item.log_file = ("Logs\\Node\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added, " + rows_updated + " updated and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_added + " records are added, " + rows_updated + " updated and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                Dao.Node().DropTempTable(tmpTable);

                _log.Error("UploadNode::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void InsertNodeRecursive(List<Node> _lstBulkInsert, List<Node> _lstLevel0, int limit)
        {
            foreach (Node _nod in _lstLevel0)
            {
                List<Node> _lstLevel1 = _lstBulkInsert.Where(rec => rec.parent_node.ToLower() == _nod.node_code.ToLower()).ToList<Node>();
                if (_lstLevel1 != null)
                {
                    int cnt1 = _lstLevel1.Count();
                    if (_lstLevel1.Count > 0)
                    {

                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(limit)));
                        for (int offset1 = 0; offset1 < loopCount1; offset1++)
                        {
                            List<Node> _lstInsert1 = new List<Node>();
                            _lstInsert1.AddRange(_lstLevel1.GetRange(offset1 * limit, limit > (cnt1 - (offset1 * limit)) ? (cnt1 - (offset1 * limit)) : limit));

                            if (_lstInsert1.Count > 0)
                                Dao.Node().InsertBatch(_lstInsert1);
                        }

                        InsertNodeRecursive(_lstBulkInsert, _lstLevel1, limit);



                    }

                }


            }
        }

        private void RefreshMongoNode()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE);
            MongoHelper<Node>.RemoveAll(collection);
            List<Node> lst = Dao.Node().SelectAll();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<Node>.Add(collection, lst);
        }

        public void UploadBufferNorm(object parameter)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\BufferNorm\\BufferNorm_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "BufferNorm_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "BufferNorm\\";
            string zipFileName = "";

            string tmpTable = "tmp_buffer_norm" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = parameter.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "Buffer Norm";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                int rows_succeeded = 0;
                int rows_failed = 0;

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\BufferNorm\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("SKU_Code") || !dt.Columns.Contains("Node_Code") || !dt.Columns.Contains("Buffering_Norm")
                    || !dt.Columns.Contains("Effective_From") || !dt.Columns.Contains("Unit_Purchase_Price") || !dt.Columns.Contains("Unit_Selling_Price")
                    || !dt.Columns.Contains("Saftey_Factor") || !dt.Columns.Contains("RLT_Days") || !dt.Columns.Contains("Max_Sales_RLT")
                    || !dt.Columns.Contains("OLT_Days") || !dt.Columns.Contains("PLT_Days") || !dt.Columns.Contains("MOQ")
                    || !dt.Columns.Contains("EOQ") || !dt.Columns.Contains("Default_Color_Zone") || !dt.Columns.Contains("Red_Zone") || !dt.Columns.Contains("Yellow_Zone"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\BufferNorm\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                int bulkInsLimit = 100;

                string date = Convert.ToDateTime(dt.Rows[0]["Effective_From"].ToString().Trim()).ToString("yyyy-MM-dd");

                List<BufferNorm> lstBufferNorms = new List<BufferNorm>();

                ////// For Invalid Check STSRT
                Dictionary<string, long> _allSKUDic = null;
                Dictionary<string, long> _allNodeDic = null;
                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU list END.";
                    Common.UpdateLog(item, logfilePath, true);

                    item.status_text = "Fetch all Node list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allNodeDic = Dao.Node().SelectAllDictionary();

                    item.status_text = "Fetch all Node list END.";
                    Common.UpdateLog(item, logfilePath, true);
                }
                ////// For Invalid Check END

                bool wkLimit = true;

                List<BufferNorm> _lstExcelStatus = new List<BufferNorm>();

                List<BufferNorm> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                                            new BufferNorm
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0),
                                                node_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0),
                                                effective_from = Convert.ToDateTime(Convert.ToDateTime(drow["Effective_From"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss")),

                                                ////buffer_norm = drow["Buffering_Norm"].ToString().Trim().Length > 0 ? Convert.ToDouble(drow["Buffering_Norm"].ToString().Trim()) : 0D,
                                                ////unit_purchase_price = drow["Unit_Purchase_Price"].ToString().Trim().Length > 0 ? Convert.ToDouble(drow["Unit_Purchase_Price"].ToString().Trim()) : 0D,
                                                ////unit_selling_price = drow["Unit_Selling_Price"].ToString().Trim().Length > 0 ? Convert.ToDouble(drow["Unit_Selling_Price"].ToString().Trim()) : 0D,
                                                ////safety_factor = drow["Saftey_Factor"].ToString().Trim().Length > 0 ? Convert.ToDouble(drow["Saftey_Factor"].ToString().Trim()) : 0D,
                                                ////rlt = drow["RLT_Days"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["RLT_Days"].ToString().Trim()) : 0,
                                                ////max_sales_per_day = drow["Max_Sales_RLT"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["Max_Sales_RLT"].ToString().Trim()) : 0,
                                                ////olt = drow["OLT_Days"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["OLT_Days"].ToString().Trim()) : 0,
                                                ////plt = drow["PLT_Days"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["PLT_Days"].ToString().Trim()) : 0,
                                                ////tlt = drow["TLT_Days"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["TLT_Days"].ToString().Trim()) : 0,
                                                ////moq = drow["MOQ"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["MOQ"].ToString().Trim()) : 0,
                                                ////eoq = drow["EOQ"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["EOQ"].ToString().Trim()) : 0,

                                                buffer_norm = Common.Instance.ConvertToDouble(drow["Buffering_Norm"].ToString().Trim()),
                                                unit_purchase_price = Common.Instance.ConvertToDouble(drow["Unit_Purchase_Price"].ToString().Trim()),
                                                unit_selling_price = Common.Instance.ConvertToDouble(drow["Unit_Selling_Price"].ToString().Trim()),
                                                safety_factor = Common.Instance.ConvertToDouble(drow["Saftey_Factor"].ToString().Trim()),

                                                rlt = Common.Instance.ConvertToDouble(drow["RLT_Days"].ToString().Trim()),
                                                max_sales_per_day = Common.Instance.ConvertToDouble(drow["Max_Sales_RLT"].ToString().Trim()),
                                                olt = Common.Instance.ConvertToDouble(drow["OLT_Days"].ToString().Trim()),
                                                plt = Common.Instance.ConvertToDouble(drow["PLT_Days"].ToString().Trim()),
                                                tlt = Common.Instance.ConvertToDouble(drow["TLT_Days"].ToString().Trim()),
                                                moq = Common.Instance.ConvertToDouble(drow["MOQ"].ToString().Trim()),
                                                eoq = Common.Instance.ConvertToDouble(drow["EOQ"].ToString().Trim()),

                                                is_valid_zone = validBufferNormZone((drow["Default_Color_Zone"].ToString().Trim().ToLower() == "n" ? "N" : "Y"), drow["Red_Zone"].ToString().Trim(), drow["Yellow_Zone"].ToString().Trim()),
                                                is_default_zone = drow["Default_Color_Zone"].ToString().Trim().ToLower() == "n" ? "N" : "Y",
                                                red_zone = Common.Instance.ConvertToDouble(drow["Red_Zone"].ToString().Trim()),
                                                yellow_zone = Common.Instance.ConvertToDouble(drow["Yellow_Zone"].ToString().Trim()),

                                                modified_by = user,
                                                created_by = user,
                                                identifier = identifier,
                                                row_status = (
                                                                ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) > 0
                                                                    && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0) > 0)
                                                                    ? validateBNMessage((drow["Default_Color_Zone"].ToString().Trim().ToLower() == "n" ? "N" : "Y"), drow["Red_Zone"].ToString().Trim(), drow["Yellow_Zone"].ToString().Trim())
                                                                    : (((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0) <= 0)
                                                                      )
                                                                    ? ("Invalid SKU Code: " + drow["SKU_Code"].ToString() + " and Node Code:" + drow["Node_Code"].ToString() + "")
                                                                    : ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        ? "Invalid SKU Code: " + drow["SKU_Code"].ToString() + ""
                                                                        : "Invalid Node Code: " + drow["Node_Code"].ToString() + ""
                                                                      )
                                                              )
                                            }
                                        ).ToList();


                List<BufferNorm> _lstTemp = _lstAllTemp.Where(rec => (rec.sku_id > 0 && rec.node_id > 0 && rec.is_valid_zone == true)).Distinct().ToList<BufferNorm>();

                rows_failed = (_lstAllTemp.Count() - _lstTemp.Count());

                if (dt != null) dt.Dispose();

                item.status_text = "Temp BufferNorm Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.BufferNorm().CreateTempTable(tmpTable);

                if (_lstTemp != null)
                {
                    int cnt1 = _lstTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<BufferNorm> _lstInsert = new List<BufferNorm>();
                            _lstInsert.AddRange(_lstTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempBufferNorm _tempTBL = new TempBufferNorm();
                            if (_tempTBL.bufferNorm == null) _tempTBL.bufferNorm = new List<BufferNorm>();

                            _tempTBL.bufferNorm.AddRange(_lstInsert);
                            _tempTBL.table_name = tmpTable;

                            Dao.BufferNorm().InsertTempBatch(_tempTBL);
                        }
                    }
                }

                item.status_text = "Temp BufferNorm Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                List<BufferNorm> _lstBulkInsert = Dao.BufferNorm().SelectTmpNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<BufferNorm> _lstDuplicate = Dao.BufferNorm().SelectTmpDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (BufferNorm _dup in _lstDuplicate)
                            {
                                List<BufferNorm> _chkDulpicate = _lstBulkInsert.Where(rec => rec.sku_id == _dup.sku_id
                                                                                    && rec.node_id == _dup.node_id
                                                                                        && rec.effective_from == _dup.effective_from).ToList<BufferNorm>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dup);

                                _dup.row_status = "Duplicate! Record already exist for SKU Code, Node Code and Effective Date!";
                                _lstExcelStatus.Add(_dup);
                            }
                        }
                    }
                }

                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstAllTemp.Where(rec => (rec.sku_id <= 0 || rec.node_id <= 0 || rec.is_valid_zone == false)).Distinct().ToList<BufferNorm>());

                List<BufferNorm> _lstDuplicate1 = Dao.BufferNorm().SelectDuplicate(tmpTable);

                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        _lstDuplicate1.Select(x => { x.row_status = "Duplicate! Record already exist for SKU Code, Node Code and Effective Date!"; return x; }).ToList().Where(x => x.row_status == "duplicate");
                        _lstExcelStatus.AddRange(_lstDuplicate1);
                    }
                }

                foreach (BufferNorm drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);


                Dao.BufferNorm().DropTempTable(tmpTable);


                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);
                if (_lstBulkInsert != null)
                {
                    lstBufferNorms = _lstBulkInsert;

                    int cnt = _lstBulkInsert.Count();

                    rows_succeeded = cnt;
                    rows_duplicated = (_lstTemp.Count() - cnt);

                    if (cnt > 0)
                    {
                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset = 0; offset < loopCount; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<BufferNorm> _lstInsert = new List<BufferNorm>();
                            _lstInsert.AddRange(_lstBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            if (_lstInsert.Count > 0)
                                Dao.BufferNorm().InsertBatch(_lstInsert);
                        }

                        List<BufferNorm> _bnLst = Dao.BufferNorm().SelectBatchInsertedBufferNorm(identifier).OrderBy(x => x.buffer_norm_id).ToList();
                        if (_bnLst != null)
                            if (_bnLst.Count > 0)
                                MongoHelper<BufferNorm>.Add(Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM), _bnLst);
                    }
                }
                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.log_file = ("Logs\\BufferNorm\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! \n The " + rows_added + " and " + rows_duplicated + " duplicated. Updating Transaction and Replenishment data.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated. Updating Transaction and Replenishment data.";
                Common.UpdateLog(item, logfilePath);


                item.status_text = "Select BN Calculation Transaction Dates START";
                Common.UpdateLog(item, logfilePath, true);

                List<Transaction> lstDates = Dao.Transaction().SelectBNCalculationTransactionDates(date);

                item.status_text = "Select BN Calculation Transaction Dates END";
                Common.UpdateLog(item, logfilePath, true);

                int update_limit = 5;
                List<Replenishment> mongolst = new List<Replenishment>();
                string Collection = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);

                item.status_text = "BN Calculation Replenishment START";
                Common.UpdateLog(item, logfilePath, true);

                foreach (var bn in lstBufferNorms)
                {
                    foreach (var d in lstDates)
                    {
                        d.sku_id = bn.sku_id;
                        d.node_id = bn.node_id;

                        List<Transaction> updateList = Dao.Transaction().GetTransactionIdsForBNCalculation(d);
                        int cnt = updateList.Count();
                        List<long> udt = updateList.Select(x => x.transaction_id).ToList();
                        int uploopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(update_limit)));

                        for (int offset = 0; offset < uploopCount; offset++)
                        {
                            Dao.Replenishment().UpdateBufferNormCalculation(udt.Skip(update_limit * offset).Take(update_limit).ToList());
                        }
                        string tdate = Convert.ToDateTime(d.transaction_date.ToString().Trim()).ToString("yyyy-MM-dd");
                        string[] transaction_date = tdate.Split(' ')[0].Split('-');
                        transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
                        transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
                        string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

                        MongoHelper<Replenishment>.Delete(Collection, Query.And(Query.EQ("transaction_date", transactionDate), Query.EQ("sku_id", d.sku_id), Query.EQ("node_id", d.node_id)));
                        List<Replenishment> lst = Dao.Replenishment().SelectByDateSKUNode(d);
                        mongolst.AddRange(lst);
                    }
                }
                item.status_text = "BN Calculation Replenishment END";
                Common.UpdateLog(item, logfilePath, true);

                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);
                if (mongolst.Count > 0)
                {
                    MongoHelper<Replenishment>.Add(Collection, mongolst);

                    ////foreach (var d in lstDates)
                    ////{
                    ////    RefreshMongoDb("sku_chart", date);
                    ////    RefreshMongoDb("inventory_turns", date);
                    ////    RefreshMongoDb("cntqty_fillrate", date);
                    ////}
                }
                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);

                item.log_file = ("Logs\\BufferNorm\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                Dao.BufferNorm().DropTempTable(tmpTable);

                _log.Error("UploadBufferNorm::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private string validateBNMessage(string IsDefault, string redZone, string yellowZone)
        {
            string str = "Row added successfully!";
            if (IsDefault == "N")
            {
                if (redZone.Trim().Length <= 0 && yellowZone.Trim().Length <= 0)
                    str = "Red Zone and Yellow Zone (%) should not be blank.";
                else if (redZone.Trim().Length > 0 && yellowZone.Trim().Length <= 0)
                    str = "Yellow Zone (%) should not be blank.";
                else if (redZone.Trim().Length <= 0 && yellowZone.Trim().Length > 0)
                    str = "Red Zone (%) should not be blank.";
                else if (redZone.Trim().Length > 0 && yellowZone.Trim().Length > 0)
                {
                    if (Common.Instance.ConvertToDouble(redZone.Trim()) >= Common.Instance.ConvertToDouble(yellowZone.Trim()))
                        str = "Red Zone (%) should not be greater or equal to Yellow Zone (%).";
                    else if (Common.Instance.ConvertToDouble(redZone.Trim()) + Common.Instance.ConvertToDouble(yellowZone.Trim()) > 100)
                        str = "Red Zone + Yellow Zone (%) should not be greater than 100 %.";
                }
                else
                    str = "Row added successfully!";
            }
            return str;
        }

        private bool validBufferNormZone(string IsDefault, string redZone, string yellowZone)
        {
            bool flag = true;

            if (IsDefault == "N")
            {
                if (redZone.Trim().Length <= 0 && yellowZone.Trim().Length <= 0)
                    flag = false;
                else if (redZone.Trim().Length > 0 && yellowZone.Trim().Length <= 0)
                    flag = false;
                else if (redZone.Trim().Length <= 0 && yellowZone.Trim().Length > 0)
                    flag = false;
                else if (redZone.Trim().Length > 0 && yellowZone.Trim().Length > 0)
                {
                    if (Common.Instance.ConvertToDouble(redZone.Trim()) >= Common.Instance.ConvertToDouble(yellowZone.Trim()))
                        flag = false;
                    else if (Common.Instance.ConvertToDouble(redZone.Trim()) + Common.Instance.ConvertToDouble(yellowZone.Trim()) > 100)
                        flag = false;
                }
                else
                    flag = true;
            }
            return flag;
        }

        private void RefreshMongoBufferNorm()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM);
            MongoHelper<BufferNorm>.RemoveAll(collection);
            List<BufferNorm> lst = Dao.BufferNorm().SelectList();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<BufferNorm>.Add(collection, lst);
        }

        private void RefreshMongoImportExportLog()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG);
            MongoHelper<ImportExportLog>.RemoveAll(collection);
            List<ImportExportLog> lst = Dao.ImportExportLog().SelectAll();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<ImportExportLog>.Add(collection, lst);
        }

        public void UploadTransaction(object param)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");
            string date = "";
            string logfile = "Logs\\Transaction\\Transaction_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "Transaction_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "\\Transaction\\";
            string zipFileName = "";

            string tmpTable = "tmp_transaction" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = param.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "Transaction";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File.";
                item.comment = "{'message':'Import In Process. Validating File.'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                _log.Info("UploadTransaction: Excel to Datatable Conversion STARTS!");
                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);
                _log.Info("UploadTransaction: Excel to Datatable Conversion END!");

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Transaction\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("SKU_Code") || !dt.Columns.Contains("Node_Code") || !dt.Columns.Contains("Supply_Node_Code")
                    || !dt.Columns.Contains("Transaction_Date") || !dt.Columns.Contains("GRN") || !dt.Columns.Contains("Sales")
                    || !dt.Columns.Contains("Closing_Quantity") || !dt.Columns.Contains("In_Transit") || !dt.Columns.Contains("Inst_Hold")
                    || !dt.Columns.Contains("Quality_Hold") || !dt.Columns.Contains("QA_Hold") || !dt.Columns.Contains("Pending_Orders")
                    || !dt.Columns.Contains("Orders_in_Hand")
                    || !dt.Columns.Contains("Open_Stock_Transfer_Order") || !dt.Columns.Contains("Open_Planned_Order") || !dt.Columns.Contains("Open_Production_order")
                    || !dt.Columns.Contains("Open_Purchase_Request") || !dt.Columns.Contains("Open_Purchase_Order"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\Transaction\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                date = Convert.ToDateTime(dt.Rows[0]["Transaction_Date"].ToString().Trim()).ToString("yyyy-MM-dd");

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                List<long> updateList = new List<long>();

                bool wkLimit = true;
                int rows_succeeded = 0;
                int rows_failed = 0;
                int rows_duplicated = 0;

                int bulkInsLimit = 100;

                ////// For Invalid Check START
                Dictionary<string, long> _allSKUDic = null;
                Dictionary<string, long> _allNodeDic = null;

                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU list END.";
                    Common.UpdateLog(item, logfilePath, true);

                    item.status_text = "Fetch all Node list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allNodeDic = Dao.Node().SelectAllDictionary();

                    item.status_text = "Fetch all Node list END.";
                    Common.UpdateLog(item, logfilePath, true);
                }
                ////// For Invalid Check END

                List<Transaction> _lstExcelStatus = new List<Transaction>();

                List<Transaction> _lstTransAllTemp = dt.AsEnumerable().Select(drow =>
                                            new Transaction
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0),
                                                node_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0),
                                                supply_node_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Supply_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Supply_Node_Code"].ToString().Trim())) : 0),
                                                transaction_date = Convert.ToDateTime(drow["Transaction_Date"].ToString().Trim()).ToString("yyyy-MM-dd"),

                                                ////grn = drow["GRN"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["GRN"].ToString().Trim()))) : 0,
                                                ////sales = drow["Sales"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Sales"].ToString().Trim()))) : 0,
                                                ////closing_quantity = drow["Closing_Quantity"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Closing_Quantity"].ToString().Trim()))) : 0,
                                                ////in_transit = drow["In_Transit"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["In_Transit"].ToString().Trim()))) : 0,
                                                ////inst_hold = drow["Inst_Hold"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Inst_Hold"].ToString().Trim()))) : 0,
                                                ////quality_hold = drow["Quality_Hold"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Quality_Hold"].ToString().Trim()))) : 0,
                                                ////qa_hold = drow["QA_Hold"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["QA_Hold"].ToString().Trim()))) : 0,
                                                ////pending_orders = drow["Pending_Orders"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Pending_Orders"].ToString().Trim()))) : 0,
                                                ////orders_in_hand = drow["Orders_in_Hand"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Orders_in_Hand"].ToString().Trim()))) : 0,
                                                ////open_stock_transfer_order = drow["Open_Stock_Transfer_Order"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Open_Stock_Transfer_Order"].ToString().Trim()))) : 0,
                                                ////open_planned_order = drow["Open_Planned_Order"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Open_Planned_Order"].ToString().Trim()))) : 0,
                                                ////open_production_order = drow["Open_Production_order"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Open_Production_order"].ToString().Trim()))) : 0,
                                                ////open_purchase_request = drow["Open_Purchase_Request"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Open_Purchase_Request"].ToString().Trim()))) : 0,
                                                ////open_purchase_order = drow["Open_Purchase_Order"].ToString().Trim().Length > 0 ? Convert.ToInt32(Math.Round(Convert.ToDecimal(drow["Open_Purchase_Order"].ToString().Trim()))) : 0,

                                                grn = Common.Instance.ConvertToDouble(drow["GRN"].ToString().Trim()),
                                                sales = Common.Instance.ConvertToDouble(drow["Sales"].ToString().Trim()),
                                                closing_quantity = Common.Instance.ConvertToDouble(drow["Closing_Quantity"].ToString().Trim()),
                                                in_transit = Common.Instance.ConvertToDouble(drow["In_Transit"].ToString().Trim()),
                                                inst_hold = Common.Instance.ConvertToDouble(drow["Inst_Hold"].ToString().Trim()),
                                                quality_hold = Common.Instance.ConvertToDouble(drow["Quality_Hold"].ToString().Trim()),
                                                qa_hold = Common.Instance.ConvertToDouble(drow["QA_Hold"].ToString().Trim()),
                                                pending_orders = Common.Instance.ConvertToDouble(drow["Pending_Orders"].ToString().Trim()),
                                                orders_in_hand = Common.Instance.ConvertToDouble(drow["Orders_in_Hand"].ToString().Trim()),
                                                open_stock_transfer_order = Common.Instance.ConvertToDouble(drow["Open_Stock_Transfer_Order"].ToString().Trim()),
                                                open_planned_order = Common.Instance.ConvertToDouble(drow["Open_Planned_Order"].ToString().Trim()),
                                                open_production_order = Common.Instance.ConvertToDouble(drow["Open_Production_order"].ToString().Trim()),
                                                open_purchase_request = Common.Instance.ConvertToDouble(drow["Open_Purchase_Request"].ToString().Trim()),
                                                open_purchase_order = Common.Instance.ConvertToDouble(drow["Open_Purchase_Order"].ToString().Trim()),

                                                transport_mode = dt.Columns.Contains("Transport_Mode") ? drow["Transport_Mode"].ToString().Trim() : "",

                                                field1 = dt.Columns.Contains("Field1") ? drow["Field1"].ToString().Trim() : "",
                                                field2 = dt.Columns.Contains("Field2") ? drow["Field2"].ToString().Trim() : "",
                                                field3 = dt.Columns.Contains("Field3") ? drow["Field3"].ToString().Trim() : "",
                                                field4 = dt.Columns.Contains("Field4") ? drow["Field4"].ToString().Trim() : "",
                                                field5 = dt.Columns.Contains("Field5") ? drow["Field5"].ToString().Trim() : "",
                                                field6 = dt.Columns.Contains("Field6") ? drow["Field6"].ToString().Trim() : "",
                                                field7 = dt.Columns.Contains("Field7") ? drow["Field7"].ToString().Trim() : "",
                                                field8 = dt.Columns.Contains("Field8") ? drow["Field8"].ToString().Trim() : "",
                                                field9 = dt.Columns.Contains("Field9") ? drow["Field9"].ToString().Trim() : "",
                                                field10 = dt.Columns.Contains("Field10") ? drow["Field10"].ToString().Trim() : "",

                                                created_by = user,
                                                identifier = identifier,
                                                row_status = (
                                                                ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) > 0
                                                                    && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0) > 0)
                                                                    ? "Row added successfully!"
                                                                    : (((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["Node_Code"].ToString().Trim())) : 0) <= 0)
                                                                      )
                                                                    ? ("Invalid SKU Code: " + drow["SKU_Code"].ToString() + " and Node Code:" + drow["Node_Code"].ToString() + "")
                                                                    : ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        ? "Invalid SKU Code: " + drow["SKU_Code"].ToString() + ""
                                                                        : "Invalid Node Code: " + drow["Node_Code"].ToString() + ""
                                                                      )
                                                              )
                                            }
                                        ).ToList();

                List<Transaction> _lstTransTemp = _lstTransAllTemp.Where(rec => (rec.sku_id > 0 && rec.node_id > 0)).Distinct().ToList<Transaction>();

                rows_failed = (_lstTransAllTemp.Count() - _lstTransTemp.Count());

                if (dt != null) dt.Dispose();

                item.status_text = "Temp Transaction Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.Transaction().CreateTempTransactionTable(tmpTable);

                if (_lstTransTemp != null)
                {
                    int cnt1 = _lstTransTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<Transaction> _lstInsert = new List<Transaction>();
                            _lstInsert.AddRange(_lstTransTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempTransaction _tempTrans = new TempTransaction();
                            if (_tempTrans.transactions == null) _tempTrans.transactions = new List<Transaction>();

                            _tempTrans.transactions.AddRange(_lstInsert);
                            _tempTrans.table_name = tmpTable;

                            Dao.Transaction().InsertTempBatch(_tempTrans);
                        }
                    }
                }

                item.status_text = "Temp Transaction Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                List<Transaction> _lstBulkInsert = Dao.Transaction().SelectTmpTransactionNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<Transaction> _lstDuplicate = Dao.Transaction().SelectTmpTransactionDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (Transaction _dupTran in _lstDuplicate)
                            {
                                List<Transaction> _chkDulpicate = _lstBulkInsert.Where(rec => rec.sku_id == _dupTran.sku_id
                                                                                    && rec.node_id == _dupTran.node_id
                                                                                        && rec.transaction_date == _dupTran.transaction_date).ToList<Transaction>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dupTran);
                                ////else
                                ////{
                                _dupTran.row_status = "Duplicate! Record already exist for SKU Code, Node Code and Transaction Date!";
                                _lstExcelStatus.Add(_dupTran);
                                ////}
                            }
                        }
                    }
                }


                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstTransAllTemp.Where(rec => (rec.sku_id <= 0 || rec.node_id <= 0)).Distinct().ToList<Transaction>());
                List<Transaction> _lstDuplicate1 = Dao.Transaction().SelectTransactionDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        _lstDuplicate1.Select(x => { x.row_status = "Duplicate! Record already exist for SKU Code, Node Code and Transaction Date!"; return x; }).ToList().Where(x => x.row_status == "duplicate");
                        _lstExcelStatus.AddRange(_lstDuplicate1);
                    }
                }
                foreach (Transaction drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int cnt1 = _lstBulkInsert.Count();

                    rows_succeeded = cnt1;
                    rows_duplicated = (_lstTransTemp.Count() - cnt1);

                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<Transaction> _lstInsert = new List<Transaction>();
                            _lstInsert.AddRange(_lstBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            Dao.Transaction().InsertBatch(_lstInsert);
                        }
                    }
                }

                List<Transaction> _lstBatchTransIds = Dao.Transaction().SelectBatchInsertedTransactionIds(identifier).ToList();

                if (_lstBatchTransIds != null)
                {
                    int cnt2 = _lstBatchTransIds.Count();
                    if (cnt2 > 0)
                    {
                        int loopCount2 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt2) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset1 = 0; offset1 < loopCount2; offset1++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt2 - (offset1 * bulkInsLimit)) ? (cnt2 - (offset1 * bulkInsLimit)) : bulkInsLimit);
                            List<Transaction> _lstInsert = new List<Transaction>();
                            _lstInsert.AddRange(_lstBatchTransIds.GetRange(offset1 * bulkInsLimit, fetchCnt));

                            if (_lstInsert != null)
                                if (_lstInsert.Count > 0)
                                    Dao.Transaction().InsertUpdateTransactionBatch(_lstInsert);
                        }
                    }
                }

                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                Dao.Transaction().DropTempTransactionTable(tmpTable);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                ////int cnt = updateList.Count(); //rows_succeeded;////Dao.Transaction().GetTransactionCountForImport(date);
                ////int limit = 5;
                ////int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                ////item.status_text = "Update Transaction DB Sync START";
                ////Common.UpdateLog(item, logfilePath, true);

                ////for (int offset = 0; offset < loopCount; offset++)
                ////{
                ////    Dao.Transaction().UpdateTransactionDBSync(updateList.Skip(limit * offset).Take(limit).ToList());
                ////}
                ////item.status_text = "Update Transaction DB Sync END";
                ////Common.UpdateLog(item, logfilePath, true);

                item.status = 1;
                item.comment = "{'message':'Refreshing Collections in process!'}";
                item.status_text = "Refreshing Collections in process!";
                Common.UpdateLog(item, logfilePath);

                RefreshMongoDb("transaction", date);
                RefreshMongoDb("replenishment", date, item, identifier);

                item.status = 1;
                item.comment = "{'message':'Refreshing Collections completes!'}";
                item.status_text = "Refreshing Collections completes!";
                Common.UpdateLog(item, logfilePath);

                Dao.Transaction().DeleteUpdateTransactionBatch(identifier);

                ////item.status_text = "Update Transaction Mongo Sync START";
                ////Common.UpdateLog(item, logfilePath, true);

                ////for (int offset = 0; offset < loopCount; offset++)
                ////{
                ////    Dao.Transaction().UpdateTransactionMongoSync(updateList.Skip(limit * offset).Take(limit).ToList());
                ////}
                ////item.status_text = "Update Transaction Mongo Sync END";
                ////Common.UpdateLog(item, logfilePath, true);

                item.log_file = ("Logs\\Transaction\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed and " + rows_duplicated + " rows duplicated!'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed and " + rows_duplicated + " rows duplicated!";
                Common.UpdateLog(item, logfilePath);

                new Worker().UploadSKUGrid(param);
            }
            catch (Exception e)
            {
                _log.Error("UploadTransaction::", e);

                Dao.Transaction().DropTempTransactionTable(tmpTable);

                Dao.Transaction().DeleteUpdateTransactionBatch(identifier);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "ErrorMessage: " + e.Message + " StackTrace: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                ////if (date.ToString().Length > 0)
                ////{
                ////Transaction tdelete = new Transaction();
                ////tdelete.transaction_date = date;
                ////tdelete.db_sync = 0;
                ////Dao.Transaction().DeleteFailedImportTransaction(tdelete);

                ////string collection = Common.Instance.GetEnumValue(Common.CollectionName.TRANSACTION);
                ////MongoHelper<Transaction>.Delete(collection, Query.EQ("transaction_date", date));
                ////}

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        public void UploadSKUGrid(object param)
        {
            try
            {
                RefreshMongoSKUGrid();
            }
            catch (Exception e)
            {
                _log.Error("UploadSKUGrid::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void RefreshMongoTransaction(string date)
        {

            string[] transaction_date = date.Split(' ')[0].Split('-');
            transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
            transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
            string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

            string collection = Common.Instance.GetEnumValue(Common.CollectionName.TRANSACTION);
            MongoHelper<Transaction>.Delete(collection, Query.EQ("transaction_date", transactionDate));
            List<Transaction> lst = Dao.Transaction().SelectByDate(date);
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<Transaction>.Add(collection, lst);
        }

        private void RefreshMongoReplenishment(string date, bool fullImport = false, ImportExportLog item = null, string identifier = null)
        {
            string logfile = "";
            string logfilePath = "";
            int limit = 500;
            int update_limit = 5;

            if (item == null)
            {
                logfile = "Logs\\Transaction\\Transaction_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
                logfilePath = AppSettings.PhysicalPath + logfile;

                item = new ImportExportLog();
                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = "Import";
                item.reference_type = "Transaction";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Refereshing DB Starts.";
                item.comment = "{'message':'Refereshing DB Starts.'}";
                item.created_by = 1;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);
            }

            logfile = item.log_file;
            logfilePath = logfile.Replace(AppSettings.ServerApplication, "");
            logfilePath = (AppSettings.PhysicalPath + logfilePath).Replace("/", "\\");

            string[] transaction_date = date.Split(' ')[0].Split('-');
            transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
            transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
            string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

            string Collection = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);
            string destCollection = Collection + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            try
            {
                MongoCollection<Replenishment> source = MongoHelper<Replenishment>.GetCollection(Collection);
                MongoCollection<Replenishment> dest = MongoHelper<Replenishment>.GetCollection(destCollection);
                List<Replenishment> mongolst = new List<Replenishment>();

                // Incremental Import
                if (!fullImport)
                {
                    item.status = 1;
                    item.comment = "{'message':'Refreshing Replenishment Records!'}";
                    item.status_text = "Refreshing Replenishment Records!";
                    Common.UpdateLog(item, logfilePath);

                    List<Transaction> updateList = Dao.Transaction().GetUpdateTransactionIdsForImport(identifier);
                    int cnt = updateList.Count();
                    int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                    for (int offset = 0; offset < loopCount; offset++)
                    {
                        MapperBatch mapperBatch = new MapperBatch();
                        mapperBatch.date = date;
                        mapperBatch.limit = limit;
                        mapperBatch.offset = limit * offset;

                        Dao.Replenishment().InsertReplenishmentList(mapperBatch);
                    }

                    item.status = 1;
                    item.comment = "{'message':'Refreshing Replenishment Records Successful!'}";
                    item.status_text = "Refreshing Replenishment Records Successful!";
                    Common.UpdateLog(item, logfilePath);

                    ////List<long> udt = updateList.Select(x => x.transaction_id).ToList();
                    ////int uploopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(update_limit)));
                    ////for (int offset = 0; offset < uploopCount; offset++)
                    ////{
                    ////    Dao.Replenishment().UpdateReplenishmentDBSync(udt.Skip(update_limit * offset).Take(update_limit).ToList());
                    ////}

                    item.status = 1;
                    item.comment = "{'message':'Copying to Dummy Collection!'}";
                    item.status_text = "Copying to Dummy Collection!";
                    Common.UpdateLog(item, logfilePath, true);

                    mongolst = Dao.Replenishment().SelectByDate(date);

                    //GetIndexesResult indexes = source.GetIndexes();
                    //foreach (var indx in indexes)
                    //{
                    //    IndexOptionsBuilder options = new IndexOptionsBuilder();
                    //    options.SetName(indx.Name);
                    //    dest.EnsureIndex(indx.Key, options);
                    //}

                    IEnumerable<BsonValue> dimVal = source.Distinct("transfilter_date").OrderByDescending(x => x);
                    //if (dimVal.Count() < AppSettings.MaxTransactionDaysInMongo)
                    //{
                    //    var cursor = source.FindAll();
                    //    if (cursor.Count() > 0)
                    //        dest.InsertBatch(source.FindAll());
                    //}
                    //else
                    //{
                    //    dimVal = dimVal.Take(AppSettings.MaxTransactionDaysInMongo - 1).ToList();
                    //    dest.InsertBatch(source.Find(Query.In("transfilter_date", dimVal)));
                    //}

                    //MongoHelper<Replenishment>.Delete(destCollection, Query.EQ("transaction_date", transactionDate));

                    item.status = 1;
                    item.comment = "{'message':'Copying to Dummy Collection Successful and adding input data to collection!'}";
                    item.status_text = "Copying to Dummy Collection Successful and adding input data to collection!";
                    Common.UpdateLog(item, logfilePath, true);

                    //MongoHelper<Replenishment>.Add(destCollection, mongolst);


                    /// Below Code to be reverted with the commented one

                    if (dimVal.Count() > AppSettings.MaxTransactionDaysInMongo)
                    {
                        dimVal = dimVal.Skip(AppSettings.MaxTransactionDaysInMongo - 1).ToList();
                        source.Remove(Query.In("transfilter_date", dimVal));
                    }

                    //source.DropAllIndexes();
                    MongoHelper<Replenishment>.Delete(Collection, Query.EQ("transaction_date", transactionDate));

                    if (mongolst != null)
                        if (mongolst.Count > 0)
                        {
                            MongoHelper<Replenishment>.Add(Collection, mongolst);
                            ////RefreshMongoDb("sku_chart", date);
                            ////RefreshMongoDb("inventory_turns", date);
                            ////RefreshMongoDb("cntqty_fillrate", date);
                        }

                    /// Above Code to be reverted with the commented one


                    ////for (int offset = 0; offset < uploopCount; offset++)
                    ////{
                    ////    Dao.Replenishment().UpdateReplenishmentMongoSync(udt.Skip(update_limit * offset).Take(update_limit).ToList());
                    ////}
                }
                // Full Import
                else
                {
                    item.status = 1;
                    item.comment = "{'message':'Full Scan in Progress! Copying to Dummy Collection!'}";
                    item.status_text = "Full Scan in Progress! Copying to Dummy Collection!";
                    Common.UpdateLog(item, logfilePath);

                    List<Transaction> dates = Dao.Transaction().SelectTransactionDates(AppSettings.MaxTransactionDaysInMongo);
                    List<Transaction> Updates_dates = new List<Transaction>();

                    foreach (var d in dates)
                    {
                        int cntExists = Dao.Replenishment().GetCountByTransactionDate(d.transaction_date);
                        if (cntExists > 0)
                        {
                            List<Replenishment> lst = Dao.Replenishment().SelectByDate(d.transaction_date);
                            mongolst.AddRange(lst);
                        }
                        else
                        {
                            Transaction t = new Transaction();
                            t.transaction_date = d.transaction_date;
                            Updates_dates.Add(t);
                            List<Transaction> updateList = Dao.Transaction().GetTransactionIdsForImport(d.transaction_date);
                            int cnt = updateList.Count();
                            int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                            for (int offset = 0; offset < loopCount; offset++)
                            {
                                MapperBatch mapperBatch = new MapperBatch();
                                mapperBatch.date = d.transaction_date;
                                mapperBatch.limit = limit;
                                mapperBatch.offset = limit * offset;

                                ////Dao.Replenishment().InsertReplenishmentList(mapperBatch);
                                Dao.Replenishment().InsertReplenishmentListFullSync(mapperBatch);
                            }

                            mongolst.AddRange(Dao.Replenishment().SelectByDate(d.transaction_date));
                        }
                    }

                    ////foreach (var d in Updates_dates)
                    ////{
                    ////    List<Transaction> updateList = Dao.Transaction().GetTransactionIdsForImport(d.transaction_date);
                    ////    int cnt = updateList.Count();
                    ////    int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));
                    ////    List<long> udt = updateList.Select(x => x.transaction_id).ToList();

                    ////    for (int offset = 0; offset < loopCount; offset++)
                    ////    {
                    ////        Dao.Replenishment().UpdateReplenishmentDBSync(udt.Skip(limit * offset).Take(limit).ToList());
                    ////    }
                    ////}

                    //source.DropAllIndexes();
                    source.RemoveAll();
                    MongoHelper<Replenishment>.Add(Collection, mongolst);

                    foreach (var d in Updates_dates)
                    {
                        //RefreshMongoDb("sku_chart", d.transaction_date);
                        //RefreshMongoDb("inventory_turns", d.transaction_date);
                        //RefreshMongoDb("cntqty_fillrate", d.transaction_date);

                        ////List<Transaction> updateList = Dao.Transaction().GetTransactionIdsForImport(d.transaction_date);
                        ////int cnt = updateList.Count();
                        ////int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));
                        ////List<long> udt = updateList.Select(x => x.transaction_id).ToList();

                        ////for (int offset = 0; offset < loopCount; offset++)
                        ////{
                        ////    Dao.Replenishment().UpdateReplenishmentMongoSync(udt.Skip(limit * offset).Take(limit).ToList());
                        ////}
                    }

                    //GetIndexesResult indexes = source.GetIndexes();
                    //foreach (var indx in indexes)
                    //{
                    //    IndexOptionsBuilder options = new IndexOptionsBuilder();
                    //    options.SetName(indx.Name);
                    //    dest.EnsureIndex(indx.Key, options);
                    //}

                    //MongoHelper<Replenishment>.Add(destCollection, mongolst);

                    item.status = 1;
                    item.comment = "{'message':'Copying to Dummy Collection Successful and adding input data to collection!'}";
                    item.status_text = "Copying to Dummy Collection Successful and adding input data to collection!";
                    Common.UpdateLog(item, logfilePath);

                    if (fullImport)
                    {
                        item.status = 1;
                        item.comment = "{'message':'Full Scan Successfully Completed!'}";
                        item.status_text = "Full Scan Successfully Completed!";
                        Common.UpdateLog(item, logfilePath);
                    }

                    object param = new object();
                    new Worker().UploadSKUGrid(param);
                }

                item.status = 1;
                item.comment = "{'message':'Adding input data to collection Successful and drop original collection!'}";
                item.status_text = "Adding input data to collection Successful and drop original collection!";
                Common.UpdateLog(item, logfilePath, true);

                //source.Drop();
                //MongoHelper<Replenishment>.RenameCollection(destCollection, Collection);

                item.status = 1;
                item.comment = "{'message':'Refereshing Records Successful'}";
                item.status_text = "Refereshing Records Successful!";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                _log.Error("RefreshMongoReplenishment::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "ErrorMessage: " + e.Message + " StackTrace: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                if (date.ToString().Length > 0)
                {
                    Replenishment r = new Replenishment();
                    r.transaction_date = date;
                    r.db_sync = 0;
                    Dao.Replenishment().DeleteFailedImportReplenishment(r);
                }

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        public void UploadSkuCategory(object parameter)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\SKUCategory\\SKUCategory_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "SKUCategory_Logfile_" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "SKUCategory\\";
            string zipFileName = "";

            string tmpTable = "tmp_sku_category" + identifier.Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = parameter.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "SKU Category";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                int bulkInsLimit = 100;

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\SKUCategory\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contains any data!'}";
                    item.status_text = "File does not contains any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("sku_code") || !dt.Columns.Contains("category_code"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\SKUCategory\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_succeeded = 0;
                int rows_failed = 0;
                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                ////// For Invalid Check STSRT
                Dictionary<string, long> _allSKUDic = null;
                Dictionary<string, long> _allCategoryDic = null;
                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU list END.";
                    Common.UpdateLog(item, logfilePath, true);

                    item.status_text = "Fetch all Category list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allCategoryDic = Dao.Category().SelectAllDictionary();

                    item.status_text = "Fetch all Category list END.";
                    Common.UpdateLog(item, logfilePath, true);
                }
                ////// For Invalid Check END

                bool wkLimit = true;


                List<SKUCategory> _lstExcelStatus = new List<SKUCategory>();

                List<SKUCategory> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                                            new SKUCategory
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) : 0),
                                                category_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) : 0),
                                                created_by = user,
                                                created_on = DateTime.Now.ToString(),
                                                identifier = identifier,
                                                row_status = (
                                                                ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) : 0) > 0
                                                                    && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) : 0) > 0)
                                                                    ? "Row added successfully!"
                                                                    : "Invalid " + ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["sku_code"].ToString().Trim())) : 0) <= 0 ? " SKU_Code: " + drow["sku_code"].ToString() + " " : "") + "  " +
                                                                    " " + ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allCategoryDic, drow["category_code"].ToString().Trim())) : 0) <= 0 ? " Category_Code: " + drow["category_code"].ToString() + " " : "") + " !"
                                                              )
                                            }
                                        ).ToList();

                List<SKUCategory> _lstTemp = _lstAllTemp.Where(rec => (rec.sku_id > 0 && rec.category_id > 0)).Distinct().ToList<SKUCategory>();

                rows_failed = (_lstAllTemp.Count() - _lstTemp.Count());

                if (dt != null) dt.Dispose();

                item.status_text = "Temp SKUCategory Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.SKUCategory().CreateTempTable(tmpTable);

                if (_lstTemp != null)
                {
                    int cnt1 = _lstTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<SKUCategory> _lstInsert = new List<SKUCategory>();
                            _lstInsert.AddRange(_lstTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempSKUCategory _tempTBL = new TempSKUCategory();
                            if (_tempTBL.skuCategory == null) _tempTBL.skuCategory = new List<SKUCategory>();

                            _tempTBL.skuCategory.AddRange(_lstInsert);
                            _tempTBL.table_name = tmpTable;

                            Dao.SKUCategory().InsertTempBatch(_tempTBL);
                        }
                    }
                }

                item.status_text = "Temp SKUCategory Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                List<SKUCategory> _lstBulkInsert = Dao.SKUCategory().SelectTmpNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<SKUCategory> _lstDuplicate = Dao.SKUCategory().SelectTmpDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (SKUCategory _dup in _lstDuplicate)
                            {
                                List<SKUCategory> _chkDulpicate = _lstBulkInsert.Where(rec => rec.sku_id == _dup.sku_id && rec.category_id == _dup.category_id).ToList<SKUCategory>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dup);

                                _dup.row_status = "Duplicate! Record already exist for SKU Code and Category Code!";
                                _lstExcelStatus.Add(_dup);
                            }
                        }
                    }
                }


                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstAllTemp.Where(rec => (rec.sku_id <= 0 || rec.category_id <= 0)).Distinct().ToList<SKUCategory>());

                List<SKUCategory> _lstDuplicate1 = Dao.SKUCategory().SelectDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        _lstDuplicate1.Select(x => { x.row_status = "Duplicate! Record already exist for SKU Code and Category Code!"; return x; }).ToList().Where(x => x.row_status == "duplicate");
                        _lstExcelStatus.AddRange(_lstDuplicate1);
                    }
                }

                foreach (SKUCategory drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);

                Dao.SKUCategory().DropTempTable(tmpTable);

                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int cnt = _lstBulkInsert.Count();

                    rows_succeeded = cnt;
                    rows_duplicated = (_lstTemp.Count() - cnt);

                    if (cnt > 0)
                    {
                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset = 0; offset < loopCount; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<SKUCategory> _lstInsert = new List<SKUCategory>();
                            _lstInsert.AddRange(_lstBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            if (_lstInsert.Count > 0)
                                Dao.SKUCategory().InsertBatch(_lstInsert);
                        }
                    }
                }
                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);

                RefreshMongoDb("sku_category");

                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);

                item.log_file = ("Logs\\SKUCategory\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                Dao.SKUCategory().DropTempTable(tmpTable);

                _log.Error("UploadSKUCategory::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void RefreshMongoSKUCategory()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CATEGORY);
            MongoHelper<SKUCategory>.RemoveAll(collection);
            List<SKUCategory> lst = Dao.SKUCategory().SelectList();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<SKUCategory>.Add(collection, lst);
        }

        public void UploadNodeSkuMapping(object parameter)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");

            string logfile = "Logs\\NodeSkuMapping\\NodeSkuMapping_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "NodeSkuMapping_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "NodeSkuMapping\\";
            string zipFileName = "";

            string tmpTable = "tmp_node_sku_mapping" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = parameter.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "Node SKU Mapping";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File...";
                item.comment = "{'message':'Import In Process. Validating File...'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                int bulkInsLimit = 100;

                item.status_text = "Import Data From Excel START";
                Common.UpdateLog(item, logfilePath, true);

                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);

                item.status_text = "Import Data From Excel END";
                Common.UpdateLog(item, logfilePath, true);

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\NodeSkuMapping\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contains any data!'}";
                    item.status_text = "File does not contains any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("SKU_code") || !dt.Columns.Contains("To_Node_Code") || !dt.Columns.Contains("From_Node_Code")
                    || !dt.Columns.Contains("Effective_Date"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\NodeSkuMapping\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                int rows_succeeded = 0;
                int rows_failed = 0;
                int rows_added = 0;
                int rows_updated = 0;
                int rows_duplicated = 0;

                ////// For Invalid Check START
                Dictionary<string, long> _allSKUDic = null;
                Dictionary<string, long> _allNodeDic = null;

                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU list END.";
                    Common.UpdateLog(item, logfilePath, true);

                    item.status_text = "Fetch all Node list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allNodeDic = Dao.Node().SelectAllDictionary();

                    item.status_text = "Fetch all Node list END.";
                    Common.UpdateLog(item, logfilePath, true);
                }
                ////// For Invalid Check END

                bool wkLimit = true;

                List<NodeSKUMapping> _lstExcelStatus = new List<NodeSKUMapping>();

                List<NodeSKUMapping> _lstAllTemp = dt.AsEnumerable().Select(drow =>
                                            new NodeSKUMapping
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) : 0),
                                                receiver_node_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) : 0),
                                                supplier_node_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) : 0),
                                                effective_date = Convert.ToDateTime(Convert.ToDateTime(drow["Effective_Date"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss")),
                                                created_by = user,
                                                identifier = identifier,
                                                activate_auto_dbm = drow["Activate_Auto_DBM"] != null ? drow["Activate_Auto_DBM"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["Activate_Auto_DBM"].ToString().Trim()) : 0 : 0,
                                                row_status = (
                                                                ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) : 0) > 0
                                                                    && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) : 0) > 0
                                                                    && (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) : 0) > 0)
                                                                    ? "Row added successfully!"
                                                                    : "Invalid " + ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_code"].ToString().Trim())) : 0) <= 0 ? " SKU_Code: " + drow["SKU_code"].ToString() + " " : "") + "  " +
                                                                    " " + ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["To_Node_Code"].ToString().Trim())) : 0) <= 0 ? " To_Node_Code: " + drow["To_Node_Code"].ToString() + " " : "") + " " +
                                                                    " " + ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allNodeDic, drow["From_Node_Code"].ToString().Trim())) : 0) <= 0 ? " From_Node_Code: " + drow["From_Node_Code"].ToString() + " " : "") + " !"
                                                              )
                                            }
                                        ).ToList();

                List<NodeSKUMapping> _lstTemp = _lstAllTemp.Where(rec => (rec.sku_id > 0 && rec.receiver_node_id > 0 && rec.supplier_node_id > 0)).Distinct().ToList<NodeSKUMapping>();

                rows_failed = (_lstAllTemp.Count() - _lstTemp.Count());

                if (dt != null) dt.Dispose();

                item.status_text = "Temp NodeSKUMapping Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.NodeSKUMapping().CreateTempTable(tmpTable);

                if (_lstTemp != null)
                {
                    int cnt1 = _lstTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<NodeSKUMapping> _lstInsert = new List<NodeSKUMapping>();
                            _lstInsert.AddRange(_lstTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempNodeSKUMapping _tempTBL = new TempNodeSKUMapping();
                            if (_tempTBL.nodeSKUMapping == null) _tempTBL.nodeSKUMapping = new List<NodeSKUMapping>();

                            _tempTBL.nodeSKUMapping.AddRange(_lstInsert);
                            _tempTBL.table_name = tmpTable;

                            Dao.NodeSKUMapping().InsertTempBatch(_tempTBL);
                        }
                    }
                }

                item.status_text = "Temp NodeSKUMapping Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                List<NodeSKUMapping> _lstBulkInsert = Dao.NodeSKUMapping().SelectTmpNotDuplicate(tmpTable);
                if (_lstBulkInsert != null)
                {
                    List<NodeSKUMapping> _lstDuplicate = Dao.NodeSKUMapping().SelectTmpDuplicate(tmpTable);

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (NodeSKUMapping _dup in _lstDuplicate)
                            {
                                List<NodeSKUMapping> _chkDulpicate = _lstBulkInsert.Where(rec => rec.sku_id == _dup.sku_id
                                                                                            && rec.receiver_node_id == _dup.receiver_node_id
                                                                                                && rec.supplier_node_id == _dup.supplier_node_id
                                                                                                    && rec.effective_date == _dup.effective_date).ToList<NodeSKUMapping>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dup);

                                _dup.row_status = "Duplicate! Record already exist for SKU Code, To Node Code, From Node Code and Effective Date!";
                                _lstExcelStatus.Add(_dup);
                            }
                        }
                    }
                }


                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstAllTemp.Where(rec => (rec.sku_id <= 0 || rec.receiver_node_id <= 0 || rec.supplier_node_id <= 0)).Distinct().ToList<NodeSKUMapping>());

                List<NodeSKUMapping> _lstDuplicate1 = Dao.NodeSKUMapping().SelectDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        _lstDuplicate1.Select(x => { x.row_status = "Duplicate! Record already exist for SKU Code, To Node Code, From Node Code and Effective Date!"; return x; }).ToList().Where(x => x.row_status == "duplicate");
                        _lstExcelStatus.AddRange(_lstDuplicate1);
                    }
                }

                foreach (NodeSKUMapping drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);

                Dao.NodeSKUMapping().DropTempTable(tmpTable);

                item.status_text = "Bulk Insert START";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int cnt = _lstBulkInsert.Count();

                    rows_succeeded = cnt;
                    rows_duplicated = (_lstTemp.Count() - cnt);

                    if (cnt > 0)
                    {
                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));

                        for (int offset = 0; offset < loopCount; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<NodeSKUMapping> _lstInsert = new List<NodeSKUMapping>();
                            _lstInsert.AddRange(_lstBulkInsert.GetRange(offset * bulkInsLimit, fetchCnt));

                            if (_lstInsert.Count > 0)
                                Dao.NodeSKUMapping().InsertBatch(_lstInsert);
                        }
                    }
                }
                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);


                item.status_text = "Refreshing Mongo START";
                Common.UpdateLog(item, logfilePath, true);

                RefreshMongoDb("node_sku_mapping");

                item.status_text = "Refreshing Mongo END";
                Common.UpdateLog(item, logfilePath, true);


                item.log_file = ("Logs\\NodeSkuMapping\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed! and " + rows_duplicated + " duplicated.";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                Dao.NodeSKUMapping().DropTempTable(tmpTable);

                _log.Error("UploadNodeSKUMapping::", e);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing.";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "Error: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void RefreshMongoNodeSkuMapping()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.NODE_SKU_MAPPING);
            MongoHelper<NodeSKUMapping>.RemoveAll(collection);
            List<NodeSKUMapping> lst = Dao.NodeSKUMapping().SelectList();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<NodeSKUMapping>.Add(collection, lst);
        }

        public void RefreshAllTempCollections()
        {
            List<Transaction> dates = Dao.Transaction().SelectTransactionDates(AppSettings.MaxTransactionDaysInMongo);

            foreach (var d in dates)
            {
                RefreshMongoSKUChart(d.transaction_date);
                RefreshInventoryTurns(d.transaction_date);
                RefreshMongoCntQtyFillRate(d.transaction_date);
            }
        }

        private void RefreshMongoSKUChart(string date)
        {
            string SkuChartCollection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART);
            string NodeSkuChartCollection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_NODE);
            string CategorySkuChartCollection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_CATEGORY);
            string SKUSkuChartCollection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_SKU);

            string[] transaction_date = date.Split(' ')[0].Split('-');
            transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
            transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
            string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

            List<SKUChart> sku_chart_lst = new List<SKUChart>();
            List<SKUChart> node_sku_chart_lst = new List<SKUChart>();
            List<SKUChart> category_sku_chart_lst = new List<SKUChart>();
            List<SKUChart> sku_sku_chart_lst = new List<SKUChart>();

            SKUChart lst = Dao.SKUChart().Select(date);
            if (lst != null)
                sku_chart_lst.Add(lst);

            List<SKUChart> nlst = Dao.SKUChart().SelectByNode(date);
            if (nlst != null)
                node_sku_chart_lst.AddRange(nlst);

            List<SKUChart> clst = Dao.SKUChart().SelectByCategory(date);
            if (clst != null)
                category_sku_chart_lst.AddRange(clst);

            List<SKUChart> slst = Dao.SKUChart().SelectBySKU(date);
            if (slst != null)
                sku_sku_chart_lst.AddRange(slst);

            if (sku_chart_lst.Count > 0)
            {
                MongoHelper<SKUChart>.Delete(SkuChartCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<SKUChart>.Add(SkuChartCollection, sku_chart_lst);
            }

            if (node_sku_chart_lst.Count > 0)
            {
                MongoHelper<SKUChart>.Delete(NodeSkuChartCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<SKUChart>.Add(NodeSkuChartCollection, node_sku_chart_lst);
            }

            if (category_sku_chart_lst.Count > 0)
            {
                MongoHelper<SKUChart>.Delete(CategorySkuChartCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<SKUChart>.Add(CategorySkuChartCollection, category_sku_chart_lst);
            }

            if (sku_sku_chart_lst.Count > 0)
            {
                MongoHelper<SKUChart>.Delete(SKUSkuChartCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<SKUChart>.Add(SKUSkuChartCollection, sku_sku_chart_lst);
            }
        }

        private void RefreshMongoSKUGrid()
        {
            Transaction maxDate = Dao.Transaction().SelectMaxTransactionDate();
            maxDate.transaction_date = Convert.ToDateTime(maxDate.transaction_date).ToString("yyyy-MM-dd HH:mm:ss");
            maxDate.avg_date = Convert.ToDateTime(maxDate.avg_date).ToString("yyyy-MM-dd HH:mm:ss");

            string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_GRID);
            MongoHelper<SKUGrid>.RemoveAll(collection);

            List<Transaction> transaction_lst = Dao.Transaction().SelectSKUNodeGroup();

            if (transaction_lst != null)
            {
                if (transaction_lst.Count > 0)
                {
                    List<Transaction> _lstTran = transaction_lst.Select(drow =>
                                            new Transaction
                                            {
                                                sku_id = drow.sku_id,
                                                node_id = drow.node_id,
                                                transaction_date = maxDate.transaction_date,
                                                avg_date = maxDate.avg_date
                                            }
                                        ).ToList();

                    int bulkInsLimit = 500;
                    if (_lstTran != null)
                    {
                        int cnt1 = _lstTran.Count();
                        if (cnt1 > 0)
                        {
                            List<SKUGrid> _LstskuGridAll = new List<SKUGrid>();

                            int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                            for (int offset = 0; offset < loopCount1; offset++)
                            {
                                int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                                List<Transaction> _lstInsert = new List<Transaction>();
                                _lstInsert.AddRange(_lstTran.GetRange(offset * bulkInsLimit, fetchCnt));

                                TempSKUGRID _tempSkuGrid = new TempSKUGRID();
                                _tempSkuGrid.transaction_date = maxDate.transaction_date;
                                _tempSkuGrid.avg_date = maxDate.avg_date;

                                if (_tempSkuGrid.transaction_list == null) _tempSkuGrid.transaction_list = new List<Transaction>();
                                _tempSkuGrid.transaction_list.AddRange(_lstInsert);

                                List<SKUGrid> _LstskuGrid = Dao.SKUGrid().SelectSKUGridList(_tempSkuGrid);

                                if (_LstskuGrid != null)
                                    if (_LstskuGrid.Count > 0)
                                        _LstskuGridAll.AddRange(_LstskuGrid);
                            }

                            if (_LstskuGridAll != null)
                                if (_LstskuGridAll.Count > 0)
                                    MongoHelper<SKUGrid>.Add(collection, _LstskuGridAll);
                        }
                    }
                }
            }


            //if (transaction_lst != null)
            //    foreach (Transaction t in transaction_lst)
            //    {
            //        //SKUGrid grid = MongoHelper<SKUGrid>.Single(collection, Query.And(Query.EQ("sku_id", t.sku_id), Query.EQ("node_id", t.node_id)));
            //        //
            //        //if (grid == null)
            //        //{
            //        t.transaction_date = maxDate.transaction_date;
            //        t.avg_date = maxDate.avg_date;

            //        SKUGrid item = Dao.SKUGrid().Select(t);
            //        if (item != null)
            //            MongoHelper<SKUGrid>.Add(collection, item);
            //        //}
            //    }
        }

        private void RefreshMongoDBM()
        {
            string collection = Common.Instance.GetEnumValue(Common.CollectionName.DYNAMIC_BUFFER_MANAGEMENT);
            MongoHelper<SKUCategory>.RemoveAll(collection);
            List<DynamicBufferManagement> lst = Dao.DynamicBufferManagement().SelectAll();
            if (lst != null)
                if (lst.Count > 0)
                    MongoHelper<DynamicBufferManagement>.Add(collection, lst);
        }

        private void RefreshInventoryTurns(string date)
        {
            string InvTurnsCollection = Common.Instance.GetEnumValue(Common.CollectionName.INVENTORY_TURNS);
            string InvTurnsSKUCollection = Common.Instance.GetEnumValue(Common.CollectionName.INVENTORY_TURNS_SKU);

            string[] transaction_date = date.Split(' ')[0].Split('-');
            transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
            transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
            string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

            List<InventoryTurns> inv_turns_lst = new List<InventoryTurns>();
            List<InventoryTurns> inv_turns_sku_lst = new List<InventoryTurns>();

            List<InventoryTurns> lst = Dao.InventoryTurns().Select(date);
            if (lst != null)
                inv_turns_lst.AddRange(lst);

            List<InventoryTurns> slst = Dao.InventoryTurns().SelectBySKU(date);
            if (slst != null)
                inv_turns_sku_lst.AddRange(slst);

            if (inv_turns_lst.Count > 0)
            {
                MongoHelper<InventoryTurns>.Delete(InvTurnsCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<InventoryTurns>.Add(InvTurnsCollection, inv_turns_lst);
            }

            if (inv_turns_sku_lst.Count > 0)
            {
                MongoHelper<InventoryTurns>.Delete(InvTurnsSKUCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<InventoryTurns>.Add(InvTurnsSKUCollection, inv_turns_sku_lst);
            }
        }

        private void RefreshMongoCntQtyFillRate(string date)
        {
            string CntqtyFillrateCollection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE);
            string CntqtyFillrateNodeCollection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_NODE);
            string CntqtyFillrateCategoryCollection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_CATEGORY);
            string CntqtyFillrateSKUCollection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_SKU);

            string[] transaction_date = date.Split(' ')[0].Split('-');
            transaction_date[2] = transaction_date[2].IndexOf("0") == 0 ? transaction_date[2].Substring(1) : transaction_date[2];
            transaction_date[1] = transaction_date[1].IndexOf("0") == 0 ? transaction_date[1].Substring(1) : transaction_date[1];
            string transactionDate = transaction_date[1] + "/" + transaction_date[2] + "/" + transaction_date[0] + " 12:00:00 AM";

            List<CntQtyFillRateChart> cntqty_fillrate_lst = new List<CntQtyFillRateChart>();
            List<CntQtyFillRateChart> cntqty_fillrate_node_lst = new List<CntQtyFillRateChart>();
            List<CntQtyFillRateChart> cntqty_fillrate_category_lst = new List<CntQtyFillRateChart>();
            List<CntQtyFillRateChart> cntqty_fillrate_sku_lst = new List<CntQtyFillRateChart>();

            List<CntQtyFillRateChart> lst = Dao.CntQtyFillRateChart().Select(date);
            if (lst != null)
                cntqty_fillrate_lst.AddRange(lst);

            List<CntQtyFillRateChart> nlst = Dao.CntQtyFillRateChart().SelectByNode(date);
            if (nlst != null)
                cntqty_fillrate_node_lst.AddRange(nlst);

            List<CntQtyFillRateChart> clst = Dao.CntQtyFillRateChart().SelectByCategory(date);
            if (clst != null)
                cntqty_fillrate_category_lst.AddRange(clst);

            List<CntQtyFillRateChart> slst = Dao.CntQtyFillRateChart().SelectBySKU(date);
            if (slst != null)
                cntqty_fillrate_sku_lst.AddRange(slst);

            if (cntqty_fillrate_lst.Count > 0)
            {
                MongoHelper<CntQtyFillRateChart>.Delete(CntqtyFillrateCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<CntQtyFillRateChart>.Add(CntqtyFillrateCollection, cntqty_fillrate_lst);
            }

            if (cntqty_fillrate_node_lst.Count > 0)
            {
                MongoHelper<CntQtyFillRateChart>.Delete(CntqtyFillrateNodeCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<CntQtyFillRateChart>.Add(CntqtyFillrateNodeCollection, cntqty_fillrate_node_lst);
            }

            if (cntqty_fillrate_category_lst.Count > 0)
            {
                MongoHelper<CntQtyFillRateChart>.Delete(CntqtyFillrateCategoryCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<CntQtyFillRateChart>.Add(CntqtyFillrateCategoryCollection, cntqty_fillrate_category_lst);
            }

            if (cntqty_fillrate_sku_lst.Count > 0)
            {
                MongoHelper<CntQtyFillRateChart>.Delete(CntqtyFillrateSKUCollection, Query.EQ("transaction_date", transactionDate));
                MongoHelper<CntQtyFillRateChart>.Add(CntqtyFillrateSKUCollection, cntqty_fillrate_sku_lst);
            }
        }


        public void UploadBOM(object param)
        {
            string identifier = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");
            string logfile = "Logs\\BOM\\BOM_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".txt";
            string logfilePath = AppSettings.PhysicalPath + logfile;
            string excellogfile = "BOM_Logfile_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
            string excellogfilepath = AppSettings.LogFolder + "\\BOM\\";
            string zipFileName = "";

            string tmpTable = "tmp_bom" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_');

            ImportExportLog item = new ImportExportLog();
            try
            {
                string[] parameters = param.ToString().Split(':');
                long user = Convert.ToInt64(parameters[0].ToString());
                string fileName = parameters[1].ToString();

                item.log_file = logfile.Replace("\\", "/");
                item.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                item.reference_title = fileName;
                item.reference_type = "BOM";
                item.type = "Import";
                item.status = 0;
                item.status_text = "Import In Process. Validating File.";
                item.comment = "{'message':'Import In Process. Validating File.'}";
                item.created_by = user;
                item.import_export_log_id = Common.InsertLog(logfilePath, item);

                _log.Info("UploadBOM: Excel to Datatable Conversion STARTS!");
                DataTable dt = Common.Instance.ImportDataFromExcel(AppSettings.FileUploadPath, fileName);
                _log.Info("UploadBOM: Excel to Datatable Conversion END!");

                ISheet sheet1;
                ICellStyle style;
                ICellStyle headerStyle;

                Download.PrepareWorkbook("Logs", "Logs", out sheet1, out headerStyle, out style);

                int row_counter = 0;
                IRow headerRow = sheet1.CreateRow(row_counter);

                Download.CreateCell(headerRow, headerStyle, 0, "Row Number", true);
                Download.CreateCell(headerRow, headerStyle, 1, "Row Status", true);

                if (dt == null)
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\BOM\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'File does not contain any data!'}";
                    item.status_text = "File does not contain any data!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }
                else if (!dt.Columns.Contains("SKU_Code") || !dt.Columns.Contains("Parent_SKU_Code") || !dt.Columns.Contains("Quantity"))
                {
                    IRow row = sheet1.CreateRow(++row_counter);
                    Download.CreateCell(row, style, 0, "1", false);
                    Download.CreateCell(row, style, 1, item.status_text, false);
                    zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                    item.log_file = ("Logs\\BOM\\" + zipFileName).Replace("\\", "/");
                    item.status = 1;
                    item.comment = "{'message':'Invalid File Template!'}";
                    item.status_text = "Invalid File Template!";
                    Common.UpdateLog(item, logfilePath);
                    return;
                }

                item.status = 1;
                item.comment = "{'message':'Validation Complete. Data Processing in Progress.'}";
                item.status_text = "Validation Complete. Data Processing in Progress.";
                Common.UpdateLog(item, logfilePath);

                bool wkLimit = true;
                int rows_succeeded = 0;
                int rows_failed = 0;
                int rows_duplicated = 0;

                int bulkInsLimit = 100;

                ////// For Invalid Check START
                Dictionary<string, long> _allSKUDic = null;
                Dictionary<string, long> _allBOMDic = null;

                if (dt.Rows.Count > 0)
                {
                    item.status_text = "Fetch all SKU list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allSKUDic = Dao.SKU().SelectAllDictionary();

                    item.status_text = "Fetch all SKU list END.";
                    Common.UpdateLog(item, logfilePath, true);


                    item.status_text = "Fetch all BOM list START.";
                    Common.UpdateLog(item, logfilePath, true);

                    _allBOMDic = Dao.BOM().SelectAllDictionary();

                    item.status_text = "Fetch all BOM list END.";
                    Common.UpdateLog(item, logfilePath, true);


                }
                ////// For Invalid Check END

                List<BOM> _lstExcelStatus = new List<BOM>();

                List<BOM> _lstBOMAllTemp = dt.AsEnumerable().Select(drow =>
                                            new BOM
                                            {
                                                row_index = dt.Rows.IndexOf(drow) + 1,
                                                sku_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0),
                                                parent_id = (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["Parent_SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["Parent_SKU_Code"].ToString().Trim())) : 0),

                                                parent_valid = (string.IsNullOrEmpty(drow["Parent_SKU_Code"].ToString().Trim()) ? true : (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["Parent_SKU_Code"].ToString().Trim())) ? true : false)),

                                                sku_code = drow["SKU_Code"].ToString().Trim(),
                                                parent_sku_code = drow["Parent_SKU_Code"].ToString().Trim(),

                                                ////quantity = drow["Quantity"].ToString().Trim().Length > 0 ? Convert.ToInt32(drow["Quantity"].ToString().Trim()) : 0,
                                                quantity = Common.Instance.ConvertToDouble(drow["Quantity"].ToString().Trim()),

                                                created_by = user,
                                                identifier = identifier,
                                                row_status = (
                                                                ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) > 0
                                                                    && (string.IsNullOrEmpty(drow["Parent_SKU_Code"].ToString().Trim()) ? true : (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["Parent_SKU_Code"].ToString().Trim())) ? true : false)) == true)
                                                                    ? "Row added successfully!"
                                                                    : (((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        && (string.IsNullOrEmpty(drow["Parent_SKU_Code"].ToString().Trim()) ? true : (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["Parent_SKU_Code"].ToString().Trim())) ? true : false)) == false)
                                                                      )
                                                                    ? ("Invalid SKU Code: " + drow["SKU_Code"].ToString() + " and Parent SKU Code:" + drow["Parent_SKU_Code"].ToString() + "")
                                                                    : ((!string.IsNullOrEmpty(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) ? Convert.ToInt64(Common.Instance.getDicValue(_allSKUDic, drow["SKU_Code"].ToString().Trim())) : 0) <= 0
                                                                        ? "Invalid SKU Code: " + drow["SKU_Code"].ToString() + ""
                                                                        : "Invalid Parent SKU Code: " + drow["Parent_SKU_Code"].ToString() + ""
                                                                      )
                                                              )
                                            }
                                        ).ToList();


                List<BOM> _lstBOMAllTemp1 = _lstBOMAllTemp.Where(rec => (rec.sku_id > 0 && rec.parent_valid == true)).Distinct().ToList<BOM>().OrderBy(x => x.row_index).ToList();

                List<BOM> _lstBOMTemp = _lstBOMAllTemp.Select(x =>
                                        {
                                            x.parent_valid = x.parent_valid == true ? true : ((
                                                (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allBOMDic, x.parent_sku_code.Trim())) || _lstBOMAllTemp1.Where(rec => rec.sku_id == x.parent_id).ToList<BOM>().Count > 0)
                                                ? true : false));

                                            x.row_status = (x.parent_valid == true ? true : ((
                                                 (!string.IsNullOrEmpty(Common.Instance.getDicValue(_allBOMDic, x.parent_sku_code.Trim())) || _lstBOMAllTemp1.Where(rec => rec.sku_id == x.parent_id).ToList<BOM>().Count > 0)
                                                 ? true : false))) ? x.row_status : "Parent SKU Code (" + x.parent_sku_code.Trim() + ") not defined in BOM!";

                                            return x;
                                        }).ToList().Where(rec => (rec.sku_id > 0 && rec.parent_valid == true)).ToList<BOM>();


                rows_failed = (_lstBOMAllTemp.Count() - _lstBOMTemp.Count());

                if (dt != null) dt.Dispose();

                item.status_text = "Temp BOM Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                Dao.BOM().CreateTempBOMTable(tmpTable);

                if (_lstBOMTemp != null)
                {
                    int cnt1 = _lstBOMTemp.Count();
                    if (cnt1 > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(bulkInsLimit)));
                        for (int offset = 0; offset < loopCount1; offset++)
                        {
                            int fetchCnt = (bulkInsLimit > (cnt1 - (offset * bulkInsLimit)) ? (cnt1 - (offset * bulkInsLimit)) : bulkInsLimit);

                            List<BOM> _lstInsert = new List<BOM>();
                            _lstInsert.AddRange(_lstBOMTemp.GetRange(offset * bulkInsLimit, fetchCnt));

                            TempBOM _tempBOMs = new TempBOM();
                            if (_tempBOMs.boms == null) _tempBOMs.boms = new List<BOM>();

                            _tempBOMs.boms.AddRange(_lstInsert);
                            _tempBOMs.table_name = tmpTable;

                            Dao.BOM().InsertTempBatch(_tempBOMs);
                        }
                    }
                }

                item.status_text = "Temp BOM Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                List<BOM> _lstBulkInsert = Dao.BOM().SelectTmpBOMNotDuplicate(tmpTable).OrderBy(x => x.row_index).ToList();
                if (_lstBulkInsert != null)
                {
                    List<BOM> _lstDuplicate = Dao.BOM().SelectTmpBOMDuplicate(tmpTable).OrderBy(x => x.row_index).ToList();

                    if (_lstDuplicate != null)
                    {
                        if (_lstDuplicate.Count > 0)
                        {
                            foreach (BOM _dupBOM in _lstDuplicate)
                            {
                                List<BOM> _chkDulpicate = _lstBulkInsert.Where(rec => rec.sku_id == _dupBOM.sku_id && rec.parent_id == _dupBOM.parent_id).ToList<BOM>();

                                if (_chkDulpicate.Count <= 0)
                                    _lstBulkInsert.Add(_dupBOM);
                                ////else
                                ////{
                                _dupBOM.row_status = "Duplicate! Record already exist for SKU Code and Parent SKU Code!";
                                _lstExcelStatus.Add(_dupBOM);
                                ////}

                            }
                        }
                    }
                }

                item.status_text = "Write Excel Sheet Status START";
                Common.UpdateLog(item, logfilePath, true);
                //////// For Excel Sheet Status START
                _lstExcelStatus.AddRange(_lstBOMAllTemp.Where(rec => (rec.sku_id <= 0 || rec.parent_valid == false)).Distinct().ToList<BOM>());

                List<BOM> _lstDuplicate1 = Dao.BOM().SelectBOMDuplicate(tmpTable);
                if (_lstDuplicate1 != null)
                {
                    if (_lstDuplicate1.Count > 0)
                    {
                        _lstDuplicate1.Select(x => { x.row_status = "Duplicate! Record already exist for SKU Code and Parent SKU Code!"; return x; }).ToList().Where(x => x.row_status == "duplicate");
                        _lstExcelStatus.AddRange(_lstDuplicate1);
                    }
                }
                foreach (BOM drow in _lstExcelStatus.OrderBy(x => x.row_index).ToList())
                {
                    ++row_counter;
                    int rowcnt = row_counter;

                    IRow row = null;
                    if (wkLimit && (rowcnt >= 65000))
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        Download.CreateCell(row, style, 1, "WorkSheet create limit outside allowable range!", false);

                        wkLimit = false;

                        item.status_text = "WorkSheet create limit outside allowable range! (Row processed " + rowcnt.ToString() + ")";
                        Common.UpdateLog(item, logfilePath, true);
                    }

                    if (wkLimit)
                    {
                        row = sheet1.CreateRow(rowcnt);
                        Download.CreateCell(row, style, 0, drow.row_index.ToString(), false);
                        if (wkLimit)
                            Download.CreateCell(row, style, 1, drow.row_status, false);
                    }
                }
                //////// For Excel Sheet Status END
                item.status_text = "Write Excel Sheet Status END";
                Common.UpdateLog(item, logfilePath, true);


                item.status_text = "Bulk Insert Start";
                Common.UpdateLog(item, logfilePath, true);

                if (_lstBulkInsert != null)
                {
                    int cnt1 = _lstBulkInsert.Count();

                    rows_succeeded = cnt1;
                    rows_duplicated = (_lstBOMTemp.Count() - cnt1);

                    if (cnt1 > 0)
                    {
                        List<BOM> _lstLevel0 = _lstBulkInsert.Where(rec => rec.parent_id == 0).ToList<BOM>();
                        if (_lstLevel0 != null)
                        {
                            int cnt = _lstLevel0.Count();
                            if (_lstLevel0.Count > 0)
                            {
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                                    List<BOM> _lstInsert = new List<BOM>();
                                    _lstInsert.AddRange(_lstLevel0.GetRange(offset * bulkInsLimit, fetchCnt));

                                    if (_lstInsert.Count > 0)
                                        Dao.BOM().InsertBatch(_lstInsert);
                                }

                                /////InsertBOMRecursive(_lstBulkInsert, _lstLevel0, bulkInsLimit);
                            }
                        }

                        List<BOM> _lstLevel1 = _lstBulkInsert.Where(rec => rec.parent_id > 0).ToList<BOM>();
                        if (_lstLevel1 != null)
                        {
                            int cnt = _lstLevel1.Count();
                            if (_lstLevel1.Count > 0)
                            {
                                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(bulkInsLimit)));
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    int fetchCnt = (bulkInsLimit > (cnt - (offset * bulkInsLimit)) ? (cnt - (offset * bulkInsLimit)) : bulkInsLimit);

                                    List<BOM> _lstInsert = new List<BOM>();
                                    _lstInsert.AddRange(_lstLevel1.GetRange(offset * bulkInsLimit, fetchCnt));

                                    if (_lstInsert.Count > 0)
                                        Dao.BOM().InsertBatch(_lstInsert);
                                }

                                /////InsertBOMRecursive(_lstBulkInsert, _lstLevel0, bulkInsLimit);
                            }
                        }
                    }
                }

                item.status_text = "Bulk Insert END";
                Common.UpdateLog(item, logfilePath, true);


                Dao.BOM().DropTempBOMTable(tmpTable);

                zipFileName = Download.WriteExcelFile(excellogfilepath, excellogfile);

                item.status = 1;
                item.comment = "{'message':'Refreshing Collections in process!'}";
                item.status_text = "Refreshing Collections in process!";
                Common.UpdateLog(item, logfilePath);

                RefreshMongoDb("bom");
                RefreshMongoDb("bom_hierarchy");

                item.status = 1;
                item.comment = "{'message':'Refreshing Collections completes!'}";
                item.status_text = "Refreshing Collections completes!";
                Common.UpdateLog(item, logfilePath);

                item.log_file = ("Logs\\BOM\\" + zipFileName).Replace("\\", "/");
                item.status = 1;
                item.comment = "{'message':'" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed and " + rows_duplicated + " rows duplicated!'}";
                item.status_text = "" + dt.Rows.Count + " rows successfully processed.\n The " + rows_succeeded + " rows are successfullly imported with " + rows_failed + " rows failed and " + rows_duplicated + " rows duplicated!";
                Common.UpdateLog(item, logfilePath);
            }
            catch (Exception e)
            {
                _log.Error("UploadBOM::", e);

                Dao.BOM().DropTempBOMTable(tmpTable);

                item.status = -1;
                item.comment = "{message:'An error occurred during file processing.', error:'" + e.StackTrace + "', details:'" + e.Message + "'}";
                item.status_text = "An error occurred during file processing";
                Common.UpdateLog(item, logfilePath);

                item.status_text = "ErrorMessage: " + e.Message + " StackTrace: " + e.StackTrace;
                Common.UpdateLog(item, logfilePath, true);

                //// Comment By Vishal
                ////throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
        }

        private void InsertBOMRecursive(List<BOM> _lstBulkInsert, List<BOM> _lstLevel0, int limit)
        {
            foreach (BOM _bom in _lstLevel0)
            {
                List<BOM> _lstLevel1 = _lstBulkInsert.Where(rec => rec.parent_id == _bom.sku_id).ToList<BOM>();
                if (_lstLevel1 != null)
                {
                    int cnt1 = _lstLevel1.Count();
                    if (_lstLevel1.Count > 0)
                    {
                        int loopCount1 = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt1) / Convert.ToDouble(limit)));
                        for (int offset1 = 0; offset1 < loopCount1; offset1++)
                        {
                            List<BOM> _lstInsert1 = new List<BOM>();
                            _lstInsert1.AddRange(_lstLevel1.GetRange(offset1 * limit, limit > (cnt1 - (offset1 * limit)) ? (cnt1 - (offset1 * limit)) : limit));

                            if (_lstInsert1.Count > 0)
                                Dao.BOM().InsertBatch(_lstInsert1);
                        }

                        InsertBOMRecursive(_lstBulkInsert, _lstLevel1, limit);

                    }
                }
            }
        }


    }
}
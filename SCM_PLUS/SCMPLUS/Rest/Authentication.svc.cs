﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using MongoDB.Driver.Builders;
using MongoDB.Web.Providers;
using log4net;
using System.IO;
using System.Configuration;
using System.Web;
using System.Net.NetworkInformation;

namespace SCMPLUS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class Authentication : IAuthentication
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));
        public string physicalAddress = "";

        public string Login(User user)
        {
            Dictionary<string, string> obj = new Dictionary<string, string>();

            bool blValid = IsValid();

            if (blValid)
            {
                User _user;
                string publicKey = Common.Instance.PublicKey;

                try
                {
                    _user = Dao.User().SelectUserCheckAuthenticate(user);
                    if (_user != null)
                    {
                        if (_user.password != user.password)
                            _user = null;
                    }
                }
                catch (Exception e)
                {
                    _log.Error("Login::", e);
                    throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
                }

                if (_user != null)
                {
                    if (_user.approved <= 0) throw new WebFaultException<string>("User is not approved!", HttpStatusCode.Forbidden);
                    if (_user.locked > 0) throw new WebFaultException<string>("User is locked!", HttpStatusCode.NotAcceptable);
                }

                if (_user == null)
                {
                    _user = Dao.User().SelectUserCheckLocked(user);
                    if (_user != null)
                    {
                        if (_user.locked > 0) throw new WebFaultException<string>("User is locked!", HttpStatusCode.NotAcceptable);

                        object value = Common.Instance.GetKey("UserLoginAttempts", _user.user_name);
                        Common.Instance.SetKey("UserLoginAttempts", _user.user_name, value == null ? 1 : (int)value + 1);
                        value = Common.Instance.GetKey("UserLoginAttempts", _user.user_name);

                        if ((int)value >= AppSettings.MaxLoginAttempts)
                        {
                            _user.locked = 1;

                            _user.modified_on = DateTime.Now.ToString();
                            Dao.User().Update(_user);

                            string collection = Common.Instance.GetEnumValue(Common.CollectionName.USER);
                            MongoHelper<User>.RemoveAll(collection);
                            List<User> lst = Dao.User().SelectAll();
                            if (lst != null)
                                if (lst.Count > 0)
                                    MongoHelper<User>.Add(collection, lst);

                            ////MongoHelper<User>.Save(Common.Instance.GetEnumValue(Common.CollectionName.USER), _user);
                        }
                    }

                    throw new WebFaultException<string>("Unauthorized!", HttpStatusCode.Unauthorized);
                }

                try
                {
                    string oAuthToken = AuthenticatorOfHeader.BasicAuth + AuthenticatorOfHeader.Encrypt(user.user_name + ":" + user.password + ":" + user.user_id, publicKey);

                    obj.Add("user_id", _user.user_id.ToString());
                    obj.Add("first_name", _user.first_name);
                    obj.Add("middle_name", _user.middle_name);
                    obj.Add("last_name", _user.last_name);
                    obj.Add("full_name", _user.middle_name != null ? _user.first_name + " " + _user.middle_name + " " + _user.last_name : _user.first_name + " " + _user.last_name);
                    obj.Add("oAuthToken", oAuthToken);

                    List<UserModule> userModules = MongoHelper<UserModule>.Find(Common.Instance.GetEnumValue(Common.CollectionName.USER_MODULE), Query.EQ("role_id", _user.role_id));
                    List<Dictionary<string, object>> permission = new List<Dictionary<string, object>>();

                    if (userModules != null)
                        foreach (var item in userModules)
                        {
                            Module module = MongoHelper<Module>.Single(Common.Instance.GetEnumValue(Common.CollectionName.MODULE), Query.EQ("module_id", item.module_id));

                            Dictionary<string, object> perm = new Dictionary<string, object>();
                            perm.Add("module_id", item.module_id);
                            perm.Add("module_name", module.module_name);
                            perm.Add("read", item.read == 1 ? item.read : item.write);
                            perm.Add("write", item.write);
                            permission.Add(perm);
                        }

                    obj.Add("permission", (new JavaScriptSerializer()).Serialize(permission));

                    Common.Instance.SetServiceRequestId();
                    Common.Instance.SetLoginKey(user.user_name, _user.user_id.ToString(), oAuthToken);
                    Common.Instance.CurrentUser = _user.user_id;
                    Common.Instance.CurrentRole = _user.role_id;
                }
                catch (Exception e)
                {
                    _log.Error("Login::", e);
                    throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
                }

                blValid = IsValid();
            }
            else
            {
                obj.Add("mackey", physicalAddress);
            }

            return (new JavaScriptSerializer()).Serialize(obj);
        }

        private bool IsValid()
        {
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up && (!nic.Description.Contains("Virtual") && !nic.Description.Contains("Pseudo")))
                    {
                        if (nic.GetPhysicalAddress().ToString().Length > 0)
                        {
                            physicalAddress = nic.GetPhysicalAddress().ToString();
                        }
                    }
                }

                string path = System.AppDomain.CurrentDomain.BaseDirectory;
                string licenseKey = File.ReadAllText(path + GenexEncryptDecrypt.GenexSecrityAlgoFactory.Decrypt(ConfigurationManager.AppSettings["LicenseFile"].ToString()));
                string macKey = GenexEncryptDecrypt.GenexSecrityAlgoFactory.Decrypt(licenseKey);

                if (physicalAddress.ToLower() == macKey.ToLower().Trim())
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
    }
}

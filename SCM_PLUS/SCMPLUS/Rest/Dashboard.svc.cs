﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using DataTablePager.Core;
using DataTablePager.Utils;
using log4net;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Web.Providers;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization;

namespace SCMPLUS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AuthenticatingHeader]

    public class Dashboard : IDashboard
    {
        private JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));

        public FormattedList<Replenishment> GetReplenishmentList(List<jsonAOData> jsonData)
        {
            _log.Info("GetReplenishmentList started: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                Replenishment filterdata = (Replenishment)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Replenishment));

                List<Replenishment> _lst = new List<Replenishment>();
                string replenishment_list = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                // For Getting Permission ID START
                IMongoQuery _queryNode_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                IMongoQuery _querySupplyNode_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "supply_node_id");
                IMongoQuery _querySKU_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                IMongoQuery _queryCategory_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                // For Getting Permission ID END

                int i = 0;
                int distCnt = dict.Count + 4;
                IMongoQuery[] qc = new IMongoQuery[distCnt];

                qc[i++] = _queryNode_per;
                qc[i++] = _querySupplyNode_per;
                qc[i++] = _querySKU_per;
                qc[i++] = _queryCategory_per;

                {
                    if (filterdata.transaction_date != null)
                    {
                        string[] transaction_date = filterdata.transaction_date.Split('/');
                        qc[i++] = Query.EQ("transfilter_date", Convert.ToInt32(transaction_date[2] + transaction_date[1] + transaction_date[0]));
                    }

                    if (filterdata.sku_id != null)
                        if (filterdata.sku_id > 0)
                            qc[i++] = Query.EQ("sku_id", filterdata.sku_id);

                    if (filterdata.sku_code != null)
                        qc[i++] = MongoHelper<Node>.MatchesStatement("sku_code", filterdata.sku_code);

                    if (filterdata.sku_name != null)
                        qc[i++] = MongoHelper<Node>.MatchesStatement("sku_name", filterdata.sku_name);

                    if (filterdata.node_code != null)
                        qc[i++] = MongoHelper<Node>.MatchesStatement("node_code", filterdata.node_code);

                    if (filterdata.skuSelection != null)
                        qc[i++] = Query.In("sku_id", new BsonArray(filterdata.skuSelection));

                    if (filterdata.categorySelection != null)
                        qc[i++] = Query.In("category_id", new BsonArray(filterdata.categorySelection));

                    if (filterdata.nodeSelection != null)
                        qc[i++] = Query.In("node_id", new BsonArray(filterdata.nodeSelection));

                    if (filterdata.supplynodeSelection != null)
                        qc[i++] = Query.In("supply_node_id", new BsonArray(filterdata.supplynodeSelection));

                    if (filterdata.bomSelection != null)
                        qc[i++] = Query.In("sku_id", new BsonArray(filterdata.bomSelection.Distinct()));

                    if (filterdata.node_id != null)
                        if (filterdata.node_id > 0)
                            qc[i++] = Query.EQ("node_id", filterdata.node_id);

                    if (filterdata.category_id != null)
                        if (filterdata.category_id > 0)
                            qc[i++] = Query.EQ("category_id", filterdata.category_id);

                    if (filterdata.closing_stock_zone != null)
                    {
                        string[] closing_stock_zone_array = filterdata.closing_stock_zone.Split(',');

                        if (closing_stock_zone_array.Length == 1)
                        {
                            qc[i++] = Query.EQ("closing_stock_zone", closing_stock_zone_array[0]);
                        }
                        else if (closing_stock_zone_array.Length > 1)
                        {
                            IMongoQuery[] qccsz = new IMongoQuery[closing_stock_zone_array.Length];
                            int iqccsz = 0;
                            foreach (string color in closing_stock_zone_array)
                            {
                                qccsz[iqccsz++] = Query.EQ("closing_stock_zone", color);
                            }
                            qc[i++] = Query.Or(qccsz);
                        }
                    }

                    if (filterdata.in_transit_zone != null)
                    {
                        string[] in_transit_zone_array = filterdata.in_transit_zone.Split(',');

                        if (in_transit_zone_array.Length == 1)
                        {
                            qc[i++] = Query.EQ("in_transit_zone", in_transit_zone_array[0]);
                        }
                        else if (in_transit_zone_array.Length > 1)
                        {
                            IMongoQuery[] qcitz = new IMongoQuery[in_transit_zone_array.Length];
                            int iqcitz = 0;
                            foreach (string color in in_transit_zone_array)
                            {
                                qcitz[iqcitz++] = Query.EQ("in_transit_zone", color);
                            }
                            qc[i++] = Query.Or(qcitz);
                        }
                    }

                    if (filterdata.supplying_node_zone != null)
                    {
                        string[] supplying_node_zone_array = filterdata.supplying_node_zone.Split(',');

                        if (supplying_node_zone_array.Length == 1)
                        {
                            qc[i++] = Query.EQ("supplying_node_zone", supplying_node_zone_array[0]);
                        }
                        else if (supplying_node_zone_array.Length > 1)
                        {
                            IMongoQuery[] qcsnz = new IMongoQuery[supplying_node_zone_array.Length];
                            int iqcsnz = 0;
                            foreach (string color in supplying_node_zone_array)
                            {
                                qcsnz[iqcsnz++] = Query.EQ("supplying_node_zone", color);
                            }
                            qc[i++] = Query.Or(qcsnz);
                        }
                    }
                }

                DataTablePager<Replenishment> dataTablePager1 = new DataTablePager<Replenishment>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                int iTotalRecords = 0;
                IMongoQuery query = distCnt > 0 ? distCnt == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));

                _log.Info("GetReplenishmentList query execution started: " + DateTime.Now.ToString("o"));
                _lst = MongoHelper<Replenishment>.Paginate(replenishment_list, query, sorted, out iTotalRecords, displayLength, displayStart);
                _log.Info("GetReplenishmentList query execution ended: " + DateTime.Now.ToString("o"));

                foreach (var item in _lst)
                {
                    item.attributes = MongoHelper<SKUAttributeMapping>.Find(Common.Instance.GetEnumValue(Common.CollectionName.SKU_ATTRIBUTE_MAPPING), Query.EQ("sku_id", item.sku_id));
                }

                DataTablePager<Replenishment> dataTablePager = new DataTablePager<Replenishment>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Replenishment> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetReplenishmentList ended: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetReplenishmentList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        HSSFWorkbook workbook = new HSSFWorkbook();
        public string ExportReplenishmentList(List<jsonAOData> jsonData)
        {
            try
            {
                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                string paramWorkbookPath = AppSettings.ExportPath;
                string exportFolder = AppSettings.ExportFolder;
                string replenishmentXlsName = AppSettings.ReplenishmentXlsName + "_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', '_').Replace('-', '_').Replace(':', '_') + ".xls";
                object paramMissing = Type.Missing;

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                string REFERENCE_TITLE = aoDataList.CheckNVValue("referenceTitle")
                    ? (string.IsNullOrEmpty(aoDataList.GetNVValue("referenceTitle")) ? replenishmentXlsName : aoDataList.GetNVValue("referenceTitle"))
                    : replenishmentXlsName;

                ImportExportLog log = new ImportExportLog();
                log.reference_date = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"));
                log.reference_title = REFERENCE_TITLE;
                log.log_file = (exportFolder + replenishmentXlsName).Replace("\\", "/");
                log.reference_type = "Replenishment";
                log.type = "Export";
                log.status = 0;
                log.comment = "";
                log.created_by = Common.Instance.CurrentUser;

                log.import_export_log_id = Dao.ImportExportLog().Insert(log);
                MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), log);

                List<Replenishment> _lst;
                DataTablePager<Replenishment> dataTablePager;
                PrepareExportReplenishmentList(jsonAOData, out _lst, out dataTablePager);

                ICellStyle headerStyle;
                ICellStyle style;
                ICellStyle blue_style;
                ICellStyle green_style;
                ICellStyle white_style;
                ICellStyle yellow_style;
                ICellStyle red_style;
                ICellStyle black_style;

                int row_counter = 0;
                int column_counter = 0;
                List<aoColumns> aoColumns = dataTablePager.aoColumns();

                int sheetLimit = 65000;
                int lstCnt = _lst.Count();
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstCnt) / Convert.ToDouble(sheetLimit)));
                ISheet[] sheet = new ISheet[loopCount];
                sheet = PrepareWorkbook(sheet, out headerStyle, out style, out blue_style, out green_style, out white_style, out yellow_style, out red_style, out black_style);
                IRow headerRow = null;

                for (int offset = 0; offset < loopCount; offset++)
                {
                    row_counter = 0;

                    ISheet sht = sheet[offset];
                    headerRow = sht.CreateRow(row_counter);
                    int fetchCnt = (sheetLimit > (lstCnt - (offset * sheetLimit)) ? (lstCnt - (offset * sheetLimit)) : sheetLimit);
                    List<Replenishment> _lstBatch = new List<Replenishment>();
                    _lstBatch.AddRange(_lst.GetRange(offset * sheetLimit, fetchCnt));

                    foreach (Replenishment item in _lstBatch)
                    {
                        column_counter = 0;
                        IRow row = sht.CreateRow(++row_counter);

                        foreach (aoColumns ao in aoColumns)
                        {
                            if (ao.bVisible)
                            {
                                CreateCell(headerRow, headerStyle, column_counter, ao.sTitle, true);

                                switch (ao.mData)
                                {
                                    case "transaction_date":
                                        CreateCell(row, style, column_counter, item.transaction_date, false);
                                        ++column_counter;
                                        break;
                                    case "sku_code":
                                        CreateCell(row, style, column_counter, item.sku_code, false);
                                        ++column_counter;
                                        break;
                                    case "sku_name":
                                        CreateCell(row, style, column_counter, item.sku_name, false);
                                        ++column_counter;
                                        break;
                                    case "node_code":
                                        CreateCell(row, style, column_counter, item.node_code, false);
                                        ++column_counter;
                                        break;
                                    case "node_name":
                                        CreateCell(row, style, column_counter, item.node_name, false);
                                        ++column_counter;
                                        break;
                                    case "sales":
                                        CreateCell(row, style, column_counter, item.sales.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "closing_quantity":
                                        CreateCell(row, style, column_counter, item.closing_quantity.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "buffer_norm":
                                        CreateCell(row, style, column_counter, item.buffer_norm == null ? "0" : item.buffer_norm.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "closing_stock_zone":
                                        switch (item.closing_stock_zone)
                                        {
                                            case "White":
                                                CreateCell(row, white_style, column_counter, "", false);
                                                break;
                                            case "Black":
                                                CreateCell(row, black_style, column_counter, "", false);
                                                break;
                                            case "Red":
                                                CreateCell(row, red_style, column_counter, "", false);
                                                break;
                                            case "Green":
                                                CreateCell(row, green_style, column_counter, "", false);
                                                break;
                                            case "Yellow":
                                                CreateCell(row, yellow_style, column_counter, "", false);
                                                break;
                                            case "Blue":
                                                CreateCell(row, blue_style, column_counter, "", false);
                                                break;
                                        }
                                        ++column_counter;
                                        break;
                                    case "replenishment_quantity":
                                        CreateCell(row, style, column_counter, item.replenishment_quantity.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "mrq":
                                        CreateCell(row, style, column_counter, item.mrq.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "in_transit":
                                        CreateCell(row, style, column_counter, item.in_transit.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "in_transit_zone":
                                        switch (item.in_transit_zone)
                                        {
                                            case "White":
                                                CreateCell(row, white_style, column_counter, "", false);
                                                break;
                                            case "Black":
                                                CreateCell(row, black_style, column_counter, "", false);
                                                break;
                                            case "Red":
                                                CreateCell(row, red_style, column_counter, "", false);
                                                break;
                                            case "Green":
                                                CreateCell(row, green_style, column_counter, "", false);
                                                break;
                                            case "Yellow":
                                                CreateCell(row, yellow_style, column_counter, "", false);
                                                break;
                                            case "Blue":
                                                CreateCell(row, blue_style, column_counter, "", false);
                                                break;
                                        }
                                        ++column_counter;
                                        break;
                                    case "supply_node_code":
                                        CreateCell(row, style, column_counter, item.supply_node_code, false);
                                        ++column_counter;
                                        break;
                                    case "supply_node_name":
                                        CreateCell(row, style, column_counter, item.supply_node_name, false);
                                        ++column_counter;
                                        break;
                                    case "supplying_node_zone":
                                        switch (item.supplying_node_zone)
                                        {
                                            case "White":
                                                CreateCell(row, white_style, column_counter, "", false);
                                                break;
                                            case "Black":
                                                CreateCell(row, black_style, column_counter, "", false);
                                                break;
                                            case "Red":
                                                CreateCell(row, red_style, column_counter, "", false);
                                                break;
                                            case "Green":
                                                CreateCell(row, green_style, column_counter, "", false);
                                                break;
                                            case "Yellow":
                                                CreateCell(row, yellow_style, column_counter, "", false);
                                                break;
                                            case "Blue":
                                                CreateCell(row, blue_style, column_counter, "", false);
                                                break;
                                        }
                                        ++column_counter;
                                        break;
                                    case "inst_hold":
                                        CreateCell(row, style, column_counter, item.inst_hold.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "quality_hold":
                                        CreateCell(row, style, column_counter, item.quality_hold.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "qa_hold":
                                        CreateCell(row, style, column_counter, item.qa_hold.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "pending_orders":
                                        CreateCell(row, style, column_counter, item.pending_orders.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "orders_in_hand":
                                        CreateCell(row, style, column_counter, item.orders_in_hand.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "transport_mode":
                                        CreateCell(row, style, column_counter, item.transport_mode, false);
                                        ++column_counter;
                                        break;

                                    case "open_stock_transfer_order":
                                        CreateCell(row, style, column_counter, item.open_stock_transfer_order.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "open_planned_order":
                                        CreateCell(row, style, column_counter, item.open_planned_order.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "open_purchase_request":
                                        CreateCell(row, style, column_counter, item.open_purchase_request.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "open_production_order":
                                        CreateCell(row, style, column_counter, item.open_production_order.ToString(), false);
                                        ++column_counter;
                                        break;
                                    case "open_purchase_order":
                                        CreateCell(row, style, column_counter, item.open_purchase_order.ToString(), false);
                                        ++column_counter;
                                        break;

                                    case "field1":
                                        CreateCell(row, style, column_counter, item.field1, false);
                                        ++column_counter;
                                        break;
                                    case "field2":
                                        CreateCell(row, style, column_counter, item.field2, false);
                                        ++column_counter;
                                        break;
                                    case "field3":
                                        CreateCell(row, style, column_counter, item.field3, false);
                                        ++column_counter;
                                        break;
                                    case "field4":
                                        CreateCell(row, style, column_counter, item.field4, false);
                                        ++column_counter;
                                        break;
                                    case "field5":
                                        CreateCell(row, style, column_counter, item.field5, false);
                                        ++column_counter;
                                        break;
                                    case "field6":
                                        CreateCell(row, style, column_counter, item.field6, false);
                                        ++column_counter;
                                        break;
                                    case "field7":
                                        CreateCell(row, style, column_counter, item.field7, false);
                                        ++column_counter;
                                        break;
                                    case "field8":
                                        CreateCell(row, style, column_counter, item.field8, false);
                                        ++column_counter;
                                        break;
                                    case "field9":
                                        CreateCell(row, style, column_counter, item.field9, false);
                                        ++column_counter;
                                        break;
                                    case "field10":
                                        CreateCell(row, style, column_counter, item.field10, false);
                                        ++column_counter;
                                        break;
                                }
                            }
                        }
                    }
                }
                //Write the stream data of workbook to the root directory
                FileStream file = new FileStream(paramWorkbookPath + replenishmentXlsName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                string zipFileName = replenishmentXlsName + ".zip";

                if (!Common.Instance.CreateZip(replenishmentXlsName, AppSettings.ExportPath, zipFileName, AppSettings.ExportPath, AppSettings.ExportFilePassword))
                    throw new WebFaultException<string>("Error creating zip file!", HttpStatusCode.InternalServerError);

                if (File.Exists(paramWorkbookPath + replenishmentXlsName))
                {
                    File.Delete(paramWorkbookPath + replenishmentXlsName);
                }

                log.log_file = (exportFolder + zipFileName).Replace("\\", "/");
                log.status = 1;
                log.comment = "{rows_affected:" + _lst.Count + "}";
                log.status_text = "" + _lst.Count + " rows successfully processed.";
                Dao.ImportExportLog().Update(log);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", log.import_export_log_id));
                upItem.status = log.status;
                upItem.comment = log.comment;
                upItem.log_file = log.log_file;
                upItem.status_text = log.status_text;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);

                return zipFileName;
            }
            catch (Exception e)
            {
                _log.Error("ExportReplenishmentList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        private ISheet[] PrepareWorkbook(ISheet[] sheets, out ICellStyle headerStyle, out ICellStyle style, out ICellStyle blue_style, out ICellStyle green_style, out ICellStyle white_style, out ICellStyle yellow_style, out ICellStyle red_style, out ICellStyle black_style)
        {
            workbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = AppSettings.Company;
            workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Replenishment";
            workbook.SummaryInformation = si;

            if (sheets.Length > 0)
            {
                for (int i = 0; i < sheets.Length; i++)
                {
                    sheets[i] = workbook.CreateSheet("Replenishment_Sheet" + (i + 1).ToString());
                }
            }

            headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.BorderTop = BorderStyle.THIN;

            HSSFFont font = (HSSFFont)workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            headerStyle.SetFont(font);

            style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;

            ICellStyle zone_style = workbook.CreateCellStyle();
            zone_style.BorderBottom = BorderStyle.THIN;
            zone_style.BorderLeft = BorderStyle.THIN;
            zone_style.BorderRight = BorderStyle.THIN;
            zone_style.BorderTop = BorderStyle.THIN;

            blue_style = workbook.CreateCellStyle();
            blue_style.BorderBottom = BorderStyle.THIN;
            blue_style.BorderLeft = BorderStyle.THIN;
            blue_style.BorderRight = BorderStyle.THIN;
            blue_style.BorderTop = BorderStyle.THIN;
            blue_style.FillForegroundColor = HSSFColor.BLUE.index;
            blue_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            green_style = workbook.CreateCellStyle();
            green_style.BorderBottom = BorderStyle.THIN;
            green_style.BorderLeft = BorderStyle.THIN;
            green_style.BorderRight = BorderStyle.THIN;
            green_style.BorderTop = BorderStyle.THIN;
            green_style.FillForegroundColor = HSSFColor.GREEN.index;
            green_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            white_style = workbook.CreateCellStyle();
            white_style.BorderBottom = BorderStyle.THIN;
            white_style.BorderLeft = BorderStyle.THIN;
            white_style.BorderRight = BorderStyle.THIN;
            white_style.BorderTop = BorderStyle.THIN;
            white_style.FillForegroundColor = HSSFColor.WHITE.index;
            white_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            yellow_style = workbook.CreateCellStyle();
            yellow_style.BorderBottom = BorderStyle.THIN;
            yellow_style.BorderLeft = BorderStyle.THIN;
            yellow_style.BorderRight = BorderStyle.THIN;
            yellow_style.BorderTop = BorderStyle.THIN;
            yellow_style.FillForegroundColor = HSSFColor.YELLOW.index;
            yellow_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            red_style = workbook.CreateCellStyle();
            red_style.BorderBottom = BorderStyle.THIN;
            red_style.BorderLeft = BorderStyle.THIN;
            red_style.BorderRight = BorderStyle.THIN;
            red_style.BorderTop = BorderStyle.THIN;
            red_style.FillForegroundColor = HSSFColor.RED.index;
            red_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            black_style = workbook.CreateCellStyle();
            black_style.BorderBottom = BorderStyle.THIN;
            black_style.BorderLeft = BorderStyle.THIN;
            black_style.BorderRight = BorderStyle.THIN;
            black_style.BorderTop = BorderStyle.THIN;
            black_style.FillForegroundColor = HSSFColor.BLACK.index;
            black_style.FillPattern = FillPatternType.SOLID_FOREGROUND;

            return sheets;
        }

        private void PrepareExportReplenishmentList(string jsonAOData, out List<Replenishment> _lst, out DataTablePager<Replenishment> dataTablePager)
        {
            List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

            Replenishment filterdata = (Replenishment)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(Replenishment));

            _lst = new List<Replenishment>();
            string replenishment_list = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);

            Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

            int i = 0;
            IMongoQuery[] qc = new IMongoQuery[dict.Count];

            {
                if (filterdata.transaction_date != null)
                {
                    string[] transaction_date = filterdata.transaction_date.Split('/');
                    qc[i++] = Query.EQ("transfilter_date", Convert.ToInt32(transaction_date[2] + transaction_date[1] + transaction_date[0]));
                }

                if (filterdata.sku_id != null)
                    if (filterdata.sku_id > 0)
                        qc[i++] = Query.EQ("sku_id", filterdata.sku_id);

                if (filterdata.sku_code != null)
                    qc[i++] = MongoHelper<Node>.MatchesStatement("sku_code", filterdata.sku_code);

                if (filterdata.sku_name != null)
                    qc[i++] = MongoHelper<Node>.MatchesStatement("sku_name", filterdata.sku_name);

                if (filterdata.node_code != null)
                    qc[i++] = MongoHelper<Node>.MatchesStatement("node_code", filterdata.node_code);

                if (filterdata.skuSelection != null)
                    qc[i++] = Query.In("sku_id", new BsonArray(filterdata.skuSelection));

                if (filterdata.categorySelection != null)
                    qc[i++] = Query.In("category_id", new BsonArray(filterdata.categorySelection));

                if (filterdata.nodeSelection != null)
                    qc[i++] = Query.In("node_id", new BsonArray(filterdata.nodeSelection));

                if (filterdata.supplynodeSelection != null)
                    qc[i++] = Query.In("supply_node_id", new BsonArray(filterdata.supplynodeSelection));

                if (filterdata.bomSelection != null)
                    qc[i++] = Query.In("sku_id", new BsonArray(filterdata.bomSelection.Distinct()));

                if (filterdata.node_id != null)
                    if (filterdata.node_id > 0)
                        qc[i++] = Query.EQ("node_id", filterdata.node_id);

                if (filterdata.category_id != null)
                    if (filterdata.category_id > 0)
                        qc[i++] = Query.EQ("category_id", filterdata.category_id);

                if (filterdata.closing_stock_zone != null)
                {
                    string[] closing_stock_zone_array = filterdata.closing_stock_zone.Split(',');

                    if (closing_stock_zone_array.Length == 1)
                    {
                        qc[i++] = Query.EQ("closing_stock_zone", closing_stock_zone_array[0]);
                    }
                    else if (closing_stock_zone_array.Length > 1)
                    {
                        IMongoQuery[] qccsz = new IMongoQuery[closing_stock_zone_array.Length];
                        int iqccsz = 0;
                        foreach (string color in closing_stock_zone_array)
                        {
                            qccsz[iqccsz++] = Query.EQ("closing_stock_zone", color);
                        }
                        qc[i++] = Query.Or(qccsz);
                    }
                }

                if (filterdata.in_transit_zone != null)
                {
                    string[] in_transit_zone_array = filterdata.in_transit_zone.Split(',');

                    if (in_transit_zone_array.Length == 1)
                    {
                        qc[i++] = Query.EQ("in_transit_zone", in_transit_zone_array[0]);
                    }
                    else if (in_transit_zone_array.Length > 1)
                    {
                        IMongoQuery[] qcitz = new IMongoQuery[in_transit_zone_array.Length];
                        int iqcitz = 0;
                        foreach (string color in in_transit_zone_array)
                        {
                            qcitz[iqcitz++] = Query.EQ("in_transit_zone", color);
                        }
                        qc[i++] = Query.Or(qcitz);
                    }
                }

                if (filterdata.supplying_node_zone != null)
                {
                    string[] supplying_node_zone_array = filterdata.supplying_node_zone.Split(',');

                    if (supplying_node_zone_array.Length == 1)
                    {
                        qc[i++] = Query.EQ("supplying_node_zone", supplying_node_zone_array[0]);
                    }
                    else if (supplying_node_zone_array.Length > 1)
                    {
                        IMongoQuery[] qcsnz = new IMongoQuery[supplying_node_zone_array.Length];
                        int iqcsnz = 0;
                        foreach (string color in supplying_node_zone_array)
                        {
                            qcsnz[iqcsnz++] = Query.EQ("supplying_node_zone", color);
                        }
                        qc[i++] = Query.Or(qcsnz);
                    }
                }
            }

            DataTablePager<Replenishment> dataTablePager1 = new DataTablePager<Replenishment>(jsonAOData, _lst.AsQueryable());
            var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
            i = 0;
            IMongoSortBy sortQuery = SortBy.Ascending("_id");
            sorted.ForEach(sort =>
            {
                sortQuery = sort.SortDirection.ToString().ToLower() == "asc"
                    ? SortBy.Ascending(sort.Name.Contains("_zone") ? sort.Name + "_sort" : sort.Name)
                    : SortBy.Descending(sort.Name.Contains("_zone") ? sort.Name + "_sort" : sort.Name);
            });

            int iTotalRecords = 0;
            IMongoQuery query = dict.Count > 0 ? dict.Count == 1 ? qc[0] : Query.And(qc) : Query.Not(Query.EQ("_id", ""));
            _lst = MongoHelper<Replenishment>.Paginate(replenishment_list, query, sorted, out iTotalRecords);

            dataTablePager = new DataTablePager<Replenishment>(jsonAOData, _lst.AsQueryable());
            dataTablePager.iTotalRecords = iTotalRecords;
            FormattedList<Replenishment> formattedList = dataTablePager.Filter(isExport: true);

            _lst = formattedList.aaData;
        }

        private void CreateCell(IRow row, ICellStyle style, int column, string value, bool isBold)
        {
            ICell cell = row.CreateCell(column);
            cell.CellStyle = style;
            cell.SetCellValue(value);
        }

        public List<TransactionComment> GetTransactionCommentList(DataFilter item)
        {
            try
            {
                List<TransactionComment> _lst = new List<TransactionComment>();
                _lst = Dao.Transaction().SelectCommentList(item);
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetTransactionCommentList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public void SaveTransactionCommentList(TransactionComment item)
        {
            try
            {
                if (item != null)
                {
                    item.created_by = Common.Instance.CurrentUser;
                    item.created_on = DateTime.Now.ToString();
                    item.transaction_comment_id = Dao.Transaction().InsertComment(item);
                    WriteConcernResult rst = MongoHelper<TransactionComment>.Add(Common.Instance.GetEnumValue(Common.CollectionName.TRANSACTION_COMMENT), item);
                    if (rst.Ok)
                    {
                        int commentCnt = MongoHelper<TransactionComment>.Count(Common.Instance.GetEnumValue(Common.CollectionName.TRANSACTION_COMMENT), Query.And(Query.EQ("sku_id", item.sku_id), Query.EQ("node_id", item.node_id)));
                        IMongoUpdate _update = Update.Combine(Update.Set("transaction_comment", commentCnt));
                        MongoHelper<BsonDocument>.Update(Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT), Query.And(Query.EQ("sku_id", item.sku_id), Query.EQ("node_id", item.node_id)), _update);

                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error("SaveTransactionCommentList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<SKUChart> GetSKUChart(List<jsonAOData> jsonData)
        {
            _log.Info("GetSKUChart started: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<SKUChart> _lst = new List<SKUChart>();

                string sc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART);
                string nsc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_NODE);
                string csc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_CATEGORY);
                string ssc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_SKU);

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);
                int iTotalRecords = 0;

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                MapperBatch paramRepl = new MapperBatch();
                paramRepl.fromdate = sqlFromDate;
                paramRepl.todate = sqlToDate;
                List<Replenishment> _lstReplDates = new List<Replenishment>();

                _lstReplDates = Dao.Replenishment().SelectReplenishmentDates(paramRepl);
                if (_lstReplDates.Count > 0)
                {
                    if (filterdata.skuSelection == null && filterdata.categorySelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();


                        foreach (Replenishment _repl in _lstReplDates)
                        {
                            param.date = _repl.transaction_date;
                            List<SKUChart> _chrt = new List<SKUChart>();

                            if (filterdata.skuSelection == null)
                                _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                            else
                                _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            if (_chrt != null)
                                if (_chrt.Count > 0)
                                    _lst.AddRange(_chrt);
                        }

                        //if (filterdata.skuSelection == null)
                        //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                        //else
                        //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                        //_lst = Dao.SKUChart().SelectByNodeCategory(param);

                        //_lst = MongoHelper<SKUChart>.Find(sc_collection, Query.And(Query.GTE("transfilter_date", fromDate), Query.LTE("transfilter_date", toDate), _queryNode_per, _querySKU_per, _queryCategory_per));
                    }
                    else
                    {
                        var sort = new BsonDocument
                    {
                        {
                            "$sort",
                            new BsonDocument
                                {
                                    {"transfilter_date", 1}
                                }
                        }
                    };

                        var match = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "transfilter_date", new BsonDocument { { "$gte", fromDate }, { "$lte", toDate } }
                                    }
                                }
                        }
                    };

                        var group = new BsonDocument
                    {
                        { "$group",
                            new BsonDocument
                                {
                                    { "_id", new BsonDocument
                                                 {
                                                     { "transfilter_date","$transfilter_date" },
                                                     { "display_date","$display_date" }
                                                 }
                                    },
                                    {
                                        "closing_quantity", new BsonDocument
                                                     {
                                                         { "$sum", "$closing_quantity" }
                                                     }
                                    },
                                    {
                                        "green_zone_qty", new BsonDocument
                                                     {
                                                         { "$sum", "$green_zone_qty" }
                                                     }
                                    },
                                    {
                                        "yellow_zone_qty", new BsonDocument
                                                     {
                                                         { "$sum", "$yellow_zone_qty" }
                                                     }
                                    },
                                    {
                                        "red_zone_qty", new BsonDocument
                                                     {
                                                         { "$sum", "$red_zone_qty" }
                                                     }
                                    },
                                    {
                                        "in_transit", new BsonDocument
                                                     {
                                                         { "$sum", "$in_transit" }
                                                     }
                                    },
                                    {
                                        "sales", new BsonDocument
                                                     {
                                                         { "$sum", "$sales" }
                                                     }
                                    }
                                }
                      }
                    };

                        var project = new BsonDocument
                    {
                        {
                            "$project",
                            new BsonDocument
                                {
                                    {"_id", 0},
                                    {"transfilter_date","$_id.transfilter_date"},
                                    {"display_date","$_id.display_date"},
                                    {"closing_quantity", 1},
                                    {"green_zone_qty", 1},
                                    {"yellow_zone_qty", 1},
                                    {"red_zone_qty", 1},
                                    {"in_transit", 1},
                                    {"sales", 1}
                                }
                        }
                    };

                        var matchSKU = new BsonDocument();
                        if (filterdata.skuSelection != null)
                        {
                            matchSKU = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchCategory = new BsonDocument();
                        if (filterdata.categorySelection != null)
                        {
                            matchCategory = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchNode = new BsonDocument();
                        if (filterdata.nodeSelection != null)
                        {
                            matchNode = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        _log.Info("GetSKUChart query execution start: " + DateTime.Now.ToString("o"));
                        if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idSKU = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeSKU(param);
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idSKU = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeSKU(param);

                            //MongoCollection<Replenishment> repl = MongoHelper<Replenishment>.GetCollection(sc_collection);
                            //var pipeline = new[] { match, matchSKU, matchNode, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<SKUChart>).ToList();
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeCategory(param);

                            //MongoCollection<Replenishment> repl = MongoHelper<Replenishment>.GetCollection(sc_collection);
                            //var pipeline = new[] { match, matchCategory, matchNode, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<SKUChart>).ToList();
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.idSKU = filterdata.skuSelection.OfType<long>().ToList();

                            if (filterdata.skuSelection != null && filterdata.categorySelection != null)
                                param.idCategory = filterdata.categorySelection.ToList<long>();
                            else
                            {
                                if (_matchCategoryPermission)
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            }

                            if (_matchNodePermission)
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeSKU(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(ssc_collection);
                            //var pipeline = new[] { match, matchSKU, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<SKUChart>).ToList();
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id2 = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeCategory(param);

                            ////MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(csc_collection);
                            ////var pipeline = new[] { match, matchCategory, group, project, sort };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<SKUChart>).ToList();
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeSKU(param);

                            ////MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(nsc_collection);
                            ////var pipeline = new[] { match, matchNode, group, project, sort };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<SKUChart>).ToList();
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<SKUChart> _chrt = new List<SKUChart>();

                                if (filterdata.skuSelection == null)
                                    _chrt = Dao.SKUChart().SelectSkuChartReport(param);
                                else
                                    _chrt = Dao.SKUChart().SelectSkuChartReportSKU(param);

                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //if (filterdata.skuSelection == null)
                            //    _lst = Dao.SKUChart().SelectSkuChartReport(param);
                            //else
                            //    _lst = Dao.SKUChart().SelectSkuChartReportSKU(param);

                            //_lst = Dao.SKUChart().SelectByNodeSKU(param);
                        }
                        _log.Info("GetSKUChart query execution end: " + DateTime.Now.ToString("o"));
                    }
                }

                _lst = _lst.OrderBy(x => x.transfilter_date).ToList();
                iTotalRecords = _lst.Count;
                DataTablePager<SKUChart> dataTablePager = new DataTablePager<SKUChart>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKUChart> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetSKUChart ended: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUChart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }


        public FormattedList<BufferZoneList> GetBufferZoneschat(List<jsonAOData> jsonData)
        {
            _log.Info("GetBufferZoneschat start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<BufferZoneList> _lst = new List<BufferZoneList>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);

                string Date = "";

                //if (filterdata.date != null)
                //{
                //    string[] date = filterdata.date.Split('/');
                //    date[1] = date[1].IndexOf("0") == 0 ? date[1].Substring(1) : date[1];
                //    date[0] = date[0].IndexOf("0") == 0 ? date[0].Substring(1) : date[0];
                //    Date = date[1] + "/" + date[0] + "/" + date[2] + " 12:00:00 AM";
                //}

                if (filterdata.date != null)
                {
                    string[] transaction_date = filterdata.date.Split('/');
                    Date = transaction_date[2] + transaction_date[1] + transaction_date[0];
                }

                MongoCollection<Replenishment> repl = MongoHelper<Replenishment>.GetCollection(collection);

                var sort = new BsonDocument
                {
                    {
                        "$sort",
                        new BsonDocument
                            {
                                {"closing_stock_zone_sort", 1}
                            }
                    }
                };

                var match = new BsonDocument
                {
                    {
                        "$match",
                        new BsonDocument
                            {
                                {
                                    "transfilter_date", Convert.ToInt32(Date)
                                }
                            }
                    }
                };

                var group = new BsonDocument
                {
                    { "$group",
                        new BsonDocument
                            {
                                { "_id", new BsonDocument
                                             {
                                                 { "closing_stock_zone","$closing_stock_zone" },
                                                 { "closing_stock_zone_sort","$closing_stock_zone_sort" }
                                             }
                                },
                                {
                                    "closing_quantity", new BsonDocument
                                                 {
                                                     { "$sum", "$closing_quantity" }
                                                 }
                                },
                                {
                                    "count", new BsonDocument
                                                 {
                                                     { "$sum", 1 }
                                                 }
                                }
                            }
                  }
                };

                var project = new BsonDocument
                {
                    {
                        "$project",
                        new BsonDocument
                            {
                                {"_id", 0},
                                {"zone","$_id.closing_stock_zone"},
                                {"sort","$_id.closing_stock_zone_sort"},
                                {"closing_quantity", 1},
                                {"count", 1}
                            }
                    }
                };

                var matchSKU = new BsonDocument();
                if (filterdata.skuSelection != null)
                {
                    matchSKU = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                    }
                                }
                        }
                    };
                }

                var matchCategory = new BsonDocument();
                if (filterdata.categorySelection != null)
                {
                    matchCategory = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                    }
                                }
                        }
                    };
                }

                var matchNode = new BsonDocument();
                if (filterdata.nodeSelection != null)
                {
                    matchNode = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                    }
                                }
                        }
                    };
                }

                var matchSKUPermission = new BsonDocument();
                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                {
                    if (_matchArraySKUPermission.Length > 0)
                    {
                        _matchSKUPermission = true;
                        matchSKUPermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$nin", new BsonArray(_matchArraySKUPermission) } }
                                        }
                                    }
                            }
                        };
                    }
                }

                var matchCategoryPermission = new BsonDocument();
                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                {
                    if (_matchArrayCategoryPermission.Length > 0)
                    {
                        _matchCategoryPermission = true;
                        matchCategoryPermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(_matchArrayCategoryPermission) } }
                                        }
                                    }
                            }
                        };
                    }
                }

                var matchNodePermission = new BsonDocument();
                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                {
                    if (_matchArrayNodePermission.Length > 0)
                    {
                        _matchNodePermission = true;
                        matchNodePermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(_matchArrayNodePermission) } }
                                        }
                                    }
                            }
                        };
                    }
                }

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                {
                    if (_matchArraySupplyNodePermission.Length > 0)
                    {
                        _matchSupplyNodePermission = true;
                        matchSupplyNodePermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "supply_node_id", new BsonDocument { { "$in", new BsonArray(_matchArraySupplyNodePermission) } }
                                        }
                                    }
                            }
                        };
                    }
                }

                var matchSupplyNode = new BsonDocument();
                if (filterdata.supplynodeSelection != null)
                {
                    matchSupplyNode = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "supply_node_id", new BsonDocument { { "$in", new BsonArray(filterdata.supplynodeSelection) } }
                                    }
                                }
                        }
                    };
                }


                var matchBOM = new BsonDocument();
                if (filterdata.bomSelection != null)
                {
                    matchBOM = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.bomSelection.Distinct()) } }
                                    }
                                }
                        }
                    };
                }

                _log.Info("GetBufferZoneschat query execution start: " + DateTime.Now.ToString("o"));
                if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                {
                    var pipeline = new[] { match, matchSKU, matchCategory, matchNode, matchSupplyNode, matchBOM, group, project, sort };
                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.skuSelection != null && filterdata.categorySelection != null)
                {
                    var pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                {
                    var pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    var pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.skuSelection != null)
                {
                    var pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.categorySelection != null)
                {
                    var pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.nodeSelection != null)
                {
                    var pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNodePermission, group, project, sort };

                    if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNode, matchBOM, group, project, sort };
                    else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                        pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNode, group, project, sort };
                    else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                        pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchBOM, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.supplynodeSelection != null)
                {
                    var pipeline = new[] { match, matchSupplyNode, matchSKUPermission, matchCategoryPermission, matchNodePermission, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else if (filterdata.bomSelection != null)
                {
                    var pipeline = new[] { match, matchBOM, matchSKUPermission, matchCategoryPermission, matchNodePermission, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                else
                {
                    var pipeline = new[] { match, matchSKUPermission, matchCategoryPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                    var r = repl.Aggregate(pipeline);
                    _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                }
                _log.Info("GetBufferZoneschat query execution end: " + DateTime.Now.ToString("o"));

                _lst = _lst.OrderBy(x => x.sort).ToList();
                List<BufferZoneList> nList = new List<BufferZoneList>();

                long total_count = 0;
                foreach (var item in _lst)
                {
                    BufferZoneList bitem = new BufferZoneList();
                    switch (item.zone.ToString())
                    {
                        case "Blue":
                            total_count += item.count;
                            bitem.zone = "Blue";
                            bitem.zone_name = "Very High Inventory Zone";
                            bitem.count = item.count;
                            bitem.closing_quantity = item.closing_quantity;
                            nList.Add(bitem);
                            break;
                        case "Green":
                            total_count += item.count;
                            bitem.zone = "Green";
                            bitem.zone_name = "High Inventory Zone";
                            bitem.count = item.count;
                            bitem.closing_quantity = item.closing_quantity;
                            nList.Add(bitem);
                            break;
                        case "Yellow":
                            total_count += item.count;
                            bitem.zone = "Yellow";
                            bitem.zone_name = "Right Inventory Zone";
                            bitem.count = item.count;
                            bitem.closing_quantity = item.closing_quantity;
                            nList.Add(bitem);
                            break;
                        case "Red":
                            total_count += item.count;
                            bitem.zone = "Red";
                            bitem.zone_name = "Low Inventory Zone";
                            bitem.count = item.count;
                            bitem.closing_quantity = item.closing_quantity;
                            nList.Add(bitem);
                            break;
                        case "Black":
                            total_count += item.count;
                            bitem.zone = "Black";
                            bitem.zone_name = "Stockout Inventory Zone";
                            bitem.count = item.count;
                            bitem.closing_quantity = item.closing_quantity;
                            nList.Add(bitem);
                            break;
                    }
                }

                foreach (var item in nList)
                {
                    double per = (double)item.count / (double)total_count;
                    item.percentage = Math.Round(per * 100, 2);
                }

                int iTotalRecords = nList.Count;
                DataTablePager<BufferZoneList> dataTablePager = new DataTablePager<BufferZoneList>(jsonAOData, nList.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<BufferZoneList> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetBufferZoneschat end: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetBufferZoneschat::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }


        public List<FormattedList<BufferZoneList>> GetBufferZones(List<jsonAOData> jsonData)
        {
            _log.Info("GetBufferZones start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));


                //List<List<BufferZoneList>> _lstOfList = new List<List<BufferZoneList>>();
                List<FormattedList<BufferZoneList>> formattedListOfList = new List<FormattedList<BufferZoneList>>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.REPLENISHMENT);

                string intDate = ""; string dateTo = ""; string dateFrom = ""; string strFromDate = ""; string strToDate = "";


                //if (filterdata.date != null)
                //{
                //    string[] date = filterdata.date.Split('/');
                //    date[1] = date[1].IndexOf("0") == 0 ? date[1].Substring(1) : date[1];
                //    date[0] = date[0].IndexOf("0") == 0 ? date[0].Substring(1) : date[0];
                //    Date = date[1] + "/" + date[0] + "/" + date[2] + " 12:00:00 AM";
                //}

                if (filterdata.from_date != null)
                {
                    string[] transaction_date = filterdata.from_date.Split('/');
                    //dateFrom = transaction_date[2] + transaction_date[1] + transaction_date[0];
                    strFromDate = transaction_date[1] + "/" + transaction_date[0] + "/" + transaction_date[2];
                }

                if (filterdata.to_date != null)
                {
                    string[] transaction_date = filterdata.to_date.Split('/');
                    //dateTo = transaction_date[2] + transaction_date[1] + transaction_date[0];
                    strToDate = transaction_date[1] + "/" + transaction_date[0] + "/" + transaction_date[2];
                }

                //int fromDate = (!string.IsNullOrEmpty(dateFrom) ? Convert.ToInt32(dateFrom) : 0),
                //    toDate = (!string.IsNullOrEmpty(dateTo) ? Convert.ToInt32(dateTo) : 0);

                for (DateTime date = Convert.ToDateTime(strFromDate); date.Date <= Convert.ToDateTime(strToDate); date = date.AddDays(1))
                {
                    intDate = date.ToString("yyyyMMdd");
                    List<BufferZoneList> _lst = new List<BufferZoneList>();
                    MongoCollection<Replenishment> repl = MongoHelper<Replenishment>.GetCollection(collection);

                    var sort = new BsonDocument
                {
                    {
                        "$sort",
                        new BsonDocument
                            {
                                {"closing_stock_zone_sort", 1}
                            }
                    }
                };

                    var match = new BsonDocument
                {
                    {
                        "$match",
                        new BsonDocument
                            {
                                {
                                    "transfilter_date", Convert.ToInt32(intDate)
                                }
                            }
                    }
                };

                    var group = new BsonDocument
                {
                    { "$group",
                        new BsonDocument
                            {
                                { "_id", new BsonDocument
                                             {
                                                 { "closing_stock_zone","$closing_stock_zone" },
                                                 { "closing_stock_zone_sort","$closing_stock_zone_sort" }
                                             }
                                },
                                {
                                    "closing_quantity", new BsonDocument
                                                 {
                                                     { "$sum", "$closing_quantity" }
                                                 }
                                },
                                {
                                    "count", new BsonDocument
                                                 {
                                                     { "$sum", 1 }
                                                 }
                                }
                            }
                  }
                };

                    var project = new BsonDocument
                {
                    {
                        "$project",
                        new BsonDocument
                            {
                                {"_id", 0},
                                {"zone","$_id.closing_stock_zone"},
                                {"sort","$_id.closing_stock_zone_sort"},
                                {"closing_quantity", 1},
                                {"count", 1}
                            }
                    }
                };

                    var matchSKU = new BsonDocument();
                    if (filterdata.skuSelection != null)
                    {
                        matchSKU = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                    }
                                }
                        }
                    };
                    }

                    var matchCategory = new BsonDocument();
                    if (filterdata.categorySelection != null)
                    {
                        matchCategory = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                    }
                                }
                        }
                    };
                    }

                    var matchNode = new BsonDocument();
                    if (filterdata.nodeSelection != null)
                    {
                        matchNode = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                    }
                                }
                        }
                    };
                    }

                    var matchSKUPermission = new BsonDocument();
                    bool _matchSKUPermission = false;
                    long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                    if (_matchArraySKUPermission != null)
                    {
                        if (_matchArraySKUPermission.Length > 0)
                        {
                            _matchSKUPermission = true;
                            matchSKUPermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$nin", new BsonArray(_matchArraySKUPermission) } }
                                        }
                                    }
                            }
                        };
                        }
                    }

                    var matchCategoryPermission = new BsonDocument();
                    bool _matchCategoryPermission = false;
                    long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                    if (_matchArrayCategoryPermission != null)
                    {
                        if (_matchArrayCategoryPermission.Length > 0)
                        {
                            _matchCategoryPermission = true;
                            matchCategoryPermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(_matchArrayCategoryPermission) } }
                                        }
                                    }
                            }
                        };
                        }
                    }

                    var matchNodePermission = new BsonDocument();
                    bool _matchNodePermission = false;
                    long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                    if (_matchArrayNodePermission != null)
                    {
                        if (_matchArrayNodePermission.Length > 0)
                        {
                            _matchNodePermission = true;
                            matchNodePermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(_matchArrayNodePermission) } }
                                        }
                                    }
                            }
                        };
                        }
                    }

                    var matchSupplyNodePermission = new BsonDocument();
                    bool _matchSupplyNodePermission = false;
                    long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                    if (_matchArraySupplyNodePermission != null)
                    {
                        if (_matchArraySupplyNodePermission.Length > 0)
                        {
                            _matchSupplyNodePermission = true;
                            matchSupplyNodePermission = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "supply_node_id", new BsonDocument { { "$in", new BsonArray(_matchArraySupplyNodePermission) } }
                                        }
                                    }
                            }
                        };
                        }
                    }

                    var matchSupplyNode = new BsonDocument();
                    if (filterdata.supplynodeSelection != null)
                    {
                        matchSupplyNode = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "supply_node_id", new BsonDocument { { "$in", new BsonArray(filterdata.supplynodeSelection) } }
                                    }
                                }
                        }
                    };
                    }


                    var matchBOM = new BsonDocument();
                    if (filterdata.bomSelection != null)
                    {
                        matchBOM = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.bomSelection.Distinct()) } }
                                    }
                                }
                        }
                    };
                    }

                    _log.Info("GetBufferZones query execution start: " + DateTime.Now.ToString("o"));
                    if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                    {
                        var pipeline = new[] { match, matchSKU, matchCategory, matchNode, matchSupplyNode, matchBOM, group, project, sort };
                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.skuSelection != null && filterdata.categorySelection != null)
                    {
                        var pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchCategory, matchNodePermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                    {
                        var pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchNode, matchCategoryPermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                    {
                        var pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchCategory, matchNode, matchSKUPermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.skuSelection != null)
                    {
                        var pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchSKU, matchCategoryPermission, matchNodePermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.categorySelection != null)
                    {
                        var pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchCategory, matchSKUPermission, matchNodePermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.nodeSelection != null)
                    {
                        var pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNodePermission, group, project, sort };

                        if (filterdata.supplynodeSelection != null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNode, matchBOM, group, project, sort };
                        else if (filterdata.supplynodeSelection != null && filterdata.bomSelection == null)
                            pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchSupplyNode, group, project, sort };
                        else if (filterdata.supplynodeSelection == null && filterdata.bomSelection != null)
                            pipeline = new[] { match, matchNode, matchSKUPermission, matchCategoryPermission, matchBOM, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.supplynodeSelection != null)
                    {
                        var pipeline = new[] { match, matchSupplyNode, matchSKUPermission, matchCategoryPermission, matchNodePermission, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else if (filterdata.bomSelection != null)
                    {
                        var pipeline = new[] { match, matchBOM, matchSKUPermission, matchCategoryPermission, matchNodePermission, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    else
                    {
                        var pipeline = new[] { match, matchSKUPermission, matchCategoryPermission, matchNodePermission, matchSupplyNodePermission, group, project, sort };

                        var r = repl.Aggregate(pipeline);
                        _lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<BufferZoneList>).ToList();
                    }
                    _log.Info("GetBufferZones query execution end: " + DateTime.Now.ToString("o"));

                    _lst = _lst.OrderBy(x => x.sort).ToList();
                    List<BufferZoneList> nList = new List<BufferZoneList>();

                    long total_count = 0;
                    foreach (var item in _lst)
                    {
                        BufferZoneList bitem = new BufferZoneList();
                        switch (item.zone.ToString())
                        {
                            case "Blue":
                                total_count += item.count;
                                bitem.zone = "Blue";
                                bitem.date = date.ToString("dd/MM/yyyy");
                                bitem.zone_name = "Very High Inventory Zone";
                                bitem.count = item.count;
                                bitem.closing_quantity = item.closing_quantity;
                                nList.Add(bitem);
                                break;
                            case "Green":
                                total_count += item.count;
                                bitem.zone = "Green";
                                bitem.date = date.ToString("dd/MM/yyyy");
                                bitem.zone_name = "High Inventory Zone";
                                bitem.count = item.count;
                                bitem.closing_quantity = item.closing_quantity;
                                nList.Add(bitem);
                                break;
                            case "Yellow":
                                total_count += item.count;
                                bitem.zone = "Yellow";
                                bitem.date = date.ToString("dd/MM/yyyy");
                                bitem.zone_name = "Right Inventory Zone";
                                bitem.count = item.count;
                                bitem.closing_quantity = item.closing_quantity;
                                nList.Add(bitem);
                                break;
                            case "Red":
                                total_count += item.count;
                                bitem.zone = "Red";
                                bitem.date = date.ToString("dd/MM/yyyy");
                                bitem.zone_name = "Low Inventory Zone";
                                bitem.count = item.count;
                                bitem.closing_quantity = item.closing_quantity;
                                nList.Add(bitem);
                                break;

                            case "Black":
                                total_count += item.count;
                                bitem.zone = "Black";
                                bitem.date = date.ToString("dd/MM/yyyy");
                                bitem.zone_name = "Stockout Inventory Zone";
                                bitem.count = item.count;
                                bitem.closing_quantity = item.closing_quantity;
                                nList.Add(bitem);
                                break;
                        }
                    }

                    foreach (var item in nList)
                    {
                        double per = (double)item.count / (double)total_count;
                        item.percentage = Math.Round(per * 100, 2);
                    }

                    int iTotalRecords = nList.Count;
                    DataTablePager<BufferZoneList> dataTablePager = new DataTablePager<BufferZoneList>(jsonAOData, nList.AsQueryable());
                    dataTablePager.iTotalRecords = iTotalRecords;
                    FormattedList<BufferZoneList> formattedList = dataTablePager.Filter(isExport: true);
                    formattedListOfList.Add(formattedList);
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                    _log.Info("GetBufferZones end: " + DateTime.Now.ToString("o"));
                }
                return formattedListOfList;
            }
            catch (Exception e)
            {
                _log.Error("GetBufferZones::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<SKUGrid> GetSKUGrid(List<jsonAOData> jsonData)
        {
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<SKUGrid> _lst = new List<SKUGrid>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_GRID);

                PropertyInfo[] props = filterdata.GetType().GetProperties();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                // For Getting Permission ID START
                IMongoQuery _queryNode_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                IMongoQuery _querySKU_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                IMongoQuery _queryCategory_per = Common.Instance.GetPermissionObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                // For Getting Permission ID END

                int i = 0;
                int distCnt = dict.Count + 3;
                IMongoQuery[] qc = new IMongoQuery[distCnt];

                qc[i++] = _queryNode_per;
                qc[i++] = _querySKU_per;
                qc[i++] = _queryCategory_per;

                if (filterdata.skuSelection != null)
                    qc[i++] = Query.In("sku_id", new BsonArray(filterdata.skuSelection));
                if (filterdata.categorySelection != null)
                    qc[i++] = Query.In("category_id", new BsonArray(filterdata.categorySelection));
                if (filterdata.nodeSelection != null)
                    qc[i++] = Query.In("node_id", new BsonArray(filterdata.nodeSelection));
                if (filterdata.bomSelection != null)
                    qc[i++] = Query.In("sku_id", new BsonArray(filterdata.bomSelection));

                DataTablePager<SKUGrid> dataTablePager1 = new DataTablePager<SKUGrid>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();
                IMongoSortBy sortQuery = SortBy.Ascending("_id");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;

                IMongoQuery query = distCnt > 0 ? distCnt == 1 ? qc[0] : Query.And(qc) : Query.Null;
                _lst = MongoHelper<SKUGrid>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                DataTablePager<SKUGrid> dataTablePager = new DataTablePager<SKUGrid>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<SKUGrid> formattedList = dataTablePager.Filter(isExport: true);

                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetSKUGrid::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<ImportExportLog> GetImportLogList(List<jsonAOData> jsonData)
        {
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                List<ImportExportLog> _lst = new List<ImportExportLog>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG);

                DataTablePager<ImportExportLog> dataTablePager1 = new DataTablePager<ImportExportLog>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();

                IMongoSortBy sortQuery = SortBy.Descending("reference_date");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = Query.EQ("type", "Import");
                _lst = MongoHelper<ImportExportLog>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                foreach (var item in _lst)
                {
                    if (item.log_file != null)
                        item.log_file = AppSettings.ServerApplication + item.log_file.Replace("\\", "/");
                }

                DataTablePager<ImportExportLog> dataTablePager = new DataTablePager<ImportExportLog>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<ImportExportLog> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetImportLogList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<ImportExportLog> GetExportLogList(List<jsonAOData> jsonData)
        {
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                List<ImportExportLog> _lst = new List<ImportExportLog>();
                string collection = Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG);

                DataTablePager<ImportExportLog> dataTablePager1 = new DataTablePager<ImportExportLog>(jsonAOData, _lst.AsQueryable());
                var sorted = dataTablePager1.searchAndSortables.Where(x => x.IsCurrentlySorted == true).OrderBy(x => x.SortOrder).ToList();

                IMongoSortBy sortQuery = SortBy.Descending("reference_date");
                sorted.ForEach(sort =>
                {
                    sortQuery = sort.SortDirection.ToString().ToLower() == "asc" ? SortBy.Ascending(sort.Name) : SortBy.Descending(sort.Name);
                });

                int iTotalRecords = 0;
                IMongoQuery query = Query.EQ("type", "Export");
                _lst = MongoHelper<ImportExportLog>.Paginate(collection, query, sorted, out iTotalRecords, displayLength, displayStart);

                foreach (var item in _lst)
                {
                    if (item.log_file != null)
                        item.log_file = AppSettings.ServerApplication + item.log_file.Replace("\\", "/");
                }

                DataTablePager<ImportExportLog> dataTablePager = new DataTablePager<ImportExportLog>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<ImportExportLog> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetImportLogList::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<InventoryTurns> GetInventoryTurns(List<jsonAOData> jsonData)
        {
            _log.Info("GetInventoryTurns start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<InventoryTurns> _lst = new List<InventoryTurns>();
                string it_collection = Common.Instance.GetEnumValue(Common.CollectionName.INVENTORY_TURNS);
                string its_collection = Common.Instance.GetEnumValue(Common.CollectionName.INVENTORY_TURNS_SKU);

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);


                {
                    var sort = new BsonDocument
                    {
                        {
                            "$sort",
                            new BsonDocument
                                {
                                    {"category_id", 1}
                                }
                        }
                    };

                    var match = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "transfilter_date", new BsonDocument { { "$gte", fromDate }, { "$lte", toDate } }
                                    }
                                }
                        }
                    };

                    var group = new BsonDocument
                    {
                        { "$group",
                            new BsonDocument
                                {
                                    { "_id", new BsonDocument
                                                 {
                                                     { "category_id","$category_id" },
                                                     { "category_name","$category_name" }
                                                 }
                                    },
                                    {
                                        "count", new BsonDocument
                                                     {
                                                         { "$sum", "$count" }
                                                     }
                                    },
                                    {
                                        "sales", new BsonDocument
                                                     {
                                                         { "$sum", "$sales" }
                                                     }
                                    },
                                    {
                                        "closing_quantity", new BsonDocument
                                                     {
                                                         { "$sum", "$closing_quantity" }
                                                     }
                                    },
                                    {
                                        "transaction_dates", new BsonDocument
                                                    {
                                                        { "$addToSet", "$transaction_date" }
                                                    }
                                    }
                                }
                      }
                    };

                    var project = new BsonDocument
                    {
                        {
                            "$project",
                            new BsonDocument
                                {
                                    {"_id", 0},
                                    {"category_name","$_id.category_name"},
                                    {"transaction_dates", 1},
                                    {"count", 1},
                                    {"sales", 1},
                                    {"closing_quantity", 1}
                                }
                        }
                    };

                    var matchSKU = new BsonDocument();
                    if (filterdata.skuSelection != null)
                    {
                        matchSKU = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                        }
                                    }
                            }
                        };
                    }

                    var matchCategory = new BsonDocument();
                    if (filterdata.categorySelection != null)
                    {
                        matchCategory = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                        }
                                    }
                            }
                        };
                    }

                    var matchNode = new BsonDocument();
                    if (filterdata.nodeSelection != null)
                    {
                        matchNode = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                        }
                                    }
                            }
                        };
                    }

                    bool _matchSKUPermission = false;
                    long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                    if (_matchArraySKUPermission != null)
                        if (_matchArraySKUPermission.Length > 0)
                            _matchSKUPermission = true;

                    bool _matchCategoryPermission = false;
                    long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                    if (_matchArrayCategoryPermission != null)
                        if (_matchArrayCategoryPermission.Length > 0)
                            _matchCategoryPermission = true;

                    bool _matchNodePermission = false;
                    long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                    if (_matchArrayNodePermission != null)
                        if (_matchArrayNodePermission.Length > 0)
                            _matchNodePermission = true;

                    var matchSupplyNodePermission = new BsonDocument();
                    bool _matchSupplyNodePermission = false;
                    long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                    if (_matchArraySupplyNodePermission != null)
                        if (_matchArraySupplyNodePermission.Length > 0)
                            _matchSupplyNodePermission = true;

                    _log.Info("GetInventoryTurns query execution start: " + DateTime.Now.ToString("o"));
                    if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                    {
                        //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(its_collection);
                        //var pipeline = new[] { match, group, project, sort };

                        //if (_matchSKUPermission && _matchCategoryPermission)
                        //    pipeline = new[] { match, matchSKUPermission, matchCategoryPermission, group, project, sort };

                        //var r = repl.Aggregate(pipeline);
                        //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<InventoryTurns>).ToList();

                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeSKU(param);
                    }
                    else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                        param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                        param.idCategory = filterdata.categorySelection.ToList<long>();

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();


                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeSKU(param);
                    }
                    else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                        param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                        if (_matchCategoryPermission)
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeSKU(param);
                    }
                    else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                        param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                        if (_matchSKUPermission)
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeCategory(param);
                    }
                    else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                    {
                        //MongoCollection<InventoryTurns> repl = MongoHelper<InventoryTurns>.GetCollection(its_collection);
                        //var pipeline = new[] { match, matchSKU, group, project, sort };

                        //var r = repl.Aggregate(pipeline);
                        //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<InventoryTurns>).ToList();

                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                        if (_matchCategoryPermission && _matchNodePermission)
                        {
                            if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                            else
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeSKU(param);

                    }
                    else if (filterdata.categorySelection != null)
                    {
                        //MongoCollection<InventoryTurns> repl = MongoHelper<InventoryTurns>.GetCollection(it_collection);
                        //var pipeline = new[] { match, matchCategory, group, project, sort };

                        //var r = repl.Aggregate(pipeline);
                        //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<InventoryTurns>).ToList();

                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                        if (_matchSKUPermission && _matchNodePermission)
                        {
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNodeCategory(param);

                    }
                    else if (filterdata.nodeSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                        if (_matchSKUPermission && _matchCategoryPermission)
                        {
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        }

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNode(param);
                    }
                    else if (filterdata.supplynodeSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;
                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.supplynodeSelection == null)
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        else
                            param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                        //_lst = Dao.InventoryTurns().SelectByNode(param);
                    }
                    else if (filterdata.bomSelection != null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        _lst = Dao.InventoryTurns().SelectInventoryTurnsReport(param);
                    }
                }
                _log.Info("GetInventoryTurns query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = _lst.Count;
                _lst = _lst.Skip(displayStart).Take(displayLength).ToList();
                DataTablePager<InventoryTurns> dataTablePager = new DataTablePager<InventoryTurns>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<InventoryTurns> formattedList = dataTablePager.Filter(isExport: true);

                _log.Info("GetInventoryTurns end: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetInventoryTurns::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<StockoutAvailabilityChart> GetAvailabilityChart(List<jsonAOData> jsonData)
        {
            _log.Info("GetAvailabilityChart started: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<StockoutAvailabilityChart> _lst = new List<StockoutAvailabilityChart>();

                string sc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART);
                string nsc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_NODE);
                string csc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_CATEGORY);
                string ssc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_SKU);

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                int iTotalRecords = 0;

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                MapperBatch paramRepl = new MapperBatch();
                paramRepl.fromdate = sqlFromDate;
                paramRepl.todate = sqlToDate;
                List<Replenishment> _lstReplDates = new List<Replenishment>();

                _lstReplDates = Dao.Replenishment().SelectReplenishmentDates(paramRepl);
                if (_lstReplDates.Count > 0)
                {

                    if (filterdata.skuSelection == null && filterdata.categorySelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        foreach (Replenishment _repl in _lstReplDates)
                        {
                            param.date = _repl.transaction_date;
                            List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                            _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            if (_chrt != null)
                                if (_chrt.Count > 0)
                                    _lst.AddRange(_chrt);
                        }

                        //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                        //_lst = Dao.StockoutAvailability().SelectByNodeSKU(param);

                        //_lst = MongoHelper<StockoutAvailabilityChart>.Find(sc_collection, Query.And(Query.GTE("transfilter_date", fromDate), Query.LTE("transfilter_date", toDate)));
                    }
                    else
                    {
                        var sort = new BsonDocument
                    {
                        {
                            "$sort",
                            new BsonDocument
                                {
                                    {"transfilter_date", 1}
                                }
                        }
                    };

                        var match = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "transfilter_date", new BsonDocument { { "$gte", fromDate }, { "$lte", toDate } }
                                    }
                                }
                        }
                    };

                        var group = new BsonDocument
                    {
                        { "$group",
                            new BsonDocument
                                {
                                    { "_id", new BsonDocument
                                                 {
                                                     { "transfilter_date","$transfilter_date" },
                                                     { "display_date","$display_date" }
                                                 }
                                    },
                                        {
                                            "total_sku_count", new BsonDocument
                                                         {
                                                             { "$sum", "$total_sku_count" }
                                                         }
                                        },
                                        {
                                            "rb_sku_count", new BsonDocument
                                                         {
                                                             { "$sum", "$rb_sku_count"}
                                                         }
                                        }
                                }
                        }
                    };

                        var project = new BsonDocument
                    {
                        {
                            "$project",
                            new BsonDocument
                                {
                                    {"_id", 0},
                                    {"transfilter_date","$_id.transfilter_date"},
                                    {"display_date","$_id.display_date"},
                                    {"total_sku_count", 1},
                                    {"rb_sku_count", 1}
                                }
                        }
                    };

                        var matchSKU = new BsonDocument();
                        if (filterdata.skuSelection != null)
                        {
                            matchSKU = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchCategory = new BsonDocument();
                        if (filterdata.categorySelection != null)
                        {
                            matchCategory = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchNode = new BsonDocument();
                        if (filterdata.nodeSelection != null)
                        {
                            matchNode = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                        }
                                    }
                            }
                        };
                        }


                        _log.Info("GetAvailabilityChart query execution start: " + DateTime.Now.ToString("o"));
                        if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeSKU(param);
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeSKU(param);
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeCategory(param);
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission && _matchNodePermission)
                            {
                                if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                                else
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeSKU(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(ssc_collection);
                            //var pipeline = new[] { match, matchSKU, group, sort, project };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<StockoutAvailabilityChart>).ToList();
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(csc_collection);
                            //var pipeline = new[] { match, matchCategory, group, sort, project };

                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<StockoutAvailabilityChart>).ToList();
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();


                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(nsc_collection);
                            //var pipeline = new[] { match, matchNode, group, sort, project };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<StockoutAvailabilityChart>).ToList();
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<StockoutAvailabilityChart> _chrt = new List<StockoutAvailabilityChart>();
                                _chrt = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.StockoutAvailability().SelectStockoutAvailabilityReport(param);
                            //_lst = Dao.StockoutAvailability().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(nsc_collection);
                            //var pipeline = new[] { match, matchNode, group, sort, project };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<StockoutAvailabilityChart>).ToList();
                        }
                        _log.Info("GetAvailabilityChart query execution end: " + DateTime.Now.ToString("o"));
                    }
                }

                _lst = _lst.OrderBy(x => x.transfilter_date).ToList();
                iTotalRecords = _lst.Count;
                DataTablePager<StockoutAvailabilityChart> dataTablePager = new DataTablePager<StockoutAvailabilityChart>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<StockoutAvailabilityChart> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetAvailabilityChart ended: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetAvailabilityChart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<ExcessInventoryChart> GetExcessInventoryChart(List<jsonAOData> jsonData)
        {
            _log.Info("GetExcessInventoryChart started: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<ExcessInventoryChart> _lst = new List<ExcessInventoryChart>();

                string sc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART);
                string nsc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_NODE);
                string csc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_CATEGORY);
                string ssc_collection = Common.Instance.GetEnumValue(Common.CollectionName.SKU_CHART_SKU);

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                int iTotalRecords = 0;

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                MapperBatch paramRepl = new MapperBatch();
                paramRepl.fromdate = sqlFromDate;
                paramRepl.todate = sqlToDate;
                List<Replenishment> _lstReplDates = new List<Replenishment>();

                _lstReplDates = Dao.Replenishment().SelectReplenishmentDates(paramRepl);
                if (_lstReplDates.Count > 0)
                {
                    if (filterdata.skuSelection == null && filterdata.categorySelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                        foreach (Replenishment _repl in _lstReplDates)
                        {
                            param.date = _repl.transaction_date;
                            List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                            _chrt = Dao.ExcessInventory().SelectReportData(param);
                            if (_chrt != null)
                                if (_chrt.Count > 0)
                                    _lst.AddRange(_chrt);
                        }

                        //_lst = MongoHelper<ExcessInventoryChart>.Find(sc_collection, Query.And(Query.GTE("transfilter_date", fromDate), Query.LTE("transfilter_date", toDate)));
                    }
                    else
                    {
                        var sort = new BsonDocument
                    {
                        {
                            "$sort",
                            new BsonDocument
                                {
                                    {"transfilter_date", 1}
                                }
                        }
                    };

                        var match = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "transfilter_date", new BsonDocument { { "$gte", fromDate }, { "$lte", toDate } }
                                    }
                                }
                        }
                    };

                        var group = new BsonDocument
                    {
                        { "$group",
                            new BsonDocument
                                {
                                    { "_id", new BsonDocument
                                                 {
                                                     { "transfilter_date","$transfilter_date" },
                                                     { "display_date","$display_date" }
                                                 }
                                    },
                                    {
                                        "total_sku_count", new BsonDocument
                                                     {
                                                         { "$sum", "$total_sku_count" }
                                                     }
                                    },
                                    {
                                        "blue_sku_count", new BsonDocument
                                                     {
                                                         { "$sum", "$blue_sku_count"}
                                                     }
                                    }
                                }
                      }
                    };

                        var project = new BsonDocument
                    {
                        {
                            "$project",
                            new BsonDocument
                                {
                                    {"_id", 0},
                                    {"transfilter_date","$_id.transfilter_date"},
                                    {"display_date","$_id.display_date"},
                                    {"total_sku_count", 1},
                                    {"blue_sku_count", 1}
                                }
                        }
                    };

                        var matchSKU = new BsonDocument();
                        if (filterdata.skuSelection != null)
                        {
                            matchSKU = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchCategory = new BsonDocument();
                        if (filterdata.categorySelection != null)
                        {
                            matchCategory = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchNode = new BsonDocument();
                        if (filterdata.nodeSelection != null)
                        {
                            matchNode = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        _log.Info("GetExcessInventoryChart query execution start: " + DateTime.Now.ToString("o"));
                        if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.ExcessInventory().SelectByNodeSKU(param);
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.ExcessInventory().SelectByNodeSKU(param);
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.ExcessInventory().SelectByNodeCategory(param);
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission && _matchNodePermission)
                            {
                                if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                                else
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            // _lst = Dao.ExcessInventory().SelectByNodeSKU(param);

                            ////MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(ssc_collection);
                            ////var pipeline = new[] { match, matchSKU, group, project, sort };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<ExcessInventoryChart>).ToList();
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id2 = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            //_lst = Dao.ExcessInventory().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(csc_collection);
                            //var pipeline = new[] { match, matchCategory, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<ExcessInventoryChart>).ToList();
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;

                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            // _lst = Dao.ExcessInventory().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(nsc_collection);
                            //var pipeline = new[] { match, matchNode, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<ExcessInventoryChart>).ToList();
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<ExcessInventoryChart> _chrt = new List<ExcessInventoryChart>();
                                _chrt = Dao.ExcessInventory().SelectReportData(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst.AddRange(_chrt);
                            }

                            // _lst = Dao.ExcessInventory().SelectByNodeCategory(param);

                            //MongoCollection<SKUChart> repl = MongoHelper<SKUChart>.GetCollection(nsc_collection);
                            //var pipeline = new[] { match, matchNode, group, project, sort };
                            //var r = repl.Aggregate(pipeline);
                            //_lst = r.ResultDocuments.Select(BsonSerializer.Deserialize<ExcessInventoryChart>).ToList();
                        }
                    }
                }
                _log.Info("GetExcessInventoryChart query execution end: " + DateTime.Now.ToString("o"));

                _lst = _lst.OrderBy(x => x.transfilter_date).ToList();
                iTotalRecords = _lst.Count;
                DataTablePager<ExcessInventoryChart> dataTablePager = new DataTablePager<ExcessInventoryChart>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<ExcessInventoryChart> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetExcessInventoryChart ended: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetExcessInventoryChart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<CntQtyFillRateChart> GetCountQuantityChart(List<jsonAOData> jsonData)
        {
            _log.Info("GetCountQuantityChart started: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                string cqfr_collection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE);
                string ncqfr_collection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_NODE);
                string ccqfr_collection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_CATEGORY);
                string scqfr_collection = Common.Instance.GetEnumValue(Common.CollectionName.CNTQTY_FILLRATE_SKU);

                List<CntQtyFillRateChart> _final_lst = new List<CntQtyFillRateChart>();
                List<CntQtyFillRateChart> _lst = new List<CntQtyFillRateChart>();
                List<CntQtyFillRateChart> _lst1 = new List<CntQtyFillRateChart>();
                List<CntQtyFillRateChart> _lst2 = new List<CntQtyFillRateChart>();

                Dictionary<string, object> dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(aoDataList.GetNVValue("filterdata"));

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                int iTotalRecords = 0;

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                MapperBatch paramRepl = new MapperBatch();
                paramRepl.fromdate = sqlFromDate;
                paramRepl.todate = sqlToDate;
                List<Replenishment> _lstReplDates = new List<Replenishment>();

                _lstReplDates = Dao.Replenishment().SelectReplenishmentDates(paramRepl);
                if (_lstReplDates.Count > 0)
                {

                    if (filterdata.skuSelection == null && filterdata.categorySelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null)
                    {
                        MapperBatch param = new MapperBatch();
                        param.fromdate = sqlFromDate;
                        param.todate = sqlToDate;

                        if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                        {
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                        }

                        if (filterdata.bomSelection != null)
                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();


                        foreach (Replenishment _repl in _lstReplDates)
                        {
                            param.date = _repl.transaction_date;
                            List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                            _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            if (_chrt != null)
                                if (_chrt.Count > 0)
                                    _lst2.AddRange(_chrt);
                        }

                        //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                        //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeSKU(param);

                        // _lst2 = MongoHelper<CntQtyFillRateChart>.Find(cqfr_collection, Query.And(Query.GTE("transfilter_date", fromDate), Query.LTE("transfilter_date", toDate)));
                    }
                    else
                    {
                        var sort = new BsonDocument
                    {
                        {
                            "$sort",
                            new BsonDocument
                                {
                                    {"transfilter_date", 1}
                                }
                        }
                    };

                        var match = new BsonDocument
                    {
                        {
                            "$match",
                            new BsonDocument
                                {
                                    {
                                        "transfilter_date", new BsonDocument { { "$gte", fromDate }, { "$lte", toDate } }
                                    },
                                    { "closing_stock_zone",new BsonDocument{{"$ne","White"}} }
                                }
                        }
                    };

                        var matchSKU = new BsonDocument();
                        if (filterdata.skuSelection != null)
                        {
                            matchSKU = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "sku_id", new BsonDocument { { "$in", new BsonArray(filterdata.skuSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchCategory = new BsonDocument();
                        if (filterdata.categorySelection != null)
                        {
                            matchCategory = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "category_id", new BsonDocument { { "$in", new BsonArray(filterdata.categorySelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        var matchNode = new BsonDocument();
                        if (filterdata.nodeSelection != null)
                        {
                            matchNode = new BsonDocument
                        {
                            {
                                "$match",
                                new BsonDocument
                                    {
                                        {
                                            "node_id", new BsonDocument { { "$in", new BsonArray(filterdata.nodeSelection) } }
                                        }
                                    }
                            }
                        };
                        }

                        // For Group by transfilter_date and closing_stock_zone START
                        var group = new BsonDocument
                    {
                        { "$group",
                            new BsonDocument
                                {
                                    { "_id", new BsonDocument
                                                 {
                                                     { "transfilter_date", "$transfilter_date" },
                                                     { "display_date", "$display_date" },
                                                     { "closing_stock_zone", "$closing_stock_zone" }
                                                 }
                                    },
                                    {
                                        "sku_ids", new BsonDocument
                                                    {
                                                        { "$addToSet", "$str_sku_ids" }
                                                    }
                                    },
                                    { "buffer_norm", new BsonDocument
                                                     {
                                                         { "$sum", "$buffer_norm" }
                                                     }
                                    },
                                    { "closing_quantity", new BsonDocument
                                                     {
                                                         { "$sum", "$closing_quantity" }
                                                     }
                                    }
                               }
                        }
                    };

                        var project = new BsonDocument
                    {
                        {
                            "$project",
                            new BsonDocument
                                {
                                    {"_id", 0},
                                    {"transfilter_date","$_id.transfilter_date"},
                                    {"display_date","$_id.display_date"},
                                    {"closing_stock_zone","$_id.closing_stock_zone"},
                                    {"buffer_norm",1},
                                    {"closing_quantity", 1},
                                    {"sku_ids", 1}
                                }
                        }
                    };

                        _log.Info("GetCountQuantityChart(Group by transfilter_date and closing_stock_zone) query execution start: " + DateTime.Now.ToString("o"));
                        if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null && filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeSKU(param);
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            // _lst2 = Dao.CntQtyFillRateChart().SelectByNodeSKU(param);
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeCategory(param);
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission && _matchNodePermission)
                            {
                                if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                                else
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeSKU(param);

                            ////MongoCollection<CntQtyFillRateChart> repl = MongoHelper<CntQtyFillRateChart>.GetCollection(scqfr_collection);
                            ////var pipeline = new[] { match, matchSKU, sort, group, project };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst2 = r.ResultDocuments.Select(BsonSerializer.Deserialize<CntQtyFillRateChart>).ToList();
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();


                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeCategory(param);

                            ////MongoCollection<CntQtyFillRateChart> repl = MongoHelper<CntQtyFillRateChart>.GetCollection(ccqfr_collection);
                            ////var pipeline = new[] { match, matchCategory, sort, group, project };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst2 = r.ResultDocuments.Select(BsonSerializer.Deserialize<CntQtyFillRateChart>).ToList();
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeCategory(param);

                            ////MongoCollection<CntQtyFillRateChart> repl = MongoHelper<CntQtyFillRateChart>.GetCollection(ncqfr_collection);
                            ////var pipeline = new[] { match, matchNode, sort, group, project };
                            ////var r = repl.Aggregate(pipeline);
                            ////_lst2 = r.ResultDocuments.Select(BsonSerializer.Deserialize<CntQtyFillRateChart>).ToList();
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            MapperBatch param = new MapperBatch();
                            param.fromdate = sqlFromDate;
                            param.todate = sqlToDate;

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            foreach (Replenishment _repl in _lstReplDates)
                            {
                                param.date = _repl.transaction_date;
                                List<CntQtyFillRateChart> _chrt = new List<CntQtyFillRateChart>();
                                _chrt = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                                if (_chrt != null)
                                    if (_chrt.Count > 0)
                                        _lst2.AddRange(_chrt);
                            }

                            //_lst2 = Dao.CntQtyFillRateChart().SelectCntQtyFillRateChartReport(param);
                            //_lst2 = Dao.CntQtyFillRateChart().SelectByNodeCategory(param);
                        }
                    }
                }
                _log.Info("GetCountQuantityChart(Group by transfilter_date and closing_stock_zone) query execution end: " + DateTime.Now.ToString("o"));

                // For Group by transfilter_date and closing_stock_zone END

                ////foreach (var item in _lst2)
                ////{
                ////    if (item.sku_ids != null)
                ////    {
                ////        int cnt = 0;
                ////        foreach (var skuIds in item.sku_ids)
                ////        {
                ////            cnt += skuIds.Split(',').Length;
                ////        }
                ////        item.zone_count = cnt;
                ////    }
                ////    else if (item.str_sku_ids != null)
                ////    {
                ////        item.zone_count = item.str_sku_ids.Split(',').Length;
                ////    }
                ////}

                _lst1 = _lst2.GroupBy(x => x.transfilter_date)
                            .Select(grp => new CntQtyFillRateChart
                            {
                                all_sku_count = grp.First().all_sku_count,
                                transfilter_date = grp.First().transfilter_date,
                                display_date = grp.First().display_date,
                                all_count = grp.Sum(s => s.zone_count),
                                bry_shortage_qty = grp.Sum(s =>
                                    s.closing_stock_zone == "Black"
                                    || s.closing_stock_zone == "Red"
                                    || s.closing_stock_zone == "Yellow"
                                    ? s.buffer_norm : 0) -
                                    grp.Sum(s =>
                                    s.closing_stock_zone == "Black"
                                    || s.closing_stock_zone == "Red"
                                    || s.closing_stock_zone == "Yellow"
                                    ? s.closing_quantity : 0),
                                bry_count = grp.Count(s =>
                                    s.closing_stock_zone == "Black"
                                    || s.closing_stock_zone == "Red"
                                    || s.closing_stock_zone == "Yellow"),
                                total_buffer_norm = grp.Sum(s => s.buffer_norm)
                            }).ToList();

                // For Inner Join list of (Group by transfilter_date) and list of (Group by transfilter_date and closing_stock_zone) START
                _lst = (from l2 in _lst2
                        join l1 in _lst1
                                     on l2.transfilter_date equals l1.transfilter_date
                        select new CntQtyFillRateChart()
                        {
                            all_sku_count = l1.all_sku_count,
                            display_date = l2.display_date,
                            transfilter_date = l2.transfilter_date,
                            closing_stock_zone = l2.closing_stock_zone,
                            zone_count = l2.zone_count,
                            zone_per = l1.all_count > 0 ? Math.Round((l2.zone_count / l1.all_count) * 100, 2) : 0,
                            buffer_norm = Math.Round(l2.buffer_norm, 2),
                            closing_quantity = l2.closing_quantity,
                            shortage_qty = (l2.buffer_norm - l2.closing_quantity) < 0 ? 0 : Math.Round(l2.buffer_norm - l2.closing_quantity, 2),
                            bry_count = l1.bry_count,
                            all_count = l1.all_count,
                            total_buffer_norm = l1.total_buffer_norm,
                            shortage_qty_per = l1.total_buffer_norm > 0 ? Math.Round(((l2.buffer_norm - l2.closing_quantity) < 0 ? 0 : (l2.buffer_norm - l2.closing_quantity) / l1.total_buffer_norm) * 100, 2) : 0,
                            bry_shortage_qty = Math.Round(l1.bry_shortage_qty, 2),
                            count_fill_rate = l1.all_count > 0 ? Math.Round(((1 - (l1.bry_count / l1.all_count)) * 100), 2) : 0,
                            qty_fill_rate = l1.total_buffer_norm > 0 ? Math.Round((100 - ((l1.bry_shortage_qty / l1.total_buffer_norm) * 100)), 2) : 0
                        }).ToList();
                // For Inner Join list of (Group by transfilter_date) and list of (Group by transfilter_date and closing_stock_zone) END

                _lst = _lst.OrderBy(x => x.transfilter_date).ToList();

                int[] _dateList = _lst.Select(x => x.transfilter_date).Distinct().ToArray();
                foreach (var _date in _dateList)
                {
                    CntQtyFillRateChart _obj = new CntQtyFillRateChart();

                    _obj = _lst.FirstOrDefault(r => r.transfilter_date == _date && r.closing_stock_zone == "Blue");
                    if (_obj != null)
                    {
                        _obj.cnt_fill_rate = _obj.count_fill_rate.ToString();
                        _obj.quantity_fill_rate = _obj.qty_fill_rate.ToString();
                        _final_lst.Add(_obj);
                    }

                    _obj = _lst.FirstOrDefault(r => r.transfilter_date == _date && r.closing_stock_zone == "Green");
                    if (_obj != null)
                    {
                        _obj.display_date = "";
                        _obj.cnt_fill_rate = "";
                        _obj.quantity_fill_rate = "";
                        _final_lst.Add(_obj);
                    }

                    _obj = _lst.FirstOrDefault(r => r.transfilter_date == _date && r.closing_stock_zone == "Yellow");
                    if (_obj != null)
                    {
                        _obj.display_date = "";
                        _obj.cnt_fill_rate = "";
                        _obj.quantity_fill_rate = "";
                        _final_lst.Add(_obj);
                    }

                    _obj = _lst.FirstOrDefault(r => r.transfilter_date == _date && r.closing_stock_zone == "Red");
                    if (_obj != null)
                    {
                        _obj.display_date = "";
                        _obj.cnt_fill_rate = "";
                        _obj.quantity_fill_rate = "";
                        _final_lst.Add(_obj);
                    }

                    _obj = _lst.FirstOrDefault(r => r.transfilter_date == _date && r.closing_stock_zone == "Black");
                    if (_obj != null)
                    {
                        _obj.display_date = "";
                        _obj.cnt_fill_rate = "";
                        _obj.quantity_fill_rate = "";
                        _final_lst.Add(_obj);
                    }
                }

                iTotalRecords = _final_lst.Count;
                DataTablePager<CntQtyFillRateChart> dataTablePager = new DataTablePager<CntQtyFillRateChart>(jsonAOData, _final_lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<CntQtyFillRateChart> formattedList = dataTablePager.Filter(isExport: true);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                _log.Info("GetCountQuantityChart ended: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetCountQuantityChart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<dynamic> GetMatrix1Chart(List<jsonAOData> jsonData)
        {
            _log.Info("GetMatrix1Chart start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<dynamic> _lst = new List<dynamic>();

                string Date = "";

                if (filterdata.date != null)
                {
                    string[] date = filterdata.date.Split('/');
                    Date = date[2] + "-" + date[1] + "-" + date[0];
                }

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                _log.Info("GetMatrix1Chart query execution start: " + DateTime.Now.ToString("o"));

                MapperBatch param = new MapperBatch();
                param.date = Date;

                if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                {
                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.ToList<long>();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission)
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission)
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                {

                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission && _matchNodePermission)
                    {
                        if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                        else
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);

                }
                else if (filterdata.categorySelection != null)
                {
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchNodePermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchCategoryPermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.supplynodeSelection != null)
                {

                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }
                else if (filterdata.bomSelection != null)
                {

                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.bomSelection.Length > 0)
                        param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                    param.node = Dao.Matrix1().SelectMatrix1Node(param);
                    if (param.node.Count <= 0) param.node = null;

                    _lst = Dao.Matrix1().SelectReports(param);
                }

                _log.Info("GetMatrix1Chart query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = _lst.Count;

                List<dynamic> _lstTotal = _lst;

                if (_lstTotal.Count <= 1)
                    _lst = new List<dynamic>();

                _lst = _lst.Skip(displayStart).Take(displayLength).ToList();

                if (_lst.Count > 1)
                {
                    _lst.Add(_lstTotal[iTotalRecords - 1]);
                }

                DataTablePager<dynamic> dataTablePager = new DataTablePager<dynamic>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<dynamic> formattedList = dataTablePager.Filter(isExport: true);

                _log.Info("GetMatrix1Chart end: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetMatrix1Chart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }

        }

        public List<Matrix1> GetMatrix1Column(DataFilter param = null)
        {
            try
            {
                MapperBatch param1 = new MapperBatch();

                if (param.nodeSelection == null)
                    param1.idNode = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "object_id").ToList();
                else
                    param1.idNode = param.nodeSelection.ToList();

                param1.node = Dao.Matrix1().SelectMatrix1Node(param1);

                if (param1.node.Count <= 0) param1.node = null;
                List<Matrix1> _lst = new List<Matrix1>();
                _lst = Dao.Matrix1().SelectMatrix1Column(param1);

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetMatrix1Column::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }


        public List<Matrix1> GetMatrix2Column(DataFilter filterdata = null)
        {
            try
            {
                List<Matrix1> _lst = new List<Matrix1>();

                Matrix1 _obj = new Matrix1(); _obj.column_name = "Node Code"; _obj.column_value = "node_code"; _lst.Add(_obj);
                _obj = new Matrix1(); _obj.column_name = "Node Name"; _obj.column_value = "node_name"; _lst.Add(_obj);
                _obj = new Matrix1(); _obj.column_name = "SKU Code"; _obj.column_value = "sku_code"; _lst.Add(_obj);
                _obj = new Matrix1(); _obj.column_name = "SKU Name"; _obj.column_value = "sku_name"; _lst.Add(_obj);

                string sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                MapperBatch param = new MapperBatch();
                param.fromdate = sqlFromDate;
                param.todate = sqlToDate;
                param.replenishment = Dao.Replenishment().SelectReplenishmentDates(param);

                if (param.replenishment != null)
                {
                    foreach (Replenishment _rpl in param.replenishment)
                    {
                        _obj = new Matrix1();

                        string[] dateFormate = _rpl.transaction_date.Split('-');
                        string _transDate = dateFormate[2] + "/" + dateFormate[1] + "/" + dateFormate[0];

                        _obj.column_name = _transDate;
                        _obj.column_value = _transDate;
                        _lst.Add(_obj);
                    }
                }

                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
                return _lst;
            }
            catch (Exception e)
            {
                _log.Error("GetMatrix1Column::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }

        public FormattedList<dynamic> GetMatrix2Chart(List<jsonAOData> jsonData)
        {
            _log.Info("GetMatrix2Chart start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<dynamic> _lst = new List<dynamic>();

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                _log.Info("GetMatrix2Chart query execution start: " + DateTime.Now.ToString("o"));

                int recCount = 0;
                MapperBatch param = new MapperBatch();
                param.fromdate = sqlFromDate;
                param.todate = sqlToDate;
                param.replenishment = Dao.Replenishment().SelectReplenishmentDates(param);

                if (param.replenishment != null)
                {
                    if (param.replenishment.Count > 0)
                    {
                        int limit = displayLength;
                        int cnt = displayLength;
                        int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                        if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                        {
                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            }

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.ToList<long>();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission)
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission)
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                        {

                            param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                            if (_matchCategoryPermission && _matchNodePermission)
                            {
                                if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                                else
                                    param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.categorySelection != null)
                        {
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchNodePermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.nodeSelection != null)
                        {
                            param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                            if (_matchSKUPermission && _matchCategoryPermission)
                            {
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.supplynodeSelection != null)
                        {
                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.supplynodeSelection == null)
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            else
                                param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                            if (filterdata.bomSelection != null)
                                if (filterdata.bomSelection.Length > 0)
                                    param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                        else if (filterdata.bomSelection != null)
                        {

                            if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                            {
                                param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                                param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                                param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                                param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                            }

                            if (filterdata.bomSelection.Length > 0)
                                param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();

                            if (param.replenishment.Count <= 0) param.replenishment = null;

                            Matrix1 _objMatrix1 = Dao.Matrix1().SelectMatrix2RecordCount(param);
                            if (_objMatrix1 != null) recCount = _objMatrix1.rec_count;

                            if (recCount > 0)
                            {
                                for (int offset = 0; offset < loopCount; offset++)
                                {
                                    param.limit = limit;
                                    param.offset = displayStart;

                                    List<dynamic> _batchList = Dao.Matrix1().SelectMatrix2Report(param);
                                    if (_batchList != null)
                                        if (_batchList.Count > 0)
                                            _lst.AddRange(_batchList);
                                }
                            }
                        }
                    }
                }
                _log.Info("GetMatrix2Chart query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = recCount;

                DataTablePager<dynamic> dataTablePager = new DataTablePager<dynamic>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<dynamic> formattedList = dataTablePager.Filter(isExport: true);

                _log.Info("GetMatrix2Chart end: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetMatrix2Chart::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }

        }

        public FormattedList<Penetration> GetPenetrationReport(List<jsonAOData> jsonData)
        {
            _log.Info("GetPenetrationReport start: " + DateTime.Now.ToString("o"));
            try
            {
                int displayStart, displayLength;
                string DISPLAY_START = "iDisplayStart", DISPLAY_LENGTH = "iDisplayLength";

                string jsonAOData = json_serializer.Serialize(jsonData);
                Enforce.That(string.IsNullOrEmpty(jsonAOData) == false, "jsonAOData can not be null");

                List<NameValuePair<string, string>> aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(jsonAOData);

                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out displayStart), "DataTableFilters.PrepAOData - can not convert iDisplayStart");
                Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out displayLength), "DataTableFilters.PrepAOData - can not convert iDisplayLength");

                DataFilter filterdata = (DataFilter)json_serializer.Deserialize(aoDataList.GetNVValue("filterdata"), typeof(DataFilter));

                List<Penetration> _lst = new List<Penetration>();

                string strfromDate = "", strtoDate = "", sqlFromDate = "", sqlToDate = "";
                if (filterdata.from_date != null && filterdata.to_date != null)
                {
                    string[] from_date = filterdata.from_date.Split('/');
                    strfromDate = from_date[2] + from_date[1] + from_date[0];
                    sqlFromDate = from_date[2] + "-" + from_date[1] + "-" + from_date[0];

                    string[] to_date = filterdata.to_date.Split('/');
                    strtoDate = to_date[2] + to_date[1] + to_date[0];
                    sqlToDate = to_date[2] + "-" + to_date[1] + "-" + to_date[0];
                }

                int fromDate = (!string.IsNullOrEmpty(strfromDate) ? Convert.ToInt32(strfromDate) : 0),
                    toDate = (!string.IsNullOrEmpty(strtoDate) ? Convert.ToInt32(strtoDate) : 0);

                bool _matchSKUPermission = false;
                long[] _matchArraySKUPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SKU", "sku_id");
                if (_matchArraySKUPermission != null)
                    if (_matchArraySKUPermission.Length > 0)
                        _matchSKUPermission = true;

                bool _matchCategoryPermission = false;
                long[] _matchArrayCategoryPermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "CATEGORY", "category_id");
                if (_matchArrayCategoryPermission != null)
                    if (_matchArrayCategoryPermission.Length > 0)
                        _matchCategoryPermission = true;

                bool _matchNodePermission = false;
                long[] _matchArrayNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "NODE", "node_id");
                if (_matchArrayNodePermission != null)
                    if (_matchArrayNodePermission.Length > 0)
                        _matchNodePermission = true;

                var matchSupplyNodePermission = new BsonDocument();
                bool _matchSupplyNodePermission = false;
                long[] _matchArraySupplyNodePermission = Common.Instance.GetPermissionArrayObjectIDs(Common.Instance.CurrentRole, "SUPPLYNODE", "node_id");
                if (_matchArraySupplyNodePermission != null)
                    if (_matchArraySupplyNodePermission.Length > 0)
                        _matchSupplyNodePermission = true;

                _log.Info("GetPenetrationReport query execution start: " + DateTime.Now.ToString("o"));

                int recCount = 0;
                MapperBatch param = new MapperBatch();
                param.fromdate = sqlFromDate;
                param.todate = sqlToDate;

                int limit = displayLength;
                int cnt = displayLength;
                int loopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(cnt) / Convert.ToDouble(limit)));

                if (filterdata.categorySelection == null && filterdata.skuSelection == null && filterdata.nodeSelection == null && filterdata.supplynodeSelection == null && filterdata.bomSelection == null)
                {
                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }
                }
                else if (filterdata.skuSelection != null && filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.ToList<long>();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.skuSelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission)
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.categorySelection != null && filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission)
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.skuSelection != null || (filterdata.skuSelection != null && filterdata.categorySelection != null))
                {
                    param.id2 = filterdata.skuSelection.OfType<long>().ToList();

                    if (_matchCategoryPermission && _matchNodePermission)
                    {
                        if ((filterdata.skuSelection != null && filterdata.categorySelection != null))
                            param.idCategory = filterdata.categorySelection.OfType<long>().ToList();
                        else
                            param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();

                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.categorySelection != null)
                {
                    param.idCategory = filterdata.categorySelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchNodePermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.nodeSelection != null)
                {
                    param.id1 = filterdata.nodeSelection.OfType<long>().ToList();

                    if (_matchSKUPermission && _matchCategoryPermission)
                    {
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.supplynodeSelection != null)
                {
                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.supplynodeSelection == null)
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    else
                        param.idSupplyNode = filterdata.supplynodeSelection.ToList<long>();

                    if (filterdata.bomSelection != null)
                        if (filterdata.bomSelection.Length > 0)
                            param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }
                else if (filterdata.bomSelection != null)
                {

                    if (_matchSKUPermission && _matchCategoryPermission && _matchNodePermission && _matchSupplyNodePermission)
                    {
                        param.idCategory = _matchArrayCategoryPermission.OfType<long>().ToList();
                        param.idSKU = _matchArraySKUPermission.OfType<long>().ToList();
                        param.idNode = _matchArrayNodePermission.OfType<long>().ToList();
                        param.idSupplyNode = _matchArraySupplyNodePermission.OfType<long>().ToList();
                    }

                    if (filterdata.bomSelection.Length > 0)
                        param.idBOM = filterdata.bomSelection.Distinct().OfType<long>().ToList();
                }

                param.offset = displayStart;
                param.limit = displayLength;

                List<Penetration> _batchList = Dao.DynamicBufferManagement().SelectPenetrationReport(param);

                if (_batchList != null && _batchList.Count > 0)
                    _lst.AddRange(_batchList);

                _log.Info("GetPenetrationReport query execution end: " + DateTime.Now.ToString("o"));

                int iTotalRecords = Dao.DynamicBufferManagement().SelectPenetrationReportCount(param).rec_count;

                DataTablePager<Penetration> dataTablePager = new DataTablePager<Penetration>(jsonAOData, _lst.AsQueryable());
                dataTablePager.iTotalRecords = iTotalRecords;
                FormattedList<Penetration> formattedList = dataTablePager.Filter(isExport: true);

                _log.Info("GetPenetrationReport end: " + DateTime.Now.ToString("o"));
                return formattedList;
            }
            catch (Exception e)
            {
                _log.Error("GetPenetrationReport::", e);
                throw new WebFaultException<string>(e.Message, HttpStatusCode.InternalServerError);
            }
            finally
            {
                Common.Instance.SetServiceRequestId();
            }
        }
    }
}
﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Web.UI.HtmlControls;

namespace SCMPLUS
{
    [ServiceContract]
    public interface IAuthentication
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "login", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string Login(User user);
    }
}

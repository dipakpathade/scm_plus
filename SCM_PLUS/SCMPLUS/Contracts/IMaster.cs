﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using DataTablePager.Core;

namespace SCMPLUS
{
    [ServiceContract]
    public interface IMaster
    {
        #region Node
        [OperationContract]
        [WebGet(UriTemplate = "getNodes", ResponseFormat = WebMessageFormat.Json)]
        List<Node> GetNodes();

        [OperationContract]
        [WebInvoke(UriTemplate = "getNodeList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Node> GetNodeList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveNodesHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Node> GetActiveNodesHierarchy(DataFilter param);

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveSupplyNodesHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Node> GetActiveSupplyNodesHierarchy(DataFilter param);

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveNodesPermissionHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Node> GetActiveNodesPermissionHierarchy(DataFilter param);

        #endregion

        #region Category
        [OperationContract]
        [WebGet(UriTemplate = "getCategories", ResponseFormat = WebMessageFormat.Json)]
        List<Category> GetCategories();

        [OperationContract]
        [WebInvoke(UriTemplate = "getCategoryList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Category> GetCategoryList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveCategoryHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Category> GetActiveCategoryHierarchy(DataFilter param);

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveCategoryPermissionHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Category> GetActiveCategoryPermissionHierarchy(DataFilter param);
        #endregion

        #region SKU
        [OperationContract]
        [WebGet(UriTemplate = "getSkus", ResponseFormat = WebMessageFormat.Json)]
        List<SKU> GetSKUs();

        [OperationContract]
        [WebInvoke(UriTemplate = "getSKUList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<SKU> GetSKUList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getSKUPermissionList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<SKU> GetSKUPermissionList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebGet(UriTemplate = "getAttributes", ResponseFormat = WebMessageFormat.Json)]
        List<Attributes> GetAttributes();
        #endregion

        #region Region
        [OperationContract]
        [WebInvoke(UriTemplate = "getRegionList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Region> GetRegionList(List<jsonAOData> jsonAOData);
        #endregion

        #region Buffer Norm
        [OperationContract]
        [WebInvoke(UriTemplate = "getBufferNormList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<BufferNorm> GetBufferNormList(List<jsonAOData> jsonAOData);
        #endregion

        #region Vendor
        [OperationContract]
        [WebInvoke(UriTemplate = "getVendorList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Vendor> GetVendorList(List<jsonAOData> jsonAOData);
        #endregion

        #region User
        [OperationContract]
        [WebInvoke(UriTemplate = "getUserList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<User> GetUserList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "saveUser", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        void SaveUser(User _user);

        [OperationContract]
        [WebGet(UriTemplate = "getUserPermissions", ResponseFormat = WebMessageFormat.Json)]
        string GetUserPermissions();

        #endregion

        #region Node SKU Mapping
        [OperationContract]
        [WebInvoke(UriTemplate = "getNodeSKUList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<NodeSKUMapping> GetNodeSKUList(List<jsonAOData> jsonAOData);
        #endregion

        #region Module
        [OperationContract]
        [WebInvoke(UriTemplate = "saveModule", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        void SaveModule(Module _module);

        [OperationContract]
        [WebGet(UriTemplate = "getActiveModules", ResponseFormat = WebMessageFormat.Json)]
        List<Module> GetActiveModules();
        #endregion

        #region Dynamic Buffer Management
        [OperationContract]
        [WebInvoke(UriTemplate = "saveDynamicBufferManagement", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        void SaveDynamicBufferManagement(DynamicBufferManagement item);

        [OperationContract]
        [WebGet(UriTemplate = "triggerPenetrationCalculation", ResponseFormat = WebMessageFormat.Json)]
        void TriggerPenetrationCalculation();

        [OperationContract]
        [WebGet(UriTemplate = "getDynamicBufferManagement", ResponseFormat = WebMessageFormat.Json)]
        List<DynamicBufferManagement> GetDynamicBufferManagement();
        #endregion

        #region Role
        [OperationContract]
        [WebGet(UriTemplate = "getActiveRoles", ResponseFormat = WebMessageFormat.Json)]
        List<Role> GetActiveRoles();

        [OperationContract]
        [WebInvoke(UriTemplate = "getRoleList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Role> GetRoleList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "savRole", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        void SaveRole(Role _role);
        #endregion

        [OperationContract]
        [WebInvoke(UriTemplate = "getActiveBOMHierarchy", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<BOM> GetActiveBOMHierarchy(DataFilter param);

        #region SKU Category
        [OperationContract]
        [WebInvoke(UriTemplate = "getSKUCategoryList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<SKUCategory> GetSKUCategoryList(List<jsonAOData> jsonAOData);
        #endregion
    }
}
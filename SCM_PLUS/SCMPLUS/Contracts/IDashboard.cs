﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using DataTablePager.Core;
using MongoDB.Bson;

namespace SCMPLUS
{
    [ServiceContract]
    public interface IDashboard
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "GetReplenishmentList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Replenishment> GetReplenishmentList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportReplenishment", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportReplenishmentList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getTransactionComment", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<TransactionComment> GetTransactionCommentList(DataFilter item);

        [OperationContract]
        [WebInvoke(UriTemplate = "saveTransactionComment", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        void SaveTransactionCommentList(TransactionComment item);

        [OperationContract]
        [WebInvoke(UriTemplate = "getSKUChart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<SKUChart> GetSKUChart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getBufferZones", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<FormattedList<BufferZoneList>> GetBufferZones(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getBufferZoneschat", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<BufferZoneList> GetBufferZoneschat(List<jsonAOData> jsonData);


        [OperationContract]
        [WebInvoke(UriTemplate = "getSKUGrid", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<SKUGrid> GetSKUGrid(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getImportLogList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<ImportExportLog> GetImportLogList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getInventoryTurns", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<InventoryTurns> GetInventoryTurns(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getAvailabilityChart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<StockoutAvailabilityChart> GetAvailabilityChart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getExcessInventoryChart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<ExcessInventoryChart> GetExcessInventoryChart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getCountQuantityChart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<CntQtyFillRateChart> GetCountQuantityChart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getExportLogList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<ImportExportLog> GetExportLogList(List<jsonAOData> jsonAOData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getMatrix1Chart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<dynamic> GetMatrix1Chart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getMatrix1Column", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Matrix1> GetMatrix1Column(DataFilter param);

        [OperationContract]
        [WebInvoke(UriTemplate = "getMatrix2Column", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        List<Matrix1> GetMatrix2Column(DataFilter param);

        [OperationContract]
        [WebInvoke(UriTemplate = "getMatrix2Chart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<dynamic> GetMatrix2Chart(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "getPenetrationReport", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        FormattedList<Penetration> GetPenetrationReport(List<jsonAOData> jsonData);
    }
}

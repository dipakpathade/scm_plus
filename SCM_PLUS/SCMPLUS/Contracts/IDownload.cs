﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Collections.Generic;

namespace SCMPLUS
{
    [ServiceContract]
    public interface IDownload
    {
        [OperationContract]
        [WebGet(UriTemplate = "File/{fileName}")]
        Stream DownloadFile(string fileName);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportCategoryList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportCategoryList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportNodeList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportNodeList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportSKUList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportSKUList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportRegionList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportRegionList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportBufferNormList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportBufferNormList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportVendorList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportVendorList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportUserList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportUserList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportNodeSKUList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportNodeSKUList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportRoleList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportRoleList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportSKUCategoryList", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportSKUCategoryList(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportMatrix1List", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportMatrix1List(List<jsonAOData> jsonData);

        [OperationContract]
        [WebInvoke(UriTemplate = "exportMatrix2List", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, Method = "POST")]
        string ExportMatrix2List(List<jsonAOData> jsonData);
    }
}

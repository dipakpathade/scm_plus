﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Mvc;

namespace SCMPLUS
{
    [ServiceContract]
    public interface IUpload
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "refreshMongoDatabase", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void RefreshMongoDb(string collection, string date = null, ImportExportLog item = null, string identifier = null);

        [OperationContract]
        [WebInvoke(UriTemplate = "importTemplate", Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        DocumentResult ImportTemplate(Stream file);

        [OperationContract]
        [WebGet(UriTemplate = "mongoReplenishmentFullSync", ResponseFormat = WebMessageFormat.Json)]
        void MongoReplenishmentFullSync();

        [OperationContract]
        [WebGet(UriTemplate = "refreshAllTempCollections", ResponseFormat = WebMessageFormat.Json)]
        void RefreshAllTempCollections();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;

namespace DataTablePager.Utils
{
    public enum Compare
    {
        Equal = ExpressionType.Equal,
        NotEqual = ExpressionType.NotEqual,
        LessThan = ExpressionType.LessThan,
        GreaterThan = ExpressionType.GreaterThan,
        LessThanOrEqual = ExpressionType.LessThanOrEqual,
        GreaterThanOrEqual = ExpressionType.GreaterThanOrEqual,
        Contains = ExpressionType.TypeIs + 1,
        In = ExpressionType.TypeIs + 2
    }
    
    public static class AnonPredicate
    {
        /// <summary>
        /// Evaluate whether a property of an AnonymousType against a value.
        /// </summary>
        /// <typeparam name="T">Type of property being evaluated</typeparam>
        /// <param name="propertyValue">Value of the property from the AnonymousType</param>
        /// <param name="comparesTo">Enum representing Equal, GreaterThan, etc</param>
        /// <param name="objectValue">Target or expected value of property</param>
        /// <returns>True when comparison evaluates as expected</returns>
        public static bool Evaluate<T>(T propertyValue, Compare comparesTo, object objectValue)
        {
            ParameterExpression parameter = Expression.Parameter(typeof(T), "x");            
            
            Expression leftConstant = Expression.Constant(propertyValue, typeof(T));
            Expression rightConstant = Expression.Constant(objectValue, objectValue.GetType());

            var comparison = CreateComparisonExpression<T>(leftConstant, comparesTo, rightConstant);
            Expression<Func<T, bool>> predicate =
                Expression.Lambda<Func<T, bool>>(comparison, parameter);
 
            var execDelegate = predicate.Compile();
            return execDelegate(propertyValue);
        }
    
        /// <summary>
        /// Dynamically build comparison expression.
        /// </summary>
        /// <typeparam name="T">Type of parameters of Expressions.</typeparam>
        /// <param name="left">Property as Expression</param>
        /// <param name="comparesTo">Enum that identifies Equal, LessThan, etc</param>
        /// <param name="right">Value to compare as Expression</param>
        /// <returns></returns>
        public static Expression CreateComparisonExpression<T>(Expression left, Compare comparesTo, Expression right)
        {
            switch (comparesTo)
            {
                case Compare.Equal:
                    return Expression.Equal(left, right);

                case Compare.GreaterThan:
                    return Expression.GreaterThan(left, right);

                case Compare.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(left, right);

                case Compare.LessThan:
                    return Expression.LessThan(left, right);

                case Compare.LessThanOrEqual:
                    return Expression.LessThanOrEqual(left, right);

                case Compare.NotEqual:
                    return Expression.NotEqual(left, right);

                case Compare.Contains:
                    MethodInfo contains = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                    return Expression.Call(left, contains, right);

                case Compare.In:
                    //  You are accepting a List<T>, where T corresponds to the property on your class
                    //  I.E.  List<int> => Employee.Age as Integer.  
                    //  Comparison is Left Compares Right, hence you need the type of Left
                    return Expression.Call(typeof(Enumerable), "Contains", new Type[] { left.Type }, right, left);

                default:
                    throw new ArgumentException("Query.CreateComparisonExpression - comparison not supported");

            }
        }
    }


}

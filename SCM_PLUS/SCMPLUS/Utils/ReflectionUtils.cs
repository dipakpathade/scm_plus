﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Linq.Expressions;

namespace DataTablePager.Utils
{
    /// <summary>
    /// 
    /// </summary>
    #region License
    // Code is from James Newton-King, creator of JSON.Net 
    //  
    // Copyright 2006 James Newton-King
    // http://www.newtonsoft.com
    //
    // This work is licensed under the Creative Commons Attribution 2.5 License
    // http://creativecommons.org/licenses/by/2.5/
    //
    // You are free:
    //    * to copy, distribute, display, and perform the work
    //    * to make derivative works
    //    * to make commercial use of the work
    //
    // Under the following conditions:
    //    * You must attribute the work in the manner specified by the author or licensor:
    //          - If you find this component useful a link to http://www.newtonsoft.com would be appreciated.
    //    * For any reuse or distribution, you must make clear to others the license terms of this work.
    //    * Any of these conditions can be waived if you get permission from the copyright holder.
    #endregion
    internal static class ReflectionUtils
    {
        public static T Convert<T>(object initialValue)
        {
            Type targetType = typeof(T);
            Type initialType = (initialValue != null) ? initialValue.GetType() : null;

            TypeConverter converter = TypeDescriptor.GetConverter(targetType);

            if (converter == null || !converter.CanConvertFrom(initialType))
                throw new ApplicationException(string.Format("Could not convert from {0} to {1}", initialType, targetType));

            return (T)converter.ConvertFrom(initialValue);
        }

        public static string GetName<T>(Expression<Func<T>> e)
        {
            var member = (MemberExpression)e.Body;
            return member.Member.Name;
        }
    }
}

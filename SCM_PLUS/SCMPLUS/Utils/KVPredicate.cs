﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace DataTablePager.Utils
{
    static class KVPredicate
    {
        public static Expression<Func<NameValuePair<string, string>, bool>> KeyPredicate(string name)
        {
            var predicate = PredicateBuilder.True<NameValuePair<string, string>>();
            predicate = predicate.And<NameValuePair<string, string>>(nv => nv.Name == name);

            return predicate;
        }

        public static Expression<Func<NameValuePair<string, string>, bool>> ValueIs(string name, string value)
        {
            var predicate = PredicateBuilder.True<NameValuePair<string, string>>();
            predicate = predicate.And<NameValuePair<string, string>>(nv => nv.Value == value);

            return predicate;
        }

        public static string GetNVValue(this List<NameValuePair<string, string>> nvPairs, string name)
        {
            return nvPairs.AsQueryable()
                            .Where(KeyPredicate(name))
                            .SingleOrDefault()
                            .Value;
        }

        public static bool CheckNVValue(this List<NameValuePair<string, string>> nvPairs, string name)
        {
            return nvPairs.AsQueryable()
                            .Where(KeyPredicate(name))
                            .SingleOrDefault() == null
                            ? false : true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTablePager.Utils
{
    [Serializable]
    public class NameValuePair<TName, TValue>
    {
        public TName Name { get; set; }
        public TValue Value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public NameValuePair(TName name, TValue value)
        {
            Name = name;
            Value = value;
        }

        public NameValuePair() { }
    }    
}

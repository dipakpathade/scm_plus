﻿using System.Runtime.Serialization;

namespace SCMPLUS
{
    [DataContract]
    public class jsonAOData
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string value { get; set; }
    }
}
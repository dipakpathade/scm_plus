﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTablePager.Core
{
    public class aoColumns
    {
        public aoColumns()
        {
        }

        public int _ColReorder_iOrigCol { get; set; }
        public bool bSearchable { get; set; }
        public bool bSortable { get; set; }
        public bool bUseRendered { get; set; }
        public bool bVisible { get; set; }
        public string mData { get; set; }
        public string sTitle { get; set; }
        public string sType { get; set; }
    }
}

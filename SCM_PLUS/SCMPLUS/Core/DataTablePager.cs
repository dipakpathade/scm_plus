﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Reflection;
using System.Linq.Expressions;
using DataTablePager.Utils;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace DataTablePager.Core
{
    public class DataTablePager<T> where T : class
    {
        private const string INDIVIDUAL_SEARCH_KEY_PREFIX = "sSearch_";
        private const string INDIVIDUAL_SORT_KEY_PREFIX = "iSortCol_";
        private const string INDIVIDUAL_SORT_DIRECTION_KEY_PREFIX = "sSortDir_";
        private const string DISPLAY_START = "iDisplayStart";
        private const string DISPLAY_LENGTH = "iDisplayLength";
        private const string ECHO = "sEcho";
        private const string ASCENDING_SORT = "asc";
        private const string GENERIC_SEARCH = "sSearch";
        private const string COLUMNS = "sColumns";
        private const string BSEARCHABLE = "bSearchable_";
        private const string BSORTABLE = "bSortable_";

        private IQueryable<T> queryable;
        private Type type;
        public List<SearchAndSortable> searchAndSortables;
        private List<string> sortKeyPrefix;

        private const string AOCOLUMNS = "aoColumns";
        //public string BVISIBLE = "bVisible_";
        //public string STITLE = "sTitle_";
        private List<NameValuePair<string, string>> aoDataList;
        private List<string> columns;

        private int displayStart;
        private int displayLength;
        private int echo;
        private string genericSearch;

        public int iTotalRecords;

        private T DataSet { get; set; }
        private JavaScriptSerializer json_serializer = new JavaScriptSerializer();

        public DataTablePager(string jsonAOData, IQueryable<T> queryable)
        {
            this.queryable = queryable;
            this.type = typeof(T);
            this.aoDataList = new List<NameValuePair<string, string>>();
            this.sortKeyPrefix = new List<string>();
            this.searchAndSortables = new List<SearchAndSortable>();

            PrepAOData(jsonAOData);
        }

        #region Methods

        /// <summary>
        /// Apply the search terms to all the columns in the data set
        /// </summary>
        /// <returns>Data set as FormattedList</returns>
        public FormattedList<T> Filter(bool isExport = false)
        {
            var formattedList = new FormattedList<T>();

            //  What are the columns in the data set            
            formattedList.Import(this.columns.ToArray());

            //  Return same sEcho that was posted.  Prevents XSS attacks.
            formattedList.sEcho = this.echo;

            //  Return count of all records
            //formattedList.iTotalRecords = this.queryable.Count();
            formattedList.iTotalRecords = iTotalRecords;

            //  Filtered Data
            var records = this.queryable;//.Where(GenericSearchFilter());
            //records = ApplySort(records);

            //  What is filtered data set count now.  This is NOT the 
            //  count of what is returned to client
            formattedList.iTotalDisplayRecords = (records.FirstOrDefault() == null) ? 0 : iTotalRecords;// records.Count();

            //  Take a page
            var pagedRecords = !isExport ? records.Skip(this.displayStart).Take(this.displayLength) : records.Skip(0).Take(formattedList.iTotalDisplayRecords);

            ////  Convert to List of List<string>.  Take only columns specified
            //var aaData = new List<List<string>>();
            //var thisRec = new List<string>();

            ////  No columns specified - take entire object
            //if (this.columns.Count == 0)
            //{
            //    pagedRecords.ToList()
            //       .ForEach(rec => aaData.Add(rec.PropertiesToList()));
            //    formattedList.aaData = pagedRecords.ToList();
            //}
            //else
            //{
            //    pagedRecords.ToList()
            //       .ForEach(rec => aaData.Add(rec.PropertiesToList(this.columns)));
            //    formattedList.aaData = pagedRecords.ToList();
            //}

            formattedList.aaData = pagedRecords.ToList();

            return formattedList;
        }

        /// <summary>
        /// Parse the aoDataObject structure and retrieve the following items:
        /// 
        /// </summary>
        /// <param name="aoDataObject"></param>
        private void PrepAOData(string aoData)
        {
            Enforce.That(string.IsNullOrEmpty(aoData) == false,
                            "DataTablePager.PrepAOData - aoData can not be null or empty");

            this.aoDataList = json_serializer.Deserialize<List<NameValuePair<string, string>>>(aoData);

            Enforce.That(int.TryParse(aoDataList.GetNVValue(ECHO), out this.echo),
                                   "DataTableFilters.PrepAOData - can not convert sEcho");

            Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_START), out this.displayStart),
                                    "DataTableFilters.PrepAOData - can not convert iDisplayStart");

            Enforce.That(int.TryParse(aoDataList.GetNVValue(DISPLAY_LENGTH), out this.displayLength),
                                    "DataTableFilters.PrepAOData - can not convert iDisplayLength");

            this.genericSearch = aoDataList.GetNVValue(GENERIC_SEARCH);

            //  Sort columns
            this.sortKeyPrefix = aoDataList.Where(x => x.Name.StartsWith(INDIVIDUAL_SORT_KEY_PREFIX))
                                            .Select(x => x.Value)
                                            .ToList();

            //  Column list
            var cols = aoDataList.Where(x => x.Name == "sColumns"
                                            & string.IsNullOrEmpty(x.Value) == false)
                                     .SingleOrDefault();

            if (cols == null)
            {
                this.columns = new List<string>();
            }
            else
            {
                this.columns = cols.Value
                                    .Split(',')
                                    .ToList();
            }

            //  What column is searchable and / or sortable
            //  What properties from T is identified by the columns
            var properties = typeof(T).GetProperties();
            int i = 0;

            //  Search and store all properties from T
            this.columns.ForEach(col =>
            {
                if (string.IsNullOrEmpty(col) == false)
                {
                    var searchable = new SearchAndSortable(col, i, false, false);
                    var searchItem = aoDataList.Where(x => x.Name == BSEARCHABLE + i.ToString())
                                     .ToList();

                    if (searchItem.Count > 0)
                    {
                        searchable.IsSearchable = (searchItem[0].Value.ToString().ToLower() == "false") ? false : true;

                        //if (col.IndexOf(".") < 0)
                        //{
                        searchable.Property = properties.Where(x => x.Name == col).SingleOrDefault();
                        //}
                        //else
                        //{
                        //    string obj = col.Substring(0, col.IndexOf("."));
                        //    var propertyInfo = properties.Where(x => x.Name == obj).SingleOrDefault();
                        //    if (propertyInfo.PropertyType.IsClass && (propertyInfo.IsDefined(typeof(DataMemberAttribute), true)))
                        //    {
                        //        obj = col.Substring(col.IndexOf(".") + 1, col.Length - (col.IndexOf(".") + 1));
                        //        var _properties = propertyInfo.PropertyType.GetProperties();
                        //        object headerObj = Activator.CreateInstance(propertyInfo.PropertyType);
                        //        searchable.Property = _properties.Where(x => x.Name == obj).SingleOrDefault();
                        //    }
                        //}

                        searchAndSortables.Add(searchable);
                    }
                }

                i++;
            });

            //  Sort
            searchAndSortables.ForEach(sortable =>
            {
                var sort = aoDataList.Where(x => x.Name == BSORTABLE + sortable.ColumnIndex.ToString())
                                            .ToList();

                if (sort.Count > 0)
                {
                    sortable.IsSortable = (sort[0].Value.ToString().ToLower() == "false") ? false : true;
                    sortable.SortOrder = -1;

                    //  Is this item amongst currently sorted columns?
                    int order = 0;
                    this.sortKeyPrefix.ForEach(keyPrefix =>
                    {
                        if (sortable.ColumnIndex == Convert.ToInt32(keyPrefix))
                        {
                            sortable.IsCurrentlySorted = true;

                            //  Is this the primary sort column or secondary?
                            sortable.SortOrder = order;

                            //  Ascending or Descending?
                            var ascDesc = aoDataList.Where(x => x.Name == INDIVIDUAL_SORT_DIRECTION_KEY_PREFIX + order)
                                                        .SingleOrDefault();
                            if (ascDesc != null)
                            {
                                sortable.SortDirection = ascDesc.Value;
                            }
                        }

                        order++;
                    });
                }
            });
        }

        /// <summary>
        /// Create a Lambda Expression that is chain of Or Expressions
        /// for each column.  Each column will be tested if it contains the
        /// generic search string.  
        /// 
        /// Query logic = (or ... or ...) And (or ... or ...)
        /// </summary>        
        /// <returns>Expression of T, bool</returns>
        private Expression<Func<T, bool>> GenericSearchFilter()
        {
            //  Create a list of searchable properties
            var filterProperties = this.searchAndSortables.Where(x => x.IsSearchable)
                                                            .Select(x => x.Property)
                                                            .ToList();

            //  When no filterProperties or search terms, return a true expression
            if (string.IsNullOrEmpty(this.genericSearch) || filterProperties.Count == 0)
            {
                return x => true;
            }

            var paramExpression = Expression.Parameter(this.type, "val");
            Expression compoundOrExpression = Expression.Call(Expression.Property(paramExpression, filterProperties[0]), "ToString", null);
            Expression compoundAndExpression = Expression.Call(Expression.Property(paramExpression, filterProperties[0]), "ToString", null);
            MethodInfo convertToString = typeof(Convert).GetMethod("ToString", Type.EmptyTypes);

            //  Split search expression to handle multiple words
            var searchTerms = this.genericSearch;//.genericSearch.Split(' ');

            for (int i = 0; i < searchTerms.Length; i++)
            {
                var searchExpression = Expression.Constant(searchTerms.ToLower()); ;// Expression.Constant(searchTerms[i].ToLower());

                //  For each property, create a contains expression
                //  column => column.ToLower().Contains(searchTerm)                
                var propertyQuery = (from property in filterProperties
                                     let toStringMethod = Expression.Call(
                                                         Expression.Call(Expression.Property(paramExpression, property), convertToString, null),
                                                             typeof(string).GetMethod("ToLower", new Type[0]))
                                     select Expression.Call(toStringMethod, typeof(string).GetMethod("Contains"), searchExpression)).ToArray();

                for (int j = 1; j < propertyQuery.Length; j++)
                {
                    //  Nothing to "or" to yet
                    if (j == 1)
                    {
                        compoundOrExpression = propertyQuery[0];
                        compoundOrExpression = Expression.Or(compoundOrExpression, propertyQuery[j]);
                    }

                    if (j > 1)
                    {
                        bool process = true;
                        process = aoDataList.GetNVValue(BSEARCHABLE + j.ToString()).ToString().Length > 0 ? Convert.ToBoolean(aoDataList.GetNVValue(BSEARCHABLE + j.ToString())) : false;

                        if (process)
                            compoundOrExpression = Expression.Or(compoundOrExpression, propertyQuery[j]);
                    }
                }

                //  First time around there is no And, only first set of or's
                if (i == 0)
                {
                    compoundAndExpression = compoundOrExpression;
                }
                else
                {
                    compoundAndExpression = Expression.And(compoundAndExpression, compoundOrExpression);
                }
            }

            //  Create Lambda
            return Expression.Lambda<Func<T, bool>>(compoundAndExpression, paramExpression);
        }

        /// <summary>
        /// Sort the queryable items according to column selected
        /// </summary>
        private IQueryable<T> ApplySort(IQueryable<T> records)
        {
            var sorted = this.searchAndSortables.Where(x => x.IsCurrentlySorted == true)
                                                .OrderBy(x => x.SortOrder)
                                                .ToList();

            //  Are we at initialization of grid with no column selected?
            if (sorted.Count == 0)
            {
                string firstSortColumn = this.sortKeyPrefix.First();
                int firstColumn = int.Parse(firstSortColumn);

                string sortDirection = "asc";
                sortDirection = this.aoDataList.Where(x => x.Name == INDIVIDUAL_SORT_DIRECTION_KEY_PREFIX +
                                                                          "0")
                                                    .Single()
                                                    .Value
                                                    .ToLower();

                if (string.IsNullOrEmpty(sortDirection))
                {
                    sortDirection = "asc";
                }

                //  Initial display will set order to first column - column 0
                //  When column 0 is not sortable, find first column that is
                var sortable = this.searchAndSortables.Where(x => x.ColumnIndex == firstColumn)
                                                        .SingleOrDefault();
                if (sortable == null)
                {
                    sortable = this.searchAndSortables.First(x => x.IsSortable);
                }

                return records.OrderBy(sortable.Name, sortDirection, true);
            }
            else
            {
                //  Traverse all columns selected for sort
                sorted.ForEach(sort =>
                {
                    records = records.OrderBy(sort.Name, sort.SortDirection,
                        (sort.SortOrder == 0) ? true : false);
                });

                return records;
            }
        }

        public List<aoColumns> aoColumns()
        {
            return json_serializer.Deserialize<List<aoColumns>>(aoDataList.GetNVValue(AOCOLUMNS));
        }

        #endregion
    }
}

﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Configuration;
using MongoDB.Driver;

namespace SCMPLUS
{
    public class Global : HttpApplication
    {
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

        string con = ConfigurationManager.ConnectionStrings["MongoDBConnString"].ConnectionString;
        public static MongoClient client = null;

        void Application_Start(object sender, EventArgs e)
        {
            client = new MongoClient(con);

            // Code that runs on application startup
            log4net.Config.XmlConfigurator.Configure();

            Application["UsersLoggedIn"] = new Dictionary<string, string>();
            Application["UserLoginAttempts"] = new Dictionary<object, object>();
            RegisterRoutes();
            QuartzHelper.startDBMFlow();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
            log4net.LogManager.Shutdown();
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
            string publicKey = RSA.ToXmlString(false);
            string privateKey = RSA.ToXmlString(true);

            Common.Instance.PublicKey = publicKey;
            Common.Instance.PrivateKey = privateKey;
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("Rest/User", new WebServiceHostFactory(), typeof(Authentication)));
            RouteTable.Routes.Add(new ServiceRoute("Rest/Upload", new WebServiceHostFactory(), typeof(Upload)));
            RouteTable.Routes.Add(new ServiceRoute("Rest/Download", new WebServiceHostFactory(), typeof(Download)));
            RouteTable.Routes.Add(new ServiceRoute("Rest/Dashboard", new WebServiceHostFactory(), typeof(Dashboard)));
            RouteTable.Routes.Add(new ServiceRoute("Rest/Master", new WebServiceHostFactory(), typeof(Master)));
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }
    }
}

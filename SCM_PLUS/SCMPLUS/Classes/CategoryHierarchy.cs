﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class CategoryHierarchy
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public List<Category> categoryList { get; set; }
    }
}
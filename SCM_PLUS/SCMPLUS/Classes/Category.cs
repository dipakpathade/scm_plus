﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class Category
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public string category_code { get; set; }

        [DataMember]
        public long parent_id { get; set; }

        [DataMember]
        public string parent_category { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public int is_valid { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }

        [DataMember]
        public List<Category> category_list { get; set; }

        [DataMember]
        public List<SKU> sku_list { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }
    }

    [DataContract]
    public class TempCategory
    {
        [DataMember]
        public List<Category> category { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }

}
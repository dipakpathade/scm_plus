﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class UserDataPermissions
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long? user_data_id { get; set; }

        [DataMember]
        public long? role_id { get; set; }

        [DataMember]
        public string object_type { get; set; }

        [DataMember]
        public long? object_id { get; set; }

        [DataMember]
        public int chk_state { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
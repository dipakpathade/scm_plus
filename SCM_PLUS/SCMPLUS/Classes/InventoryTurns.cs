﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class InventoryTurns
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public double count { get; set; }

        [DataMember]
        public double sales { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public double yearly_sales { get; set; }

        [DataMember]
        public string[] transaction_dates { get; set; }

        [DataMember]
        public string str_transaction_dates { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            if (str_transaction_dates != null)
            {
                if (str_transaction_dates.Trim().Length > 0)
                {
                    transaction_dates = str_transaction_dates.Split(',');
                }
            }

            if (transaction_dates != null)
            {
                if (transaction_dates.Length > 0)
                {
                    yearly_sales = sales / transaction_dates.Length * 365;
                }
            }

            str_transaction_dates = null;
            transaction_dates = null;

            count = Common.Instance.OnSerializingRound(count);
            sales = Common.Instance.OnSerializingRound(sales);
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            yearly_sales = Common.Instance.OnSerializingRound(yearly_sales);
        }
    }
}
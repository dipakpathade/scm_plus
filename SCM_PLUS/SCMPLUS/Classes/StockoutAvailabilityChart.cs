﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;
using System.Linq;

namespace SCMPLUS
{
    [DataContract]
    public class StockoutAvailabilityChart
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public double total_sku_count { get; set; }

        [DataMember]
        public double all_sku_count { get; set; }

        [DataMember]
        public double rb_sku_count { get; set; }

        [DataMember]
        public double stock_out { get; set; }

        [DataMember]
        public double availability { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            if (rb_sku_count > 0 && total_sku_count > 0)
            {
                stock_out = Math.Round(((rb_sku_count / total_sku_count) * 100), 2);

                availability = Math.Round((100 - ((rb_sku_count / total_sku_count) * 100)), 2);
            }
            else
            {
                stock_out = 0;
                availability = 100;
            }
        }
    }
}
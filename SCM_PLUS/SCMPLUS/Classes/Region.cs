﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class Region
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long region_id { get; set; }

        [DataMember]
        public string region_name { get; set; }

        [DataMember]
        public string region_code { get; set; }

        [DataMember]
        public long country_id { get; set; }

        [DataMember]
        public Country country { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class NodeSKUMapping
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long node_sku_mapping_id { get; set; }

        [DataMember]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime effective_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public SKU sku { get; set; }

        [DataMember]
        public long receiver_node_id { get; set; }

        [DataMember]
        public Node receiver_node { get; set; }

        [DataMember]
        public long? supplier_node_id { get; set; }

        [DataMember]
        public Node supplier_node { get; set; }

        [DataMember]
        public int activate_auto_dbm { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            //display_date = effective_date.Replace(" 12:00:00 AM", "");
        }
    }

    [DataContract]
    public class TempNodeSKUMapping
    {
        [DataMember]
        public List<NodeSKUMapping> nodeSKUMapping { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }


}
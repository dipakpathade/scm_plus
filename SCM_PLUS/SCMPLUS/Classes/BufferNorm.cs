﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class BufferNorm
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long buffer_norm_id { get; set; }

        [DataMember]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime effective_from { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public double buffer_norm { get; set; }

        [DataMember]
        public double unit_selling_price { get; set; }

        [DataMember]
        public double unit_purchase_price { get; set; }

        [DataMember]
        public double olt { get; set; }

        [DataMember]
        public double plt { get; set; }

        [DataMember]
        public double tlt { get; set; }

        [DataMember]
        public double rlt { get; set; }

        [DataMember]
        public double max_sales_per_day { get; set; }

        [DataMember]
        public double safety_factor { get; set; }

        [DataMember]
        public double moq { get; set; }

        [DataMember]
        public double eoq { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public SKU sku { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public Node node { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }

        [DataMember]
        public string is_default_zone { get; set; }

        [DataMember]
        public double red_zone { get; set; }

        [DataMember]
        public double yellow_zone { get; set; }

        [DataMember]
        public bool is_valid_zone { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            //display_date = effective_from.Replace(" 12:00:00 AM", "");

            buffer_norm = Common.Instance.OnSerializingRound(buffer_norm);
            unit_selling_price = Common.Instance.OnSerializingRound(unit_selling_price);
            unit_purchase_price = Common.Instance.OnSerializingRound(unit_purchase_price);
            olt = Common.Instance.OnSerializingRound(olt);
            plt = Common.Instance.OnSerializingRound(plt);
            tlt = Common.Instance.OnSerializingRound(tlt);
            rlt = Common.Instance.OnSerializingRound(rlt);
            max_sales_per_day = Common.Instance.OnSerializingRound(max_sales_per_day);
            safety_factor = Common.Instance.OnSerializingRound(safety_factor);
            moq = Common.Instance.OnSerializingRound(moq);
            eoq = Common.Instance.OnSerializingRound(eoq);
            red_zone = Common.Instance.OnSerializingRound(red_zone);
            yellow_zone = Common.Instance.OnSerializingRound(yellow_zone);
        }
    }

    [DataContract]
    public class TempBufferNorm
    {
        [DataMember]
        public List<BufferNorm> bufferNorm { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }
}
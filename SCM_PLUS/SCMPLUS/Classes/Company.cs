﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class Company
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long comapny_id { get; set; }

        [DataMember]
        public string company_name { get; set; }

        [DataMember]
        public string company_address { get; set; }

        [DataMember]
        public string company_logo { get; set; }

        [DataMember]
        public long company_phone { get; set; }

        [DataMember]
        public long location_id { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
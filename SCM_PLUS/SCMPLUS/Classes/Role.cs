﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class Role
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long role_id { get; set; }

        [DataMember]
        public string role_name { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }

        [DataMember]
        public List<UserModule> user_module_list { get; set; }

        [DataMember]
        public List<UserDataPermissions> user_node_list { get; set; }

        [DataMember]
        public List<UserDataPermissions> user_supply_node_list { get; set; }

        [DataMember]
        public List<UserDataPermissions> user_category_list { get; set; }

        [DataMember]
        public List<UserDataPermissions> user_sku_list { get; set; }
    }
}
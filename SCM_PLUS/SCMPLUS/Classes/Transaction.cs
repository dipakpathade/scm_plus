﻿using System.Runtime.Serialization;
using MongoDB.Bson;
using System;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace SCMPLUS
{
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long transaction_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long supply_node_id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string avg_date { get; set; }

        [DataMember]
        public double grn { get; set; }

        [DataMember]
        public double sales { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public string closing_stock_zone { get; set; }

        [DataMember]
        public double in_transit { get; set; }

        [DataMember]
        public double inst_hold { get; set; }

        [DataMember]
        public double quality_hold { get; set; }

        [DataMember]
        public double qa_hold { get; set; }

        [DataMember]
        public double pending_orders { get; set; }

        [DataMember]
        public double orders_in_hand { get; set; }

        [DataMember]
        public string transport_mode { get; set; }

        [DataMember]
        public int db_sync { get; set; }

        [DataMember]
        public int mongo_sync { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }

        [DataMember]
        public double open_stock_transfer_order { get; set; }

        [DataMember]
        public double open_planned_order { get; set; }

        [DataMember]
        public double open_purchase_request { get; set; }

        [DataMember]
        public double open_production_order { get; set; }

        [DataMember]
        public double open_purchase_order { get; set; }

        [DataMember]
        public string field1 { get; set; }

        [DataMember]
        public string field2 { get; set; }

        [DataMember]
        public string field3 { get; set; }

        [DataMember]
        public string field4 { get; set; }

        [DataMember]
        public string field5 { get; set; }

        [DataMember]
        public string field6 { get; set; }

        [DataMember]
        public string field7 { get; set; }

        [DataMember]
        public string field8 { get; set; }

        [DataMember]
        public string field9 { get; set; }

        [DataMember]
        public string field10 { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            grn = Common.Instance.OnSerializingRound(grn);
            sales = Common.Instance.OnSerializingRound(sales);
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            in_transit = Common.Instance.OnSerializingRound(in_transit);
            inst_hold = Common.Instance.OnSerializingRound(inst_hold);
            quality_hold = Common.Instance.OnSerializingRound(quality_hold);
            qa_hold = Common.Instance.OnSerializingRound(qa_hold);
            pending_orders = Common.Instance.OnSerializingRound(pending_orders);
            orders_in_hand = Common.Instance.OnSerializingRound(orders_in_hand);
            open_stock_transfer_order = Common.Instance.OnSerializingRound(open_stock_transfer_order);
            open_planned_order = Common.Instance.OnSerializingRound(open_planned_order);
            open_purchase_request = Common.Instance.OnSerializingRound(open_purchase_request);
            open_production_order = Common.Instance.OnSerializingRound(open_production_order);
            open_purchase_order = Common.Instance.OnSerializingRound(open_purchase_order);
        }
    }

    [DataContract]
    public class TempTransaction
    {
        [DataMember]
        public List<Transaction> transactions { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }
}
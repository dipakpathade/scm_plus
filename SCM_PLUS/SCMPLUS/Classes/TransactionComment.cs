﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class TransactionComment
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long transaction_comment_id { get; set; }

        [DataMember]
        public long transaction_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public string comment_type { get; set; }

        [DataMember]
        public string comment { get; set; }

        [DataMember]
        public string reason { get; set; }

        [DataMember]
        public string author { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            User _user = MongoHelper<User>.Single(Common.Instance.GetEnumValue(Common.CollectionName.USER), Query.EQ("user_id", created_by));
            if (_user != null) author = string.Format("{0} {1}", _user.first_name, _user.last_name);

            if (comment != null)
                if (comment.Trim().Length > 0)
                    comment = HttpUtility.HtmlDecode(comment);

        }
    }
}
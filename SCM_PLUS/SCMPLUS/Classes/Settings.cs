﻿using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class Settings
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string value { get; set; }
    }
}
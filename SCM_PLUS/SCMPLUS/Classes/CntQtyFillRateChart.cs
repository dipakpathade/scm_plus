﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class CntQtyFillRateChart
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public double buffer_norm { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public string closing_stock_zone { get; set; }

        [DataMember]
        public double zone_count { get; set; }

        [DataMember]
        public double zone_per { get; set; }

        [DataMember]
        public double shortage_qty { get; set; }

        [DataMember]
        public double shortage_qty_per { get; set; }

        [DataMember]
        public double count_fill_rate { get; set; }

        [DataMember]
        public string cnt_fill_rate { get; set; }

        [DataMember]
        public double qty_fill_rate { get; set; }

        [DataMember]
        public string quantity_fill_rate { get; set; }

        [DataMember]
        public double all_count { get; set; }

        [DataMember]
        public double all_sku_count { get; set; }

        [DataMember]
        public double bry_shortage_qty { get; set; }

        [DataMember]
        public double bry_count { get; set; }

        [DataMember]
        public double total_buffer_norm { get; set; }

        [DataMember]
        public string[] sku_ids { get; set; }

        [DataMember]
        public string str_sku_ids { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            buffer_norm = Common.Instance.OnSerializingRound(buffer_norm);
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            zone_count = Common.Instance.OnSerializingRound(zone_count);
            zone_per = Common.Instance.OnSerializingRound(zone_per);
            shortage_qty = Common.Instance.OnSerializingRound(shortage_qty);
            shortage_qty_per = Common.Instance.OnSerializingRound(shortage_qty_per);
            count_fill_rate = Common.Instance.OnSerializingRound(count_fill_rate);
            qty_fill_rate = Common.Instance.OnSerializingRound(qty_fill_rate);
            all_count = Common.Instance.OnSerializingRound(all_count);
            bry_shortage_qty = Common.Instance.OnSerializingRound(bry_shortage_qty);
            bry_count = Common.Instance.OnSerializingRound(bry_count);
            total_buffer_norm = Common.Instance.OnSerializingRound(total_buffer_norm);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class SKUAttributeMapping
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long sku_attribute_mapping_id { get; set; }

        [DataMember]
        public string value { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public string sku_code { get; set; }

        [DataMember]
        public long attribute_id { get; set; }

        [DataMember]
        public string attribute_name { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            if (attribute_id > 0)
            {
                Attributes _attributes = MongoHelper<Attributes>.Single(Common.Instance.GetEnumValue(Common.CollectionName.ATTRIBUTE), Query.EQ("attribute_id", attribute_id));
                if (_attributes != null) attribute_name = _attributes.attribute_name;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class MapperBatch
    {
        [DataMember]
        public long id { get; set; }

        [DataMember]
        public string date { get; set; }

        [DataMember]
        public string fromdate { get; set; }

        [DataMember]
        public string todate { get; set; }

        [DataMember]
        public int limit { get; set; }

        [DataMember]
        public int offset { get; set; }

        [DataMember]
        public long skuId { get; set; }

        [DataMember]
        public long nodeId { get; set; }

        [DataMember]
        public int flag1 { get; set; }

        [DataMember]
        public int flag2 { get; set; }

        [DataMember]
        public List<long> id1 { get; set; }

        [DataMember]
        public List<long> id2 { get; set; }

        [DataMember]
        public List<long> id3 { get; set; }

        [DataMember]
        public List<long> idSKU { get; set; }

        [DataMember]
        public List<long> idNode { get; set; }

        [DataMember]
        public List<long> idCategory { get; set; }

        [DataMember]
        public List<long> idSupplyNode { get; set; }

        [DataMember]
        public List<long> idBOM { get; set; }

        [DataMember]
        public List<Node> node { get; set; }

        [DataMember]
        public List<Replenishment> replenishment { get; set; }

    }
}
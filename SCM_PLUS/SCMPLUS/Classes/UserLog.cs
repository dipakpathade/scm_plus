﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class UserLog
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long user_log_id { get; set; }

        [DataMember]
        public long user_id { get; set; }

        [DataMember]
        public string login_datetime { get; set; }

        [DataMember]
        public string user_agent { get; set; }

        [DataMember]
        public string user_ip { get; set; }

        [DataMember]
        public string user_location { get; set; }

        [DataMember]
        public string auth_token { get; set; }
    }
}
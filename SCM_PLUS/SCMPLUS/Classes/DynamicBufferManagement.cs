﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class DynamicBufferManagement
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long dynamic_buffer_management_id { get; set; }

        [DataMember]
        public int activate_auto_dbm { get; set; }

        [DataMember]
        public int excess_inv_occ_count { get; set; }

        [DataMember]
        public int excess_inv_decrease_per { get; set; }

        [DataMember]
        public int excess_inv_wait_cycle { get; set; }

        [DataMember]
        public int excess_inv_penetration_percentage { get; set; }

        [DataMember]
        public int stockout_inv_occ_count { get; set; }

        [DataMember]
        public int stockout_inv_decrease_per { get; set; }

        [DataMember]
        public int stockout_inv_wait_cycle { get; set; }

        [DataMember]
        public int stockout_inv_penetration_percentage { get; set; }

        [DataMember]
        public int override_sku_location_dbm { get; set; }

        [DataMember]
        public int activate_auto_dcm { get; set; }

        [DataMember]
        public double red_percentage { get; set; }

        [DataMember]
        public double yellow_percentage { get; set; }

        [DataMember]
        public double green_percentage { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
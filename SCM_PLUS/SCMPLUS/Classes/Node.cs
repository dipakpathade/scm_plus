﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class Node
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public string node_code { get; set; }

        [DataMember]
        public string node_name { get; set; }

        [DataMember]
        public string address_line1 { get; set; }

        [DataMember]
        public string address_line2 { get; set; }

        [DataMember]
        public string latitude { get; set; }

        [DataMember]
        public string longitude { get; set; }

        [DataMember]
        public long parent_id { get; set; }

        [DataMember]
        public string parent_node { get; set; }

        [DataMember]
        public long location_id { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }

        [DataMember]
        public List<Node> node_list { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }
    }

    [DataContract]
    public class TempNode
    {
        [DataMember]
        public List<Node> node { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }
}
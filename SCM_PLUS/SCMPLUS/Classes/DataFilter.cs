﻿using System.Runtime.Serialization;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class DataFilter
    {
        [DataMember]
        public string from_date { get; set; }

        [DataMember]
        public string to_date { get; set; }

        [DataMember]
        public string date { get; set; }

        [DataMember]
        public long id { get; set; }

        [DataMember]
        public long id1 { get; set; }

        [DataMember]
        public long[] skuSelection { get; set; }

        [DataMember]
        public long[] categorySelection { get; set; }

        [DataMember]
        public long[] nodeSelection { get; set; }

        [DataMember]
        public long[] supplynodeSelection { get; set; }

        [DataMember]
        public long[] bomSelection { get; set; }

        [DataMember]
        public string node_code { get; set; }

        [DataMember]
        public string node_name { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public string category_code { get; set; }

        [DataMember]
        public string sku_name { get; set; }

        [DataMember]
        public string sku_code { get; set; }
    }
}
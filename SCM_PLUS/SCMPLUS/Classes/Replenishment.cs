﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class Replenishment
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long transaction_id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long? sku_id { get; set; }

        [DataMember]
        public string sku_name { get; set; }

        [DataMember]
        public string sku_code { get; set; }

        [DataMember]
        public long? node_id { get; set; }

        [DataMember]
        public string node_code { get; set; }

        [DataMember]
        public string node_name { get; set; }

        [DataMember]
        public long? category_id { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public string category_code { get; set; }

        [DataMember]
        public double buffer_norm { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public double in_transit { get; set; }

        [DataMember]
        public double sales { get; set; }

        [DataMember]
        public double inst_hold { get; set; }

        [DataMember]
        public double quality_hold { get; set; }

        [DataMember]
        public double qa_hold { get; set; }

        [DataMember]
        public double pending_orders { get; set; }

        [DataMember]
        public double orders_in_hand { get; set; }

        [DataMember]
        public string transport_mode { get; set; }

        [DataMember]
        public long supply_node_id { get; set; }

        [DataMember]
        public string supply_node_code { get; set; }

        [DataMember]
        public string supply_node_name { get; set; }

        [DataMember]
        public double supplying_quantity { get; set; }

        [DataMember]
        public double supplying_buffer_norm { get; set; }

        [DataMember]
        public double green_zone_qty { get; set; }

        [DataMember]
        public double yellow_zone_qty { get; set; }

        [DataMember]
        public double red_zone_qty { get; set; }

        [DataMember]
        public string closing_stock_zone { get; set; }

        [DataMember]
        public int closing_stock_zone_sort { get; set; }

        [DataMember]
        public string in_transit_zone { get; set; }

        [DataMember]
        public int in_transit_zone_sort { get; set; }

        [DataMember]
        public string supplying_node_zone { get; set; }

        [DataMember]
        public int supplying_node_zone_sort { get; set; }

        [DataMember]
        public double replenishment_quantity { get; set; }

        [DataMember]
        public double final_quantity { get; set; }

        [DataMember]
        public double mrq { get; set; }

        [DataMember]
        public int transaction_comment { get; set; }

        [DataMember]
        public List<SKUAttributeMapping> attributes { get; set; }

        [DataMember]
        public long[] skuSelection { get; set; }

        [DataMember]
        public long[] categorySelection { get; set; }

        [DataMember]
        public long[] nodeSelection { get; set; }

        [DataMember]
        public long[] supplynodeSelection { get; set; }

        [DataMember]
        public long[] bomSelection { get; set; }

        [DataMember]
        public int db_sync { get; set; }

        [DataMember]
        public int mongo_sync { get; set; }


        [DataMember]
        public double open_stock_transfer_order { get; set; }

        [DataMember]
        public double open_planned_order { get; set; }

        [DataMember]
        public double open_purchase_request { get; set; }

        [DataMember]
        public double open_production_order { get; set; }

        [DataMember]
        public double open_purchase_order { get; set; }

        [DataMember]
        public string field1 { get; set; }

        [DataMember]
        public string field2 { get; set; }

        [DataMember]
        public string field3 { get; set; }

        [DataMember]
        public string field4 { get; set; }

        [DataMember]
        public string field5 { get; set; }

        [DataMember]
        public string field6 { get; set; }

        [DataMember]
        public string field7 { get; set; }

        [DataMember]
        public string field8 { get; set; }

        [DataMember]
        public string field9 { get; set; }

        [DataMember]
        public string field10 { get; set; }

        [DataMember]
        public int excess_inv_wait_cycle { get; set; }

        [DataMember]
        public double grn { get; set; }

        [DataMember]
        public double mtd_grn { get; set; }

        [DataMember]
        public double mtd_sales { get; set; }

        [DataMember]
        public int stockout_inv_wait_cycle { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            buffer_norm = Common.Instance.OnSerializingRound(buffer_norm);
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            in_transit = Common.Instance.OnSerializingRound(in_transit);
            sales = Common.Instance.OnSerializingRound(sales);
            inst_hold = Common.Instance.OnSerializingRound(inst_hold);
            quality_hold = Common.Instance.OnSerializingRound(quality_hold);
            qa_hold = Common.Instance.OnSerializingRound(qa_hold);
            pending_orders = Common.Instance.OnSerializingRound(pending_orders);
            orders_in_hand = Common.Instance.OnSerializingRound(orders_in_hand);
            supplying_quantity = Common.Instance.OnSerializingRound(supplying_quantity);
            supplying_buffer_norm = Common.Instance.OnSerializingRound(supplying_buffer_norm);
            green_zone_qty = Common.Instance.OnSerializingRound(green_zone_qty);
            yellow_zone_qty = Common.Instance.OnSerializingRound(yellow_zone_qty);
            red_zone_qty = Common.Instance.OnSerializingRound(red_zone_qty);
            replenishment_quantity = Common.Instance.OnSerializingRound(replenishment_quantity);
            final_quantity = Common.Instance.OnSerializingRound(final_quantity);
            mrq = Common.Instance.OnSerializingRound(mrq);
            open_stock_transfer_order = Common.Instance.OnSerializingRound(open_stock_transfer_order);
            open_planned_order = Common.Instance.OnSerializingRound(open_planned_order);
            open_purchase_request = Common.Instance.OnSerializingRound(open_purchase_request);
            open_production_order = Common.Instance.OnSerializingRound(open_production_order);
            open_purchase_order = Common.Instance.OnSerializingRound(open_purchase_order);
            grn = Common.Instance.OnSerializingRound(grn);
            mtd_grn = Common.Instance.OnSerializingRound(mtd_grn);
            mtd_sales = Common.Instance.OnSerializingRound(mtd_sales);
        }
    }
}
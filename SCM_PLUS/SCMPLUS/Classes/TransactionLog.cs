﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class TransactionLog
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long transaction_log_id { get; set; }

        [DataMember]
        public long transaction_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long supply_node_id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public int grn { get; set; }

        [DataMember]
        public int sales { get; set; }

        [DataMember]
        public int closing_quantity { get; set; }

        [DataMember]
        public int in_transit { get; set; }

        [DataMember]
        public int inst_hold { get; set; }

        [DataMember]
        public int quality_hold { get; set; }

        [DataMember]
        public int qa_hold { get; set; }

        [DataMember]
        public int pending_orders { get; set; }

        [DataMember]
        public int orders_in_hand { get; set; }

        [DataMember]
        public string transport_mode { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public string transaction_comments { get; set; }
    }
}
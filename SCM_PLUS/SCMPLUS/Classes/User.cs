﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class User
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long user_id { get; set; }

        [DataMember]
        public string user_name { get; set; }

        [DataMember]
        public string first_name { get; set; }

        [DataMember]
        public string middle_name { get; set; }

        [DataMember]
        public string last_name { get; set; }

        [DataMember]
        public string email { get; set; }

        [DataMember]
        public long mobile { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public long role_id { get; set; }

        [DataMember]
        public long? region_id { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public int approved { get; set; }

        [DataMember]
        public int locked { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
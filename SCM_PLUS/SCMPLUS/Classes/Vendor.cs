﻿using System.Runtime.Serialization;
using MongoDB.Bson;

namespace SCMPLUS
{
    [DataContract]
    public class Vendor
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long vendor_id { get; set; }

        [DataMember]
        public string vendor_code { get; set; }

        [DataMember]
        public string vendor_name { get; set; }

        [DataMember]
        public int moq { get; set; }

        [DataMember]
        public long region_id { get; set; }

        [DataMember]
        public Region region { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
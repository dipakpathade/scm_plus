﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class UserNode
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long user_node_id { get; set; }

        [DataMember]
        public long user_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
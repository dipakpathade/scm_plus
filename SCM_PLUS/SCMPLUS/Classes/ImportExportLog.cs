﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class ImportExportLog
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long import_export_log_id { get; set; }

        [DataMember]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime reference_date { get; set; }

        [DataMember]
        public string reference_type { get; set; }

        [DataMember]
        public string reference_title { get; set; }

        [DataMember]
        public string type { get; set; }

        [DataMember]
        public string log_file { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public string status_text { get; set; }

        [DataMember]
        public string comment { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class Attributes
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long attribute_id { get; set; }

        [DataMember]
        public string attribute_name { get; set; }

        [DataMember]
        public string attribute_code { get; set; }

        [DataMember]
        public string data_type { get; set; }

        [DataMember]
        public int optional { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long? modified_by { get; set; }

        [DataMember]
        public string modified_on { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class Matrix1
    {
        [DataMember]
        public string column_name { get; set; }

        [DataMember]
        public string column_value { get; set; }

        [DataMember]
        public int rec_count { get; set; }

    }

}
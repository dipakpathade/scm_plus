﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class SKUCategory
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long sku_category_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public SKU sku { get; set; }

        [DataMember]
        public Category category { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }
    }

    [DataContract]
    public class TempSKUCategory
    {
        [DataMember]
        public List<SKUCategory> skuCategory { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }
}
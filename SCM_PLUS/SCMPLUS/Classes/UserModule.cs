﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class UserModule
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long? user_module_id { get; set; }

        [DataMember]
        public long? role_id { get; set; }

        [DataMember]
        public long? module_id { get; set; }

        [DataMember]
        public int read { get; set; }

        [DataMember]
        public int write { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
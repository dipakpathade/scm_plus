﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class SKUGrid
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public string sku_name { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public string node_name { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public double avg_sales { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public double buffer_norm { get; set; }

        [DataMember]
        public double final_quantity { get; set; }

        [DataMember]
        public string one { get; set; }

        [DataMember]
        public string two { get; set; }

        [DataMember]
        public string three { get; set; }

        [DataMember]
        public string four { get; set; }

        [DataMember]
        public string five { get; set; }

        [DataMember]
        public string six { get; set; }

        [DataMember]
        public string seven { get; set; }

        [DataMember]
        public string eight { get; set; }

        [DataMember]
        public string nine { get; set; }

        [DataMember]
        public string ten { get; set; }

        [DataMember]
        public string eleven { get; set; }

        [DataMember]
        public string twelve { get; set; }

        [DataMember]
        public string thirteen { get; set; }

        [DataMember]
        public string fourteen { get; set; }

        [DataMember]
        public string fifteen { get; set; }

        [DataMember]
        public string sixteen { get; set; }

        [DataMember]
        public string seventeen { get; set; }

        [DataMember]
        public string eighteen { get; set; }

        [DataMember]
        public string nineteen { get; set; }

        [DataMember]
        public string twenty { get; set; }

        [DataMember]
        public string twentyone { get; set; }

        [DataMember]
        public string twentytwo { get; set; }

        [DataMember]
        public string twentythree { get; set; }

        [DataMember]
        public string twentyfour { get; set; }

        [DataMember]
        public string twentyfive { get; set; }

        [DataMember]
        public string twentysix { get; set; }

        [DataMember]
        public string twentyseven { get; set; }

        [DataMember]
        public string twentyeight { get; set; }

        [DataMember]
        public string twentynine { get; set; }

        [DataMember]
        public string thirty { get; set; }

        [DataMember]
        public string one_qty { get; set; }

        [DataMember]
        public string two_qty { get; set; }

        [DataMember]
        public string three_qty { get; set; }

        [DataMember]
        public string four_qty { get; set; }

        [DataMember]
        public string five_qty { get; set; }

        [DataMember]
        public string six_qty { get; set; }

        [DataMember]
        public string seven_qty { get; set; }

        [DataMember]
        public string eight_qty { get; set; }

        [DataMember]
        public string nine_qty { get; set; }

        [DataMember]
        public string ten_qty { get; set; }

        [DataMember]
        public string eleven_qty { get; set; }

        [DataMember]
        public string twelve_qty { get; set; }

        [DataMember]
        public string thirteen_qty { get; set; }

        [DataMember]
        public string fourteen_qty { get; set; }

        [DataMember]
        public string fifteen_qty { get; set; }

        [DataMember]
        public string sixteen_qty { get; set; }

        [DataMember]
        public string seventeen_qty { get; set; }

        [DataMember]
        public string eighteen_qty { get; set; }

        [DataMember]
        public string nineteen_qty { get; set; }

        [DataMember]
        public string twenty_qty { get; set; }

        [DataMember]
        public string twentyone_qty { get; set; }

        [DataMember]
        public string twentytwo_qty { get; set; }

        [DataMember]
        public string twentythree_qty { get; set; }

        [DataMember]
        public string twentyfour_qty { get; set; }

        [DataMember]
        public string twentyfive_qty { get; set; }

        [DataMember]
        public string twentysix_qty { get; set; }

        [DataMember]
        public string twentyseven_qty { get; set; }

        [DataMember]
        public string twentyeight_qty { get; set; }

        [DataMember]
        public string twentynine_qty { get; set; }

        [DataMember]
        public string thirty_qty { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            avg_sales = Common.Instance.OnSerializingRound(avg_sales);
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            buffer_norm = Common.Instance.OnSerializingRound(buffer_norm);
            final_quantity = Common.Instance.OnSerializingRound(final_quantity);
        }
    }

    [DataContract]
    public class TempSKUGRID
    {
        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string avg_date { get; set; }

        [DataMember]
        public List<Transaction> transaction_list { get; set; }
    }
}
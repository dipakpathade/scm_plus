﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class BufferZone
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long transaction_id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public string buffer_norm_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public long zone_count { get; set; }

        [DataMember]
        public string closing_stock_zone { get; set; }
    }

    public class BufferZoneMapReduce
    {
        public object id { get; set; }
        public BufferZone value { get; set; }
    }

    public class BufferZoneList
    {
        public string zone { get; set; }
        public string zone_name { get; set; }
        public string date { get; set; }
        public double closing_quantity { get; set; }
        public long count { get; set; }
        public double percentage { get; set; }
        public int sort { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
        }
    }
}
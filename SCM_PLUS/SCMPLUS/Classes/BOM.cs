﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    [DataContract]
    public class BOM
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long bom_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public string sku_name { get; set; }

        [DataMember]
        public string sku_code { get; set; }

        [DataMember]
        public string sku_description { get; set; }

        [DataMember]
        public long parent_id { get; set; }

        [DataMember]
        public bool parent_valid { get; set; }

        [DataMember]
        public string parent_sku_code { get; set; }

        [DataMember]
        public string parent_sku_name { get; set; }

        [DataMember]
        public double quantity { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public int is_valid { get; set; }


        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public long[] categorySelection { get; set; }

        [DataMember]
        public List<SKUAttributeMapping> attributes { get; set; }

        [DataMember]
        public string identifier { get; set; }

        [DataMember]
        public long row_index { get; set; }

        [DataMember]
        public string row_status { get; set; }

        [DataMember]
        public List<BOM> bom_list { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            quantity = Common.Instance.OnSerializingRound(quantity);
        }
    }

    [DataContract]
    public class TempBOM
    {
        [DataMember]
        public List<BOM> boms { get; set; }

        [DataMember]
        public string table_name { get; set; }
    }
}
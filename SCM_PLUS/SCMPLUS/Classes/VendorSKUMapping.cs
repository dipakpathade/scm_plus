﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class VendorSKUMapping
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long vendor_sku_mapping_id { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long vendor_id { get; set; }

        [DataMember]
        public long region_id { get; set; }

        [DataMember]
        public int moq { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
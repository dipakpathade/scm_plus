﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class AuditLog
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long audit_log_id { get; set; }

        [DataMember]
        public string object_id { get; set; }

        [DataMember]
        public string object_type { get; set; }

        [DataMember]
        public string reason { get; set; }

        [DataMember]
        public string log { get; set; }

        [DataMember]
        public long? created_by { get; set; }

        [DataMember]
        public string created_on { get; set; }
    }
}
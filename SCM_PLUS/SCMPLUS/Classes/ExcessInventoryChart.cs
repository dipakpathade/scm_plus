﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;
using System.Linq;

namespace SCMPLUS
{
    [DataContract]
    public class ExcessInventoryChart
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public double total_sku_count { get; set; }

        [DataMember]
        public double all_sku_count { get; set; }

        [DataMember]
        public double blue_sku_count { get; set; }

        [DataMember]
        public double excess_inventory { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            total_sku_count = Common.Instance.OnSerializingRound(total_sku_count);
            blue_sku_count = Common.Instance.OnSerializingRound(blue_sku_count);

            excess_inventory = (blue_sku_count > 0 && total_sku_count > 0) ? Math.Round(((blue_sku_count / total_sku_count) * 100), 2) : 0;
        }
    }
}
﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using System;

namespace SCMPLUS
{
    [DataContract]
    public class SKUChart
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public string display_date { get; set; }

        [DataMember]
        public int transfilter_date { get; set; }

        [DataMember]
        public long sku_id { get; set; }

        [DataMember]
        public long node_id { get; set; }

        [DataMember]
        public long category_id { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public double in_transit { get; set; }

        [DataMember]
        public double sales { get; set; }

        [DataMember]
        public double green_zone_qty { get; set; }

        [DataMember]
        public double yellow_zone_qty { get; set; }

        [DataMember]
        public double red_zone_qty { get; set; }

        [DataMember]
        public double total_sku_count { get; set; }

        [DataMember]
        public double blue_sku_count { get; set; }

        [DataMember]
        public double rb_sku_count { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            in_transit = Common.Instance.OnSerializingRound(in_transit);
            sales = Common.Instance.OnSerializingRound(sales);
            green_zone_qty = Common.Instance.OnSerializingRound(green_zone_qty);
            yellow_zone_qty = Common.Instance.OnSerializingRound(yellow_zone_qty);
            red_zone_qty = Common.Instance.OnSerializingRound(red_zone_qty);
            total_sku_count = Common.Instance.OnSerializingRound(total_sku_count);
            blue_sku_count = Common.Instance.OnSerializingRound(blue_sku_count);
            rb_sku_count = Common.Instance.OnSerializingRound(rb_sku_count);
        }
    }
}
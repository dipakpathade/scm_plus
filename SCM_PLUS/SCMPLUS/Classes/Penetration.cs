﻿using System.Runtime.Serialization;
using MongoDB.Bson;
using System;
using MongoDB.Bson.Serialization.Attributes;

namespace SCMPLUS
{
    [DataContract]
    public class Penetration
    {
        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public long penetration_id { get; set; }

        [DataMember]
        public string transaction_date { get; set; }

        [DataMember]
        public long? sku_id { get; set; }

        [DataMember]
        public string sku_code { get; set; }

        [DataMember]
        public string sku_name { get; set; }

        [DataMember]
        public long? node_id { get; set; }

        [DataMember]
        public string node_code { get; set; }

        [DataMember]
        public string node_name { get; set; }

        [DataMember]
        public long? category_id { get; set; }

        [DataMember]
        public string category_code { get; set; }

        [DataMember]
        public string category_name { get; set; }

        [DataMember]
        public double closing_quantity { get; set; }

        [DataMember]
        public string closing_stock_zone { get; set; }

        [DataMember]
        public double buffer_norm { get; set; }

        [DataMember]
        public string effective_from { get; set; }

        [DataMember]
        public double rlt { get; set; }

        [DataMember]
        public string supplying_node_zone { get; set; }

        [DataMember]
        public double red_zone_quantity { get; set; }

        [DataMember]
        public double green_zone_quantity { get; set; }

        [DataMember]
        public double tmr_penetration { get; set; }

        [DataMember]
        public double tmr_cumulative_penetration { get; set; }

        [DataMember]
        public double tmg_penetration { get; set; }

        [DataMember]
        public double tmg_cumulative_penetration { get; set; }

        [DataMember]
        public int trigger_bn_change { get; set; }

        [DataMember]
        public int count_reset { get; set; }

        [DataMember]
        public string cooling_upto { get; set; }

        [DataMember]
        public string created_on { get; set; }

        [DataMember]
        public double tmr_penetration_percentage { get; set; }

        [DataMember]
        public double tmg_penetration_percentage { get; set; }

        [DataMember]
        public double new_buffer_norm { get; set; }

        [DataMember]
        public string new_effective_from { get; set; }

        [DataMember]
        public int rec_count { get; set; }

        [OnSerializing]
        void SerializingMethod(StreamingContext sc)
        {
            closing_quantity = Common.Instance.OnSerializingRound(closing_quantity);
            buffer_norm = Common.Instance.OnSerializingRound(buffer_norm);
            rlt = Common.Instance.OnSerializingRound(rlt);
            red_zone_quantity = Common.Instance.OnSerializingRound(red_zone_quantity);
            green_zone_quantity = Common.Instance.OnSerializingRound(green_zone_quantity);
            tmr_penetration = Common.Instance.OnSerializingRound(tmr_penetration);
            tmr_cumulative_penetration = Common.Instance.OnSerializingRound(tmr_cumulative_penetration);
            tmg_penetration = Common.Instance.OnSerializingRound(tmg_penetration);
            tmg_cumulative_penetration = Common.Instance.OnSerializingRound(tmg_cumulative_penetration);
            new_buffer_norm = Common.Instance.OnSerializingRound(new_buffer_norm);
        }
    }
}
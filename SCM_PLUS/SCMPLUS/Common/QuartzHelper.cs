﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Web.Hosting;
using System.Dynamic;
using Quartz.Impl;
using Quartz;
using Quartz.Impl.Matchers;
using System.Threading.Tasks;

namespace SCMPLUS
{
    public static class QuartzHelper
    {

        public static void startDBMFlow()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            TriggerKey key = new TriggerKey("DBM");
            var triggerJob = scheduler.GetTrigger(key);

            if (triggerJob.Status != TaskStatus.Running
                || triggerJob.Status != TaskStatus.WaitingToRun
                || triggerJob.Status != TaskStatus.WaitingForChildrenToComplete)
            {
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<DbmQuartzHelper>().Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("DBM")
                    .WithSimpleSchedule(x => x
                                       .WithIntervalInHours(24)
                                       .RepeatForever()
                                       //.WithIntervalInMinutes(1)
                                       //.WithRepeatCount(0)
                                       )
                    .UsingJobData("type", "DBM")
                    .StartAt(DateBuilder.DateOf(0, 0, 0))
                    .Build();

                scheduler.ScheduleJob(job, trigger);
            }
        }

        public static void startManualDBMFlow()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            TriggerKey key = new TriggerKey("ManualDBM");
            var triggerJob = scheduler.GetTrigger(key);

            if (triggerJob.Status != TaskStatus.Running
                || triggerJob.Status != TaskStatus.WaitingToRun
                || triggerJob.Status != TaskStatus.WaitingForChildrenToComplete)
            {
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<DbmQuartzHelper>().Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("ManualDBM")
                    .UsingJobData("type", "ManualDBM")
                    .WithSimpleSchedule(x => x
                                       .WithIntervalInMinutes(1)
                                       .WithRepeatCount(0)
                                       )
                    .StartNow()
                    .Build();

                scheduler.ScheduleJob(job, trigger);
            }
        }
    }
}

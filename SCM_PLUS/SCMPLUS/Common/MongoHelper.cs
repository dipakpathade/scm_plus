﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataTablePager.Core;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Text.RegularExpressions;

namespace MongoDB.Web.Providers
{
    internal class MongoHelper<T> where T : class, new()
    {
        private static void IsClassMapRegistered()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(T)))
                BsonClassMap.RegisterClassMap<T>(nm => { nm.AutoMap(); nm.SetIgnoreExtraElements(true); });
        }

        internal static MongoCollection<T> GetCollection(string collectionName)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            MongoCollection<T> Collection = database.GetCollection<T>(collectionName);
            return Collection;
        }

        internal static void RenameCollection(string oldCollectionName, string newCollectionName)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            database.RenameCollection(oldCollectionName, newCollectionName, true);
        }

        internal static void RemoveAll(string collectionName)
        {
            MongoCollection<T> Collection = GetCollection(collectionName);
            if (Collection != null)
                Collection.RemoveAll();
        }

        internal static List<T> All(string collectionName)
        {
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            MongoCollection<T> Collection = database.GetCollection<T>(collectionName);
            return Collection.FindAll().ToList<T>();
        }

        internal static T Single(string collectionName, IMongoQuery _query)
        {
            T retval = default(T);
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            retval = database.GetCollection<T>(collectionName).Find(query: _query).AsQueryable().SingleOrDefault();
            return retval;
        }

        internal static T One(string collectionName, IMongoQuery _query)
        {
            T retval = default(T);
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            retval = database.GetCollection<T>(collectionName).Find(query: _query).AsQueryable().Take(1).SingleOrDefault();
            return retval;
        }

        internal static List<T> Find(string collectionName, IMongoQuery _query)
        {
            List<T> retval = default(List<T>);
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            retval = database.GetCollection<T>(collectionName).Find(query: _query).AsQueryable().ToList<T>();
            return retval;
        }

        internal static List<T> Find(string collectionName, IMongoQuery _query, IMongoSortBy _sort_by)
        {
            List<T> retval = default(List<T>);
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            retval = database.GetCollection<T>(collectionName).Find(query: _query).SetSortOrder(_sort_by).AsQueryable().ToList<T>();
            return retval;
        }

        internal static List<T> Find(string name, IMongoQuery _query, string[] includeColumns)
        {
            List<T> retval = default(List<T>);
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            retval = database.GetCollection<T>(name).Find(query: _query).SetFields(Fields.Include(includeColumns)).AsQueryable().ToList<T>();
            return retval;
        }

        internal static WriteConcernResult Add(string collectionName, T item)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).Insert(item);
        }

        internal static WriteConcernResult Save(string collectionName, T item)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).Save(item);
        }

        internal static IEnumerable<WriteConcernResult> Add(string collectionName, IEnumerable<T> items)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).InsertBatch(items);
        }

        internal static WriteConcernResult Delete(string collectionName, IMongoQuery _query)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).Remove(_query);
        }

        internal static WriteConcernResult Update(string collectionName, IMongoQuery _query, IMongoUpdate _update)
        {
            IsClassMapRegistered();
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).Update(_query, _update, UpdateFlags.Multi);
        }

        internal static T getNextSequence(string column)
        {
            T retval = default(T);
            IsClassMapRegistered();

            MongoDatabase database = ConnectionHelper.GetDatabase();
            MongoCursor obj = database.GetCollection("counter").FindAll();

            foreach (BsonDocument x in obj)
            {
                x[column] = x[column].ToInt32() + 1;
                database.GetCollection("counter").Save(x);
            }

            retval = database.GetCollection<T>("counter").FindAll().AsQueryable().SingleOrDefault();
            return retval;
        }

        internal static long[] getIdArray(string collectionName, string key, IMongoQuery _query)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            IEnumerable<BsonValue> IDs = database.GetCollection<T>(collectionName).Distinct(key, _query);
            return IDs.Select(x => Convert.ToInt64(x)).ToArray();
        }

        internal static long[] getIdArray(string collectionName, string key)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            IEnumerable<BsonValue> IDs = database.GetCollection<T>(collectionName).Distinct(key);
            return IDs.Select(x => Convert.ToInt64(x)).ToArray();
        }

        /// <summary>
        /// Counts the specified condition.
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <returns></returns>
        internal static int Count(string collectionName, IMongoQuery condition)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return Convert.ToInt32(database.GetCollection<T>(collectionName).Find(query: condition).Count());
        }

        /// <summary>
        /// Count all elements
        /// </summary>
        /// <returns></returns>
        internal static long Count(string collectionName)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            return database.GetCollection<T>(collectionName).Count();
        }

        /// <summary>
        /// Paginates by an specified condition.
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="displayLength">The pagesize.</param>
        /// <param name="page">The page.</param>
        /// <param name="pOrderByClause">The OrderBy Clause.</param>
        /// <param name="pOrderByDescending">if set to <c>true</c> [order by descending].</param>
        /// <returns></returns>
        internal static List<T> Paginate(string collectionName, IMongoQuery condition, List<SearchAndSortable> sorted, out int iTotalRecords, int displayLength = 0, int displayStart = 0)
        {
            MongoDatabase database = ConnectionHelper.GetDatabase();
            IMongoSortBy pOrderByClause = SortBy.Ascending("_id");

            int ai = 0, di = 0;
            if (sorted.Count > 0)
            {
                int asc = 0, desc = 0;
                sorted.ForEach(sort =>
                {
                    if (sort.SortDirection.ToString().ToLower() == "asc") asc += 1;
                    else desc += 1;
                });

                string[] akeys = new string[asc];
                string[] dkeys = new string[desc];

                sorted.ForEach(sort =>
                {
                    if (sort.SortDirection.ToString().ToLower() == "asc")
                    {
                        akeys[ai] = sort.Name.Contains("_zone") ? sort.Name + "_sort" : sort.Name;
                        ++ai;
                    }
                    else
                    {
                        dkeys[di] = sort.Name.Contains("_zone") ? sort.Name + "_sort" : sort.Name;
                        ++di;
                    }
                });

                if (akeys.Length > 0 && dkeys.Length > 0)
                    pOrderByClause = SortBy.Ascending(akeys).Descending(dkeys);
                else if (akeys.Length > 0)
                    pOrderByClause = SortBy.Ascending(akeys);
                else if (dkeys.Length > 0)
                    pOrderByClause = SortBy.Descending(dkeys);
            }

            string keyName = pOrderByClause.ToString().Split(':')[0].Replace('"', '{').Replace("{", "").Trim();

            if (database.GetCollection<T>(collectionName).GetIndexes().Count() < 64)
            {
                var keys = new IndexKeysBuilder();

                if (pOrderByClause.ToString().Split(':')[1].Replace('"', '{').Replace("{", "").IndexOf("-1") >= 0)
                    keys.Descending(keyName);
                else
                    keys.Ascending(keyName);

                var options = new IndexOptionsBuilder();
                options.SetBackground(false);
                options.SetDropDups(false);
                database.GetCollection<T>(collectionName).EnsureIndex(keys, options);
            }

            if (displayLength != 0 || displayStart != 0)
            {
                var query = database.GetCollection<T>(collectionName).Find(query: condition).SetSortOrder(pOrderByClause).SetSkip(displayStart).SetLimit(displayLength).AsQueryable().ToList();
                iTotalRecords = Count(collectionName, condition);
                return query.ToList<T>();
            }
            else
            {
                var query = database.GetCollection<T>(collectionName).Find(query: condition).AsQueryable().ToList();
                iTotalRecords = query.Count;
                return query.ToList<T>();
            }
        }

        /// <summary>
        /// Search indexes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="search">The search value</param>
        /// <param name="limit">Limit size of results</param>
        /// <param name="foundedRecords">total number of founded records in index</param>
        /// <returns></returns>
        internal static List<T> Search(string collectionName, string field, string search, int page, int pagesize, out long foundedRecords)
        {
            var query = Query.Matches(field, new BsonRegularExpression(search, "i"));

            MongoDatabase database = ConnectionHelper.GetDatabase();
            var totallist = database.GetCollection<T>(collectionName).Find(query).ToList();

            foundedRecords = totallist.Count;
            return totallist.Skip(pagesize * (page - 1)).Take(pagesize).ToList();
        }

        internal static MapReduceResult MapReduce(string collectionName, IMongoQuery condition, string map, string reduce, MapReduceOptionsBuilder options = null)
        {
            MongoCollection<T> collection = MongoHelper<T>.GetCollection(collectionName);
            MapReduceOptionsBuilder opt = options;
            if (opt == null)
            {
                opt = new MapReduceOptionsBuilder();
                opt.SetOutput(MapReduceOutput.Inline);
            }

            if (condition != null)
            {
                MapReduceResult results = collection.MapReduce(condition, new BsonJavaScript(map), new BsonJavaScript(reduce), opt);
                return results;
            }
            else
            {
                MapReduceResult results = collection.MapReduce(new BsonJavaScript(map), new BsonJavaScript(reduce), opt);
                return results;
            }
        }

        internal static IMongoQuery MatchesStatement(string columnName, string searchValue)
        {
            return Query.Matches(columnName, "/^.*" + Regex.Escape(searchValue) + ".*/i");
        }
    }
}

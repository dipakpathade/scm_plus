using System;
using System.Threading;
using System.Collections.Generic;

namespace SCMPLUS
{
    public class Worker
    {
        public void RequestStop()
        {
            Thread.CurrentThread.Abort();
        }

        public void UploadBufferNorm(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadBufferNorm);
            workerThread.Name = "UploadBufferNorm";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadCategory(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadCategory);
            workerThread.Name = "UploadCategory";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadNode(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadNode);
            workerThread.Name = "UploadNode";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadNodeSkuMapping(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadNodeSkuMapping);
            workerThread.Name = "UploadNodeSkuMapping";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadSKU(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadSKU);
            workerThread.Name = "UploadSKU";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadSkuCategory(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadSkuCategory);
            workerThread.Name = "UploadSkuCategory";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadTransaction(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadTransaction);
            workerThread.Name = "UploadTransaction";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadSKUGrid(object param)
        {
            Thread workerThread = new Thread(new Upload().UploadSKUGrid);
            workerThread.Name = "UploadSKUGrid";
            workerThread.Start(param);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void StartFullSync(object param)
        {
            Thread workerThread = new Thread(new Upload().StartFullSync);
            workerThread.Name = "StartFullSync";
            workerThread.Start(param);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }

        public void UploadBOM(string fileName)
        {
            Thread workerThread = new Thread(new Upload().UploadBOM);
            workerThread.Name = "UploadBOM";
            workerThread.Start(Common.Instance.CurrentUser + ":" + fileName);
            workerThread.IsBackground = true;

            if (!workerThread.IsAlive)
                this.RequestStop();
        }
    }
}
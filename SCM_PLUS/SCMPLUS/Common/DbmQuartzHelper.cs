﻿using MongoDB.Web.Providers;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace SCMPLUS
{
    public class DbmQuartzHelper : IJob
    {
        public DbmQuartzHelper()
        {

        }

        public bool isJobRunning(IJobExecutionContext ctx, String jobName)
        {
            IReadOnlyCollection<IJobExecutionContext> currentJobs = ctx.Scheduler.GetCurrentlyExecutingJobs().Result;

            foreach (JobExecutionContextImpl jobCtx in currentJobs)
            {
                String thisJobName = jobCtx.JobDetail.Key.Name;
                String thisGroupName = jobCtx.JobDetail.Key.Group;
                if (jobName == thisJobName && jobCtx.FireTimeUtc != ctx.FireTimeUtc)
                {
                    return true;
                }
            }
            return false;
        }

        Task IJob.Execute(IJobExecutionContext context)
        {
            JobKey key = context.JobDetail.Key;

            JobDataMap dataMap = context.Trigger.JobDataMap;

            string type = dataMap.GetString("type");

            switch (type)
            {
                case "DBM":
                    if (!isJobRunning(context, type))
                    {
                        DbmWorkflow();
                    }
                    break;
                case "ManualDBM":
                    if (!isJobRunning(context, type))
                    {
                        DbmWorkflow();
                    }
                    break;
            }

            return null;
        }

        private void DbmWorkflow()
        {
            try
            {
                DynamicBufferManagement dbm = Dao.DynamicBufferManagement().SelectLatestDynamicBufferManagement();

                if (dbm.activate_auto_dbm == 1)
                {
                    Replenishment rpl = Dao.Replenishment().SelectMaxReplenishmentDate();
                    List<Penetration> penetrations = Dao.DynamicBufferManagement().SelectPenetrationListByTransactionDate(rpl.transaction_date);

                    if (penetrations == null || penetrations.Count == 0)
                    {
                        Dao.DynamicBufferManagement().InsertPenetrationListForTransactionDate(rpl.transaction_date);

                        List<Penetration> pList = Dao.DynamicBufferManagement().SelectPenetrationListByTransactionDateForBNChange(rpl.transaction_date);

                        if (pList != null && pList.Count > 0)
                        {
                            string identifier = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                            foreach (Penetration item in pList)
                            {
                                if (item.tmr_cumulative_penetration > 0)
                                {
                                    BufferNorm bn = new BufferNorm();
                                    bn.sku_id = (long)item.sku_id;
                                    bn.node_id = (long)item.node_id;
                                    BufferNorm bnObj = Dao.BufferNorm().SelectBufferNormBySKUNodeMaxEffectiveFrom(bn);

                                    bnObj.effective_from = DateTime.Now.Date;
                                    bnObj.identifier = identifier;
                                    var t = Convert.ToDecimal(dbm.stockout_inv_decrease_per) / 100m;
                                    var m = Convert.ToDecimal(bnObj.buffer_norm) * t;

                                    bnObj.buffer_norm = Math.Ceiling(bnObj.buffer_norm + Convert.ToDouble(m));

                                    List<BufferNorm> _bnLst = new List<BufferNorm>();
                                    _bnLst.Add(bnObj);
                                    Dao.BufferNorm().InsertBatch(_bnLst);

                                    item.new_buffer_norm = bnObj.buffer_norm;
                                    item.new_effective_from = identifier;
                                    Dao.DynamicBufferManagement().UpdatePenetrationBufferNorm(item);

                                    BufferNorm bufferNorm = Dao.BufferNorm().SelectBufferNormBySKUNodeMaxEffectiveFrom(bn);
                                    List<BufferNorm> bufferNormList = new List<BufferNorm>();
                                    bufferNormList.Add(bufferNorm);
                                    MongoHelper<BufferNorm>.Add(Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM), bufferNormList);
                                }
                                else if (item.tmg_cumulative_penetration > 0)
                                {
                                    BufferNorm bn = new BufferNorm();
                                    bn.sku_id = (long)item.sku_id;
                                    bn.node_id = (long)item.node_id;

                                    BufferNorm bnObj = Dao.BufferNorm().SelectBufferNormBySKUNodeMaxEffectiveFrom(bn);
                                    bnObj.effective_from = DateTime.Now.Date;
                                    bnObj.identifier = identifier;
                                    var t = Convert.ToDecimal(dbm.excess_inv_decrease_per) / 100m;
                                    var m = Convert.ToDecimal(bnObj.buffer_norm) * t;

                                    bnObj.buffer_norm = Math.Ceiling(bnObj.buffer_norm - Convert.ToDouble(m));

                                    List<BufferNorm> _bnLst = new List<BufferNorm>();
                                    _bnLst.Add(bnObj);
                                    Dao.BufferNorm().InsertBatch(_bnLst);

                                    item.new_buffer_norm = bnObj.buffer_norm;
                                    item.new_effective_from = identifier;
                                    Dao.DynamicBufferManagement().UpdatePenetrationBufferNorm(item);

                                    BufferNorm bufferNorm = Dao.BufferNorm().SelectBufferNormBySKUNodeMaxEffectiveFrom(bn);
                                    List<BufferNorm> bufferNormList = new List<BufferNorm>();
                                    bufferNormList.Add(bufferNorm);
                                    MongoHelper<BufferNorm>.Add(Common.Instance.GetEnumValue(Common.CollectionName.BUFFER_NORM), bufferNormList);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}

﻿using System.Runtime.Serialization;

namespace SCMPLUS
{
    [DataContract]
    public class DocumentResult
    {
        [DataMember]
        public string file_name { get; set; }
        [DataMember]
        public string base64string { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace SCMPLUS
{
    public class NumeralAlphaCompare : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            int nIndexX = x.Replace(":", " ").IndexOf(" ");
            int nIndexY = y.Replace(":", " ").IndexOf(" ");
            bool bSpaceX = nIndexX > -1;
            bool bSpaceY = nIndexY > -1;

            long nX;
            long nY;
            if (bSpaceX && bSpaceY)
            {
                if (long.TryParse(x.Substring(0, nIndexX).Replace(".", ""), out nX)
                    && long.TryParse(y.Substring(0, nIndexY).Replace(".", ""), out nY))
                {
                    if (nX < nY)
                    {
                        return -1;
                    }
                    else if (nX > nY)
                    {
                        return 1;
                    }
                }
            }
            else if (bSpaceX)
            {
                if (long.TryParse(x.Substring(0, nIndexX).Replace(".", ""), out nX)
                    && long.TryParse(y, out nY))
                {
                    if (nX < nY)
                    {
                        return -1;
                    }
                    else if (nX > nY)
                    {
                        return 1;
                    }
                }
            }
            else if (bSpaceY)
            {
                if (long.TryParse(x, out nX)
                    && long.TryParse(y.Substring(0, nIndexY).Replace(".", ""), out nY))
                {
                    if (nX < nY)
                    {
                        return -1;
                    }
                    else if (nX > nY)
                    {
                        return 1;
                    }
                }
            }
            else
            {
                if (long.TryParse(x, out nX)
                    && long.TryParse(y, out nY))
                {
                    if (nX < nY)
                    {
                        return -1;
                    }
                    else if (nX > nY)
                    {
                        return 1;
                    }
                }
            }
            return x.CompareTo(y);
        }
    }
}
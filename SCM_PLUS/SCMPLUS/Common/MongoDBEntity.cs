﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Runtime.Serialization;

namespace MongoDB.Web.Providers
{
    public abstract class MongoDBEntity
    {
        [DataMember]
        public ObjectId _id { get; set; }
    }
}

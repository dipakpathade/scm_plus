﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.ServiceModel.Web;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Security.Cryptography;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

namespace SCMPLUS
{
    public class AuthenticatorOfHeader : IDispatchMessageInspector
    {
        public const string BasicAuth = "Basic";
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

        #region "DispatchMessageInspector"

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            // Check to see if there is an Authorization in the header, otherwise throw a 401
            if (WebOperationContext.Current.IncomingRequest.Headers["oAuthToken"] == null || WebOperationContext.Current.IncomingRequest.Headers["RequestId"] == null)
                throw new WebFaultException<string>("oAuthToken/RequestId has not been set.", HttpStatusCode.Unauthorized);

            //if (Common.Instance.ServiceAuthToken != WebOperationContext.Current.IncomingRequest.Headers["RequestId"].ToString())
            //    throw new WebFaultException<string>(WebOperationContext.Current.IncomingRequest.Headers["RequestId"].ToString() + " Invalid 'RequestId' header. " + Common.Instance.ServiceAuthToken, HttpStatusCode.Unauthorized);

            // Decode the header, check password
            string publicKey = Common.Instance.PublicKey;
            string privateKey = Common.Instance.PrivateKey;

            string encodedUnamePwd = GetEncodedCredentialsFromHeader();

            //if (string.IsNullOrEmpty(encodedUnamePwd))
            //        throw new WebFaultException<string>("Invalid 'oAuthToken' header.", HttpStatusCode.Unauthorized);

            try
            {
                //if (HttpContext.Current.Session["ServiceAuthToken"] == null)
                //    throw new WebFaultException<string>("Your Session has expired.", HttpStatusCode.Unauthorized);

                // Decode the credentials
                string decryptedData = Decrypt(encodedUnamePwd, privateKey);

                // Validate User and Password
                string[] authParts = decryptedData.Split(':');
                User _user = null;

                if (authParts != null)
                {
                    if (authParts.Length > 1)
                    {
                        User _obj_user = new User();
                        _obj_user.user_name = authParts[0];
                        _obj_user.password = authParts[1];
                        _user = Dao.User().SelectUserCheckAuthenticate(_obj_user);
                    }
                }

                if (_user == null)
                    throw new WebFaultException<string>("Invalid 'oAuthToken' header.", HttpStatusCode.Unauthorized);

                Common.Instance.CurrentUser = _user.user_id;
                Common.Instance.CurrentRole = _user.role_id;

                if (Common.Instance.GetLoginKey(authParts[0], _user.user_id.ToString()) != WebOperationContext.Current.IncomingRequest.Headers["oAuthToken"].ToString())
                    throw new WebFaultException<string>("Your Session has expired due to concurrent login.", HttpStatusCode.Conflict);
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            //if (reply.Version.Envelope == EnvelopeVersion.Soap11)
            //{
            //    Common.Instance.ServiceAuthToken = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Guid.NewGuid().ToString()));
            //    MessageHeader header = MessageHeader.CreateHeader("RequestId", Common.Instance.ServiceAuthToken, !reply.IsFault);
            //    reply.Headers.Add(header);
            //}
        }

        #endregion

        /// <summary>
        /// Basic auth encodes uname and pwd pair. We take the credential string from the HTTP header.
        /// </summary>
        /// <returns></returns>
        private static string GetEncodedCredentialsFromHeader()
        {
            WebOperationContext ctx = WebOperationContext.Current;

            // credentials are in the Authorization Header
            string credsHeader = ctx.IncomingRequest.Headers["oAuthToken"];
            if (credsHeader != null)
            {
                // make sure that we have 'Basic' auth header. Anything else can't be handled
                string creds = null;
                int credsPosition = credsHeader.IndexOf(BasicAuth, StringComparison.OrdinalIgnoreCase);
                if (credsPosition != -1)
                {
                    // 'Basic' creds were found
                    credsPosition += BasicAuth.Length;
                    if (credsPosition < credsHeader.Length - 1)
                    {
                        creds = credsHeader.Substring(credsPosition, credsHeader.Length - credsPosition);
                        return creds;
                    }
                    return null;
                }
                else
                {
                    // we did not find Basic auth header but some other type of auth. We can't handle it. Return null.
                    return null;
                }
            }

            // no auth header was found
            return null;
        }

        public static string Encrypt(string data, string publicKey)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(publicKey);
            var dataToEncrypt = ASCIIEncoding.ASCII.GetBytes(data);
            var encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            var length = encryptedByteArray.Count();
            var item = 0;
            var sb = new StringBuilder();
            foreach (var x in encryptedByteArray)
            {
                item++;
                sb.Append(x);
                if (item < length)
                    sb.Append(",");
            }
            return Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(sb.ToString()));
        }

        public static string Decrypt(string data, string privateKey)
        {
            data = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(data));
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            string[] dataArray = data.Split(new char[] { ',' });
            byte[] dataByte = new byte[dataArray.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataByte[i] = Convert.ToByte(dataArray[i]);
            }
            rsa.FromXmlString(privateKey);
            byte[] decryptedByte = rsa.Decrypt(dataByte, false);
            return ASCIIEncoding.ASCII.GetString(decryptedByte);
        }
    }
}
﻿using System;
using System.Configuration;
using System.Runtime.Serialization;

namespace SCMPLUS
{
    [DataContract]
    public class AppSettings
    {
        public static string PhysicalPath
        {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }

        public static string LicenseFile
        {
            get { return ConfigurationManager.AppSettings["LicenseFile"]; }
        }

        public static string ExportFolder
        {
            get { return ConfigurationManager.AppSettings["exportFolder"]; }
        }

        public static string ExportPath
        {
            get { return AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["exportFolder"]; }
        }

        public static string FileUploadPath
        {
            get { return AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["fileUploadFolder"]; }
        }

        public static string Company
        {
            get { return ConfigurationManager.AppSettings["company"]; }
        }

        public static string ReplenishmentXlsName
        {
            get { return ConfigurationManager.AppSettings["replenishmentXLS"]; }
        }

        public static string CategoryXlsName
        {
            get { return ConfigurationManager.AppSettings["categoryXLS"]; }
        }

        public static string NodeXlsName
        {
            get { return ConfigurationManager.AppSettings["nodeXLS"]; }
        }

        public static string RegionXlsName
        {
            get { return ConfigurationManager.AppSettings["regionXLS"]; }
        }

        public static string SKUXlsName
        {
            get { return ConfigurationManager.AppSettings["skuXLS"]; }
        }

        public static string SKUNodeXlsName
        {
            get { return ConfigurationManager.AppSettings["skunodeXLS"]; }
        }

        public static string SKUCategoryXlsName
        {
            get { return ConfigurationManager.AppSettings["skucategoryXLS"]; }
        }

        public static string BufferNormXlsName
        {
            get { return ConfigurationManager.AppSettings["buffernormXLS"]; }
        }

        public static string TransactionXlsName
        {
            get { return ConfigurationManager.AppSettings["transactionXLS"]; }
        }

        public static string VendorXlsName
        {
            get { return ConfigurationManager.AppSettings["vendorXLS"]; }
        }

        public static string UserXlsName
        {
            get { return ConfigurationManager.AppSettings["userXLS"]; }
        }

        public static int MaxLoginAttempts
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["maxLoginAttempts"]); }
        }

        public static string ExportFilePassword
        {
            get { return ConfigurationManager.AppSettings["exportFilePassword"]; }
        }

        public static string ServerApplication
        {
            get { return ConfigurationManager.AppSettings["serverApplication"]; }
        }

        public static int MaxTransactionDaysInMongo
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["maxTransactionDays"]); }
        }

        public static string LogFolder
        {
            get { return AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["logFolder"]; }
        }

        public static string RoleXlsName
        {
            get { return ConfigurationManager.AppSettings["roleXLS"]; }
        }

        public static string Matrix1XlsName
        {
            get { return ConfigurationManager.AppSettings["matrix1yXLS"]; }
        }

        public static string Matrix2XlsName
        {
            get { return ConfigurationManager.AppSettings["matrix2yXLS"]; }
        }
    }
}
﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using SCMPLUS;

namespace MongoDB.Web.Providers
{
    internal class ConnectionHelper
    {
        private static MongoDatabase db;

        internal static MongoDatabase GetDatabase()
        {
            if (db != null)
            {
                return db;
            }
            else
            {
                string con = ConfigurationManager.ConnectionStrings["MongoDBConnString"].ConnectionString;
                string databaseName = MongoUrl.Create(con).DatabaseName;

                MongoServer server = Global.client.GetServer();

                db = server.GetDatabase(databaseName);

                if (server != null)
                    if (server.State == MongoServerState.Connected)
                        server.Disconnect();

                return db;
            }
        }
    }
}

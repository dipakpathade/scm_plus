﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Web;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using MongoDB.Bson.Serialization;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Data.OleDb;
using MongoDB.Web.Providers;
using MongoDB.Driver.Builders;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SCMPLUS
{
    [DataContract]
    public class Common
    {
        private static Common _instance;
        public static Common Instance
        {
            get
            {
                if (_instance == null) { _instance = new Common(); }
                return _instance;
            }
        }

        public void ClassMapRegistered<Class>() where Class : class
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Class)))
                BsonClassMap.RegisterClassMap<Class>(nm => { nm.AutoMap(); nm.SetIgnoreExtraElements(true); });
        }

        public enum CollectionName
        {
            [StringValue("attribute")]
            ATTRIBUTE = 1,
            [StringValue("audit_log")]
            AUDIT_LOG = 2,
            [StringValue("buffer_norm")]
            BUFFER_NORM = 3,
            [StringValue("category")]
            CATEGORY = 4,
            [StringValue("city")]
            CITY = 5,
            [StringValue("comapny")]
            COMAPNY = 6,
            [StringValue("country")]
            COUNTRY = 7,
            [StringValue("dynamic_buffer_management")]
            DYNAMIC_BUFFER_MANAGEMENT = 8,
            [StringValue("import_export_log")]
            IMPORT_EXPORT_LOG = 9,
            [StringValue("location")]
            LOCATION = 10,
            [StringValue("module")]
            MODULE = 11,
            [StringValue("node")]
            NODE = 12,
            [StringValue("node_sku_mapping")]
            NODE_SKU_MAPPING = 13,
            [StringValue("region")]
            REGION = 14,
            [StringValue("replenishment")]
            REPLENISHMENT = 15,
            [StringValue("role")]
            ROLE = 16,
            [StringValue("sku")]
            SKU = 17,
            [StringValue("sku_attribute_mapping")]
            SKU_ATTRIBUTE_MAPPING = 18,
            [StringValue("sku_category")]
            SKU_CATEGORY = 19,
            [StringValue("state")]
            STATE = 20,
            [StringValue("transaction")]
            TRANSACTION = 21,
            [StringValue("transaction_comment")]
            TRANSACTION_COMMENT = 22,
            [StringValue("transaction_log")]
            TRANSACTION_LOG = 23,
            [StringValue("user")]
            USER = 24,
            [StringValue("user_category")]
            USER_CATEGORY = 25,
            [StringValue("user_log")]
            USER_LOG = 26,
            [StringValue("user_module")]
            USER_MODULE = 27,
            [StringValue("user_node")]
            USER_NODE = 28,
            [StringValue("vendor")]
            VENDOR = 29,
            [StringValue("vendor_sku_mapping")]
            VENDOR_SKU_MAPPING = 30,
            [StringValue("category_hierarchy")]
            CATEGORY_HIERARCHY = 31,
            [StringValue("sku_chart")]
            SKU_CHART = 32,
            [StringValue("sku_grid")]
            SKU_GRID = 33,
            [StringValue("inventory_turns")]
            INVENTORY_TURNS = 34,
            [StringValue("sku_chart_node")]
            SKU_CHART_NODE = 35,
            [StringValue("sku_chart_category")]
            SKU_CHART_CATEGORY = 36,
            [StringValue("sku_chart_sku")]
            SKU_CHART_SKU = 37,
            [StringValue("inventory_turns_sku")]
            INVENTORY_TURNS_SKU = 38,
            [StringValue("cntqty_fillrate")]
            CNTQTY_FILLRATE = 39,
            [StringValue("cntqty_fillrate_node")]
            CNTQTY_FILLRATE_NODE = 40,
            [StringValue("cntqty_fillrate_category")]
            CNTQTY_FILLRATE_CATEGORY = 41,
            [StringValue("cntqty_fillrate_sku")]
            CNTQTY_FILLRATE_SKU = 42,
            [StringValue("user_data_permissions")]
            USER_DATA_PERMISSIONS = 43,
            [StringValue("bom")]
            BOM = 44,
            [StringValue("bom_hierarchy")]
            BOM_HIERARCHY = 45
        };

        public enum CommentTypes
        {
            [StringValue("closing_stock_zone")]
            CLOSING_STOCK_ZONE = 1
        }

        /// <summary>
        /// For retrive enum string value
        /// </summary>
        /// <param name="value">Enum value</param>
        /// <returns>enum string value</returns>
        public string GetEnumValue(Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());
            StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }

        private DataTable xlsxToDT(string fileName)
        {
            var prevCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            try
            {
                DataTable table = new DataTable();
                XSSFWorkbook workbook = new XSSFWorkbook(new FileStream(fileName, FileMode.Open, FileAccess.Read));
                ISheet sheet = workbook.GetSheetAt(0);
                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;
                for (int i = headerRow.FirstCellNum; i < cellCount; i++)
                {
                    DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue);
                    table.Columns.Add(column);
                }
                int rowCount = sheet.LastRowNum;
                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    DataRow dataRow = table.NewRow();
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                        {
                            //EXCEPTION GENERATING IN THIS CODE
                            dataRow[j] = row.GetCell(j).ToString();
                            ////////////////////////////
                        }
                    }
                    table.Rows.Add(dataRow);
                }
                workbook = null;
                sheet = null;
                return table;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = prevCulture;
            }
        }

        public DataTable ImportDataFromExcel(string path, string filename)
        {
            string BasePath = path;
            string FileName = filename;
            if (File.Exists(BasePath + FileName))
            {
                return ImportUsingOLEDB(BasePath, FileName);
                //return xlsxToDT(BasePath + FileName);
            }
            else
                return null;
        }

        public DataTable ImportUsingOLEDB(string path, string filename)
        {
            string BasePath = path;
            string FileName = filename;
            if (File.Exists(BasePath + FileName))
            {
                string Constring = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + BasePath + FileName + "; Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"");
                if (!string.IsNullOrEmpty(Constring))
                {
                    OleDbConnection oconn = new OleDbConnection(Constring);
                    oconn.Open();
                    DataTable dt = oconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    String[] excelSheets = new String[dt.Rows.Count];
                    for (int j = 0; j < dt.Rows.Count; j++)
                        excelSheets[j] = dt.Rows[j]["TABLE_NAME"].ToString();

                    OleDbCommand ocmd = new OleDbCommand("select * from [" + excelSheets[0] + "]", oconn);
                    OleDbDataAdapter da = new OleDbDataAdapter(ocmd);
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds);
                        oconn.Close();
                        ocmd.Dispose();
                        da.Dispose();
                    }
                    catch
                    { throw; }
                    finally
                    {
                        oconn.Close();
                        ocmd.Dispose();
                        da.Dispose();
                        oconn.Dispose();
                    }

                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }

        public string PublicKey
        {
            get
            {
                if (HttpContext.Current.Session["PublicKey"] == null)
                    return "";
                return HttpContext.Current.Session["PublicKey"].ToString();
            }
            set
            {
                HttpContext.Current.Session["PublicKey"] = value;
            }
        }

        public string PrivateKey
        {
            get
            {
                if (HttpContext.Current.Session["PrivateKey"] == null)
                    return "";
                return HttpContext.Current.Session["PrivateKey"].ToString();
            }
            set
            {
                HttpContext.Current.Session["PrivateKey"] = value;
            }
        }

        public Int64 CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUser"] == null)
                    return 0;
                return Convert.ToInt64(HttpContext.Current.Session["CurrentUser"].ToString());
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }

        public Int64 CurrentRole
        {
            get
            {
                if (HttpContext.Current.Session["CurrentRole"] == null)
                    return 0;
                return Convert.ToInt64(HttpContext.Current.Session["CurrentRole"].ToString());
            }
            set
            {
                HttpContext.Current.Session["CurrentRole"] = value;
            }
        }

        public string ServiceAuthToken
        {
            get
            {
                if (HttpContext.Current.Session["ServiceAuthToken"] == null)
                    return "";
                return HttpContext.Current.Session["ServiceAuthToken"].ToString();
            }
            set
            {
                if (HttpContext.Current.Session["ServiceAuthToken"] != null)
                    HttpContext.Current.Session["ServiceAuthToken"] = value;

                HttpContext.Current.Session.Add("ServiceAuthToken", value);
            }
        }

        public void SetServiceRequestId()
        {
            Common.Instance.ServiceAuthToken = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Guid.NewGuid().ToString()));
            WebOperationContext.Current.OutgoingResponse.Headers.Add("RequestId", Common.Instance.ServiceAuthToken);
        }

        public enum ReplenishmentColumn
        {
            [StringValue("Transaction Date")]
            A = 1,
            [StringValue("SKU Code")]
            B = 2,
            [StringValue("SKU Name")]
            C = 3,
            [StringValue("Node Code")]
            D = 4,
            [StringValue("Node Name")]
            E = 5,
            [StringValue("Sale")]
            F = 6,
            [StringValue("Closing Stock")]
            G = 7,
            [StringValue("Buffer Norm")]
            H = 8,
            [StringValue("Closing Stock Zone")]
            I = 9,
            [StringValue("Replenishment Quantity")]
            J = 10,
            [StringValue("MRQ")]
            K = 11,
            [StringValue("In Transit Quantity")]
            L = 12,
            [StringValue("In Transit Zone")]
            M = 13,
            [StringValue("Supplier Node Code")]
            N = 14,
            [StringValue("Supplier Node Name")]
            O = 15,
            [StringValue("Supplier Node Zone")]
            P = 16,
            [StringValue("Inst. Stock")]
            Q = 17,
            [StringValue("Quality Hold Stock")]
            R = 18,
            [StringValue("QA hold Stock")]
            S = 19,
            [StringValue("PO")]
            T = 20,
            [StringValue("OIH")]
            U = 21,
            [StringValue("Transport Mode")]
            V = 22
        }

        public enum ColorCode
        {
            [StringValue("White")]
            WHITE = 1,
            [StringValue("Black")]
            BLACK = 2,
            [StringValue("Red")]
            RED = 3,
            [StringValue("Yellow")]
            YELLOW = 4,
            [StringValue("Green")]
            GREEN = 5,
            [StringValue("Blue")]
            BLUE = 6
        }

        public void SetKey(string keyName, object key, object value)
        {
            Dictionary<object, object> d = (Dictionary<object, object>)HttpContext.Current.Application[keyName];
            if (d != null)
            {
                lock (d)
                {
                    if (d.Keys.Contains(key))
                        d[key] = value;
                    else
                        d.Add(key, value);
                }
            }
        }

        public object GetKey(string keyName, object key)
        {
            Dictionary<object, object> d = (Dictionary<object, object>)HttpContext.Current.Application[keyName];
            if (d != null)
            {
                lock (d)
                {
                    if (d.Keys.Contains(key))
                        return d[key];
                }
            }
            return null;
        }

        public void SetLoginKey(string user_name, string id, string AuthToken)
        {
            Dictionary<string, string> d = (Dictionary<string, string>)HttpContext.Current.Application["UsersLoggedIn"];
            if (d != null)
            {
                lock (d)
                {
                    string key = (user_name + id);
                    if (d.Keys.Contains(key))
                        d[key] = AuthToken;
                    else
                        d.Add(key, AuthToken);
                }
            }
        }

        public string GetLoginKey(string user_name, string id)
        {
            Dictionary<string, string> d = (Dictionary<string, string>)HttpContext.Current.Application["UsersLoggedIn"];
            if (d != null)
            {
                lock (d)
                {
                    string key = (user_name + id);
                    if (d.Keys.Contains(key))
                        return d[key];
                }
            }
            return "";
        }

        public bool CreateZip(string filename, string filePath, string zipFileName, string zipFilePath, string password)
        {
            string path = filePath + filename;
            string zipPath = zipFilePath + zipFileName;

            if (System.IO.File.Exists(zipPath))
            {
                System.IO.File.Delete(zipPath);
            }

            FileStream fsOut = File.Create(zipPath);
            ZipOutputStream zipStream = new ZipOutputStream(fsOut);

            try
            {
                zipStream.SetLevel(9); //0-9, 9 being the highest level of compression

                zipStream.Password = password;  // optional. Null is the same as not setting. Required if using AES.

                FileInfo fi = new FileInfo(path);

                string entryName = filename; // Makes the name in zip based on the folder
                entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                ZipEntry newEntry = new ZipEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity

                // Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                // A password on the ZipOutputStream is required if using AES.
                //   newEntry.AESKeySize = 256;

                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length;

                zipStream.PutNextEntry(newEntry);

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using (FileStream streamReader = File.OpenRead(path))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();

                zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
                zipStream.Close();

                return true;
            }
            catch
            {
                zipStream.Close();
                return false;
            }
        }

        public static long InsertLog(string logfilePath, ImportExportLog item)
        {
            item.import_export_log_id = Dao.ImportExportLog().Insert(item);
            MongoHelper<ImportExportLog>.Add(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), item);

            using (StreamWriter file = new StreamWriter(logfilePath))
            {
                // Write to the file:
                file.WriteLine(DateTime.Now);
                file.WriteLine(item.status_text);
                file.WriteLine();
                file.Close();
                file.Dispose();
            }

            return item.import_export_log_id;
        }

        public static void UpdateLog(ImportExportLog item, string logfileName, bool fileOnly = false)
        {
            if (!fileOnly)
            {
                Dao.ImportExportLog().Update(item);

                ImportExportLog upItem = MongoHelper<ImportExportLog>.Single(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), Query.EQ("import_export_log_id", item.import_export_log_id));
                upItem.log_file = item.log_file;
                upItem.status = item.status;
                upItem.status_text = item.status_text;
                upItem.comment = item.comment;

                MongoHelper<ImportExportLog>.Save(Common.Instance.GetEnumValue(Common.CollectionName.IMPORT_EXPORT_LOG), upItem);
            }

            using (StreamWriter file = new StreamWriter(logfileName, true))
            {
                // Write to the file:
                file.WriteLine(DateTime.Now);
                file.WriteLine(item.status_text);
                file.WriteLine();
                file.Close();
                file.Dispose();
            }
        }

        public IMongoQuery GetPermissionObjectIDs(long rID, string objType, string qColmName)
        {
            int i = 0;
            IMongoQuery[] qc = new IMongoQuery[2];
            qc[i++] = Query.EQ("role_id", rID);
            qc[i++] = Query.EQ("object_type", objType.ToUpper().Trim());

            long[] permissionIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS), "object_id", Query.And(qc)).Distinct().ToArray();
            long[] tempArray = new long[permissionIDs.Length + 1];
            IMongoQuery _query_per = null;
            if (permissionIDs != null)
            {
                if (permissionIDs.Length > 0)
                {
                    for (int j = 0; j < permissionIDs.Length; j++)
                    {
                        tempArray[j] = permissionIDs[j];
                    }
                    _query_per = (objType.Trim().ToUpper() == "SKU") ? Query.NotIn(qColmName, new BsonArray(tempArray)) : Query.In(qColmName, new BsonArray(tempArray));
                }
                else
                    _query_per = (objType.Trim().ToUpper() == "SKU") ? Query.NE(qColmName, 0) : Query.EQ(qColmName, 0);
            }
            else
                _query_per = (objType.Trim().ToUpper() == "SKU") ? Query.NE(qColmName, 0) : Query.EQ(qColmName, 0);

            return _query_per;
        }

        public long[] GetPermissionArrayObjectIDs(long rID, string objType, string qColmName)
        {
            int i = 0;
            IMongoQuery[] qc = new IMongoQuery[2];
            qc[i++] = Query.EQ("role_id", rID);
            qc[i++] = Query.EQ("object_type", objType.ToUpper().Trim());

            long[] permissionIDs = MongoHelper<SKUCategory>.getIdArray(Common.Instance.GetEnumValue(Common.CollectionName.USER_DATA_PERMISSIONS), "object_id", Query.And(qc)).Distinct().ToArray();
            long[] tempArray = new long[permissionIDs.Length + 1];
            if (permissionIDs != null)
            {
                if (permissionIDs.Length > 0)
                {
                    for (int j = 0; j < permissionIDs.Length; j++)
                    {
                        tempArray[j] = permissionIDs[j];
                    }
                }
            }

            return tempArray;
        }

        public string getDicValue(Dictionary<string, long> d, string keyName)
        {
            if (d != null)
            {
                if (d.Count > 0)
                {
                    lock (d)
                    {
                        if (d.Keys.Contains(keyName))
                            return d[keyName].ToString();
                        else
                            return null;
                    }
                }
                else
                    return null;
            }
            else
                return null;
        }

        public double ConvertToDouble(string value)
        {
            ////return value.Trim().Length > 0 ? Convert.ToDouble(value.Trim()) : 0D;
            return value.Trim().Length > 0 ? Math.Round(Convert.ToDouble(value.Trim()), 2) : 0D;
        }

        public double OnSerializingRound(double value)
        {
            ////return value.Trim().Length > 0 ? Convert.ToDouble(value.Trim()) : 0D;
            return value > 0 ? Math.Round(value, 2) : value;
        }
    }

    /// <summary>
    /// For Getting String Enum value array
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        public string StringValue { get; protected set; }

        public StringValueAttribute(string value)
        {
            this.StringValue = value;
        }
    }
}